
% --------------------------------------------------------------------------
%
% Post processing for controller testing
%
% This script plots:
%   A metric of "controller performance" vs "network complexity" for a
%     series of lattice primitives (of varying size)
%
% --------------------------------------------------------------------------

set(0,'defaultlinelinewidth',1); 
set(0,'defaultlinemarkersize',15);
clear; clf; clc;

% colormap('hicontrast')
close all;
global_path_setup

figNum = 45;
dataFolder = 'allOutputsDataFolder/cTst/';

dataFiles = dir(strcat(dataFolder,'*_dataLog.mat'));

numFiles = length(dataFiles);

trussIndices = [];
rectIndices = [];
areaIOU_metric = [];

% parse files and separate truss from rect logs
for ii=1:numFiles
  if ~isempty(strfind(dataFiles(ii).name, 'pRect'))
    % disp(strcat('Found pRect file at index: ', num2str(ii)));
    rectIndices = [rectIndices ii];
  elseif ~isempty(strfind(dataFiles(ii).name, 'pTruss'))
    % disp(strcat('Found pTruss file at index: ', num2str(ii)));
    trussIndices = [trussIndices ii];
  end

  aa = load(strcat(dataFolder, dataFiles(ii).name));
  combinedDataSets(ii) = aa;
end


% -----------------------------------------------------------------
% Plot the dataset area-iou errors
% -----------------------------------------------------------------
PLOT_ALL_AREA_IOUS = 0;
SAVE_PLOTS_OF_AREA_IOUS = 0;

for ii=1:numFiles
  name = dataFiles(ii).name;
  tmp = load(strcat(dataFolder,name));
  dataLog = tmp.dataLog;

  % storing dataset area iou into common array
  %   resize array with NaN padding at the end to account for potentially
  %   variable trial run times
  if length(dataLog.simTime) > size(areaIOU_metric,1)
    areaIOU_metric(size(areaIOU_metric,1)+1:length(dataLog.simTime),:) = NaN;
  end
  areaIOU_metric(1:length(dataLog.simTime),ii) = dataLog.errArea;
  %   if the latest trial is shorter than before, NaN pad the end of this trial
  areaIOU_metric(length(dataLog.simTime)+1:end, ii) = NaN;

  numCellsInTrial(ii) = size(dataLog.startConfig,1);
  numNodesInTrial(ii) = size(dataLog.pos,2);

  if PLOT_ALL_AREA_IOUS
    if isfield(dataLog, 'errArea')
      fprintf(2, '>> %s contains errArea already. Plotting... \n', name);
      outf = figure (777); plot(dataLog.simTime, dataLog.errArea, 'o--');
      title(name);
      if SAVE_PLOTS_OF_AREA_IOUS
        print (outf, strcat(name(1:end-4),'.png'), '-dpng', '-r300');
      end
    else
      fprintf(2, '>> %s does not contain errArea.\n', name);
    end
  end

end

% keyboard

trialLen = size(areaIOU_metric,1);

% -----------------------------------------------------------------
% remove bad calculations, if any
% -----------------------------------------------------------------
areaIOU_metric(areaIOU_metric>1) = NaN;


% -----------------------------------------------------------------
% basic filter: show controller tests that achieved at least
% 75% shape match (determined by visually checking all trials)
% filter should NOT remove more than 10% of trials as outliers
% -----------------------------------------------------------------
trialsFiltered = [];
for kk=1:size(areaIOU_metric,2)
  if length(trialsFiltered) > .1*size(areaIOU_metric,2)
    break
  end
  if mean(areaIOU_metric(end-3:end,kk)) < 0.75
    areaIOU_metric(:,kk) = areaIOU_metric(:,kk)*NaN;
    trialsFiltered = [trialsFiltered kk];
  end
end




% -----------------------------------------------------------------
% smooth the dataset
% -----------------------------------------------------------------
for kk=1:size(areaIOU_metric,2)
  % figure(88); hold on;
  % plot(1:trialLen, areaIOU_metric(:,kk), 'r-');
  areaIOU_metric (:,kk) = smooth(areaIOU_metric(:,kk), 5);
  % areaIOU_metric (:,kk) = smooth(areaIOU_metric(:,kk), 'rloess');
  % plot(1:trialLen, areaIOU_metric(:,kk), 'b-');
  % keyboard
end


% -----------------------------------------------------------------
%
% Working with areaIOU metric until this point
% Switching to errors for plotting
%
% -----------------------------------------------------------------
areaIOU_errors = 1 - areaIOU_metric;


% -----------------------------------------------------------------
% plot dataset
% -----------------------------------------------------------------
lineOpacity = .35;

colors = hicontrast(12);


figNum = figNum+1; figure(figNum); hold all;
subplot(1,3,1); hold all;
tmp = areaIOU_errors;
for kk=1:size(areaIOU_errors,2)
  p3 = plot(1:trialLen, tmp(:,kk),'-.', 'Color', colors(3,:));
  p3.Color(4) = lineOpacity;
end
plot(1:trialLen, mean(areaIOU_errors,2, 'omitnan'), ...
     'k.-', 'LineWidth', 3);
title('All Trials');
xlabel('Time (s)'); ylabel(sprintf('Shape-Match Errors:\nArea IOU to Target'));


subplot(1,3,2); hold all;
tmp = areaIOU_errors(:,rectIndices);
for kk=1:length(rectIndices)
  p3 = plot(1:trialLen, tmp(:,kk),'-.', 'Color', colors(2,:));
  p3.Color(4) = lineOpacity;
end
plot(1:trialLen, mean(areaIOU_errors(:,rectIndices),2, 'omitnan'), ...
     'k.-', 'LineWidth', 3);
title('Square Lattices');
xlabel('Time (s)'); ylabel(sprintf('Shape-Match Errors:\nArea IOU to Target'));


subplot(1,3,3); hold all;
tmp = areaIOU_errors(:,trussIndices);
for kk=1:length(trussIndices)
  p3 = plot(1:trialLen, tmp(:,kk),'-.', 'Color', colors(4,:));
  p3.Color(4) = lineOpacity;
end
plot(1:trialLen, mean(areaIOU_errors(:,trussIndices),2, 'omitnan'), ...
     'k.-', 'LineWidth', 3);
title('Triangular Lattices');
xlabel('Time (s)'); ylabel(sprintf('Shape-Match Errors:\nArea IOU to Target'));

% f = figure(figNum);
% print(f,'cTst_areaErrors_seg_rect_truss.png','-dpng','-r300');
% print(f,'cTst_areaErrors_seg_rect_truss.pdf','-dpdf','-r300');


% -----------------------------------------------------------------
% setup of structures for statistical analysis
% -----------------------------------------------------------------
allStats = struct();
allStats.sMetric = areaIOU_metric(1,:);
allStats.sError = 1 - allStats.sMetric;
allStats.fMetric = areaIOU_metric(end,:);
allStats.fError = 1 - allStats.fMetric;


rectStats = struct();
rectStats.sMetric = areaIOU_metric(1,rectIndices);
rectStats.sError = 1 - rectStats.sMetric;
rectStats.fMetric = areaIOU_metric(end,rectIndices);
rectStats.fError = 1 - rectStats.fMetric;


trussStats = struct();
trussStats.sMetric = areaIOU_metric(1,trussIndices);
trussStats.sError = 1 - trussStats.sMetric;
trussStats.fMetric = areaIOU_metric(end,trussIndices);
trussStats.fError = 1 - trussStats.fMetric;


% figure;
% subplot(1,2,1); hist(rect_startMetric);
% subplot(1,2,2); hist(rect_finalMetric);

% figure;
% subplot(1,2,1); hist(truss_startMetric);
% subplot(1,2,2); hist(truss_finalMetric);



% -----------------------------------------------------------------
% Descriptive statistics of the errors
% -----------------------------------------------------------------
allStats.meanStartError = mean(allStats.sError, 'omitnan');
rectStats.meanStartError = mean(rectStats.sError, 'omitnan');
trussStats.meanStartError = mean(trussStats.sError, 'omitnan');

allStats.meanFinalError = mean(allStats.fError, 'omitnan');
rectStats.meanFinalError = mean(rectStats.fError, 'omitnan');
trussStats.meanFinalError = mean(trussStats.fError, 'omitnan');

allStats.stdStartError = std(allStats.sError, 'omitnan');
rectStats.stdStartError = std(rectStats.sError, 'omitnan');
trussStats.stdStartError = std(trussStats.sError, 'omitnan');

allStats.stdFinalError = std(allStats.fError, 'omitnan');
rectStats.stdFinalError = std(rectStats.fError, 'omitnan');
trussStats.stdFinalError = std(trussStats.fError, 'omitnan');


% -----------------------------------------------------------------
% Hypothesis testing: Using errors only, for consistency 
% -----------------------------------------------------------------

% 1 ------------
% Proving the controller does anything statistically significant
% H0: start and final errors have equal means
% Method: matched sample t-test between distribution of startM and finalM
[h1,p1, ci1, stats1] = ttest(allStats.sError,allStats.fError)
if h1 == 0
  fprintf(2,'\tsErr & fErr are distr with equal means, with p = %.2f\n', p1);
else
  fprintf(2,'\tsErr & fErr are distr with diff means, with p = %.2f\n', p1);
end



% 2 ------------
% Proving the controller causes improvement of shape metric
% H0: Error change has negative mean
% Method: 1 sample left-tailed t-test of (startErr - finalErr)
[h2,p2, ci2, stats2] = ttest(allStats.sError-allStats.fError, 0, 'tail','left')
if h2 == 0
  fprintf(2,'\terr change is statistically < 0, with p = %.2f\n', p2);
else
  fprintf(2,'\terr change is statistically >= 0, with p = %.2f\n', p2);
end

% 3 ------------
% Proving the controller causes sufficient decrease in shape metric errors
% 
% 4 ------------
% Proving the controller performs similarly for different lattice shapes
% Compare errors at start for rect vs truss (fair comparison test )
% 
% 5 ------------
% Proving the controller performs similarly for different lattice shapes
% Compare errors at finish for rect vs truss (consistent improvement test)
% 
% 6 ------------
% Proving the controller performs similarly for different lattice shapes
% Compare error-decrease for rect vs truss (equal performance test)



% -----------------------------------------------------------------
% 
% Final set of figures for publication
% 
% -----------------------------------------------------------------
finalPrint =  0;


% 
% FIGURE 1
% ----------------------
% ----------------------
figNum = figNum+1; 
fig_cntrl_effectiveness = figure(figNum); 
hold all;

lineOpacity = .2;
colors = hicontrast(12);

startErrors = allStats.sError';
startTime = zeros(size(allStats.sError));
finalErrors = allStats.fError';
finalTime = max(dataLog.simTime)*ones(size(allStats.fError));
bb = boxplot([startErrors finalErrors], [startTime finalTime], ...
  'positions', [0,max(dataLog.simTime)], ...
  'widths', 1,...
  'Whisker', 1,...
  'notch', 'on');
% set(bb,'')

tmp = areaIOU_errors;
for kk=1:size(areaIOU_errors,2)
  p3 = plot(dataLog.simTime, tmp(:,kk),':', 'Color', colors(3,:));
  p3.Color(4) = lineOpacity;
end
plot(dataLog.simTime, mean(areaIOU_errors,2, 'omitnan'), ...
     'k-', 'LineWidth', 1);
title('Shape-Match Errors For Simulation Trials');
xlabel('Time (s)'); 
ylabel(sprintf('Area IOU Error'));
axis auto

xlim([-1 max(dataLog.simTime)+1])
ylim([0 0.45]);  
aa = get(gca,'XAxis');
aa.TickValues = dataLog.simTime(1:2:end);
aa.TickLabels = num2cell(dataLog.simTime(1:2:end));

% Add significance level text
quiver(9.5,0.3,-10.4,0, 'k-.', 'MaxHeadSize', 0.02);
quiver(9.5,0.3, 10.4,0, 'k-.', 'MaxHeadSize', 0.02);
text(8,0.325,'p < 0.001');

if finalPrint
  print(fig_cntrl_effectiveness,...
    'publicationFigures/fig_cTst_effectiveness.pdf',...
    '-dpdf','-r300');
  print(fig_cntrl_effectiveness,...
    'publicationFigures/fig_cTst_effectiveness.png',...
    '-dpng','-r300');
end




% 
% FIGURE 2
% ----------------------
% ----------------------
figNum = figNum+1; 
fig_cntrl_perform_by_lattice = figure(figNum); 
hold all;

lineOpacity = .2;
colors = hicontrast(12);

allFinalErrs = allStats.fError';
allRectFinalErrs = NaN*allFinalErrs; 
allRectFinalErrs(1:length(rectIndices)) = rectStats.fError'; 
allTrussFinalErrs = NaN*allFinalErrs; 
allTrussFinalErrs(1:length(trussIndices)) = trussStats.fError';

boxplot([allFinalErrs allRectFinalErrs allTrussFinalErrs],...
  'Widths', 0.25,...
  'Whisker', 1,...
  'notch', 'on');
ylim([0 0.45]);
set(gca,'XTickLabels',...
  {'All Trials', 'Rectangular Lattices', 'Triangular Lattices'});

title('Shape-Match Errors by Lattice Type');
xlabel(''); 
ylabel('Area IOU Error');


if finalPrint
  print(fig_cntrl_perform_by_lattice,...
    'publicationFigures/fig_cTst_perform_by_lattice.pdf',...
    '-dpdf','-r300');
  print(fig_cntrl_perform_by_lattice,...
    'publicationFigures/fig_cTst_perform_by_lattice.png',...
    '-dpng','-r300');
end






% 
% FIGURE 3
% ----------------------
% ----------------------
figNum = figNum+1; 
fig_cntrl_corr_nodes = figure(figNum); 
hold all;

lineOpacity = .2;
colors = hicontrast(12);

numNodesInRectTrial = numNodesInTrial(rectIndices)';
finalRectErrors = rectStats.fError';
idx=find(isnan(finalRectErrors))
numNodesInRectTrial(idx) = [];
finalRectErrors(idx)=[];

plot(numNodesInRectTrial, finalRectErrors, '.');
ylim([0 0.3]);

[c,gof] = fit(numNodesInRectTrial,finalRectErrors,'poly1');
c = [c.p1 c.p2];
plot(numNodesInRectTrial, polyval(c,numNodesInRectTrial),'-.');

text(50, 0.2, sprintf('R^2 = %.3f',gof.adjrsquare));

xlim([0 max(numNodesInRectTrial)+5]);

title('Controller Errors vs. Nodes in Mesh');
xlabel('# of Nodes in Mesh');
ylabel('Area IOU Error');

if finalPrint
  print(fig_cntrl_corr_nodes,...
    'publicationFigures/fig_cntrl_corr_nodes.pdf',...
    '-dpdf','-r300');
  print(fig_cntrl_perform_by_lattice,...
    'publicationFigures/fig_cntrl_corr_nodes.png',...
    '-dpng','-r300');
end







% 
% FIGURE 4
% ----------------------
% ----------------------
figNum = figNum+1; 
fig_cntrl_corr_cells = figure(figNum); 
hold all;

lineOpacity = .2;
colors = hicontrast(12);

numCellsInRectTrial = numCellsInTrial(rectIndices)';
finalRectErrors = rectStats.fError';
idx=find(isnan(finalRectErrors))
numCellsInRectTrial(idx) = [];
finalRectErrors(idx)=[];

plot(numCellsInRectTrial, finalRectErrors, '.');
ylim([0 0.3]);

[c,gof] = fit(numCellsInRectTrial,finalRectErrors,'poly1');
c = [c.p1 c.p2];
plot(numCellsInRectTrial, polyval(c,numCellsInRectTrial),'-.');

text(100, 0.2, sprintf('R^2 = %.3f',gof.adjrsquare));

xlim([0 max(numCellsInRectTrial)+5]);

title('Controller Errors vs. Cells in Mesh');
xlabel('# of Cells in Mesh');
ylabel('Area IOU Error');


if finalPrint
  print(fig_cntrl_corr_cells,...
    'publicationFigures/fig_cntrl_corr_cells.pdf',...
    '-dpdf','-r300');
  print(fig_cntrl_perform_by_lattice,...
    'publicationFigures/fig_cntrl_corr_cells.png',...
    '-dpng','-r300');
end





return


% 
% FIGURE 5: Small MACRO
% ----------------------
% ----------------------
figNum = figNum+1; 
fig_cntrl_perform_small_rect = figure(figNum); 
hold all;

lineOpacity = .2;
colors = hicontrast(12);
colormap(colors);

% use the raw data to give higher resolution for this figure
xdata = dataLog.simTime;
ydata = 1-combinedDataSets(rectIndices(2)).dataLog.errArea;
ydata = areaIOU_errors(:,rectIndices(2));


% f = fit(xdata', ydata', 'pchipinterp'); 
% plot(f, xdata, ydata,'.');
plot(xdata, ydata, '.');
plot(xdata, ydata, '.');
plot(xdata, ydata, '.');
plot(xdata, ydata, '.');

legend('off');
title('');
xlabel('Time (s)');
ylabel('Area IOU Error');

if finalPrint
  print(fig_cntrl_perform_small_rect,...
    'publicationFigures/fig_cntrl_perform_small_rect.pdf',...
    '-dpdf','-r300');
  print(fig_cntrl_perform_small_rect,...
    'publicationFigures/fig_cntrl_perform_small_rect.png',...
    '-dpng','-r300');
end






% -----------------------------------------------------------------
%
%
%
% E n d   Of   F i l e
%
%
%
% -----------------------------------------------------------------











% % -----------------------------------------------------------------
% %
% %
% %
% % Normalization technique: only look at relative shape-match improvements
% % This normalization is not reasonable
% %
% %
% %
% % -----------------------------------------------------------------





% % -----------------------------------------------------------------
% % normalize start conditions
% % -----------------------------------------------------------------
% normalizeDiff = [];
% for kk=1:numFiles
%   stInd = find(~isnan(areaIOU_metric(:,kk)),1);
%   normalizeDiff = [normalizeDiff areaIOU_metric(stInd,kk)];
% end
% areaIOU_metric = areaIOU_metric - repmat(normalizeDiff,trialLen,1);

% figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,1); hold all;
% plot(1:trialLen, areaIOU_metric,'.--');
% plot(1:trialLen, mean(areaIOU_metric,2), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);

% % figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,2); hold all;
% plot(1:trialLen, areaIOU_metric(:,rectIndices),'r.--');
% plot(1:trialLen, mean(areaIOU_metric(:,rectIndices),2, 'omitnan'), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);


% % figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,3); hold all;
% plot(1:trialLen, areaIOU_metric(:,trussIndices),'b.--');
% plot(1:trialLen, mean(areaIOU_metric(:,trussIndices),2, 'omitnan'), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);


% % -----------------------------------------------------------------
% % remove trials where controller did not cause improvements
% % -----------------------------------------------------------------
% areaIOU_filtered = areaIOU_metric;
% filterInd = [];
% for kk=1:numFiles
%   stInd = find(~isnan(areaIOU_metric(:,kk)),1);
%   finInd = find(~isnan(areaIOU_metric(:,kk)),1,'last');

%   % filterCond = false;
%   % filterCond = areaIOU_metric(finInd,kk) < areaIOU_metric(stInd,kk);
%   filterCond = areaIOU_metric(finInd,kk)-areaIOU_metric(stInd,kk) < .1;
%   % filterCond = (areaIOU_metric(finInd,kk) < 0.09) || ...
%   %             (areaIOU_metric(finInd,kk)/areaIOU_metric(stInd,kk) < 1.1);
%   % filterCond = (areaIOU_metric(finInd,kk) < 0.05) || ...
%   %             (areaIOU_metric(finInd,kk)/areaIOU_metric(stInd,kk) < 1.1);

%   if filterCond
%     filterInd = [filterInd kk];
%   end

% end
% % areaIOU_filtered(:,filterInd) = [];
% areaIOU_filtered(:,filterInd) = areaIOU_filtered(:,filterInd)*NaN;
% filteredFiles = size(areaIOU_filtered,2);

% figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,1); hold all;
% plot(1:trialLen, areaIOU_filtered,'.--');
% plot(1:trialLen, mean(areaIOU_filtered,2, 'omitnan'), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);

% % figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,2); hold all;
% plot(1:trialLen, areaIOU_filtered(:,rectIndices),'r.--');
% plot(1:trialLen, mean(areaIOU_filtered(:,rectIndices),2, 'omitnan'), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);


% % figNum=figNum+1; figure(figNum); hold all;
% subplot(1,3,3); hold all;
% plot(1:trialLen, areaIOU_filtered(:,trussIndices),'b.--');
% plot(1:trialLen, mean(areaIOU_filtered(:,trussIndices),2, 'omitnan'), 'k.-', 'LineWidth', 3);
% % ylim([0 .3]);


% % -----------------------------------------------------------------


