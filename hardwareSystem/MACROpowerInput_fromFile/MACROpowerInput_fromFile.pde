// ---------------------------------------------------------------------------
// 
// MACRO powerInput from file// 
// Latest asof: May 1, 2017
// Author: Ahsan Nawroj
// Yale University
// 
// ---------------------------------------------------------------------------
// This program is run on the desktop in Processing, supplying current input
// commands that are found in an input file
// 
// Input file contains lines of the form: "time,nodeNum,iOut"
// 
// The program reads the input file, and at times specified, sends to the 
// arduino messages of the form "nodeNum,iOut"
// For all rows with the same "time" variable, the "nodeNum,iOut" message
// is sent out together, followed by a message "run" to execute the currents.
// 
import processing.serial.*;
import java.util.Date;

//String inputFileNumber = "5835010910514_2xTimeDila_5ptSmoothing";
//String inputFileNumber = "5835010910514_2xTimeDila_10ptSmoothing";
//String inputFileNumber = "5835010910514_2xTimeDila_15ptSmoothing";
String inputFileNumber = "5835010910514_2xTimeDila_20ptSmoothing";
BufferedReader reader;
String line;
 
Serial serToArduino;  // Create object from Serial class
String val;     // Data received from the serial port

int lastSentTime = -1;

int beginTextFileRead = 0;

void setup() {
  reader = createReader("input_"+inputFileNumber+".txt");    
  String portName = Serial.list()[0]; 
  serToArduino = new Serial(this, portName, 9600);
  
  delay(2000);
}
 
void draw() {
  while (beginTextFileRead == 0) {
    flushSerial(0);
    return;
  }
  
  if (lastSentTime == -1) lastSentTime = 0;
  flushSerial(1);
  
  try {
    line = reader.readLine();
  } catch (IOException e) {
    e.printStackTrace(); line = null;
  }

  if (line == null) {// Stop reading because of an error or file is empty
    serToArduino.write("end");
    while(true) flushSerial(0);
  } else {
    println(">>INPUT: "+line);    
    String[] pieces = split(line, ",");

    int time = (int) (float(pieces[0])*1000); // convert times to ms
    int delayTime = time - lastSentTime;
    int nodeNum = int(pieces[1]);
    float iOut = float(pieces[2]);    
    String msgOut = "" + nodeNum + "," + iOut;
        
    if (delayTime > 0) {
      serToArduino.write("run");
      println(">>Sent command to run on arduino");
      delay(delayTime);
      lastSentTime = time;
    }
    
    flushSerial(1);    
    serToArduino.write(msgOut);
    println(">>Sent command: " + msgOut);
    flushSerial(1);
    delay(20);
  }

  flushSerial(0);
} 



void flushSerial(int timeOut) {
  int printedSomething = 0;
  // flush out the serial channel to screen
  while(serToArduino.available() > 0) {
    val = serToArduino.readStringUntil('\n'); // read it and store it in val
    if (val == null) {break;}
    val = val.trim();
    println(val);
    printedSomething = 1;
  }

  if ((timeOut==1) && (printedSomething != 0))
    flushSerial(timeOut);
}


void delay (int delayTime) {
  int time = millis();
  while(millis() - time <= delayTime)
    flushSerial(0);
}


void keyPressed() {
 if (key == 'R' || key == 'r'){
   beginTextFileRead = 1;
 }
}