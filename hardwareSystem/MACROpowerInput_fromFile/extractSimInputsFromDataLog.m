
function extractSimInputsFromDataLog (timeStampString) 
  % global_path_setup
  if exist('dataLog', 'var')
    clear dataLog;
  end

  if ~exist('timeStampString', 'var')
    timeStampString = '';
  end

  ff = dir(strcat(timeStampString,'*','dataLog.mat'));
  ff = ff(1);
  logFileName = ff.name;
  idx = strfind(logFileName,'_');
  logFileStamp = logFileName(1:idx-1);

  load(logFileName)

  nodeToPower = 3;

    % keyboard
  inputSequence = [];

  simInputs = dataLog.simInputs;

  nodeVoltages = squeeze(simInputs(:,1,:));
  Ae = dataLog.macroComponents.Ae;

  cellCurrents = (Ae*nodeVoltages)/1.5;
  nodeCurrents = Ae'*cellCurrents;

  inputSequence = nodeCurrents';

  nNodes = size(inputSequence,2);
  nodeNum = 1:nNodes;
  idx = [];
  for ii=1:nNodes
    if norm(inputSequence(:,ii)) < 0.01
      idx = [idx ii];
    end
  end
  inputSequence(:,idx) = [];
  nodeNum(:,idx) = [];

  nNodes = size(inputSequence,2);
  idx=[];
  for ii=1:nNodes
    nrm1 = norm(abs(inputSequence(:,ii)));
    for jj=1:nNodes
      if ii==jj
        continue;
      end
      nrm2 = norm(abs(inputSequence(:,jj)));
      if nrm1-nrm2 < 0.01
        if ~isempty(idx) && (any(idx(:,1) == ii) || any(idx(:,2) == ii))
          continue
        end
        idx = [idx; [ii jj]'];
      end
    end
  end
  inputSequence(:,idx(1,:)) = abs(inputSequence(:,idx(1,:)));
  inputSequence(:,idx(2,:)) = -abs(inputSequence(:,idx(2,:)));

  inputSequence(:,nodeNum==nodeToPower) = [];
  nodeNum(nodeNum==nodeToPower) = [];

  time = dataLog.simTime';

% 
% 
% TOTALLY ARBITRARY: STRETCH TIME TO SLOW DOWN THE ACTIVATION
% 
% 

  timeDila = 2;
  smoothingLen = 20;


  time = timeDila*time;
  for ii=1:size(inputSequence,2)
    mm = max(inputSequence(:,ii));
    inputSequence(:,ii) = smooth(inputSequence(:,ii), smoothingLen);
    inputSequence(:,ii) = inputSequence(:,ii)/max(inputSequence(:,ii))*mm;
  end

  figure(88);
  plot(time, inputSequence);

  outfname = strcat('input_',logFileStamp, '_', ...
    num2str(timeDila),'xTimeDila_', ...
    num2str(smoothingLen), 'ptSmoothing', ...
    '.txt');

  for jj=1:size(time)
    tt = time(jj);
    for kk=1:size(nodeNum)
      nd = nodeNum(kk);
      nd =1;        
      outf = fopen(outfname,'a+');
      fprintf(outf,'%.2f,%d,%.2f\n',tt,nd,inputSequence(jj,kk));        
  end
  fclose(outf);
end

