// ---------------------------------------------------------------------------
// 
// MACRO power controller ***CALIBRATION***
// Revision 2
// 
// Latest asof: April 28, 2017
// Author: Ahsan Nawroj
// Yale University
// 
// ---------------------------------------------------------------------------
// This program is run on an Arduino Uno, tasked with controlling activation
// of a set of Transistor-based current sinking circuits (nodal circuits). 
// 
// Each nodal circuit contains the following:
//  A TIP120 high current transistor
//  An LED connected to the base to signal the presence of significant voltage
//    and report "activation of the transistor circuit"
//  A power resistor connected to the emitter
//  A wire out from the collector to a node of a test MACRO
// 
// The Vpp power source for the collector current is provided by a power 
// supply that is connected to another node of the MACRO. When activated by 
// voltage inputs from this program, each nodal circuit conducts current 
// along its transistor by an amount dictated by the base voltage applied by
// this program. 
// 
// This program calibrates the transistor circuits at each node
// Calibration process for each nodal circuit:
// 1.  Nodal switch is pressed
// 2.  Program enters calibration loop for this node
// 3.  Program picks a selection of output voltage between 0V to 5V
// 4.  As the voltage is swept up, with updates every 2 seconds 
// 5.  The user presses the nodal switch again to quit calibration
// 

#define NUM_CKTS 3
#define MAX_CALIB_PTS 20
#define VfromCnt(x) ((x)/1024.0*5.0)
#define CntFromV(x) ((ceil) ((x)/5.0*1024))

// define 
int nodalCircuitOut [NUM_CKTS] = {3,5,6}; 
int switchBankInputs [NUM_CKTS] = {9,10,11}; 

int switchStates [NUM_CKTS] = {0, 0, 0};
int nodalCircuitOutVolts [NUM_CKTS] = {0.0, 0.0, 0.0};

float calib_m [NUM_CKTS] = {0.0, 0.0, 0.0};
float calib_c [NUM_CKTS] = {0.0, 0.0, 0.0};

int calibrationMode = 0;

int calibDataCount = 0;
int cktToCalib = -1;
float calibDataVoltage [MAX_CALIB_PTS];
float calibDataCurrent [MAX_CALIB_PTS]; 


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Setup method
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void setup() {

  int ii=0;
  // initialize pin states
  for (ii=0; ii < NUM_CKTS; ii++) {
    pinMode(switchBankInputs[ii], INPUT);

    pinMode(nodalCircuitOut[ii], OUTPUT);
    analogWrite(nodalCircuitOut[ii], 0);
  }

  for (ii=0; ii<MAX_CALIB_PTS; ii++) {
    calibDataVoltage[ii] = 0.00;
    calibDataCurrent[ii] = 0.00;
  }

  Serial.begin(9600);
  Serial.println("Pin states initialized");

  printCalibrationFcns();
  randomSeed(analogRead(0));
}



// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Loop Method
// --------------------------------------------------------------------------
// Calibration function
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void loop() {
  int ii=0;

  // do nothing until at least one circuit is activated for calibration
  // record switch states continuously until one switch is hit
  while (!calibrationMode) {
    for (ii=0; ii < NUM_CKTS; ii++) {
      switchStates[ii] = digitalRead (switchBankInputs[ii]);

      if (switchStates[ii] == 1) {
        cktToCalib = ii;
        Serial.print("Switch hit for ckt: ");
        Serial.println(cktToCalib);

        // wait until switch is unpressed again
        while (digitalRead(switchBankInputs[cktToCalib])) delay(5);

        // start calibration: break out of loop
        calibrationMode = 1;
        break;

      } else {
        delay(10);
      }
    }
  }

  Serial.println("Entering calibraiton process.");

  long randomVoltage;

  calibDataCount = 0;
  while (~digitalRead(switchBankInputs[cktToCalib])) {
    randomVoltage = random (0,245);
    Serial.print("Applying output voltage: ");
    Serial.print(VfromCnt(randomVoltage), 4);
    analogWrite(nodalCircuitOut[cktToCalib], randomVoltage);
    printAndDelay(3000);

    Serial.println("Enter  measured current output:");
    analogWrite(nodalCircuitOut[cktToCalib], 0);

    while (~digitalRead(switchBankInputs[cktToCalib])) {
      if (Serial.available() > 0) {
        float currentInput = Serial.parseFloat();

        Serial.print("Current measured: ");
        Serial.println(currentInput, 4);

        calibDataVoltage[calibDataCount] = VfromCnt(randomVoltage);
        calibDataCurrent[calibDataCount] = currentInput;
        calibDataCount++;

        break;
      }
      delay(10);
    }

    Serial.println("Data point parsed");
    if (digitalRead(switchBankInputs[cktToCalib]))
      break;
  }

  Serial.println("Calibration complete.");

  printCalibData();
  delay(10000);
  calibrationMode = 0;
}





void printCalibrationFcns() {
  Serial.println("Calibration Functions: ");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    Serial.print("\t"); Serial.print("("); Serial.print(calib_m[ii]);
    Serial.print(","); Serial.print(calib_c[ii]); Serial.print(")");
    Serial.println("");
  }
}



void printAndDelay(int delayTime) {
  int stepTime = 250;
  int steps = delayTime/stepTime;
  int remainderTime = delayTime%stepTime;

  int ii = 0;
  Serial.print("\t");
  for (ii = 0; ii < steps; ii++) {
    delay(stepTime); Serial.print(".");
  }
  delay(remainderTime); Serial.println(".");
}


void printCalibData () {
  Serial.println("===============================");
  Serial.println("\n\n");
  Serial.print("Calibrated Circuit: ");
  Serial.println(cktToCalib);

  int ii=0;
  Serial.println("\nData table (V,I)");
  Serial.println("===============================");
  for (ii=0; ii<calibDataCount; ii++) {
    Serial.print(calibDataVoltage[ii], 4);
    Serial.print(",");
    Serial.print(calibDataCurrent[ii], 4); 
    Serial.println("");
  }

  Serial.println("\n\n");
  Serial.println("===============================");
}