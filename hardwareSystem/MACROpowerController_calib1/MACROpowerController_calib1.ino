// ---------------------------------------------------------------------------
// 
// MACRO power controller ***CALIBRATION***
// Revision 2
// 
// Latest asof: April 28, 2017
// Author: Ahsan Nawroj
// Yale University
// 
// ---------------------------------------------------------------------------
// This program is run on an Arduino Uno, tasked with controlling activation
// of a set of Transistor-based current sinking circuits (nodal circuits). 
// 
// Each nodal circuit contains the following:
//  A TIP120 high current transistor
//  An LED connected to the base to signal the presence of significant voltage
//    and report "activation of the transistor circuit"
//  A power resistor connected to the emitter
//  A wire out from the collector to a node of a test MACRO
// 
// The Vpp power source for the collector current is provided by a power 
// supply that is connected to another node of the MACRO. When activated by 
// voltage inputs from this program, each nodal circuit conducts current 
// along its transistor by an amount dictated by the base voltage applied by
// this program. 
// 
// This program calibrates the transistor circuits at each node
// Calibration process for each nodal circuit:
// 1.  Nodal switch is pressed
// 2.  Program enters calibration loop for this node
// 3.  Program runs output voltage from 0V to 5V in steps of 0.1V
// 4.  As the voltage is swept up, with updates every 2 seconds 
// 5.  When current sunk is 0.5A, the user clicks the nodal switch
// 6.  Program stores this voltage for current sunk = 500mA
// 7.  Program continues sweeping up voltage
// 8.  When current sunk is 1A, the user presses the nodal switch again
// 9.  Program stores this voltage for current sunk = 1000mA
// 10. Calibration is calculated as iOut = m*Vout + c, (m,c) is noted per ckt
// 

#define NUM_CKTS 3
#define VfromCnt(x) ((x)/1024.0*5.0)
#define CntFromV(x) ((ceil) ((x)/5.0*1024))

// define 
int nodalCircuitOut [NUM_CKTS] = {3,5,6}; 
int switchBankInputs [NUM_CKTS] = {9,10,11}; 

int switchStates [NUM_CKTS] = {0, 0, 0};
int nodalCircuitOutVolts [NUM_CKTS] = {0.0, 0.0, 0.0};

float calib_m [NUM_CKTS] = {0.0, 0.0, 0.0};
float calib_c [NUM_CKTS] = {0.0, 0.0, 0.0};

int calibrationMode = 0;


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Setup method
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void setup() {

  int ii=0;
  // initialize pin states
  for (ii=0; ii < NUM_CKTS; ii++) {
    pinMode(switchBankInputs[ii], INPUT);

    pinMode(nodalCircuitOut[ii], OUTPUT);
    analogWrite(nodalCircuitOut[ii], 0);
  }

  Serial.begin(9600);
  Serial.println("Pin states initialized");

  printCalibrationFcns();
}



// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Loop Method
// --------------------------------------------------------------------------
// Calibration function
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void loop() {
  int ii=0;
  int cktToCalib = -1;

  // do nothing until at least one circuit is activated for calibration
  // record switch states continuously until one switch is hit
  while (!calibrationMode) {
    for (ii=0; ii < NUM_CKTS; ii++) {
      switchStates[ii] = digitalRead (switchBankInputs[ii]);

      if (switchStates[ii] == 1) {
        cktToCalib = ii;
        Serial.print("Switch hit for ckt: ");
        Serial.println(cktToCalib);

        // wait until switch is unpressed again
        while (digitalRead(switchBankInputs[cktToCalib])) delay(5);

        // start calibration: break out of loop
        calibrationMode = 1;
        break;

      } else {
        delay(10);
      }
    }
  }

  Serial.println("Entering calibraiton process.");

  // the switch that is hit is stored in cktToCalib
  // calib step 1: sweep voltage from 0 to 5V, with step delay of 1500 ms 
  int i1 = 500; // calibration current target 1: mA
  int i2 = 750; // calibration current target 2: mA
  float vForI1 = -5; float vForI2 = -5;

  int vTarget = CntFromV (5);
  int vStart1 = 80; //CntFromV (0.4); // start sweep from 2V
  int vStart2 = 123; //CntFromV (0.6); // start sweep from 2V
  int vStep = 5; //CntFromV (0.025); // sweep step is 0.1V
  int sweepStepDelay = 2000; // ms
  int currentV = 0;
  
  int calibSet = 0;

  // ----------------------------------------------------------------------
  // Calibration setp 1: Voltage for i=0.5A
  // ----------------------------------------------------------------------
  Serial.println("*** Set current for 0.5A, click node switch ***"); 
  Serial.print("Sweeping voltage from "); Serial.print(VfromCnt(vStart1));
  Serial.print(" to "); Serial.print(VfromCnt(vTarget));
  Serial.print(", at step size "); Serial.println(VfromCnt(vStep));

  for (currentV = vStart1; currentV < vTarget; currentV += vStep) {
    analogWrite(nodalCircuitOut[cktToCalib], currentV);
    Serial.print("\tCurrent voltage: "); Serial.print(VfromCnt(currentV));

    // check over step delay time of 1500ms for switch input
    // break out this time into chunks to avoid asynchronous inputs
    int checkTimeItr = 20;
    for (ii=0; ii < checkTimeItr; ii++){
      delay(ceil(sweepStepDelay/checkTimeItr));
      if (digitalRead(switchBankInputs[cktToCalib])) {
        calibSet = 1;
        Serial.print("\n\tStopped sweep at V: ");
        Serial.println(VfromCnt(currentV));
        break;
      }
      Serial.print(".");
    }
    
    if (calibSet) { // calibration point is set
      vForI1 = VfromCnt(currentV);
      Serial.print("Voltage set for current = 0.5A: ");
      Serial.print(vForI1); Serial.println("V"); break;
    } else {
      Serial.print("\n\t\t(delay at power off)");
      analogWrite(nodalCircuitOut[cktToCalib], 0);
      printAndDelay(2000);
      continue; 
    }
  }


  analogWrite(nodalCircuitOut[cktToCalib], 0);
  Serial.println("Step 1 of calibration complete");
  printAndDelay(7000);

  // ----------------------------------------------------------------------
  // Calibration setp 2: Voltage for i=0.75A
  // ----------------------------------------------------------------------
  calibSet = 0;
  Serial.println("*** Set current for 0.75A, click node switch ***"); 
  Serial.print("Sweeping voltage from "); Serial.print(VfromCnt(vStart2));
  Serial.print(" to "); Serial.print(VfromCnt(vTarget));
  Serial.print(", at step size "); Serial.println(VfromCnt(vStep));

  for (currentV = vStart2; currentV < vTarget; currentV += vStep) {
    analogWrite(nodalCircuitOut[cktToCalib], currentV);
    Serial.print("\tCurrent voltage: "); Serial.print(VfromCnt(currentV));

    // check over step delay time of 1500ms for switch input
    // break out this time into chunks to avoid asynchronous inputs
    int checkTimeItr = 20;
    for (ii=0; ii < checkTimeItr; ii++){
      delay(ceil(sweepStepDelay/checkTimeItr));
      if (digitalRead(switchBankInputs[cktToCalib])) {
        calibSet = 1;
        Serial.print("\n\tStopped sweep at V: ");
        Serial.println(VfromCnt(currentV));
        break;
      }
      Serial.print(".");
    }
    
    if (calibSet) { // calibration point is set
      vForI2 = VfromCnt(currentV);
      Serial.print("Voltage set for current = 0.75A: ");
      Serial.print(vForI2); Serial.println("V"); break;
    } else {
      Serial.print("\n\t\t(delay at power off)");
      analogWrite(nodalCircuitOut[cktToCalib], 0);
      printAndDelay(2000);
      continue; 
    }
  }


  analogWrite(nodalCircuitOut[cktToCalib], 0);
  Serial.println("");
  Serial.println("Step 2 of calibration complete");

  Serial.print("Calibration summary for ckt "); Serial.println(cktToCalib);
  Serial.print("i=0.5A, V = "); Serial.println(vForI1);
  Serial.print("i=0.75A, V = "); Serial.println(vForI2);

  Serial.println("");


  // ----------------------------------------------------------------------
  // Calibration setp 3: Calculate parameters for this nodal circuit
  // ----------------------------------------------------------------------
  calib_m [cktToCalib] = (0.75-0.5) / (vForI2 - vForI1);
  calib_c [cktToCalib] = 0.75 - calib_m [cktToCalib]*vForI2;


  printCalibrationFcns();
  delay(1000);
  calibrationMode = 0;
}





void printCalibrationFcns() {
  Serial.println("Calibration Functions: ");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    Serial.print("\t"); Serial.print("("); Serial.print(calib_m[ii]);
    Serial.print(","); Serial.print(calib_c[ii]); Serial.print(")");
    Serial.println("");
  }
}



void printAndDelay(int delayTime) {
  int stepTime = 250;
  int steps = delayTime/stepTime;
  int remainderTime = delayTime%stepTime;

  int ii = 0;
  Serial.print("\t");
  for (ii = 0; ii < steps; ii++) {
    delay(stepTime); Serial.print(".");
  }
  delay(remainderTime); Serial.println(".");
}
