import processing.serial.*;
import java.util.Date;

PrintWriter output;
Serial myPort;  // Create object from Serial class
String val;     // Data received from the serial port
Date d = new Date();
  
void setup() {
  String portName = Serial.list()[0]; 
  myPort = new Serial(this, portName, 9600);
  
  String outfname = "calibrationData_" + d.getTime() + ".txt";
  output = createWriter(outfname);
  
}

void draw() {
  if ( myPort.available() > 0) {  // If data is available,
    val = myPort.readStringUntil('\n'); // read it and store it in val
    if (val == null) {
      return;
    }
      
    output.print(val);
    print(val); //print it out in the console
  }
}

void keyPressed () {
  output.flush();
  output.close();
  exit();
}