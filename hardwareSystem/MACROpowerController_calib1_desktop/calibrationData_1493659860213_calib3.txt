Pin states initialized
Calibration Functions: 
	(0.00,0.00)
	(0.00,0.00)
	(0.00,0.00)
Switch hit for ckt: 2
Entering calibraiton process.
*** Set current for 0.5A, click node switch ***
Sweeping voltage from 0.39 to 5.00, at step size 0.02
	Current voltage: 0.39....................
		(delay at power off)	.........
	Current voltage: 0.42....................
		(delay at power off)	.........
	Current voltage: 0.44...............
	Stopped sweep at V: 0.44
Voltage set for current = 0.5A: 0.44V
Step 1 of calibration complete
	.............................
*** Set current for 0.75A, click node switch ***
Sweeping voltage from 0.60 to 5.00, at step size 0.02
	Current voltage: 0.60................
	Stopped sweep at V: 0.60
Voltage set for current = 0.75A: 0.60V

Step 2 of calibration complete
Calibration summary for ckt 2
i=0.5A, V = 0.44
i=0.75A, V = 0.60

Calibration Functions: 
	(0.00,0.00)
	(0.00,0.00)
	(1.55,-0.18)
Switch hit for ckt: 2
Entering calibraiton process.
*** Set current for 0.5A, click node switch ***
Sweeping voltage from 0.39 to 5.00, at step size 0.02
	Current voltage: 0.39....................
		(delay at power off)	.........
	Current voltage: 0.42....................
		(delay at power off)	.........
	Current voltage: 0.44....................
		(delay at power off)	.........
	Current voltage: 0.46........
	Stopped sweep at V: 0.46
Voltage set for current = 0.5A: 0.46V
Step 1 of calibration complete
	.............................
*** Set current for 0.75A, click node switch ***
Sweeping voltage from 0.60 to 5.00, at step size 0.02
	Current voltage: 0.60....................
		(delay at power off)	.........
	Current voltage: 0.63....................
		(delay at power off)	.........
	Current voltage: 0.65..........
	Stopped sweep at V: 0.65
Voltage set for current = 0.75A: 0.65V

Step 2 of calibration complete
Calibration summary for ckt 2
i=0.5A, V = 0.46
i=0.75A, V = 0.65

Calibration Functions: 
	(0.00,0.00)
	(0.00,0.00)
	(1.35,-0.13)
Switch hit for ckt: 0
Entering calibraiton process.
*** Set current for 0.5A, click node switch ***
Sweeping voltage from 0.39 to 5.00, at step size 0.02
	Current voltage: 0.39....................
		(delay at power off)	.........
	Current voltage: 0.42....................
		(delay at power off)	.........
	Current voltage: 0.44....................
		(delay at power off)	.........
	Current voltage: 0.46....................
		(delay at power off)	.........
	Current voltage: 0.49....................
		(delay at power off)	.........
	Current voltage: 0.51............
	Stopped sweep at V: 0.51
Voltage set for current = 0.5A: 0.51V
Step 1 of calibration complete
	.............................
*** Set current for 0.75A, click node switch ***
Sweeping voltage from 0.60 to 5.00, at step size 0.02
	Current voltage: 0.60....................
		(delay at power off)	.........
	Current voltage: 0.63....................
		(delay at power off)	.........
	Current voltage: 0.65....................
		(delay at power off)	.........
	Current voltage: 0.67............
	Stopped sweep at V: 0.67
Voltage set for current = 0.75A: 0.67V

Step 2 of calibration complete
Calibration summary for ckt 0
i=0.5A, V = 0.51
i=0.75A, V = 0.67

Calibration Functions: 
	(1.55,-0.30)
	(0.00,0.00)
	(1.35,-0.13)
Switch hit for ckt: 1
Entering calibraiton process.
*** Set current for 0.5A, click node switch ***
Sweeping voltage from 0.39 to 5.00, at step size 0.02
	Current voltage: 0.39....................
		(delay at power off)	.........
	Current voltage: 0.42....................
		(delay at power off)	.........
	Current voltage: 0.44....................
		(delay at power off)	.........
	Current voltage: 0.46.........
	Stopped sweep at V: 0.46
Voltage set for current = 0.5A: 0.46V
Step 1 of calibration complete
	.............................
*** Set current for 0.75A, click node switch ***
Sweeping voltage from 0.60 to 5.00, at step size 0.02
	Current voltage: 0.60....................
		(delay at power off)	.........
	Current voltage: 0.63....................
		(delay at power off)	.........
	Current voltage: 0.65....................
		(delay at power off)	.........
	Current voltage: 0.67..........
	Stopped sweep at V: 0.67
Voltage set for current = 0.75A: 0.67V

Step 2 of calibration complete
Calibration summary for ckt 1
i=0.5A, V = 0.46
i=0.75A, V = 0.67

Calibration Functions: 
	(1.55,-0.30)
	(1.19,-0.05)
	(1.35,-0.13)
