
// Define which pin to be used to communicate with Base pin of TIP120 transistor
int TIP120pin = 11; //for this project, I pick Arduino's PMW pin 11
int pushButton = 7;
int currentStatus = 0;

void setup() {
  pinMode(TIP120pin, OUTPUT); // Set pin for output to control TIP120 Base pin
  pinMode(pushButton, INPUT);
  analogWrite(TIP120pin, 0); // By changing values from 0 to 255 you can control motor speed
  Serial.begin(9600);
  Serial.println("Initialized.");
}

void loop() {
  if (digitalRead(pushButton) == HIGH) {
    Serial.println(">> Current ON!");
    analogWrite(TIP120pin, 255);
    currentStatus = 1;
  }
  while (digitalRead(pushButton) == HIGH){
    analogWrite(TIP120pin, 255);
    delay(100);
  }

  if (currentStatus == 1) {
    currentStatus = 0;
    analogWrite(TIP120pin, 0);
    Serial.println(">> Current OFF");
    Serial.println(">> Current OFF");
  }

  delay(10);
}

