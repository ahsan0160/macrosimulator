// ---------------------------------------------------------------------------
// 
// MACRO power controller
// Revision 2
// 
// Latest asof: April 20, 2017
// Author: Ahsan Nawroj
// Yale University
// 
// ---------------------------------------------------------------------------
// This program is run on an Arduino Uno, tasked with controlling activation
// of a set of Transistor-based current sinking circuits (nodal circuits). 
// 
// Each nodal circuit contains the following:
//  A TIP120 high current transistor
//  An LED connected to the base to signal the presence of significant voltage
//    and report "activation of the transistor circuit"
//  A power resistor connected to the emitter
//  A wire out from the collector to a node of a test MACRO
// 
// The Vpp power source for the collector current is provided by a power 
// supply that is connected to another node of the MACRO. When activated by 
// voltage inputs from this program, each nodal circuit conducts current 
// along its transistor by an amount dictated by the base voltage applied by
// this program. 
// 
// This program operates each nodal circuit activation in one of two ways:
//  1. In response to a logic HIGH from a bank of switches
//  2. In response to a directive from Matlab, which executes shape control in 
//    the test MACRO using an independent vision system
// 
#include "cktCalib.h"
// #define OP_MODE_ONLINE
#define OP_MODE_SWITCH
#define _DEBUG_
// #define _TEST_CALIB_
#define NUM_CKTS 3
#define VfromCnt(x) ((x)/1024.0*5.0)
#define CntFromV(x) ((ceil) ((x)/5.0*1024))

int nodalCircuitOut [NUM_CKTS] = {3,5,6}; 
int switchBankInputs [NUM_CKTS] = {9,10,11}; 

float nodalCircuitOutVolts [NUM_CKTS] = {0.0, 0.0, 0.0};

float calib_m [NUM_CKTS] = {0,0,0};
float calib_c [NUM_CKTS] = {0,0,0};

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Setup method
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void setup() {

  int ii=0;
  // initialize pin states
  for (ii=0; ii < NUM_CKTS; ii++) {
    pinMode(switchBankInputs[ii], INPUT);

    pinMode(nodalCircuitOut[ii], OUTPUT);
    analogWrite(nodalCircuitOut[ii], 0);
  }

  Serial.begin(9600);
  Serial.println("Pin states initialized");

  printSwitchStates();
  printCktStates();
  loadCktCalib();
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// Loop Method
// --------------------------------------------------------------------------
// Output mode 1: in response to switches
// Output mode 2: offline (read from serial, execute)
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void loop() {
  
  #ifdef OP_MODE_ONLINE
    // Read Serial channel
    // Parse input for "circuitNumber, currentOut" pairs
    // Set current for specified circuit and loop
    // Drive current to all channels when message received says "run"
    int runCurrents = 0;
    float *currentsToCkt = (float*) malloc(NUM_CKTS*sizeof(float));
    for (int ii=0; ii<NUM_CKTS; ii++) {
      currentsToCkt[ii] = 0.0;
    }

    Serial.setTimeout(10);
    while (~anySwitchHit()) {
      if (runCurrents) {
        for (int ii=0; ii<NUM_CKTS; ii++) {
          driveCurrent(ii, currentsToCkt[ii]);

          Serial.print("Driving "); Serial.print(currentsToCkt[ii],3);
          Serial.print("A, to ckt "); Serial.println(ii);
        } 
        runCurrents = 0;
      }

      if (Serial.available() > 0) {
        String msg = Serial.readString(); msg.trim();
        // String dd = "Received:--" + msg + "--"; Serial.println(dd);

        if (msg == "run") {
          runCurrents = 1; 
          // Serial.println("Received run command");
          continue;
        }

        if (msg == "end") {
          runCurrents = 1; 
          // Serial.println("Reset all currents");
          for (int ii=0; ii<NUM_CKTS; ii++) {
            currentsToCkt[ii] = 0;
          } continue;
        }

        int cktNum;
        float iOut = 0;
        for (int ii=0; ii<=msg.length(); ii++) {
          if (msg[ii] == ',') {
            cktNum = (int) msg.substring(0,ii).toInt();
            iOut = (float) msg.substring(ii+1).toFloat();
            currentsToCkt[cktNum] = iOut;
            break;
          }
        }
      }

    } delay(10); return;

  #endif


  #ifdef OP_MODE_SWITCH
    // read switches; when switch is set, output 250mA of current
    for (int ii=0; ii < NUM_CKTS; ii++) {
      if (digitalRead(switchBankInputs[ii]) == 1) {
        Serial.println("Driving 250mA current through ckt " + ii);
        // driveCurrent(ii, 0.25); 
        // driveCurrent(ii, 1.25); 
        // driveCurrent(ii, 1.30); // produces 0.6A on 1,2, 0.04 on 3 (?!) 
        setNodeVoltage(ii, 1020); 
        printAndDelay(500);
      } else driveCurrent(ii, 0);
    } delay(10); return;
  #endif

  delay(10);
}




// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// H E L P E R   F U N C T I O N S
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void driveCurrent (int cktNum, float currentOut) {
  analogWrite(nodalCircuitOut[cktNum], vForIout(cktNum, currentOut));
}

void setNodeVoltage (int cktNum, int voltageSetting) {
  analogWrite(nodalCircuitOut[cktNum], voltageSetting);
}

int vForIout (int cktNum, float currentOut) {
  float mm = calib_m[cktNum];
  float cc = calib_c[cktNum];

  float V = (currentOut - cc)/mm;

  int VinCountFormat = (ceil) (1024*V/5.0);
  // if (VinCountFormat > 1023)
  //   VinCountFormat = 1023;

  #ifdef _DEBUG_
    Serial.print("  (i,v,vCnt)=(");
    Serial.print(currentOut);
    Serial.print(","); Serial.print(V);
    Serial.print(","); Serial.print(VinCountFormat);
    Serial.print(")  ");
    #ifdef OP_MODE_SWITCH
      Serial.println("");
    #endif
  #endif

  return VinCountFormat;
}

void printAndDelay(int delayTime) {
  int stepTime = 250;
  int steps = delayTime/stepTime;
  int remainderTime = delayTime%stepTime;

  int ii = 0;
  Serial.print("\t");
  for (ii = 0; ii < steps; ii++) {
    delay(stepTime); Serial.print(".");
  }
  delay(remainderTime); Serial.println(".");
}

int anySwitchHit () {
  for (int ii=0; ii<NUM_CKTS; ii++){
      if (digitalRead(switchBankInputs[ii])) {
        return 1;
      }
  } return 0;
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// C A L I B R A T I O N   F U N C T I O N S
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void loadCktCalib() {
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    calib_m[ii] = calib [ii][0];
    calib_c[ii] = calib[ii][1];
  }
  printCalibrationFcns();

  #ifdef _TEST_CALIB_
  testCalib();
  #endif
}

void testCalib() {
  Serial.println("\nTesting Calibration:");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    Serial.print("\tSetting i-Out to 0.25A for ckt: "); Serial.print(ii);
    analogWrite(nodalCircuitOut[ii], vForIout(ii,0.25));
    printAndDelay(3000);
    analogWrite(nodalCircuitOut[ii], 0);
    delay(2000);
  }

  for (ii=0; ii < NUM_CKTS; ii++) {
    Serial.print("\tSetting i-Out to 0.4A for ckt: "); Serial.print(ii);
    analogWrite(nodalCircuitOut[ii], vForIout(ii,0.4));
    printAndDelay(3000);
    analogWrite(nodalCircuitOut[ii], 0);
    delay(2000);
  }
  Serial.println("Calibration test complete\n");
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// P R I N T   F U N C T I O N S
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void printSwitchStates() {
  Serial.print("Switch States: ");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    int jj = digitalRead(switchBankInputs[ii]);
    Serial.print(jj);
    if (ii != NUM_CKTS - 1) {
      Serial.print(",\t");
    } else {
      Serial.println("");
    }
  }
}

void printCktStates() {
  Serial.print("Circuit Voltages: ");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    float jj = nodalCircuitOutVolts[ii];
    Serial.print(jj);
    if (ii != NUM_CKTS - 1) {
      Serial.print(",\t");
    } else {
      Serial.println("");
    }
  }
}

void printCalibrationFcns() {
  Serial.println("Calibration Functions: ");
  int ii=0;
  for (ii=0; ii < NUM_CKTS; ii++) {
    Serial.print("\t"); Serial.print("("); 
    Serial.print(calib_m[ii], 3);
    Serial.print(","); 
    Serial.print(calib_c[ii], 3); 
    Serial.print(")");
    Serial.println("");
  }
}





