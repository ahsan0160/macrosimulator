Pin states initialized
Calibration Functions: 
  (0.00,0.00)
  (0.00,0.00)
  (0.00,0.00)
Switch hit for ckt: 0
Entering calibraiton process.
Applying output voltage: 2.8662 .............
Enter  measured current output:
Current measured: 0.3580
Data point parsed
Applying output voltage: 2.3828 .............
Enter  measured current output:
Current measured: 1.1190
Data point parsed
Applying output voltage: 0.9863 .............
Enter  measured current output:
Current measured: 0.9930
Data point parsed
Applying output voltage: 1.6016 .............
Enter  measured current output:
Current measured: 0.3480
Data point parsed
Applying output voltage: 1.4062 .............
Enter  measured current output:
Current measured: 0.1520
Data point parsed
Applying output voltage: 2.3779 .............
Enter  measured current output:
Current measured: 1.1400
Data point parsed
Applying output voltage: 2.6855 .............
Enter  measured current output:
Current measured: 0.1830
Data point parsed
Applying output voltage: 1.7383 .............
Enter  measured current output:
Current measured: 0.4870
Data point parsed
Applying output voltage: 2.7881 .............
Enter  measured current output:
Current measured: 0.2850
Data point parsed
Applying output voltage: 0.3711 .............
Enter  measured current output:
Current measured: 0.3650
Data point parsed
Calibration complete.
===============================



Calibrated Circuit: 0

Data table (V,I)
===============================
2.8662,0.3580
2.3828,1.1190
0.9863,0.9930
1.6016,0.3480
1.4062,0.1520
2.3779,1.1400
2.6855,0.1830
1.7383,0.4870
2.7881,0.2850
0.3711,0.3650



===============================
