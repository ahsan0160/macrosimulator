function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_rect (nx,ny)
% -----------------------------------------------------------------------------
% MACRO Input file:
% Provides a network description, and a deformation goal for the MACRO
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

% template structure
% -----------------------------------------------------------------------------
edgeSet = [1,2; 2,4; 1,3; 3,4]';
edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';
startPos = [0 0; 1 0; 0 1; 1 1]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together 
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
	% keep track of overall shape created thus far
	shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

	% template of duplicating shape
	templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

	xx = 1; yy = 1;
	optOffsetX = 0; optOffsetY = 0;
	for yy=1:ny
		
		xx = 1; [xx, yy]
		if yy == 1
			optOffsetY = 0;
		else
			% optOffsetY is found by placing template at origin 
			% and sliding along y axis
			initOffset = [0; 0];
			searchDir = [0; 1];
			optOffsetY = searchForOptSlide (shapeP, templateP, ...
				shapeE, templateE, initOffset, searchDir);
			optOffsetY = optOffsetY(2);
		end			
		disp(sprintf('found opt yy = %.2f', optOffsetY));

		for xx=1:nx
			if xx == 1 && yy == 1
				continue;
			end

			[xx, yy]
			disp(sprintf('stacking in x at y=%.2f', optOffsetY));
			if xx==1 
				optOffsetX = 0;
			else
				initOffset = [0; optOffsetY];
				searchDir = [1; 0];
				optOffsetX = searchForOptSlide (shapeP, templateP, ...
					shapeE, templateE, initOffset, searchDir);
				optOffsetX = optOffsetX(1);
			end
			disp(sprintf('found opt x = %.2f', optOffsetX));
				
			offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

			% add new appropiately placed template to shape
			% -------------------------
			[shapeP, shapeE] = shapeAdditionCleanup(...
				shapeP, shapeE, templateP + offset, templateE);
 			figure(3); clf; showShape(shapeP, 'r');

			% keyboard
		end
	end
	nodeCount = size(shapeP,2);
	[A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
	return
	keyboard
	startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
	0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];
netName = strcat('primitive_rect_',num2str(nx),'_',num2str(ny));
end
% -----------------------------------------------------------------------------





function plotProgress (shapeP, newP)
	figure(1); clf; hold all; axis equal;
	tmp2 = [shapeP newP]; tmp2Bnd = boundary(tmp2',1);
	shapeBnd = boundary(shapeP',1); tmpBnd = boundary(newP',1);
	plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
	plot(newP(1,tmpBnd), newP(2,tmpBnd), 'go--');
	plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
end

