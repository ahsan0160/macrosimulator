	for yy=1:ny
		xx = 1; [xx, yy]

		if yy == 1
			optOffsetY = 0;
		else
			% optOffsetY is found by placing template at origin 
			% and sliding along y axis

			initOffset = [0; 0];
			searchDir = [0; 1];
			optOffsetY = searchForOptSlide (shapeP, templateP, ...
				shapeE, templateE, initOffset, searchDir)
		end
			

		% place template at origin + alpha along y-axis
		large = max(shapeP(2,:))+1;
		initOffset = large*[eps; yy-1>0]
		tmpP = templateP + repmat(initOffset, 1, size(templateP,2));
		plotProgress (shapeP, tmpP);

		% slide along y-axis
		% -------------------------	
		% optOffsetY = searchForOptSlide (shapeP, tmpP, ...
		% 	shapeE, templateE, initOffset, [0 1]);
		searchDir = [0; 1];
		optOffsetY = searchForOptSlide (shapeP, templateP, ...
			shapeE, templateE, searchDir, [1 0]);

		offset = repmat(optOffsetY, 1, size(tmpP,2));
		tmpP = templateP + offset;
		plotProgress (shapeP, tmpP);

		disp(sprintf('found opt yy = %.2f', optOffsetY(2)));

		for xx=1:nx
			[xx, yy]
			disp(sprintf('stacking in x at y=%.2f', optOffsetY(2)));
			if xx==1 && yy==1
				continue;
			end

			% place template at origin + alpha along x-axis
			large = max(shapeP(1,:))+1;
			initOffset = large*[(xx-1)>0; 0] + [0; optOffsetY(2)+0.2]
			tmpP = templateP + repmat(initOffset, 1, size(templateP,2));
			
			% slide along x-axis
			% -------------------------
			% optOffsetX = searchForOptSlide (shapeP, tmpP, ...
				% shapeE, templateE, initOffset, [1 0]);
				searchDir = [1; 0];
			optOffsetX = searchForOptSlide (shapeP, templateP, ...
				shapeE, templateE, searchDir, [1 0]);

			offset = repmat([optOffsetX(1); optOffsetY(2)], 1, size(tmpP,2));
			tmpP = templateP + offset;
			plotProgress (shapeP, tmpP);

			disp(sprintf('found opt x = %.2f', optOffsetX(1)));

			% add new appropiately placed template to shape
			% -------------------------
			[shapeP, shapeE] = shapeAdditionCleanup(...
				shapeP, shapeE, tmpP, templateE);
 			figure(3); clf; showShape(shapeP, 'r');

			nodeCount = size(shapeP,2);
			[A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
			keyboard
		end
	end
