function out = searchForOptSlide (shapeP, newP, shapeE, newE, ...
	initOffset, searchDir)
% -----------------------------------------------------------------------------
% create a virtual nodeset for shapeP and newP using given node pos and edges
% slide newP until it makes contact with shapeP
% -----------------------------------------------------------------------------
	
	% add nVirtual new nodes along the boundary only
	shapeBnd = boundary(shapeP',1);
	newBnd = boundary(newP',1);
	vShapeP = shapeP;
	vNewP = newP;
	nVirtual = 20;

	vIndex = linspace(0,1,nVirtual+2);
	vIndex = vIndex(2:end-1);
	% for ii=1:length(shapeBnd)-1 % for each edge along the boundary
	% 	st = shapeBnd(ii); fin = shapeBnd(ii+1); % start and end nodes

	% for ii=1:size(shapeE,2) % for each edge 
	% 	st = shapeE(1,ii); fin = shapeE(2,ii); % start and end nodes
	% 	a = shapeP(:,st); b = shapeP(:,fin); ab = b - a;

	% 	vNodes = (b-a)*vIndex + repmat(a,1,nVirtual);
	% 	% noise = 0.03*(rand(size(vNodes))-0.5);
	% 	noise = 0;
	% 	vNodes = vNodes + noise;

	% 	vShapeP = [vShapeP vNodes];
	% end

	% for ii=1:size(newE,2) % for each edge 
	% 	st = newE(1,ii); fin = newE(2,ii); % start and end nodes
	% 	a = newP(:,st); b = newP(:,fin); ab = b - a;

	% 	vNodes = (b-a)*vIndex + repmat(a,1,nVirtual);
	% 	vNewP = [vNewP vNodes];
	% end

	vNewP = vNewP + repmat(initOffset, 1, size(vNewP, 2));

	figure(4); clf; showShape(vShapeP);
		% keyboard

	vShapeBnd = boundary(vShapeP',1);
	vShapeArea = polyarea(vShapeP(1,vShapeBnd),vShapeP(2,vShapeBnd));

	vNewBnd = boundary(vNewP',1);
	vNewArea = polyarea(vNewP(1,vNewBnd),vNewP(2,vNewBnd));

	intermPts = 50;


	% xOff = [linspace(0,searchDir(1),intermPts); linspace(0,searchDir(2),intermPts)];
	xOff = [linspace(0,5*searchDir(1),intermPts); ...
			linspace(0,5*searchDir(2),intermPts)];

	figure(2); clf; hold all; axis equal;
	showShape(vShapeP);
	for ii=1:size(xOff,2)
		offset = repmat(xOff(:,ii), 1, size(vNewP,2));
		tmpP = vNewP+offset;

		combArea(ii) = shapeSetOp(vShapeP, tmpP, 'union');
		% areaDiff(ii) = ( vShapeArea + vNewArea - combArea(ii) );
		intArea (ii) = shapeSetOp(vShapeP, tmpP, 'intersection');

		ttP = [vShapeP, tmpP];
		bnd = boundary(ttP');
		bndArea = polyarea(ttP(1,bnd), ttP(2,bnd));
		areaDiff(ii) = vShapeArea + vNewArea - bndArea;

		areaDiff(ii)
		figure(2); hold all; axis equal;
		showShape(tmpP);
		pause(0.1);

		if size(vShapeP,2) == 270
			figure(88); clf; hold all; axis equal;
			showShape(vShapeP);
			showShape(tmpP);
			
			keyboard;
		end
	end


	out = xOff(:,find(intArea<0.01,1));
	% out = xOff(:,find(abs(areaDiff<0.01),1));
	if isempty(out)
		out = [0;0];
	end
	figure(5);  hold all;
	plot(xOff, combArea, 'o-');
	plot(xOff, areaDiff, 'o-');
	plot(xOff, intArea, 'o-');
% keyboard

end
% -----------------------------------------------------------------------------


