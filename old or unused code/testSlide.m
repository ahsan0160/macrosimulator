
load('shapeFiles.mat');

	vShapeBnd = boundary(vShapeP',1);
	vShapeArea = polyarea(vShapeP(1,vShapeBnd),vShapeP(2,vShapeBnd));

	vNewBnd = boundary(vNewP',1);
	vNewArea = polyarea(vNewP(1,vNewBnd),vNewP(2,vNewBnd));

searchDir = [1; 0];
intermPts = 50;

xOff = [linspace(0,5*searchDir(1),intermPts); ...
		linspace(0,5*searchDir(2),intermPts)];

figure(2); clf; hold all; axis equal;
showShape(vShapeP);
for ii=1:size(xOff,2)
	offset = repmat(xOff(:,ii), 1, size(vNewP,2));
	tmpP = vNewP+offset;

	combArea(ii) = shapeSetOp(vShapeP, tmpP, 'union');
	% areaDiff(ii) = ( vShapeArea + vNewArea - combArea(ii) );
	intArea (ii) = shapeSetOp(vShapeP, tmpP, 'intersection');

	ttP = [vShapeP, tmpP];
	bnd = boundary(ttP');
	bndArea = polyarea(ttP(1,bnd), ttP(2,bnd));
	areaDiff(ii) = vShapeArea + vNewArea - bndArea;

	areaDiff(ii)
	figure(2); hold all; axis equal;
	showShape(tmpP);
	pause(0.1);

	% if size(vShapeP,2) == 270
	% 	figure(88); clf; hold all; axis equal;
	% 	showShape(vShapeP);
	% 	showShape(tmpP);

	% 	% keyboard;
	% end

	keyboard

end


