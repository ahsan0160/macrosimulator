
% -----------------------------------------------------------------------------
function areaOut = unionArea (shapeP, newP)
% -----------------------------------------------------------------------------
% -----------------------------------------------------------------------------
	[xa, ya] = polybool('union', ...
					shapeP(1,:), shapeP(2,:), ...
					newP(1,:), newP(2,:));
	unionP = [xa(~isnan(xa)); ya(~isnan(ya))];
	
	% unionP = [shapeP newP];
	unionBnd = boundary(unionP');
	areaOut = polyarea(unionP(1,unionBnd),unionP(2,unionBnd));
	% keyboard
end
% -----------------------------------------------------------------------------


