function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_truss (nx,ny)
% MACRO Input file:
% Provides a network description, and a deformation goal for the MACRO
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

edgeSet = ...
    [1,2; 
     2,3; 
     1,3;
     3,4; 
     2,4]';
edgeTheta = ...
    [0 0 0; 
     0 0 2*pi/3; 
     0 0 pi/3;
     0 0 0; 
     0 0 pi/3; 
     0 0 0; 
     0 0 pi/3]';

startPos = [
    0 0; 
    1 0; 
    0.5 sqrt(3)/2;
    1.5 sqrt(3)/2]';











% template replication if necessary
% make copies in x and y, then stitch the copies together 
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
    % keep track of overall shape created thus far
    shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

    % template of duplicating shape
    templateP = startPos; templateE = edgeSet; templateET = edgeTheta;
    templateBnd = boundary(templateP',1);
    templateArea = polyarea(templateP(1,templateBnd),templateP(2,templateBnd));

    large = 1e1;
    xx = 1; yy = 1;
    for yy=1:ny
        xx = 1
        [xx, yy]

        % place template at origin + alpha along y-axis
        large = max(shapeP(2,:))+1;
        initOffset = large*[eps; yy-1>0]
        tmpP = templateP + repmat(initOffset, 1, size(templateP,2));

        figure(1); clf; hold all; axis equal;
        tmp2 = [shapeP tmpP]; tmp2Bnd = boundary(tmp2',1);
        shapeBnd = boundary(shapeP',1); tmpBnd = boundary(tmpP',1);
        plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
        plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
        plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');


        % slide along y-axis
        % ------------------------- 
        optOffsetY = searchForOptSlide (shapeP, tmpP, initOffset, [0 1]);

        offset = repmat(optOffsetY, 1, size(tmpP,2));
        tmpP = templateP + offset;
        
        figure(1); clf; hold all; axis equal;
        tmp2 = [shapeP tmpP]; tmp2Bnd = boundary(tmp2',1);
        shapeBnd = boundary(shapeP',1); tmpBnd = boundary(tmpP',1);
        % showShape(shapeP); showShape(tmpP); showShape(tmp2);
        plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
        plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
        plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');

        disp(sprintf('found opt yy = %.2f', optOffsetY(2)));
%       keyboard

        for xx=1:nx
            [xx, yy]
            disp(sprintf('stacking in x at y=%.2f', optOffsetY(2)));
            if xx==1 && yy==1
                continue;
            end

            % slide along x-axis
            % -------------------------
            large = max(shapeP(1,:))+1;
            initOffset = large*[(xx-1)>0; 0] + [0; optOffsetY(2)]
            tmpP = templateP + repmat(initOffset, 1, size(templateP,2));

            optOffsetX = searchForOptSlide (shapeP, tmpP, initOffset, [1 0]);

            offset = repmat([optOffsetX(1); 
                             optOffsetY(2)], ...
                1, size(tmpP,2));
            tmpP = templateP + offset;

            figure(1); clf; hold all; axis equal;
            tmp2 = [shapeP tmpP]; tmp2Bnd = boundary(tmp2',1);
            shapeBnd = boundary(shapeP',1); tmpBnd = boundary(tmpP',1);
            plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
            plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
            plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');

            disp(sprintf('found opt x = %.2f', optOffsetX(1)));



            % add new appropiately placed template to shape
            % -------------------------
            [shapeP, shapeE] = shapeAdditionCleanup(...
                shapeP, shapeE, tmpP, templateE);
            figure(3); clf; hold all; axis equal;
            shapeBnd = boundary(shapeP',1); 
            plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');


            nodeCount = size(shapeP,2);
            [A,Ae] = genAdjMats(shapeE, nodeCount);
            showNet(shapeP, A);
            % keyboard
        end
    end

    return
    keyboard
    % removeEdge (shapeE, shapeET, [1,2]);

    startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end
















targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
    0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];

netName = strcat('primitive_truss_',num2str(nx),'_',num2str(ny));

% startPos = [];
% targetPos = [];
% edgeTheta = [];
% edgeSet = [];


% n =
% for ii=1:len*wid

