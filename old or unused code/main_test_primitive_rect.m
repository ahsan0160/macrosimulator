%
% Primitive test module:
% The purpose of this module is to test the selected primitive
%   1. The DOFs of the primitive are simulated
%   2. The primitive is used to tile space:
%     i. using the nx, ny parameters
%     ii. to match/fit a specified shape
%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%
set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;

global_path_setup

% output print locations
% -----------------------------------------------------------------------------
outFolder = 'primitiveTest';
debugMode = 0;
detailedFigs = 0;

% common input parameters
% -----------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties(); name = 'test';

% -----------------------------------------------------------------------------
% setup test condition
% -----------------------------------------------------------------------------
testCond = 2;

if testCond == 1

  % load primitive
  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_rect (1,1);

  % -----------------------------------------------------------------
  % (1) 1. TESTING DOFs
  % -----------------------------------------------------------------

  % 'controlAltCallback' is an anonymous function that the simulator
  % helper calls as an alternative to the macro controller

  %
  % -- OPTION 1: CONTRACT BY 20% ON X--
  contractX = 0.8; contractY = 1; noiseEps = 0;
  controlAltCallback = @(x) deal(1.5*[1 0 1 0]', zeros(4,1));

  %
  % -- OPTION 2: CONTRACT BY 20% ON Y--
  contractX = 1; contractY = 0.8; noiseEps = 0;
  controlAltCallback = @(x) deal(1.5*[1 1 0 0]', zeros(4,1));

  %
  % -- OPTION 3: CONTRACT BY 20% ON X AND Y--
  contractX = 0.8; contractY = 0.8; noiseEps = 0;
  controlAltCallback = @(x) deal(1.5*[2 1 1 0]', zeros(4,1));

  name = strcat(name, '_dof_',num2str(1), '_', ...
    strrep(num2str(contractX), '.','_'), ...
    strrep(num2str(contractY), '.','_') );
  tmp = startPos;
  tmp (:,2:end) = tmp (:,2:end) + ...
      noiseEps*(rand(size(tmp (:,2:end)))-0.5);

  scaleXY = @(xx,sx,sy) [diag([sx, sy])*xx(1:2,:)];
  targetPos = scaleXY(tmp,contractX,contractY);

  helper_runControllerSimulator

elseif testCond == 2
  % -----------------------------------------------------------------
  % (2) 2. i. TESTING TILING (NX,NY)
  % -----------------------------------------------------------------
  % load primitive
  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_rect (8,6);

  % preliminary setup to allow plotting
  nodeCount = size(startPos,2);
  [A,Ae] = genAdjMats (edgeSet, nodeCount);
  startPos = posFromTheta(...
    cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
  targetPos = targetPos * cellProps.nodeToNodeDist;

  figure(1); hold all;
  showNet(startPos,A);
  showNet(targetPos,A);

elseif testCond == 3
  % -----------------------------------------------------------------
  % (3) 2. iI. TESTING TILING (TO SHAPE)
  % -----------------------------------------------------------------

end


