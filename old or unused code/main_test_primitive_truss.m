%
% Primitive test module:
% The purpose of this module is to test the selected primitive
%   1. The DOFs of the primitive are simulated
%   2. The primitive is used to tile space:
%     i. using the nx, ny parameters
%     ii. to match/fit a specified shape
%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%
set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;

global_path_setup

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveTest';
debugMode = 0;
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties(); name = 'test';

% --------------------------------------------------------------------------
% setup test condition
% --------------------------------------------------------------------------
testCond = 2;

if testCond == 1
  % --------------------------------------
  % (1) 1. TESTING DOFs
  % --------------------------------------

  % controlAltCallback = @(x) deal(...
  %               rand(size(startPos, 2),1), ...
  %               zeros(size(startPos, 2),1));

  % load primitive
  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_truss (1,1);

  %
  % -- OPTION 1: TURN LEFT BY 10 DEGREES --
  theta = 10; % degrees
  noiseEps = 0;
  controlAltCallback = @(x) deal(1.5*[1 1 0 1]', zeros(4,1));

  %
  % -- OPTION 2: TURN RIGHT BY 10 DEGREES --
  theta = 350; % degrees
  noiseEps = 0;
  controlAltCallback = @(x) deal(1.5*[1 0 1 1]', zeros(4,1));

  name = strcat(name, '_dof_',num2str(1), '_', num2str(theta));
  tmp = startPos;
  tmp (:,2:end) = tmp (:,2:end) + ...
      noiseEps*(rand(size(tmp (:,2:end)))-0.5);
  targetPos = tmp;
  targetPos = rotatePointSet(tmp,[],theta*pi/180);

  helper_runControllerSimulator

elseif testCond == 2
  % --------------------------------------
  % (2) 2. i. TESTING TILING (NX,NY)
  % --------------------------------------
  % load primitive
  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_truss (2,2);

  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_truss (4,4);
  % keyboard
  % preliminary setup to allow plotting
  nodeCount = size(startPos,2);
  [A,Ae] = genAdjMats (edgeSet, nodeCount);
  startPos = posFromTheta(...
    cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
  targetPos = targetPos * cellProps.nodeToNodeDist;

  % keyboard
  figure(1); hold all;
  showNet(startPos,A,'b');
  showNet(targetPos,A,'r');

elseif testCond == 3
  % --------------------------------------
  % (3) 2. iI. TESTING TILING (TO SHAPE)
  % --------------------------------------

end

