function [nodeInputs] = MACROcontroller (...
	debugMode, A, Ae, startPos, targetPos)
% --------------------------------------------------------------
% 
% Starting and target node positions are spceificed by the input 
% The 'starting' configuration need not be an equilibrium 
% configuration. 
% Each MACRO is assumed to be in some equlibrium configuration 
% wth all the cells in cold martensitic state at 25 degC. 
% Control inputs for the equlibrium configuration to deform to the
% 'start' positions are spceificed.
% Each subsequent 'target' deformations are achieved from this 'start'
% configuration using control inputs that are output from this function
% 
% Control problem of finding the required node voltages to achieve
% the specified target node positions is reduced to the problem of 
% finding the required cell power that results in the target cell 
% stiffness.
% 
% (can be positive: voltages applied, or negative: cooling applied)
% Output: stiffness
% 
% --------------------------------------------------------------

addpath(genpath('controllerHelperFiles'));
% addpath('controllerHelperFiles/geom2d');

if ~exist('startPos', 'var') || ~exist('targetPos', 'var')
	disp('No inputs specified');
	return
end

% keyboard
if ~exist('inputFileName', 'var')
	inputFileName = 'test';
end
outputFilename  = strcat(inputFileName, '.gif');

m = 0.01; c = 4; cellR = 1.5;
nitiL0 = 0.8; kSpring = 0.2; springL0 = 1.75;
kM = 0.0302; kA = 0.7189; % from cell experimental data

% Inputs to the script could be only positions of nodes (K's are derived)
% OR start and target K's as well

if ~exist('startK', 'var') || ~exist('targetK', 'var')
	% Convert provided node pos inputs to start and target cell stiffnesses
	tmp = kM*ones(length(A),1);
%    keyboard
	startK = YtoK (startPos, Ae, tmp, nitiL0, kSpring, springL0);
	targetK = YtoK (targetPos, Ae, [], nitiL0, kSpring, springL0);
end
% Y, Ae, Kguess, nitiL0, kSpring, springL0
% Visually verify the pos inputs and converted stiffness inputs
% -----------------------------------------------------------------------
identifiedStartPos = KtoY(startK, Ae, startPos);
identifiedTargetPos = KtoY(targetK, Ae, targetPos);
figure(1); showNet(startPos, A, [1 0 0], 2);
figure(1); showNet(targetPos, A, [0 1 0], 2);
figure(1); showNet(identifiedStartPos, A, [1 0 0], 3, '--');
figure(1); showNet(identifiedTargetPos, A, [0 1 0], 3, '--');

% obtain f1 as two fits (forward T and backward T)
% -----------------------------------------------------------------------
f = mappingTtoK (kM, kA);
fMtoA = f{1}; fAtoM = f{2};



dt = 2; % s
T0 = 25*ones(size(startK)); % degC
eqK = kM*ones(size(startK));

% CONTROL INPUTS: 
% POWER INPUTS TO CHANGE STIFFNESS FROM EQ-K TO TARGET-K
% --------------------------------------------------------------
a = differentiate(fMtoA, T0)/(m*c);
PtoStart = (1./(a*dt)).*(-startK+eqK)
VtoStart = PtoV(PtoStart,Ae,cellR)
VcoolToStart = PtoVCooling (PtoStart, Ae)

% CONTROL INPUTS: 
% POWER INPUTS TO CHANGE STIFFNESS FROM START-K TO TARGET-K
% --------------------------------------------------------------
a = differentiate(fMtoA, T0)/(m*c);
PtoTarget = (1./(a*dt)).*(-targetK+startK)
VtoTarget = PtoV(PtoTarget,Ae,cellR)
VcoolToTarget = PtoVCooling (PtoTarget, Ae)

% keyboard


% Simulate the use of these control inputs to the network
% --------------------------------------------------------------
X = startPos;
TT = T0;
simt0 = 0;
simdt = 0.005;
simtfin = 2;
EE = zeros(size(TT));

startPosAchieved = 0;
prevErr = inf;
saturatedPrevErr = 0;
saturationLimit = 25;
close all;
figure(99); %hold all; 

showNet(X,A); 
showNet(startPos,A, [0.8 0.8 0.3], 3, '-'); 
showNet(targetPos,A, [0 0 1], 3, '-'); 
if debugMode ~= 1
	frame = getframe;
	im = frame2im(frame);
	[imind,cm] = rgb2ind(im,256);
	saveFrameToGif(outputFilename, imind, cm, 1);
end

for t=simt0:simdt:simtfin
	if startPosAchieved ~= 1
		PP = VtoP(VtoStart,Ae) - VCoolingToP(VcoolToStart,Ae);
	else
		PP = VtoP(VtoTarget,Ae) - VCoolingToP(VcoolToTarget,Ae);
	end
	% PP
	EE = EE + PP*simdt;
	delT = EE/(m*c);
	TT = T0 + delT;
	[EE TT];
	KK = feval(fMtoA, TT);
	X = KtoY(KK,Ae, startPos,nitiL0, kSpring,springL0);
	if startPosAchieved ~= 1
		X = alignNets(X,startPos);
	else
		X = alignNets(X,targetPos);
	end
	if mod(t, 2*simdt) == 0
		
		showNet(X,A);
		drawnow
		if debugMode ~= 1
			frame = getframe;
			im = frame2im(frame);
			[imind,cm] = rgb2ind(im,256);
			saveFrameToGif(outputFilename, imind, cm, 0, simdt);
		end
		% axis auto
		% t
		pause(simdt);
	end
	if startPosAchieved ~= 1
		err = nodePosErr(X,startPos);
	else
		err = nodePosErr(X,targetPos);
	end
	fprintf(2, 'Err: %.4f\n', sum(err));
	if sum(err) < 0.2
		
		showNet(X,A);
		if startPosAchieved ~= 1
			disp('Starting position achieved.');
			title('Starting position achieved');
			hh=text(0, -0.5, 'Starting position achieved');
			startPosAchieved = 1;
		else 
			disp('Target position achieved.');
			title('Target Position achieved');
			delete(hh);
			hh=text(0, -0.5, 'Target position achieved');
			break;
		end
		drawnow
		if debugMode ~= 1
			frame = getframe;
			im = frame2im(frame);
			[imind,cm] = rgb2ind(im,256);
			saveFrameToGif(outputFilename, imind, cm, 0, 2);
		end
	end


	sErr = sum(err);
	if abs(prevErr - sErr) < 0.01
		saturatedPrevErr = saturatedPrevErr + 1;
	end
	if saturatedPrevErr > saturationLimit
		break
	end

	prevErr = sum(err);
	figure(99);

end

		
	title('Closest position to target achieved');
	if exist('hh','var') 
      delete(hh);
   end
	hh=text(0, -0.5, 'Closest position to target achieved');
	showNet(X,A, [0.1 0.1 0.1], 3, '-.'); 
	drawnow

	if debugMode ~= 1
		frame = getframe;
		im = frame2im(frame);
		[imind,cm] = rgb2ind(im,256);
		saveFrameToGif(outputFilename, imind, cm, 0, 2);
		saveFrameToGif(outputFilename, imind, cm, 0, 2);
	end
return







