%
% Primitive test module:
% The purpose of this module is to test the selected primitive
%   1. The DOFs of the primitive are simulated
%   2. The primitive is used to tile space:
%     i. using the nx, ny parameters
%     ii. to match/fit a specified shape
%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%
set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;

global_path_setup

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveTest';
debugMode = 0;
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties(); name = 'test';

% --------------------------------------------------------------------------
% setup test condition
% --------------------------------------------------------------------------
  % --------------------------------------
  % (2) 2. i. TESTING TILING (NX,NY)
  % --------------------------------------
  [name,edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_hyb2t2h (4,8);

  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_hex (10,10);

  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_hex (4,4);
  % keyboard
  % preliminary setup to allow plotting
  nodeCount = size(startPos,2);
  [A,Ae] = genAdjMats (edgeSet, nodeCount);
  startPos = posFromTheta(...
    cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
  targetPos = targetPos * cellProps.nodeToNodeDist;

  % keyboard
  figure(1); hold all;
  showNet(startPos,A,'b');
  showNet(targetPos,A,'r');

