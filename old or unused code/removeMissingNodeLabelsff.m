% -----------------------------------------------------------------------------
function [outE] = removeMissingNodeLabels (shapeE, nNodes)  
% -----------------------------------------------------------------------------
% relabels nodes in the edgeset so no missing node numbers exist
% -----------------------------------------------------------------------------
	outE = shapeE;
	ii = 1;
	for ii=1:max(outE(:)) % for each node in the label set
		indx = (outE(1,:)==ii) + (outE(2,:)==ii); % edges this node appears
		while ~any(indx) && ii <= nNodes % if node label appears nowhere

			% renumber the nodes in the edgeset to remove this particular label
			outE(1,outE(1,:)>ii) = outE(1,outE(1,:)>ii)-1; 
			outE(2,outE(2,:)>ii) = outE(2,outE(2,:)>ii)-1; 

			% check for presence of node label again: 
			% in case multiple consecutive labels are missing
			indx = (outE(1,:)==ii) + (outE(2,:)==ii);
		end
		outE;
	end
end
% -----------------------------------------------------------------------------


