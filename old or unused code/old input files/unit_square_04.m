function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = unit_square_04 ()
% MACRO Input file: 
% Provides a network description, and a deformation goal for the MACRO
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

edgeSet = [1,2; 2,4; 1,3; 3,4]';
edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';

startPos = [0 0; 1 0; 0 1; 1 1]';

% works but takes a long time
% targetPos =	[0 0; 0.975 0; 0.1 0.95; 0.925 0.875]'; 

targetPos =	[0 0; 1 0; 0.08 0.98; 0.925 0.875]';

% targetPos =	[0 0; 1 0; 0.0826 0.996; 0.9256 0.8779]';



anchors = 1;
fixedInX = [];
fixedInY = 2;
fixedInZ = [];

netName = 'unit_square_04';