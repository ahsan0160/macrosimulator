function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = two_cell_chain ()
% MACRO Input file: 
% Provides a network description, and a deformation goal for the MACRO
% 
% Summary: A pair of cells connected in a chain by three nodes
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% 
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

edgeSet = [1,2; 2,3]';
edgeTheta = [0 0 0; 0 0 0]';

startPos = [0 0; 1 0; 2 0]';
targetPos = [0 0; 0.8 0; 1.5 0]';

anchors = 1;
fixedInX = [];
fixedInY = 2;
fixedInZ = [];

netName = 'two_cell_chain';