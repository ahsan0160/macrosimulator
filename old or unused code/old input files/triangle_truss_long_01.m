function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = triangle_truss_long_01 ()
% MACRO Input file: 
% Provides a network description, and a deformation goal for the MACRO
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

edgeSet = ...
    [1,2; 
     2,3; 
     1,3; 
     3,4; 
     2,4;
     2,5;
     4,5;
     4,6;
     5,6]';
edgeTheta = ...
    [0 0 0; 
     0 0 2*pi/3; 
     0 0 pi/3; 
     0 0 0;
     0 0 pi/3;
     0 0 0;
     0 0 5*pi/3;
     0 0 0;
     0 0 pi/3]';

startPos = [
    0 0; 
    1 0; 
    0.5 1; 
    1.5 1;
    2 0;
    2.5 1]';
targetPos = [
    0 0; 
    1 0; 
    0.5 0.75; 
    1.5 0.7;
    2 -0.25;
    2.5 0.5]';
% targetPos = [
%     0 0; 
%     1 0; 
%     0.5 1; 
%     1.5 1.2;
%     1.9 0.5;
%     2 1.2]';
% targetPos = [
%     0 0; 
%     0.9 0; 
%     0.4 0.7; 
%     1.275 1.25;
%     1.5 0.5;
%     1.75 1.5]';

anchors = 1;
fixedInX = [];
fixedInY = 2;
fixedInZ = [];

netName = 'triangle_truss_long_01';