function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = unit_square_02 ()
% MACRO Input file: 
% Provides a network description, and a deformation goal for the MACRO
% Inputs: None
% Outputs: edgeSet, edgeTheta, startPos, targetPos
% >> Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2016 Yale University; All rights reserved.

edgeSet = [1,2; 2,4; 1,3; 3,4]';
edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';

startPos = [0 0; 1 0; 0 1; 1 1]';
targetPos =	[0 0; 1 0; 0 1; 0.8 0.9]';

anchors = 1;
fixedInX = [];
fixedInY = 2;
fixedInZ = [];

netName = 'unit_square_02';