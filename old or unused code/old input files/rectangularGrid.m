% MACRO Input File
% =========================================================================
% Creates a network representation of a regular rectangular grid in 2 or 3
% dimensions
% All cartesian positions are in 3D. 
%
% Output specs:
%     nodePos = n-vector of x,y,z positions of nodes (3 x n)
%     edgeSet = m-vector of edge vertex pairs (st,end) (2 x m)
%
% =========================================================================
%
% Active Cell Network Simulations
% Created 2015 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% (c) 2015 Yale University
% All rights reserved.

function [edgeSet, edgeTheta] = rectangularGrid (xx, yy, zz)
cLen = 1;

%% connectivity matrix
eOutNodes = zeros(1, (xx-1)*yy);
eInNodes = zeros(1, (xx-1)*yy);
uu=1;
for ii=1:xx
    for jj=1:yy
        for kk=1:zz
            current = sub2ind([xx yy zz], ii, jj, kk);
            
            if ii ~= xx
                bottom = sub2ind([xx yy zz], ii+1, jj, kk);
                eOutNodes(uu)=current;
                eInNodes(uu) = bottom;
                uu=uu+1;
            end
            if jj ~= yy
                right = sub2ind([xx yy zz], ii, jj+1, kk);
                eOutNodes(uu)=current;
                eInNodes(uu) = right;
                uu=uu+1;
            end
            if kk ~= zz
                below = sub2ind([xx yy zz], ii, jj, kk+1);
                eOutNodes(uu)= current;
                eInNodes(uu) = below;
                uu=uu+1;
            end
        end
    end
end

connMat = sparse(eOutNodes, eInNodes, 1, ...
    xx*yy*zz, xx*yy*zz, length(eOutNodes));
connMat = double(connMat | connMat');
% imshow(full(connMat));

%% edgeset is defined in terms of the connMat
edgeSet = zeros(2, length(nonzeros(triu(connMat))));
[edgeSet(1, :), edgeSet(2, :)]= find(triu(connMat) ~= 0);

%% edgeTheta is defined in terms of edgeSet
edgeTheta = zeros(3,length(edgeSet));
for ii=1:length(edgeSet)
    if zz == 1
        if edgeSet(2,ii) - edgeSet(1,ii) == 1
            edgeTheta(3,ii) = 0;
        elseif edgeSet(2,ii) - edgeSet(1,ii) == xx
            edgeTheta(3,ii) = pi/2;
        end
        
    else % 3d case: add all euler angles
        if edgeSet(2,ii) - edgeSet(1,ii) == 1
            edgeTheta(3,ii) = 0;
        elseif edgeSet(2,ii) - edgeSet(1,ii) == xx
            edgeTheta(3,ii) = pi/2;
        elseif edgeSet(2,ii) - edgeSet(1,ii) == xx*yy
            edgeTheta(2,ii) = pi/2;            
        end
    end
end
