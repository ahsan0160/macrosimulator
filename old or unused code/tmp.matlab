for xx=1:nx
		for yy=1:ny
			if xx==1 && yy==1
				continue;
			end

			% place template at origin + alpha along each axis
			initOffset = large*[xx-1; yy-1];
			tmpP = templateP + repmat(initOffset, 1, size(templateP,2));

			figure(1); clf; hold all; axis equal;
            shapeBnd = boundary(shapeP');
            plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
            tmpBnd = boundary(tmpP');
            plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
            tmp2 = [shapeP tmpP];
            tmp2Bnd = boundary(tmp2');
            plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
            axis equal;

            input('');

			% slide along x-axis
            optOffsetX = fminsearch(@(x) ...
            	shapeTranslatePotential (shapeP, tmpP, x), ...
            		[initOffset(1); eps]);

			offset = repmat([	optOffsetX(1)+initOffset(1); 
								initOffset(2)], ...
				1, size(tmpP,2));
			tmpP = templateP + offset;

			figure(1); clf; hold all; axis equal;
            shapeBnd = boundary(shapeP');
            plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
            tmpBnd = boundary(tmpP');
            plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
            tmp2 = [shapeP tmpP];
            tmp2Bnd = boundary(tmp2');
            plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
            axis equal;

			% keyboard
            input('');

			% slide along y-axis
            optOffsetY = fminsearch(@(x) ...
            	shapeTranslatePotential (shapeP, tmpP, x), ...
            		[eps; initOffset(2)]);

			offset = repmat([optOffsetX(1)+initOffset(1); 
							 optOffsetY(2)+initOffset(2)], ...
				1, size(tmpP,2));
			tmpP = templateP + offset;
			% tmpP = templateP + initOffset + offset;
			% tmpP = tmpP+offset;
			% offset = repmat(optOffset, 1, size(templateP,2));
			% tmpP = tmpP+offset;



			figure(1); clf; hold all; axis equal;
            shapeBnd = boundary(shapeP');
            plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
            tmpBnd = boundary(tmpP');
            plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'go--');
            tmp2 = [shapeP tmpP];
            tmp2Bnd = boundary(tmp2');
            plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
            axis equal;

            shapeP = [shapeP tmpP];

			% keyboard
			% showNet(shapeP);
		end
	end










      
% -----------------------------------------------------------------------------
function out = shapeTranslatePotential (shapeP, newP, vect)
% -----------------------------------------------------------------------------
% used for fminsearch to get closest point for the added shape to existing one
% along the provided vect
% -----------------------------------------------------------------------------
      shapeBnd = boundary(shapeP');
      shapeArea = polyarea(shapeP(1,shapeBnd),shapeP(2,shapeBnd));

      newBnd = boundary(newP');
      newArea = polyarea(newP(1,newBnd),newP(2,newBnd));

      offset = repmat(vect, 1, size(newP,2));
      tmpP = newP+offset;
      out = abs( shapeArea + newArea - unionArea(shapeP, tmpP) );

      figure(2); hold all; axis equal;
      tmpBnd = boundary(tmpP');
      plot(tmpP(1,tmpBnd), tmpP(2,tmpBnd), 'o--');
      pause(0.1);


end
% -----------------------------------------------------------------------------