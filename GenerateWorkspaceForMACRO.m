%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%

global_path_setup

set(0,'defaultlinelinewidth',4);
clear tempArray;
clear posArray;
clf; clc;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties();
name = 'test';


% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_01 ();
[name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_04 ();

nodeCount = size(startPos,2);
E = edgeSet;
tempVals = 40:0.5:50; tempArray = zeros(length(tempVals)^size(E,2), size(E,2));
posArray = zeros(3,nodeCount,size(tempArray,1));

idx = 1;
if size(E,2) == 3
  disp('WORKSPACE FOR THREE CELLS');
  for i1=1:length(tempVals)
    for i2=1:length(tempVals)
      for i3=1:length(tempVals)
        tempArray(idx,:) = ...
          [tempVals(i1) tempVals(i2) tempVals(i3)];
        idx = idx+1;
      end
    end
  end
elseif size(E,2) == 4
  disp('WORKSPACE FOR FOUR CELLS');
  for i1=1:length(tempVals)
    for i2=1:length(tempVals)
      for i3=1:length(tempVals)
        for i4=1:length(tempVals)
          tempArray(idx,:) = ...
            [tempVals(i1) tempVals(i2) tempVals(i3) tempVals(i4)];
          idx = idx+1;
        end
      end
    end
  end
end


template_macro = MACRO(edgeSet, edgeTheta);
% return

fprintf(2,'Computation started\n');
parfor kk=1:size(tempArray,1)

  macro = template_macro;
  macro.netCellT = tempArray(kk,:)';
  [L0, kExt, kComp, macro.netCellState] = ...
    macro.cellModel.cellMapTtoL0andK(...
      macro.netCellT, macro.netCellState);

  idx = sub2ind(size(macro.netCellL0), E(1,:), E(2,:));
  macro.netCellL0 (idx) = L0;
  macro.netCellKExt (idx) = kExt;
  macro.netCellKComp (idx) = kComp;

  macro.netNodeTheta = macro.computeNodeAngles(...
    macro.nodePos);

  posArray(:,:,kk) = macro.minEnergyPos();
end
fprintf(2,'Computation ended\n');

save(strcat('zzWorkspaces/', name,'_workspace.mat'), 'tempArray', 'posArray');

return


