
function ff = plotConfigTrace (series_simTime, series_config, ...
	series_errConfig, cellProps, controller)

% keyboard

ff= figure; clf; hold on;
itrCount = length(series_simTime);
ll = length(controller.targetK);
row = floor(sqrt(ll));
col = ceil(ll/row);
for jj=1:size(series_config,1)
	subplot(row,col,jj); hold on;
	c = rand(1,3);
	target = plot(series_simTime,repmat(controller.targetK(jj),itrCount, 1),...
		'Color',c, 'LineStyle','-.');
	current = plot(series_simTime,series_config(jj,:), 'Color',c);
	err = plot(series_simTime,abs(series_errConfig(jj,:)), 'Color',c,...
		'Color',c, 'LineStyle',':');

	legend([current, target, err], ...
		{'Current Stiffness', 'Target Stiffness', 'Error'})
	% clims = get(gca,'ylim');	ylim([clims(1), cellProps.kA]);
	ylim([0, cellProps.kA]);
	xlabel('Time (s)');
	ylabel('Cell Stiffness');
	title(strcat('Cell: ',num2str(jj)));
end

