
function ff = plotDesiredPath (k0, kf, series_config)

ff=figure; hold on;
set(0,'defaultlinelinewidth',1);
if ~exist('cellProps','var')
	cellProps = ActiveCellProperties();
end
if length(k0) == 1
	k0(2) = 0;
	kf(2) = 0;
end

if length(k0) == 2
	k0 = k0(1:2); kf = kf(1:2);
	kSeries = series_config(1:2,:);
	h4 = plot(kSeries(1,:), kSeries(2,:), 'ko--','LineWidth', 1);
	h1 = plot(k0(1), k0(2), 'ro');
	h2 = plot(kf(1), kf(2), 'bo');
	h3 = plot(kSeries(1, end), kSeries(2, end), 'go');
	legend([h1 h2 h3 h4], {...
		'Starting config.', 'Desired final config.', ...
		'Achieved final config.', 'Intermediate config.'})
	xlabel('k_1 (N/mm)');
	ylabel('k_2 (N/mm)');
else
	k0 = k0(1:3); kf = kf(1:3);
	kSeries = series_config(1:3,:);
	h4 = plot3(kSeries(1,:), kSeries(2,:), kSeries(3,:),'ko--','LineWidth', 1);
	h1 = plot3(k0(1), k0(2), k0(3), 'ro');
	h2 = plot3(kf(1), kf(2), kf(3), 'bo');
	h3 = plot3(kSeries(1, end), kSeries(2, end), kSeries(3, end), 'go');
	legend([h1 h2 h3 h4], {...
		'Starting config.', 'Desired final config.', ...
		'Achieved final config.', 'Intermediate config.'})
	view(3);
	xlabel('k_{Cell 1} (N/mm)');
	ylabel('k_{Cell 2} (N/mm)');
	zlabel('k_{Cell 3} (N/mm)');
	% xlim([cellProps.kM, cellProps.kA]);
	% ylim([cellProps.kM, cellProps.kA]);
	% zlim([cellProps.kM, cellProps.kA]);
	axis equal;
end

% keyboard
