
function ff = plotTtoKmap () 

	global_path_setup

	cellProps = ActiveCellProperties();

	T = linspace(10,90,200);
	kA = cellProps.kA; 		
	kM = cellProps.kM;
	T1m2a = 40;		T2m2a = 50;		
	T1a2m = 45;		T2a2m = 25;

	% piecewise linear profiles
	% -----------------------------------------------------------------------
	% define piecewise linear function for mart to aust curve
	k = kM*ones(size(T));
	k(T>T2m2a) = kA;
	k(T<T2m2a & T > T1m2a)=...
		kM + (T(T<T2m2a & T>T1m2a)-T1m2a)*(kA-kM)/(T2m2a-T1m2a);

	% define piecewise linear function for aust to mart curve
	k2 = kM*ones(size(T));
	k2(T>T1a2m) = kA;
	k2(T>T2a2m & T<T1a2m) = ...
		kM + (T(T>T2a2m & T<T1a2m)-T2a2m)*(kA-kM)/(T1a2m-T2a2m);
	ff = figure; clf; hold all; 
	h1 = plot(T,k, 'r--','LineWidth', 2);
	h2 = plot(T,k2,'b--','LineWidth', 2);

	h3 = plot(T,cellProps.fMtoA(T),'r-','LineWidth', 3);
	h4 = plot(T,cellProps.fAtoM(T),'b-','LineWidth', 3);

	xlabel('Temperature (^oC)');
	ylabel('Nitinol stiffness (N/mm)');

	legend([h1 h2 h3 h4], ...
		{'Fit: Mart. to Aust.','Fit: Aust. to Mart.',...
		 'Fourier approx: Mart. to Aust.',...
		 'Fourier approx: Aust. to Mart.'}, ...
		 'Location', 'SouthEast');

	