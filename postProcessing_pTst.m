
% --------------------------------------------------------------------------
%
% Post processing for primitive testing
%
% This script plots:
%   A series of "design performance" metrics of mesh samples of different
%     mesh primitive constructors
% 
% For a specified (or hardcoded) set of trials, the data to plot 
%   (stroke, stiffness, etc.) can be in dataLog files IF the measurements 
%   were conducted online
% If measurements were not conducted online, the dataLog files will not 
%   have measure data. 
%   In this case, there should be another file in the data directory called 
%     msrData_fname. The measurements would be in this file. 
%   If msrData_fname does not exist, this file calls for offline measurements
%     using function offline_measureStiffnessProps
%
% --------------------------------------------------------------------------
function postProcessing_pTst (str, override, setToEval)
  set(0,'defaultlinelinewidth',1); set(0,'defaultlinemarkersize',15);
  close all; global_path_setup;
  bendAnalysis = 0; compAnalysis = 0;
  if strfind(str, 'bend')
    bendAnalysis = 1;
  else
    compAnalysis = 1;
  end

  flags.SHOW_BBOX = 0;
  flags.PRINT_FIGS = 1;
  if exist('override','var')
    flags.OVERRIDE_SAFETY = override;
  else
    flags.OVERRIDE_SAFETY = 0;
  end    
  flags.SAVE_WORKSPACE = 1;

  singleTrialMultiMeasure = 0; multiTrialMultiMeasure = 0;
  if bendAnalysis
    specificSet = 22;
    singleTrialMultiMeasure = 1;
  elseif compAnalysis
    specificSet = 12;
    multiTrialMultiMeasure = 1;
  end

  if exist('setToEval','var')
    specificSet = setToEval;
  end

  figNum = 45;
  dataFolder = 'allOutputsDataFolder/pTst/';

  setFolders = dir(strcat(dataFolder,'set',num2str(specificSet),'*'));
  if ~isempty(setFolders)
    dataFolder = strcat(dataFolder,setFolders(1).name,'/');
  end
  dataFolder
  dataFiles = dir(strcat(dataFolder,'*_dataLog.mat'));
  nFiles = length(dataFiles);

  pNames = {'pHex', 'pRect', 'pTri', 'pHyb3t2s', 'pHyb2t2h', 'pHybs2o', ...
    'pHyb4th', 'pHybtshs', 'pHybshd', 'pHybt2d', 'pHyb2tsts' };

  pHandle = {@primitive_hex, @primitive_rect, @primitive_tri, ...
    @primitive_hyb3t2s, @primitive_hyb2t2h, @primitive_hybs2o, ...
    @primitive_hyb4th, @primitive_hybtshs, @primitive_hybshd, ...
    @primitive_hybt2d, @primitive_hyb2tsts};

  for ii=1:length(pHandle)
    dataByP(ii).name = func2str(pHandle{ii}); dataByP(ii).idx = [];
    dataByP(ii).kXrest = []; dataByP(ii).kYrest = [];
    dataByP(ii).kXActX = []; dataByP(ii).kYActY = [];
    dataByP(ii).kXActY = []; dataByP(ii).kYActX = [];
    dataByP(ii).bendE = []; dataByP(ii).bendK = [];
  end
  actXidx = []; actYidx = [];

  % =====================================================================
  %
  % Load the dataLog files
  %
  % =====================================================================
  loadAllData = 1;
  if exist (strcat(dataFolder,'rawData.mat')) 
    load (strcat(dataFolder,'rawData.mat')); 
    loadAllData = 0; 
  end

  if flags.OVERRIDE_SAFETY
    loadAllData = 1;
  end

  if loadAllData    
    % parse files and separate truss from rect logs
    for ii=1:nFiles
      trgt = dataFiles(ii).name; srch = @(str) ~isempty(strfind(trgt, str));
      kk = find(cellfun(srch, pNames));

      dataByP(kk).idx = [dataByP(kk).idx ii];
      aa = load(strcat(dataFolder, dataFiles(ii).name)); rawData(ii) = aa;

      if (rawData(ii).dataLog.actuationDir(1) == 1)
        actXidx = [actXidx ii];
      else
        actYidx = [actYidx ii];
      end
    end

    %%
    % =====================================================================
    %
    %
    %
    %             C   A   L   C   U   L   A   T   I   O   N   S
    %
    %
    %
    % =====================================================================
    % Do all data extraction and per-trial level processing all at once
    % =====================================================================

    all_strokeX  = []; all_strainX  = []; all_strokeY  = []; 
    all_strainY  = []; all_compKinX = []; all_compKinY = []; 
    all_bendE = []; all_bendK = [];

    if multiTrialMultiMeasure
      groups = cell(1,nFiles);
      for ii=1:length(pHandle)
        groups(dataByP(ii).idx) = {dataByP(ii).name};
      end
    elseif singleTrialMultiMeasure 
      groups = {};
    end

    for ii=1:nFiles % scroll over trials
      pos = rawData(ii).dataLog.pos; edges = rawData(ii).dataLog.edgeSet;
      res = 3; % number of virtual nodes to use between nodes
      if flags.SHOW_BBOX
        % Bounding box for the pos set (over time)
        for jj=1:size(pos,3) % scroll over time
          posBBox (:,jj,ii) = measure_meshBoundDims(pos(:,:,jj), edges, res);
        end
      else
        posBBox(:,1,ii) = measure_meshBoundDims(pos(:,:,1), edges, res);
        posBBox(:,size(pos,3),ii) = ...
          measure_meshBoundDims(pos(:,:,size(pos,3)), edges, res);
      end

      if multiTrialMultiMeasure 
        % ----------------------------------------------------
        % Overall stroke and strain for trial
        % ----------------------------------------------------
        all_strokeX(ii) = posBBox(3,1,ii) - posBBox(3,end,ii);
        all_strainX(ii) = all_strokeX(ii)/posBBox(3,1,ii);

        all_strokeY(ii) = posBBox(4,1,ii) - posBBox(4,end,ii);
        all_strainY(ii) = all_strokeY(ii)/posBBox(4,1,ii);

        % ----------------------------------------------------
        % Stiffness in X and Y stored
        % ----------------------------------------------------
        all_compKinX (:,ii) = rawData(ii).dataLog.compKinX;
        all_compKinY (:,ii) = rawData(ii).dataLog.compKinY;

        % ----------------------------------------------------
        % Bending Stiffness stored
        % ----------------------------------------------------
        if isfield(rawData(ii).dataLog, 'bendE')
          all_bendE (:,ii) = rawData(ii).dataLog.bendE;
          all_bendK (:,ii) = rawData(ii).dataLog.bendKinPTh;
        end

      elseif singleTrialMultiMeasure
        nRep = size(rawData(ii).dataLog.bendE,1);

        newStrokeX = posBBox(3,1,ii) - posBBox(3,end,ii);
        newStrainX = newStrokeX/posBBox(3,1,ii);
        newStrokeY = posBBox(4,1,ii) - posBBox(4,end,ii);
        newStrainY = newStrokeY/posBBox(4,1,ii);

        newCompKinX = rawData(ii).dataLog.compKinX;
        newCompKinY = rawData(ii).dataLog.compKinY;

        groupName = dataByP(ii).name;
        for kk = 1:size(rawData(ii).dataLog.bendE,1)
          % ----------------------------------------------------
          % Overall stroke and strain for trial
          % ----------------------------------------------------
          all_strokeX = [all_strokeX; newStrokeX];
          all_strainX = [all_strainX; newStrainX];
          all_strokeY = [all_strokeY; newStrokeY];
          all_strainY = [all_strainY; newStrainY];

          % ----------------------------------------------------
          % Stiffness in X and Y stored
          % ----------------------------------------------------
          all_compKinX = [all_compKinX; newCompKinX];
          all_compKinY = [all_compKinY; newCompKinY];

          % ----------------------------------------------------
          % Bending Stiffness stored
          % ----------------------------------------------------
          all_bendE = [all_bendE; rawData(ii).dataLog.bendE(kk,:)];
          all_bendK = [all_bendK; rawData(ii).dataLog.bendKinPTh(kk,:)];

          groups = [groups, {groupName}];
        end       
      end
    end

    % ----------------------------------------------------
    % Stroke over the course of the trials
    % ----------------------------------------------------
    strainXactX = NaN*ones(1,nFiles); strainYactY = NaN*ones(1,nFiles);
    strainXactX(actXidx) = all_strainX(actXidx);
    strainYactY(actYidx) = all_strainY(actYidx);

    if compAnalysis
      % ----------------------------------------------------
      % Stiffness at rest
      % ----------------------------------------------------
      kXrest = all_compKinX (1,:); kYrest = all_compKinY (1,:);
   
      % ----------------------------------------------------
      % Stiffness at trial fin
      % ----------------------------------------------------
      kXfin = all_compKinX(end,:); kYfin = all_compKinY(end,:);
      kYActY = NaN*ones(1,nFiles); kXActX = NaN*ones(1,nFiles);
      kXActY = NaN*ones(1,nFiles); kYActX = NaN*ones(1,nFiles);
      for ii=1:length(pHandle)
        % on axis
        dataByP(ii).kXActX = kXfin(intersect(actXidx,dataByP(ii).idx));
        dataByP(ii).kYActY = kXfin(intersect(actYidx,dataByP(ii).idx));
        kXActX(intersect(actXidx,dataByP(ii).idx)) = dataByP(ii).kXActX;
        kYActY(intersect(actYidx,dataByP(ii).idx)) = dataByP(ii).kYActY;

        % off axis
        dataByP(ii).kXActYfin = kXfin(intersect(actYidx,dataByP(ii).idx));
        dataByP(ii).kYActXfin = kYfin(intersect(actXidx,dataByP(ii).idx));
        kXActY(intersect(actYidx,dataByP(ii).idx)) = dataByP(ii).kXActYfin;
        kYActX(intersect(actXidx,dataByP(ii).idx)) = dataByP(ii).kYActXfin;
      end
      kfinOnAxis = [kXActX; kYActY];
      kfinOffAxis = [kXActY; kYActX];
    end

    if bendAnalysis
      bendKrest = all_bendK (:,1); 
      bendErest = all_bendE (:,1); 
      bendKfin = all_bendK (:,end); 
      bendEfin = all_bendE (:,end); 
      bendKoverT = all_bendK';      
      bendEoverT = all_bendE';      
    end
  end

  if flags.SAVE_WORKSPACE || flags.OVERRIDE_SAFETY
    vars = who;
    toexclude = {'flags'};
    vars = vars(~ismember(vars, toexclude));
    save(strcat(dataFolder,'rawData.mat'), vars{:});
  end

  %%
  % =====================================================================
  %
  %
  %
  %                       D   I   S   P   L   A   Y
  %                       
  %                          Compression Tests
  %
  %
  % =====================================================================
  xTickLR = 'XTickLabelRotation';
  if flags.SHOW_BBOX
    % ----------------------------------------------------
    % Display bounding boxes changing over time for each trial
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); hold all;
    for ii=1:nFiles
      trialBBox_series = posBBox(:,:,ii);
      for jj=1:size(trialBBox_series,2) % time
        bbox = trialBBox_series(:,jj)';
        % rr(jj, ii) = rectangle('Position',bbox);
        pp(jj,ii) = patch(...
          [bbox(1) bbox(1)+bbox(3) bbox(1)+bbox(3) bbox(1)], ...
          [bbox(2) bbox(2) bbox(2)+bbox(4) bbox(2)+bbox(4)], 'b');
        set(pp(jj,ii),'FaceAlpha',0.05);
      end
    end
  end

  if compAnalysis
    close all;
    xLabelAngle = 90;
    % ----------------------------------------------------
    % strain over trial; segmented by primitive
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    % Along x-axis
    s1 = subplot(1,2,1); hold all;
    title('Bulk strain along x-axis'); ylabel('strain (%)');
    boxplot(100*strainXactX, groups); set(gca,xTickLR,xLabelAngle);

    % Along y-axis
    s2 = subplot(1,2,2); hold all;
    title('Bulk strain along y-axis'); ylabel('strain (%)');
    boxplot(100*strainYactY, groups); set(gca,xTickLR,xLabelAngle);

    limAdjust(figNum,s1,s2,'upper');
    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','strains');
      print(figNum,fname, '-dpdf', '-r300');
      print(figNum,fname, '-dpng', '-r300');
    end

    %
    % ----------------------------------------------------
    % Stiffness at rest; segmented by primitive
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    kXrest(kXrest > 1e5) = NaN; kYrest(kYrest > 1e5) = NaN;

    % Along x-axis
    s1 = subplot(1,2,1); hold all;
    title('Stiffness at rest along x-axis'); ylabel('Stiffness (N/m)');
    boxplot(1000*kXrest,groups); set(gca,xTickLR,xLabelAngle);

    % Along y-axis
    s2 = subplot(1,2,2); hold all;
    title('Stiffness at rest along y-axis'); ylabel('Stiffness (N/m)');
    boxplot(1000*kYrest, groups); set(gca,xTickLR,xLabelAngle);

    limAdjust(figNum,s1,s2, 'both');
    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','sitffness_at_rest');
      print(figNum,fname, '-dpdf', '-r300');
      print(figNum,fname, '-dpng', '-r300');
    end


    % ----------------------------------------------------
    % Stiffness in at end of trial; segmented by primitive
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    % Along actuation axis
    s1 = subplot(1,2,1); hold all;
    title('Stiffness along actuation axis'); ylabel('Stiffness (N/m)');
    boxplot(1000*kfinOnAxis, groups);
    set(gca,xTickLR,xLabelAngle);

    % Off actuation axis
    s2 = subplot(1,2,2); hold all;
    title('Stiffness along off-actuation axis'); ylabel('Stiffness (N/m)');
    boxplot(1000*kfinOffAxis, groups);
    set(gca,xTickLR,xLabelAngle);

    limAdjust(figNum,s1,s2,'both');
    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','sitffness_at_fin');
      print(figNum,fname, '-dpdf', '-r300');
      print(figNum,fname, '-dpng', '-r300');
    end
  end





  %%
  % =====================================================================
  %
  %
  %
  %                       D   I   S   P   L   A   Y
  %                       
  %                          Bending Tests
  %
  %
  % =====================================================================
  if bendAnalysis
    close all;
    xLabelAngle = 90;
    % ----------------------------------------------------
    % Stiffness at rest; segmented by primitive
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;
    s1 = subplot(1,2,1); hold all;
    title('Bending stiffness at rest'); ylabel('Stiffness (N/m)');
    bendKrest(bendKrest > 1e5) = NaN;
    bendKrest(bendKrest == 0) = NaN;
    boxplot(1000*bendKrest, groups);
    set(gca,xTickLR,xLabelAngle);

    s2 = subplot(1,2,2); hold all;
    title('E at rest'); ylabel('Modulus (MPa)');
    bendErest(bendErest > 1e5) = NaN;
    bendErest(bendErest == 0) = NaN;
    boxplot(bendErest, groups);
    set(gca,xTickLR,xLabelAngle);


    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bend_stiffness_at_rest');
      print(figNum,fname, '-dpdf', '-r300');
      print(figNum,fname, '-dpng', '-r300');
    end

    % ----------------------------------------------------
    % Stiffness at end of trial; segmented by primitive
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;
    s1 = subplot(1,2,1); hold all;
    title('Bending stiffness at trial end'); ylabel('Stiffness (N/m)');
    bendKfin(bendKfin > 1e5) = NaN;
    bendKfin(bendKfin == 0) = NaN;
    boxplot(1000*bendKfin, groups);
    set(gca,xTickLR,xLabelAngle);

    s2 = subplot(1,2,2); hold all;
    title('E at trial end'); ylabel('Modulus (MPa)');
    bendEfin(bendEfin > 1e5) = NaN;
    bendEfin(bendEfin == 0) = NaN;
    boxplot(bendEfin, groups);
    set(gca,xTickLR,xLabelAngle);


    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bend_stiffness_at_fin');
      print(figNum,fname, '-dpdf', '-r300');
      print(figNum,fname, '-dpng', '-r300');
    end


    % ----------------------------------------------------
    % All stiffness over trials; segmented by primitive
    % ----------------------------------------------------
    % figNum = figNum+1; figure(figNum); %hold all;

    % % Along actuation axis
    % s1 = subplot(1,2,1); hold all;
    % title('Bending stiffness along trial'); ylabel('Stiffness (N/m)');
    % bendKoverT(bendKoverT == 0) = NaN;
    % boxplot(1000*bendKoverT, groups);
    % set(gca,xTickLR,xLabelAngle);

    % s2 = subplot(1,2,2); hold all;
    % title('E along trial'); ylabel('Modulus (MPa)');
    % bendEoverT(bendEoverT == 0) = NaN;
    % boxplot(bendEoverT, groups);
    % set(gca,xTickLR,xLabelAngle);

    % if flags.PRINT_FIGS
    %   fname = strcat('publicationFigures/fig_','bend_stiffness');
    %   print(figNum,fname, '-dpdf', '-r300');
    %   print(figNum,fname, '-dpng', '-r300');
    % end

  end

  % keyboard
end


  % Some stats
  % rectstrainsY = 100*strainYactY(intersect(actYidx, rectIdx));
  % msqStrY = mean(rectstrainsY)
  % ssqStrY = std(rectstrainsY)
  % aa = [msqStrY+ssqStrY msqStrY-ssqStrY]

  % trianglestrainsX = 100*strainXactX(intersect(actXidx, triIdx));
  % mtriStrX = mean(trianglestrainsX)
  % striStrX = std(trianglestrainsX)
  % bb1 = [mtriStrX+striStrX mtriStrX-striStrX]

  % trianglestrainsY = 100*strainYactY(intersect(actYidx, triIdx));
  % mtriStrY = mean(trianglestrainsY)
  % striStrY = std(trianglestrainsY)
  % bb2 = [mtriStrY+striStrY mtriStrY-striStrY]

  % hyb3t2sstrainsY = 100*strainYactY(intersect(actYidx, hyb3t2sIdx));
  % mhyb1StrY = mean(hyb3t2sstrainsY)
  % shyb1StrY = std(hyb3t2sstrainsY)
  % cc = [mhyb1StrY+shyb1StrY mhyb1StrY-shyb1StrY]

  % tt = cc-bb2
  % mean(tt)
  % 0.5*(tt(2)-tt(1))

  % tt2 = aa-cc
  % mean(tt2)
  % 0.5*(tt2(2)-tt2(1))

  % hexstrains = 100*strainXactX(intersect(actXidx, hexIdx));
  % mhexStrX = mean(hexstrains)
  % shexStrX = std(hexstrains)
  % bb1 = [mhexStrX+shexStrX mhexStrX-shexStrX]

  % hexstrainsY = 100*strainYactY(intersect(actYidx, hexIdx));
  % mhexStrY = mean(hexstrainsY)
  % shexStrY = std(hexstrainsY)
  % bb2 = [mhexStrY+shexStrY mhexStrY-shexStrY]


  % hyb2t2hstrainsX = 100*strainXactX(intersect(actXidx, hyb2t2hIdx));
  % mhyb2StrX = mean(hyb2t2hstrainsX)
  % shyb2StrX = std(hyb2t2hstrainsX)
  % cc1 = [mhyb2StrX+shyb2StrX mhyb2StrX-shyb2StrX]

  % hyb2t2hstrainsY = 100*strainYactY(intersect(actYidx, hyb2t2hIdx));
  % mhyb2StrY = mean(hyb2t2hstrainsY)
  % shyb2StrY = std(hyb2t2hstrainsY)
  % cc2 = [mhyb2StrY+shyb2StrY mhyb2StrY-shyb2StrY]

  % tt = cc1-bb1
  % mean(tt)
  % 0.5*(tt(2)-tt(1))

  % tt = cc2-bb2
  % mean(tt)
  % 0.5*(tt(2)-tt(1))


  % 100*(max(strainXactX)-min(strainXactX))
  % 100*(max(strainYactY)-min(strainYactY))

  % 1000*(max(kXrest)-min(kXrest))
  % 1000*(max(kYrest)-min(kYrest))


function limAdjust (figNum,s1, s2, flag)
  figure(figNum);
  lim1 = get(s1,'YLim'); 
  lim2 = get(s2,'YLim');
  
  if strfind(flag,'both')
    lim = [min([0,lim1(1),lim2(1)]) max(lim1(2),lim2(2))]; 
  elseif strfind(flag,'upper')
    lim = [0 max(lim1(2),lim2(2))]; 
  elseif strfind(flag,'lower')
    lim = [min([0,lim1(1),lim2(1)]) 1000]; 
  end

  set(s1,'YLim', lim); 
  set(s2,'YLim', lim);

end



  % -----------------------------------------------------------------
  %
  %
  %
  % E n d   Of   F i l e
  %
  %
  %
  % -----------------------------------------------------------------
