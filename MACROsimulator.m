% Runs simulation given input network specifications
% Input file contains:
%   edgeSet (tuple of node numbers)
%   edgeTheta (radian measure of edge wrt positive x-axis)
%
% Active Cell Network Simulations
% Created 2016 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% (c) 2016 Yale University
% All rights reserved.
classdef MACROsimulator < hgsetget

  properties
    macro;
    macro1;
    cellProps;
    cellModel;
    fan;

    debugMode = 1;
    animate = 1;
    timestep = 0.5;
    runtime = 10;
    netInputs = struct();
    Ae;
    inputVoltage;
    inputCooling;
    constraints;

    targetPos;

    simTime = 0;

    simOut;
    debugData;

    plotDisplay = 1;
    outFolder = '';
    outFileName = '';

    showStartPos = 1;
    showTargetPos = 1;

  %  Each animation in non-debug mode saves the final result (+init network)
  %  Additional times included in array: figPrintTimes = [5, 10, 15, 20, 25];
    figPrintTimes = 1:50;

    % format for plot params
    % ----------------------------------------------------------------
    % sizingParam = [ nodeMarkerSize, nodeArmWidth, nodeLabelFontSize,
    %                 cellThickness];
    % colorParams = [ nodeColor; nodeTextColor; cellColor];

    stPosSizingParams = [2, 1, 0, 1];
    cPosSizingParams = [30, 2, 5, 1];
    trPosSizingParams = [2, 1, 0, 1];

    stPosColorParams =     [0.8,0.8,0.8;
                            0.8,0.8,0.8;
                            0.8,0.8,0.8;
                            0, 0, 0 ];    % everything gray
    cPosColorParams =     [ 0.0,0.5,0.5;
                            1.0,1.0,1.0;
                            0.0,0.25,0.25;
                            1, 1, 1 ];    % green and darker
    trPosColorParams =    [ 0.02,0.02,0.02;
                            0.02,0.02,0.02;
                            0.02,0.02,0.02;
                            0, 0, 0 ]; % everything black
  end
 
  methods

    function obj = MACROsimulator (...
      debugMode, outFolder, fileName, macroComponents, additionalInformation)
      addpath(genpath('simulatorHelperFiles'));

      obj.outFileName = fileName;
      obj.outFolder = outFolder;

      % CELL MODEL: cell model that accounts for physics of
      % T to K and L0 changes
      global cellProps
      if ~exist('cellProps','var')
        cellProps = ActiveCellProperties();
      end

      % High resolution Active Cell model
      % ----------------------------------------------
      % obj.cellModel = Model_ActiveCell_v4();

      % Low resolution Active Cell model
      % ----------------------------------------------
      obj.cellModel = Model_ActiveCell_v5();


      obj.fan = Model_Fan;

      % ==========================================================
      % SET UP SIMULATION PARAMETERS
      % ==========================================================
      obj.debugMode = debugMode;
      obj.animate = additionalInformation.animate;
      obj.targetPos = additionalInformation.targetPos;

      obj.netInputs = macroComponents.netInputs;
      obj.Ae = macroComponents.Ae;

      obj.showStartPos = additionalInformation.showStartPos;
      obj.showTargetPos = additionalInformation.showTargetPos;


      if isempty(fieldnames(obj.netInputs))
        obj.netInputs.nodeCount = 4;
        obj.netInputs.edgeSet = [1,2; 2,4; 1,3; 3,4]';
        obj.netInputs.edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';
        obj.netInputs.anchors = 1; obj.netInputs.fixedInX = [];
        obj.netInputs.fixedInY = 2; obj.netInputs.fixedInZ = [];
        disp('No network inputs provided. Using default network.');
      end

      % plot bigger if in debug mode
      % or if there are few nodes
      if obj.debugMode == 1 && obj.netInputs.nodeCount < 20
        obj.dispSizingParam(4) =10;
      end

      % nodeConstraints
      nodeConstraints = zeros(3,obj.netInputs.nodeCount);
      nodeConstraints(:,obj.netInputs.anchors) = 1;
      nodeConstraints(1,obj.netInputs.fixedInX) = 1;
      nodeConstraints(2,obj.netInputs.fixedInY) = 1;
      nodeConstraints(3,obj.netInputs.fixedInZ) = 1;

      if ~isfield(obj.netInputs,'zEdges');
        obj.netInputs.zEdges = [];
      end

      % virtual walls: generate from [p,n] representation of planes
      obj.constraints.wallConstraints = {};
      if isfield(obj.netInputs,'walls')
        obj.constraints.wallConstraints = netInputs.walls;
      end
      obj.constraints.nodeConstraints = nodeConstraints;

      % CREATE MACRO
      % -----------------------------------------------------------
      obj.inputVoltage = zeros(obj.netInputs.nodeCount,1);
      obj.macro = MACRO(...
        obj.netInputs.edgeSet, ...
        obj.netInputs.edgeTheta, ...
        obj.constraints, ...
        obj.netInputs.zEdges, ...
        obj.inputVoltage, ...
        obj.cellModel);

      % SETUP DISPLAY PARAMETERS
      % -----------------------------------------------------------
      if obj.plotDisplay
        outf = figure(1); clf; hold all;
        obj.macro.show([], obj.cPosSizingParams, obj.cPosColorParams);
        axis auto;
        bufferPct = 0.1;
        xLimOrig = get(gca, 'xlim');
        yLimOrig = get(gca, 'ylim');
        xlim([  xLimOrig(1)-max(5, bufferPct*xLimOrig(1))
          xLimOrig(2)+max(5, bufferPct*xLimOrig(2))]);
        ylim([  yLimOrig(1)-max(5, bufferPct*yLimOrig(1))
          yLimOrig(2)+max(5, bufferPct*yLimOrig(2))]);
      end
    end




    % ===========================================================
    %
    % RUN SIMULATION
    %
    % ===========================================================
    function run (obj, timestep, runtime, inputVoltage, inputCooling)
      disp('**Starting simulation**');
      if nargin < 2
        timestep = obj.timestep;
        runtime = obj.runtime;
      end
      if nargin < 3
        runtime = obj.runtime;
      end
      if ~isempty(inputVoltage)
        obj.inputVoltage = inputVoltage;
      end
      if ~isempty(inputCooling)
        obj.inputCooling = inputCooling;
      end

      tstart = obj.simTime; j=1;
      totalComputeTime = 0;
      obj.simOut.outNodePos = [];
      obj.simOut.simTime = [];
      obj.simOut.outNodePos(:,:,1) = obj.macro.nodePos';

      for t = tstart: timestep: tstart+runtime
        aa = cputime;

      % % Realistic fan: common power output for all cells
      % newRate = obj.fan.coolingPower(obj.timestep,obj.inputCooling);

        % Idealized cooling for each cell
        newRate = (obj.Ae*inputCooling).^2/obj.fan.R;
        set(obj.macro, 'coolingRate', newRate);
        % keyboard

          noteSafe = obj.macro.run(obj.timestep, obj.inputVoltage);
        obj.simOut.outNodePos(:,:,j+1) = obj.macro.nodePos';

        % keyboard
        if obj.animate
          cla; obj.plotMACROstatus();
        end

        fprintf(2, 'Time: %.2f, StepSize: %.2f\n', ...
          t, timestep);
        % pause(timestep/10);

        tt(j) = cputime-aa;
        totalComputeTime = totalComputeTime + tt(j);

        gh = abs(t-obj.figPrintTimes) <= obj.timestep*0.99;
        if ~obj.debugMode & any(gh)
          figTime = (obj.figPrintTimes(gh));
          figure(1);
          axis off;
%           keyboard
          printFig(1,obj.outFolder, 'Sim', ...
            strcat('t_',num2str(figTime),'s'));
%           keyboard
        end

        if noteSafe
          break
        end
        j=j+1;
      end
      disp('**Simulation segment complete**');
      obj.simTime = obj.simTime + runtime;

      % simulation results
      if ~obj.debugMode
        obj.simOut.simTime = obj.macro.netSimTime;
        obj.simOut.edgeSet = obj.macro.edgeSet;
        obj.debugData.computeTime = totalComputeTime;
        obj.debugData.simTimeProfile = obj.macro.netSimTime;
        obj.debugData.energyProfile = obj.macro.netEnergy;
      end

      obj.plotMACROstatus();
    end






    % ===========================================================
    %
    % UPDATE PLOTS
    %
    % ===========================================================
    function plotMACROstatus(obj, forcePlotDisplay)
      if ~exist('forcePlotDisplay', 'var')
        forcePlotDisplay = 0;
      end
      if obj.plotDisplay || forcePlotDisplay
        outf=figure(1);

        if obj.showStartPos
          obj.macro.show(obj.macro.startPos, ...
                         obj.stPosSizingParams, obj.stPosColorParams)
        end

        obj.macro.show(obj.macro.nodePos, ...
                       obj.cPosSizingParams, obj.cPosColorParams)

        if obj.showTargetPos
          obj.macro.show(obj.targetPos, ...
                         obj.trPosSizingParams, obj.trPosColorParams)
        end

        dispMinX = 0; dispMaxX = 0; dispMinY = 0; dispMaxY = 0;
        dispMinX = min(dispMinX, min(obj.macro.nodePos(1,:)) );
        dispMaxX = max(dispMaxX, max(obj.macro.nodePos(1,:)) );
        dispMinY = min(dispMinY, min(obj.macro.nodePos(2,:)) );
        dispMaxY = max(dispMaxY, max(obj.macro.nodePos(2,:)) );

        if obj.showStartPos
          dispMinX = min(dispMinX, min(obj.macro.startPos(1,:)) );
          dispMaxX = max(dispMaxX, max(obj.macro.startPos(1,:)) );
          dispMinY = min(dispMinY, min(obj.macro.startPos(2,:)) );
          dispMaxY = max(dispMaxY, max(obj.macro.startPos(2,:)) );
        end
        if obj.showTargetPos
          dispMinX = min(dispMinX, min(obj.targetPos(1,:)) );
          dispMaxX = max(dispMaxX, max(obj.targetPos(1,:)) );
          dispMinY = min(dispMinY, min(obj.targetPos(2,:)) );
          dispMaxY = max(dispMaxY, max(obj.targetPos(2,:)) );
        end

        xlim([dispMinX-10 dispMaxX+10]);
        ylim([dispMinY-10 dispMaxY+10]);

        set(gcf,'color','w'); % set figure background to white
      end
    end







    % ===========================================================
    %
    % DELETE SIMULATOR: CLOSE SIMULATION
    %
    % ===========================================================
    function delete(obj)
      % obj is always scalar
      disp('Destroying MACROsimulator...');
      if ~obj.debugMode
        % figure(1);
        % axis equal;
        % axis off;
        % printFig(1,obj.outFolder, obj.outFileName);
      end
      disp('MACROsimulator closed.');
    end

  end

end




