
clear all; close all; clc;

for ii = 1:10
  for jj = 1:10
    set(0,'defaultlinelinewidth',4); close all; clf; clc;
    clear simulator;
    global_path_setup
    repTest = 1;

    [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_truss (ii,jj);

    main_testMACROcontroller
  end
end


for ii = 1:10
  for jj = 1:10
    set(0,'defaultlinelinewidth',4); close all; clf; clc;
    clear simulator;
    global_path_setup
    repTest = 1;

    [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (ii,jj);

    main_testMACROcontroller
  end
end



% for ii = 10:10
%   for jj = 1:10
%     set(0,'defaultlinelinewidth',4); close all; clf; clc;
%     clear simulator;
%     global_path_setup
%     repTest = 1;

%     [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (ii,jj);

%     main_testMACROcontroller
%   end
% end
