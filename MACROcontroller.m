% --------------------------------------------------------------
%
% Starting and target node positions are spceificed by the input
% The 'starting' configuration need not be an equilibrium
% configuration.
% Each MACRO is assumed to be in some equlibrium configuration
% wth all the cells in cold martensitic state at 25 degC.
% Control inputs for the equlibrium configuration to deform to the
% 'start' positions are spceificed.
% Each subsequent 'target' deformations are achieved from this 'start'
% configuration using control inputs that are output from this function
%
% Control problem of finding the required node voltages to achieve
% the specified target node positions is reduced to the problem of
% finding the required cell power that results in the target cell
% stiffness.
%
% (can be positive: voltages applied, or negative: cooling applied)
% Output: stiffness
%
% --------------------------------------------------------------
classdef MACROcontroller < hgsetget

  properties

    cellProps;

    A; Ae;
    startPos;
    targetPos;
    startK;
    targetK;

    Kp = 1;
    Kd = 1;

    dt = 0.01;
    intermediateSteps = 1;

    configErr = 0;
    controlEps = 0.001;

    prevErr = 0;

  end

  methods

    function obj = MACROcontroller (...
      debugMode, outFolder, fileName, macroComponents, additionalInformation)
      addpath(genpath('controllerHelperFiles'));
      global cellProps
      obj.cellProps = cellProps;
      obj.A = macroComponents.A;
      obj.Ae = macroComponents.Ae;
      obj.startPos = additionalInformation.startPos;
      obj.targetPos = additionalInformation.targetPos;
      % obj.startK = additionalInformation.startK;
      % obj.targetK = additionalInformation.targetK;
      % if ~exist('startK', 'var') || isempty(startK) || ...
      %   ~exist('targetK', 'var') || isempty(targetK)
        
        % Convert provided node pos inputs to start and target cell stiffnesses
        
        % tmp = obj.cellProps.kM*ones(size(obj.Ae,1),1);
        % obj.startK = YtoKfullRes (obj.startPos, obj.Ae, tmp);
        % obj.targetK = YtoKfullRes (obj.targetPos, obj.Ae, tmp);

        obj.startK = YtoKlowRes (obj.startPos, obj.Ae);
        obj.targetK = YtoKlowRes (obj.targetPos, obj.Ae);

      % end
    end

    % Function to compute the control inputs (Voltages)
    % when given start and target positions
    % If positions are unchanged from before, they are NOT supplied
    %   This reduces the recomputation of stiffnesses from positions
    function [nodeInputs, coolingInputs] = updateController (...
      obj, startPos, targetPos)
      disp('Controller updating...');

      startK = obj.startK; targetK = obj.targetK;
      if isempty(startPos)
        startPos = obj.startPos; startK = obj.startK;
      end
      if isempty(targetPos)
        targetPos = obj.targetPos; targetK = obj.targetK;
      end

      tmp = obj.cellProps.kM*ones(size(obj.Ae,1),1);
      if norm(startPos - obj.startPos) > 0.001
        % startK = YtoKfullRes (startPos, obj.Ae, tmp);
        startK = YtoKlowRes (startPos, obj.Ae);
        obj.startK = startK; obj.startPos = startPos;
      end
      if norm(targetPos - obj.targetPos) > 0.001
        % targetK = YtoKfullRes (targetPos, obj.Ae, tmp);
        targetK = YtoKlowRes (targetPos, obj.Ae);
        obj.targetK = targetK; obj.targetPos = targetPos;
      end

      intermediateKs = linspaceNDim (...
        startK, targetK, obj.intermediateSteps);

      trK = intermediateKs(:,2);

      obj.configErr = startK - trK;
      
      % keyboard
      err = obj.configErr;

      T0 = 25*ones(size(startK)); % degC
      a = differentiate(...
        obj.cellProps.fMtoA, T0)/(obj.cellProps.m*obj.cellProps.c);
      PtoTarget = (1./(a*obj.dt)).*(obj.configErr);

      VtoTarget = PtoV(PtoTarget,obj.Ae);
      % this next line is shit code
      % q = VtoTarget(3); VtoTarget(3) = VtoTarget(4); VtoTarget(4) = q;
      % VtoTarget = VtoTarget - min(VtoTarget);

      global cellProps
      nodeVoltagePower = (obj.Ae*VtoTarget).^2/cellProps.cellR;

      PtoTargetCooling = PtoTarget - nodeVoltagePower;

      VcoolToTarget = PtoVCooling (PtoTargetCooling, obj.Ae);
      VcoolToTarget = VcoolToTarget - min(VcoolToTarget);
      nodeInputs = VtoTarget;
      coolingInputs = VcoolToTarget;
    end

  end

end

