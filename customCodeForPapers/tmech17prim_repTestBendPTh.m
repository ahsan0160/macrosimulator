
clear all; %close all; clc;
clear global;
set(0,'defaultlinelinewidth',4);

slendernessRatio = 10;

aDir = 1;

% handlesToTest = [1,2,3];
handlesToTest = [1:11];
% handlesToTest = [6];

reps = 1:1;

for uu=1:length(handlesToTest)
  for rep=1:length(reps)
    for kk = 1:size(aDir,1)
      close all; clf;
      clc; clear simulator; global_path_setup; repTest = 1;
      % actuationDir = aDir(kk,:);

      h0 = primitiveInfo.pHeight(handlesToTest(uu));
      yy = h0*2;
      xx = yy*slendernessRatio;

      handleBeingTested = primitiveInfo.pHandle{handlesToTest(uu)};
      txx = numPrimitivesForDist(handleBeingTested, xx, [1 0]);
      tyy = numPrimitivesForDist(handleBeingTested, yy, [0 1]);

      [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = handleBeingTested(txx,tyy);
      % input('')
      tmech17prim_testBendPTh
    end
  end
end


