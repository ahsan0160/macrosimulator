%
% This script sets up two MACRO meshes with a particular primitive type
% (a default or from external inputs)
% The samples are actuated by callbacks and NOT the MACROcontroller
% The script interrupts the simulation periodically and takes measurements
% of strokes and stiffnesses
% The data for the trial is stored in a datalog structure for post-processing
%
% (Used for IROS 2017 primitives paper)
%

if ~exist('repTest', 'var') || repTest == 0
  clear global;
  set(0,'defaultlinelinewidth',4); close all; clear all; clf; clc;

  global_path_setup

  % Generate default networks from primitive-generators
  % -----------------------------------------------------------------------
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (25,2);
  [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_tri (15,2);
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_tri (100,2);
  bndThreshold = 0.5;
  % bndThreshold = 1.25;

  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_hybtshs (3,1);
  % bndThreshold = 2;

  actuationDir = 1; % for positive curl (anti-clockwise)
end

global cellProps
global globalTime
global outFolder

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveTest';
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;
actuateCompX = 0; actuateCompY = 0; actuateBend = 1;
measure_axialK = 0; measure_bendingK = 1;

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------
if actuateBend
  controlAltCallback = @cb_bending;
  dataLog.callback = controlAltCallback;
  if ~exist('actuationDir','var')
      actuationDir = 1;
  end
  dt = 0.5; tFinal = 10;
end

if actuateCompX || actuateCompY
  dt = 1; tFinal = 10;
  controlAltCallback = @cb_compressionAxial;
  dataLog.callback = controlAltCallback;
  if ~exist('actuationDir','var')
    if actuateCompX
      actuationDir = [1 0];
    else
      actuationDir = [0 1];
    end
  end
end


nm = strcat('pTst_',nm);
if isa(controlAltCallback,'function_handle')
  trPos = 0*stPos;
end

if actuationDir(1) == 1
  nm = strcat(nm, 'pTh');
else
  nm = strcat(nm, 'nTh');
end

% seed the random number generator
seed = ceil(rand()*1000);
rng(seed);

% For bending stiffness testing
bndThreshold = 1;
leftNodes = pointSetEdgeNodes(stPos,eSet,'left',0.01);
anch = leftNodes;
fY = []; fX = [];

if actuateBend
  % Make all edges that connect the top-layer nodes with the
  % "non-top layer" (not the same as bottom layer) into
  % high-impedance edges
  topNodes = pointSetEdgeNodes(stPos,eSet,'top',0.01);
  nonTopNodes = setdiff(1:size(stPos,2),topNodes);

  idx1 = ismember(eSet(1,:), topNodes); % edge start in topNodes
  idx2 = ismember(eSet(2,:), nonTopNodes); % edge end in nonTopNodes
  idx3 = ismember(eSet(1,:), nonTopNodes); % edge start in nonTopNodes
  idx4 = ismember(eSet(2,:), topNodes); % edge end in topNodes
  zEdges = [];
  % connections between top and non-top nodes are z-edges
  zEdges = [zEdges eSet(:,idx1 & idx2)];
  zEdges = [zEdges eSet(:,idx3 & idx4)];

  % connections within non-top nodes are z-edges
  zEdges = [zEdges eSet(:,idx3 & idx2)];
end

% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
[name,edgeSet,edgeTheta] = deal(nm, eSet, eTheta);
[startPos,targetPos] = deal(stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (anch, fX, fY, fZ);

globalTime = tic;
outFolder = nm; outFolder = strcat(num2str(globalTime),'_', nm);

bndThreshold = cellProps.nodeToNodeDist*1.25;
helper_runControllerSimulator

dataLog.randSeed = seed;
globalRunTime = toc(globalTime)

dataLog.globalRunTime = globalRunTime;
% record macroComponents into dataLog
for fn = fieldnames(macroComponents)'
  if strcmpi(fn,'netInputs')
    continue;
  end
  dataLog.(fn{1}) = macroComponents.(fn{1});
end

% save raw data file first before adding measurements
dataLogOutputFile = strcat(outFolder,'_dataLog.mat');
save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file without measurements\n');

% --------------------------------------------------------------------------
% Measurement done on recorded data after the fact
% --------------------------------------------------------------------------
if exist('measure_bendingK','var') && measure_bendingK == 1
  for ttt=1:length(dataLog.simTime)
    % if ttt == 1 %%|| ttt == 5 || ttt == 10
    if mod(ttt-1,2) == 0
        bendDir = [1]; % apply force in pos theta direction, -1 for neg
        [dataLog.bendKinPTh(:,ttt), dataLog.bendE(:,ttt)] = ...
          measure_bendingK_EQ(simulator.macro, bendDir, bndThreshold, ...
            num2str(ttt));
        fprintf(1, 'Itr: %.3f, Bending K: %.3f, E: %.3f\n',...
          itrT, dataLog.bendKinPTh(:,ttt), dataLog.bendE(:,ttt));
      end
    end
end
fprintf(1, '>>all measurements completed\n');
% --------------------------------------------------------------------------
% Full Data storage
% --------------------------------------------------------------------------
save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file with measurements\n');