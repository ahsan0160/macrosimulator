
clear all; %close all; clc;
clear global;
set(0,'defaultlinelinewidth',4);

reps = 1;

% % refinement test: linear stroke x control
% sr17_refinementTests(0,1,1,1, [],[])

% % refinement test: linear stroke x max
% sr17_refinementTests(0,1,[],1, [],[])

% % refinement test: linear stroke x min
% sr17_refinementTests(0,1,[],0, [],[])


% % refinement test: linear stroke y control
% sr17_refinementTests(0,2,1,1, [],[])

% % refinement test: linear stroke y max
% sr17_refinementTests(0,2,[],1, [],[])

% % refinement test: linear stroke x min
% sr17_refinementTests(0,2,[],0, [],[])




% % refinement test: linear stiffness x control
% sr17_refinementTests(0,5,1,[0.90 0.96], [],[])

% % refinement test: linear stiffness x max
% sr17_refinementTests(0,5,[],[0.90 0.96], [],[])

% % refinement test: linear stiffness x min
% sr17_refinementTests(0,5,[],[0.01 0.07], [],[])



% % refinement test: linear stiffness y control
% sr17_refinementTests(0,6,1,[0.90 0.96], [],1)

% % refinement test: linear stiffness y max
% sr17_refinementTests(0,6,[],[0.90 0.96], [],[])

% % refinement test: linear stiffness x min
% sr17_refinementTests(0,6,[],[0.01 0.05], [],[])





% % refinement test: bending stiffness control
% sr17_refinementTests(0,7,1,[0.90 0.91], [],[])

% % refinement test: bending stiffness max
% sr17_refinementTests(0,7,[],[0.90 0.91], [],[])

% % refinement test: bending stiffness min
% sr17_refinementTests(0,7,[],[0.01 0.1], [],[])


% refinement test: bending stroke control
sr17_refinementTests(0,8,1,[0.9 0.99], [],[])

% refinement test: bending stroke max
sr17_refinementTests(0,8,[],[0.9 0.99], [],[])

% refinement test: bending stroke min
sr17_refinementTests(0,8,[],[0.0 0.01], [],[])









% for uu=1:length(handlesToTest)
%   for rep=1:length(reps)
%     for kk = 1:size(aDir,1)
%       close all; clf;
%       clc; clear simulator; global_path_setup; repTest = 1;
%       % actuationDir = aDir(kk,:);

%       h0 = primitiveInfo.pHeight(handlesToTest(uu));
%       yy = h0*2;
%       xx = yy*slendernessRatio;

%       handleBeingTested = primitiveInfo.pHandle{handlesToTest(uu)};
%       txx = numPrimitivesForDist(handleBeingTested, xx, [1 0]);
%       tyy = numPrimitivesForDist(handleBeingTested, yy, [0 1]);

%       [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = handleBeingTested(txx,tyy);
%       % input('')
%       tmech17prim_testBendPTh
%     end
%   end
% end


