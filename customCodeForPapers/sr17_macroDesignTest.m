%
% This script 
%
% (Used for Science Robotics 2017 MACRO design paper)
%

% if ~exist('repTest', 'var') || repTest == 0
%   clear global;
%   set(0,'defaultlinelinewidth',4); close all; clear all; clf; clc;
%   global_path_setup

%   % --------------------------------------
%   % Default values to run a trial
%   % --------------------------------------
%   controlAltCallback = @cb_bending;
%   meshSizeX = 10; meshSizeY = 2;
%   actuationDir = 1; % for positive curl (anti-clockwise)
%   dt = 1; tFinal = 10;

%   % meshPrimitive = @primitive_rect;      % WORKS
%   % meshPrimitive = @primitive_hex;       % WORKS
%   % meshPrimitive = @primitive_tri;       % WORKS
%   % meshPrimitive = @primitive_hyb3t2s;   % WORKS
%   % meshPrimitive = @primitive_hyb2t2h;   % WORKS
%   % meshPrimitive = @primitive_hybs2o;    % MIGHT NEED MORE CURRENT
%   % meshPrimitive = @primitive_hyb4th;    % NEEDS A TON MORE CURRENT
%   % meshPrimitive = @primitive_hybtshs;   % NEEDS A TON MORE CURRENT
%   % meshPrimitive = @primitive_hybshd;    % NEEDS A TON MORE CURRENT
%   % meshPrimitive = @primitive_hybt2d;    % WORKS
%   meshPrimitive = @primitive_hyb2tsts;  % NEEDS A TON MORE CURRENT

% end

% global cellProps; global globalTime; global outFolder;

% % output print locations
% % --------------------------------------------------------------------------
% outFolder = 'primitiveTest'; detailedFigs = 0;

% % common input parameters
% % --------------------------------------------------------------------------
% cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
% debugMode = 0;
% recordForMeasure = 1; measureTimes = linspace(1,100);

% % --------------------------------------------------------------------------
% % setup of test parameters
% % --------------------------------------------------------------------------
% seed = ceil(rand()*1000); rng(seed); % seed the random number generator

% edgeMetric = @meshPrimitiveEdgeNodes;
% [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ]= meshPrimitive(meshSizeX,meshSizeY);
% trPos = 0*stPos;
% [anch, fX, fY, fZ, zEdges] = constraintsForCallbacks(...
%   meshPrimitive, controlAltCallback, stPos, eSet, eTheta);

% nm = strcat('pTst_',nm); 
% if isequal(controlAltCallback, @cb_bending) 
%   if actuationDir(1) == 1
%     nm = strcat(nm, 'pTh');
%   else
%     nm = strcat(nm, 'nTh');
%   end
% elseif isequal(controlAltCallback, @cb_compressionAxial)
%   if actuationDir(1) == 1
%     nm = strcat(nm, 'actX');
%   else
%     nm = strcat(nm, 'actY');
%   end   
% end

% % --------------------------------------------------------------------------
% % Run test
% % --------------------------------------------------------------------------
% [name,edgeSet,edgeTheta] = deal(nm, eSet, eTheta);
% [startPos,targetPos] = deal(stPos, trPos);
% [anchors,fixedInX,fixedInY,fixedInZ] = deal (anch, fX, fY, fZ);

% globalTime = tic;
% outFolder = nm; outFolder = strcat(num2str(globalTime),'_', nm);

% % ----- MAIN CALL TO SIMULATOR ----- 
% helper_runControllerSimulator

% globalRunTime = toc(globalTime)

% % --------------------------------------------------------------------------
% % Data storage
% % --------------------------------------------------------------------------
% dataLog.randSeed = seed;
% dataLog.globalRunTime = globalRunTime;
% % record macroComponents into dataLog
% for fn = fieldnames(macroComponents)'
%   if strcmpi(fn,'netInputs')
%     continue;
%   end
%   dataLog.(fn{1}) = macroComponents.(fn{1});
% end

% dataLogOutputFile = strcat(outFolder,'_dataLog.mat');

% save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
% save(strcat('allOutputsDataFolder/pTst/', dataLogOutputFile), 'dataLog');
% fprintf(1, '>>dataLog saved to file without measurements\n');

