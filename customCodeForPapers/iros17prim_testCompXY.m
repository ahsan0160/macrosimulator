%
% This script sets up two MACRO meshes with a particular primitive type
% (a default or from external inputs)
% The samples are actuated by callbacks and NOT the MACROcontroller
% The script interrupts the simulation periodically and takes measurements
% of strokes and stiffnesses
% The data for the trial is stored in a datalog structure for post-processing
%
% (Used for IROS 2017 primitives paper)
%

if ~exist('repTest', 'var') || repTest == 0
  clear global
  set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;
  global_path_setup

  % Generate default networks from primitive-generators
  % -----------------------------------------------------------------------
  [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (5,2);
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (1,1);
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_hybtshs (1,1);

    actuationDir = [1 0];
    % actuationDir = [0 1];
end

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveTest';
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
global globalTime
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------
controlAltCallback = @cb_compressionAxial;
if ~exist('actuationDir','var')
  actuationDir = [1 0];
end

measure_axialK = 1;
measure_bendingK = 0;

nm = strcat('pTst_',nm);
if isa(controlAltCallback,'function_handle')
  trPos = 0*stPos;
end

if actuationDir(1) == 1
  nm = strcat(nm, 'actX');
else
  nm = strcat(nm, 'actY');
end

% seed the random number generator
seed = ceil(rand()*1000);
rng(seed);

dt = 1; tFinal = 10;

% For compressive stiffness testing
bottomNodes = pointSetEdgeNodes(stPos,eSet,'bottom',0.01);
fY = bottomNodes; % bottom boundary fixed in Y

leftNodes = pointSetEdgeNodes(stPos,eSet,'left',0.01);
fX = leftNodes; % left boundary fixed in X

% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
[name,edgeSet,edgeTheta] = deal(nm, eSet, eTheta);
[startPos,targetPos] = deal(stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (anch, fX, fY, fZ);

globalTime = tic;
outFolder = nm; outFolder = strcat(num2str(globalTime),'_', nm);

helper_runControllerSimulator

globalRunTime = toc(globalTime)

dataLog.randSeed = seed;
dataLog.globalRunTime = globalRunTime;
% record macroComponents into dataLog
for fn = fieldnames(macroComponents)'
  if strcmpi(fn,'netInputs')
    continue;
  end
  dataLog.(fn{1}) = macroComponents.(fn{1});
end

% save raw data file first before adding measurements
dataLogOutputFile = strcat(outFolder,'_dataLog.mat');
save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file without measurements\n');

% --------------------------------------------------------------------------
% Measurement done on recorded data after the fact
% --------------------------------------------------------------------------
if exist('measure_axialK','var') && measure_axialK == 1
  for ttt=1:length(dataLog.simTime)
    if ttt == 1 || ttt == tFinal
      [dataLog.compKinX(:,ttt), dataLog.delXfromForceX(:,:,ttt)] ...
        = measure_compressiveK_EQ(simulator.macro,[1 0]);
      [dataLog.compKinY(:,ttt), dataLog.delXfromForceY(:,:,ttt)] ...
        = measure_compressiveK_EQ(simulator.macro,[0 1]);
      fprintf(1, 'Itr: %.3f, Stiffness X: %.3f, Stiffness Y: %.3f\n',...
        itrT, dataLog.compKinX(:,ttt), dataLog.compKinY(:,ttt));
    end
  end
end
fprintf(1, '>>all measurements completed\n');

% --------------------------------------------------------------------------
% Full Data storage
% --------------------------------------------------------------------------
save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file with measurements\n');