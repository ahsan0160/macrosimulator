%
% This script sets up a MACRO mesh (a default or from external inputs)
% and runs the MACRO controller to simulate the shape matching performance
% The data for the entire trial is saved in a data log file for post-processing
%
% (Used for RA-L 2017 with IROS 2017 option)
%

  set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;
  global_path_setup


% -----------------------------------------------------------------------
% For controller testing
% Generate default networks from primitive-generators
% Test configurations are set from geometric deformations of the "startPos"
% -----------------------------------------------------------------------
[nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_tri (2,1);
% keyboard

% output print locations
% --------------------------------------------------------------------------
outFolder = 'ral17_controllerTest';
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
global globalTime
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;


% --------------------------------------------------------------------------
% Data logger
% --------------------------------------------------------------------------
dataLog = struct();
 % time
dataLog.simTime = [];
 % pos sets
dataLog.startPos = []; dataLog.targetPos = []; dataLog.pos = [];
 % stiffness sets
dataLog.startConfig = []; dataLog.targetConfig = []; dataLog.config = [];
 % error sets
dataLog.errPos = []; dataLog.errConfig = [];
dataLog.errLen = []; dataLog.errHull = [];

dataLog.simInputs = [];

% --------------------------------------------------------------------------
% Network specific inputs
% --------------------------------------------------------------------------
controlAltCallback = [];

nm = strcat('cTst_',nm);
trPos = stPos;

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------

% seed the random number generator
% seed = ceil(rand()*1000);
seed = 337;
rng(seed);
dataLog.randSeed = seed;

% generate random number from uniform distr between a, b
randab = @(a,b) a+(b-a)*rand();

% creating a random scaling factor for compression tests
randScaling = @() randab(0.75,1);

% random angle for bending test
randBendTheta = @() randab(0*pi/180, 10*pi/180);

contractMidCellBy = 0.8;
theta1 = acos( (2-contractMidCellBy^2)/2 ); % between cell 1-2 and cell 1-3
trNode3x = cos(theta1);
trNode3y = sin(theta1);

trPos = stPos;
trPos (:,3) = [trNode3x, trNode3y];
trPos (:,4) = [1+trNode3x, trNode3y];
% keyboard
% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
anch=[1];
fY = [2];

  dt = .5;
  
[name,edgeSet,edgeTheta,startPos,targetPos] = deal(...
  nm, eSet, eTheta, stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (...
  anch, fX, fY, fZ);

dataLog.edgeSet = eSet; dataLog.edgeTheta = eTheta; dataLog.anchors = anch;
dataLog.fixedInX = fX; dataLog.fixedInY = fY; dataLog.fixedInZ = fZ;

globalTime = tic;
outFolder = nm;
outFolder = strcat(num2str(globalTime),'_', nm);
% keyboard
helper_runControllerSimulator
globalRunTime = toc(globalTime)

dataLog.globalRunTime = globalRunTime;
dataLog.macroComponents = macroComponents;

% --------------------------------------------------------------------------
% Data storage
% --------------------------------------------------------------------------
dataLogOutputFile = strcat(outFolder,'_dataLog.mat');

save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/cTst/', dataLogOutputFile), ...
  'dataLog');
