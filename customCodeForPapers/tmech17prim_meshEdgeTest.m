clear global;
set(0,'defaultlinelinewidth',4); clf; clear all; clf; clc;
global_path_setup

meshPrimitive = @primitive_rect;
meshPrimitive = @primitive_hex;
meshPrimitive = @primitive_tri;
meshPrimitive = @primitive_hyb3t2s;
% meshPrimitive = @primitive_hyb2t2h;
meshPrimitive = @primitive_hybs2o;
meshPrimitive = @primitive_hyb4th;
% meshPrimitive = @primitive_hybtshs;
% meshPrimitive = @primitive_hybshd;
% meshPrimitive = @primitive_hybt2d;
% meshPrimitive = @primitive_hyb2tsts;


meshSizeX = 6; meshSizeY = 3;
% meshSizeX = 5; meshSizeY = 7;
% meshSizeX = 10; meshSizeY = 10;
[nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = ...
  meshPrimitive(meshSizeX,meshSizeY);

figure(1); showShape(stPos, 'r', 1, eSet);
for ii=1:5
  meshPrimitiveEdgeNodes (meshPrimitive, stPos, eSet, 'left');
  meshPrimitiveEdgeNodes (meshPrimitive, stPos, eSet, 'bottom');
  meshPrimitiveEdgeNodes (meshPrimitive, stPos, eSet, 'right');
  meshPrimitiveEdgeNodes (meshPrimitive, stPos, eSet, 'top');
end

