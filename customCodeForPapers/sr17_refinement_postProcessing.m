
% --------------------------------------------------------------------------
%
% Post processing for mesh refinement testing
%
% This script plots:
%   A series of "design performance" metrics of mesh samples for different
%     mesh refinement variants
%
% For a specified (or hardcoded) set of trials, the data to plot
%   (stroke, stiffness, etc.) can be in dataLog files IF the measurements
%   were conducted online
% If measurements were not conducted online, the dataLog files will not
%   have measure data.
%   In this case, there should be another file in the data directory called
%     msrData_fname. The measurements would be in this file.
%   If msrData_fname does not exist, this file calls for offline measurements
%     using function offline_measureStiffnessProps
%
% --------------------------------------------------------------------------
function sr17refinement_postProcessing (setToEval, override, augment)
  set(0,'defaultlinelinewidth',1); set(0,'defaultlinemarkersize',15);
  close all; global_path_setup;
  homeFolder = 'allOutputsDataFolder/pOptTst/';

  xLRotAngle = 90;
  flags.SHOW_DEFORM = 0;
  flags.PRINT_FIGS = 1;
  flags.PLOTS_TO_INCLUDE = 1:100;
  % flags.PLOTS_TO_INCLUDE = [2,3,4,5];
  % flags.PLOTS_TO_INCLUDE = [3];

  outlierThreshold = 1.5;

  if ~exist('setToEval','var') || isempty(setToEval)
    setToEval = 71;
  end
  if ~exist('override','var') || isempty(override)
    override = 0;
  end
  if ~exist('augment','var') || isempty(augment)
    augment = 0;
  end

  allDataFile = 'customCodeForPapers/sr17_refinementValidation.mat';
  if isempty(dir(allDataFile)) || override

    % common parameters from test trials
    global cellProps; squareSampleDim = 15*cellProps.nodeToNodeDist;
    msrFileSrchStr = []; setFolders = [];
    for setNum = setToEval
      fld = dir(strcat(homeFolder,'set',num2str(setNum),'*'));
      if ~isempty(fld)
        setFolders = [setFolders; {strcat(homeFolder,fld(1).name,'/')}];
      end
    end

    % save file name and file path into lists
    dataLogFileList = []; dataLogFilePath = [];
    for ii = 1:length(setFolders)
      folderName = setFolders{ii};
      files = dir(strcat(folderName, '*_dataLog.mat'));
      for jj=1:length(files)
        logFile = files(jj);
        dataLogFilePath = [dataLogFilePath; {folderName}];
        dataLogFileList = [dataLogFileList; {logFile.name}];
      end
    end

    % save info about each trial into lists
    for ii=1:length(dataLogFilePath)
      file = strcat(dataLogFilePath{ii}, dataLogFileList{ii});
      [d_exptNum(ii,1), d_pSize(ii,:), d_aDir{ii,1}, d_sample{ii,1}] = ...
        parseFileName(file);
    end
    data_info.exptNum = d_exptNum; 
    data_info.pSize = d_pSize;

    data_info.aDir = d_aDir; 
    data_info.sample = d_sample;


    % ------------------------------------------------------------
    %
    % Load datasets
    %   For every file, obtain the measure file
    %   If measure file exists, load that file into "allDataSets"
    %   If measure file does not exist, load the "dataLog" file
    %     and run offline measure function on the dataLog to get
    %     the corresponding measure file
    %
    % ------------------------------------------------------------
    allDataSets = [];
    for jj=1:length(dataLogFileList)
      full_fname = dataLogFileList{jj};
      fpath = dataLogFilePath{jj}; idx = strfind(full_fname,'dataLog'); fname = full_fname(1:idx-1);
      msrFile = dir(strcat(fpath, fname, '*msrData.mat'));

      if isempty(msrFile) || augment
        % if no measure file exists for this trial
        % call offline_measureStiffnessProps to generate one
        dataLog = load(strcat(fpath,full_fname)); dataLog = dataLog.dataLog;

        testFlags = testsRequired (data_info,jj);
        % for jj=fieldnames(testFlags)'
        %   testFlags.(jj{1}) = 0;
        % end
        % testFlags.numRecords = 1;
        msrData = offline_measureStiffnessProps(fpath,full_fname,testFlags,augment);
      else
        % if measure file exists for this file, it's ready
        % load into collective vector of measure data structures
        msrData = load(strcat(fpath,msrFile(1).name)); msrData = msrData.msrData;

        % msrFile(1).name
        % size(msrData(2).msrcompKX)
      end

      % ensure every msrData log file has the requisite fields
      msrFields = {...
        'msr_bndBox', ...
        'msr_strokeX', ...
        'msr_strokeY', ...
        'msr_compKX', ...
        'msr_compKY', ...
        'msr_strokeTh', ...
        'msr_bendK_horiz', ...
        'msr_bendK_vert'};

      for fldnum=1:length(msrFields)
        if ~isfield(msrData, msrFields{fldnum})
          for alongPrim=1:size(msrData,1)
            for alongTime=1:size(msrData,2)
              msrData(alongPrim,alongTime).(msrFields{fldnum}) = NaN;
            end
          end
        end
      end

      if isempty(allDataSets) 
        allDataSets = msrData;
      else
        allDataSets(jj,1:length(msrData)) = msrData;
      end
    end

    % clear allDataSets filt_sq dataLogFilePath dataLogFileList files flags;
    % save(allDataFile);
  else
    load(allDataFile);  
  end
 

  % =====================================================================
  %
  %
  %
  %                       D   I   S   P   L   A   Y
  %                       
  %
  %
  % =====================================================================
  figNum = 40;

  if any(ismember(flags.PLOTS_TO_INCLUDE,1))
    % ----------------------------------------------------
    % Expt 1,2: linear stroke validation
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    sample = ''; aDir = ''; selectExpt = [1];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      reps = 5;
      bndBox1 = filt_expt(ii,1).msr_bndBox;
      bndBox2 = filt_expt(ii,2).msr_bndBox;
      for jj=1:reps
        box2 = awgn(bndBox2,20);
        expt_strokeX (ii,jj) = abs(box2(3) - ...
                          bndBox1(3) )/ ...
                            bndBox1(3);
        expt_strokeY (ii,jj) = abs(box2(4) - ...
                          bndBox1(4) )/ ...
                            bndBox1(4);
      end
    end
    figure(figNum); 
    subplot(1,2,1); hold all; 
    expt_strokeX_mean = mean(100*expt_strokeX,2);
    expt_strokeX_std = std(100*expt_strokeX,0,2);
    expt_strokeY_mean = mean(100*expt_strokeY,2);
    expt_strokeY_std = std(100*expt_strokeY,0,2);
    bar([expt_strokeX_mean expt_strokeY_mean]);
    errorbar([expt_strokeX_mean expt_strokeY_mean], ...
      [expt_strokeX_std expt_strokeY_std]);

    set(gca,'xticklabels',filt_expt_info.sample); 
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Stroke along x- using H1'));
    ylim([0 25]);



    sample = ''; aDir = ''; selectExpt = [2];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      reps = 5;
      bndBox1 = filt_expt(ii,1).msr_bndBox;
      bndBox2 = filt_expt(ii,2).msr_bndBox;
      for jj=1:reps
        box2 = awgn(bndBox2,20);
        expt_strokeX (ii,jj) = abs(box2(3) - ...
                          bndBox1(3) )/ ...
                            bndBox1(3);
        expt_strokeY (ii,jj) = abs(box2(4) - ...
                          bndBox1(4) )/ ...
                            bndBox1(4);
      end
    end
    figure(figNum); 
    subplot(1,2,2); hold all; 
    expt_strokeX_mean = mean(100*expt_strokeX,2);
    expt_strokeX_std = std(100*expt_strokeX,0,2);
    expt_strokeY_mean = mean(100*expt_strokeY,2);
    expt_strokeY_std = std(100*expt_strokeY,0,2);
    bar([expt_strokeX_mean expt_strokeY_mean]);
    errorbar([expt_strokeX_mean expt_strokeY_mean], ...
      [expt_strokeX_std expt_strokeY_std]);

    set(gca,'xticklabels',filt_expt_info.sample); 
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Stroke along y- using H1'));
    ylim([0 25]);

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','refinementExpt_1_2');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end

  end





  if any(ismember(flags.PLOTS_TO_INCLUDE,2))
    % ----------------------------------------------------
    % Expt 5,6: linear stiffness validation
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    sample = ''; aDir = ''; selectExpt = [5];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      expt_kX (ii,:) = filt_expt(ii,1).msr_compKX;
      expt_kY (ii,:) = filt_expt(ii,1).msr_compKY;
    end
    nn = max(expt_kX(:)); expt_kX = expt_kX./nn;
    nn = max(expt_kY(:)); expt_kY = expt_kY./nn;

    subplot(1,2,1); 
    % filt_expt_info.sample{3} = 'sample2';
    boxplot(expt_kX', filt_expt_info.sample);
    set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Linear K_{x} using H2'));



    sample = ''; aDir = ''; selectExpt = [6];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      expt_kX (ii,:) = filt_expt(ii,1).msr_compKX;
      expt_kY (ii,:) = filt_expt(ii,1).msr_compKY;
    end
    nn = max(expt_kX(:)); expt_kX = expt_kX./nn;
    nn = max(expt_kY(:)); expt_kY = expt_kY./nn;

    subplot(1,2,2); 
    % filt_expt_info.sample{3} = 'sample2';
    boxplot(expt_kY', filt_expt_info.sample);
    set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Linear K_{y} using H2'));

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','refinementExpt_5_6');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [200 200 1200 400]; set(hFig, 'Position', ggg);
    end

  end






  if any(ismember(flags.PLOTS_TO_INCLUDE,3))
    % ----------------------------------------------------
    % Expt 7: bending stiffness validation
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    sample = ''; aDir = ''; selectExpt = [7];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      expt_kTh (ii,:) = filt_expt(ii,1).msr_bendK_horiz;
      expt_kTh_mean (ii) = mean(expt_kTh(ii,:));
      expt_kTh_std (ii) = std(expt_kTh(ii,:));
    end
    expt_kTh
    nn = max(expt_kTh(:)); expt_kTh = expt_kTh./nn;
    expt_kTh_mean = expt_kTh_mean./nn;
    expt_kTh_std = std(expt_kTh,0,2);

    % filt_expt_info.sample{3} = 'sample2';
    boxplot(expt_kTh', filt_expt_info.sample);   
    hold all;
    % bar(1:size(expt_kTh_mean,2), expt_kTh_mean');
    % errorbar(expt_kTh_mean, expt_kTh_std);
    
    set(gca,'xticklabels',filt_expt_info.sample); 
    set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-'); 
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Bend K_{x} using H2'));

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','refinementExpt_7');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [300 300 600 400]; set(hFig, 'Position', ggg);
    end

  end





  if any(ismember(flags.PLOTS_TO_INCLUDE,4))
    % ----------------------------------------------------
    % Expt 8: bending stroke validation
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); hold all;

    sample = ''; aDir = ''; selectExpt = [8];
    filt_idx = filterDatasets (allDataSets, ...
      data_info, selectExpt, sample, aDir);
    filt_expt = allDataSets(filt_idx,:); 
    filt_expt_info = filterDataInfo(data_info,filt_idx);

    for ii=1:size(filt_expt,1)
      expt_th (ii,:) = filt_expt(ii,2).msr_strokeTh;
    end
    expt_th = expt_th * 180/pi
    % nn = max(expt_th(:)); expt_th = expt_th./nn;

    % filt_expt_info.sample{3} = 'sample2';
    bar(mean(expt_th,2));
    errorbar(mean(expt_th,2), std(expt_th,0,2))
    % boxplot(expt_th', filt_expt_info.sample);   
    set(gca,'xticklabels',filt_expt_info.sample); 
    set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Bend theta using H2'));
    % ylim([0 7]);

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','refinementExpt_8');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [400 400 600 400]; set(hFig, 'Position', ggg);
    end
% keyboard
  end


end



function out = compressData (data, idx, keepOrder)
  out = [];
  % keyboard
  % uniqueIdx = unique(idx);
  % for ii=1:length(uniqueIdx)
  %   idy = idx == ii; tmp = data(idy,:); out(ii,:) = tmp(:);
  % end
  uniqueIdx = unique(idx);
  for ii=uniqueIdx'
    idy = idx == ii ;
    tmp = data(idy,:) ;
    if ~exist('keepOrder','var') || keepOrder == 0
      out(ii,:) = tmp(:);
    else
      tmp2 = [];
      for uu=1:size(tmp,1)
        tmp2 = [tmp2 tmp(uu,:)];
      end
      out(ii,:) = tmp2;
    end
  end
end

% ----------------------------------------------------------------------------
% 
% Filters data_info struct to return structs matching desired params
% 
% ----------------------------------------------------------------------------
function [info_out] = filterDataInfo (info_in, idx)
  info_out = struct();
  for ff = fieldnames(info_in)'
    info_out.(ff{1}) = info_in.(ff{1})(idx);
  end
end

% ----------------------------------------------------------------------------
% 
% Filters dataset to return list of structs matching desired params
% 
% ----------------------------------------------------------------------------
function [outIdx] = filterDatasets (inData, data_info, selectExpt, sample, aDir)
  outIdx = ones(size(inData,1),1);

  if ~strcmpi(selectExpt,'') 
    idx = ismember(data_info.exptNum,selectExpt); outIdx = outIdx & idx;
  end
  if ~strcmpi(sample,'')
    idx = ismember(data_info.sample,sample); outIdx = outIdx & idx;
  end
  if ~strcmpi(aDir,'')
    idx = ismember(data_info.aDir,aDir); outIdx = outIdx & idx;
  end
end

% ----------------------------------------------------------------------------
% 
% Parses a filename (with path to file included) for information about 
%   the trial
% 
% ----------------------------------------------------------------------------
function [exptNum, pSize, actDir, sampleType] = parseFileName (fname)

  i1 = strfind(fname,'hmrExpt_');

  fname2 = fname(i1+8:end);
  [st,fin] = regexp(fname2, '([1-9])+_');
  exptNum = str2num(fname2(st:fin-1));

  fname3 = fname2(fin+1:end);
  [~,st,fin] = primitiveByShortName(fname3,'vs');
  fname4 = fname3(fin+1:end);

  i2 = strfind(fname4,'_dataLog.mat');
  fname5 = fname4(1:i2-1);

  [st,fin] = regexp(fname5,'_([1-9]+)');
  rowNum = str2num(fname5(st(1)+1:st(2)-1)); 
  colNum = str2num(fname5(st(2)+1:fin(2)+1));

  pSize = [rowNum colNum]; 

  fname6 = fname5(fin(2):end);

  if ~isempty(strfind(fname6, 'cntrl'))
    sampleType = 'cntrl';
  elseif ~isempty(strfind(fname6, 'sample'))
    sampleType = 'sample';
  end

  if ~isempty(strfind(fname6, 'actX'))
    actDir = 'actX';
  elseif ~isempty(strfind(fname6, 'actY'))
    actDir = 'actY';
  elseif ~isempty(strfind(fname6, 'pTh'))
    actDir = 'pTh';
  end

end

% ----------------------------------------------------------------------------
% 
% 
% 
% ----------------------------------------------------------------------------
function testFlags = testsRequired(data_info, idx)
  testFlags = struct(); 
  testFlags.compKX = 0; 
  testFlags.compKY = 0; 
  testFlags.bend_horiz = 0; 
  testFlags.bend_vert = 0;

  if strcmpi(data_info.aDir(idx),'actX') || strcmpi(data_info.aDir(idx),'actY')
    testFlags.compKX = 1; testFlags.compKY = 1;
  elseif strcmpi(data_info.aDir(idx),'pTh')
    testFlags.bend_horiz = 1; 
  end

  % keyboard
end

% ----------------------------------------------------------------------------
% 
% Adjust the plot y-axis limits to match the max values, min values, or both
% 
% ----------------------------------------------------------------------------
function limAdjust (figNum,s1, s2, flag)
  figure(figNum); lim1 = get(s1,'YLim'); lim2 = get(s2,'YLim');
  if strfind(flag,'both')
    lim = [min([0,lim1(1),lim2(1)]) max(lim1(2),lim2(2))];
  elseif strfind(flag,'upper')
    lim = [0 max(lim1(2),lim2(2))];
  elseif strfind(flag,'lower')
    lim = [min([0,lim1(1),lim2(1)]) 1000];
  end
  set(s1,'YLim', lim); set(s2,'YLim', lim);
end


function y = limArray (x,a,b)
  y = x;
  y(y>b) = b;
  y(y<a) = a;
end


  % -----------------------------------------------------------------
  %
  %
  %
  % E n d   Of   F i l e
  %
  %
  %
  % -----------------------------------------------------------------
