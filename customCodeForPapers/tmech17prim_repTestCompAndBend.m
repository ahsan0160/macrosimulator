% 
% This file collects the data for TMECH 17 primitives paper 
% extended from IROS 17 submitted on primitives)
% 
% This script uses the tmech17prim_testCompAndBend file to run the simulation
%   tmech17prim_testCompAndBend requires inputs about:
%     mesh size (X,Y)
%     mesh primitive
%     callback to use instead of controller
%     actuation direction for callback
%     run time
%     time step
%     recording time
%   Note NO measurements on the meshes are conducted by the tmech17prim file
% 
% (Used for TMECH 2017 primitives paper)
% 

clear all; %close all; clc;
clear global;
set(0,'defaultlinelinewidth',4);

% -----------------------------------------------
% samples and tests to run
% -----------------------------------------------
squareCompIdx = 1;
beamCompIdx = 2;
squareBendIdx = 1;
beamBendIdx = 2;

% compTests = [1, 1]; bendTests = [0, 1];
% compTests = [0, 1]; bendTests = [0, 0];
compTests = [0, 0]; bendTests = [0, 1];


% beamWidthMult = 2; % of the primitive height
% beamWidthMult = 1.5; % of the primitive height
beamWidthMult = 1; % of the primitive height
% slendernessRatio = 3;
% slendernessRatio = 10;
slendernessRatio = 15;
bendActDir = 1;

% squareSampleDim = 2;
squareSampleDim = 15;
compActDir = [1 0; 0 1];

% handlesToTest = [2];
handlesToTest = [1:11];
% handlesToTest = [9];
% handlesToTest = [4,6,7,8,9,10]; % remedy for beam comp tests
% handlesToTest = [1,4,6,7,8,9,10]; % remedy for beam bend tests

repeatEntireDataCollection = 1;

for rep=repeatEntireDataCollection

  % ------------------------------------
  % COMPRESSION ACTUATION
  % ------------------------------------
  for cmp = 1:length(compTests)
    for primIdx=handlesToTest
      for kk=1:size(compActDir,1)
        % ---------------------------------
        % Clear some of the variables
        close all; clf;
        clc; clear simulator; clear dataLog;
        global_path_setup; repTest = 1;
        % ---------------------------------

        if compTests(cmp) == 0
          continue
        end

        if cmp == squareCompIdx
          % ------------------------------------
          % SAMPLE Alpha, square sample 
          % ------------------------------------
          [xx, yy] = deal(squareSampleDim);
        elseif cmp == beamCompIdx
          % ------------------------------------
          % SAMPLE Beta, horizontal beam 
          % ------------------------------------
          h0 = primitiveInfo.pHeight(primIdx); 
          yy = h0*beamWidthMult;
          xx = yy*slendernessRatio;          
        end

        dt = 1; tFinal = 10;
        actuationDir = compActDir(kk,:);
        controlAltCallback = @cb_compressionAxial;
        meshPrimitive = primitiveInfo.pHandle{primIdx};
        meshSizeX = numPrimitivesForDist(meshPrimitive, xx, [1 0]);
        meshSizeY = numPrimitivesForDist(meshPrimitive, yy, [0 1]);
        tmech17prim_testCompAndBend % run trial

      end
    end
  end



  % ------------------------------------
  % BENDING ACTUATION
  % ------------------------------------
  for cmp = 1:length(bendTests)
    for primIdx=handlesToTest
      % ---------------------------------
      % Clear some of the variables
      close all; clf;
      clc; clear simulator; clear dataLog;
      global_path_setup; repTest = 1;
      % ---------------------------------

      if bendTests(cmp) == 0
        continue;
      end

      if cmp == squareBendIdx 
        % ------------------------------------
        % SAMPLE Alpha, square sample 
        % ------------------------------------
        [xx, yy] = deal(squareSampleDim);
      elseif cmp == beamBendIdx 
        % ------------------------------------
        % SAMPLE Beta, horizontal beam 
        % ------------------------------------
        h0 = primitiveInfo.pHeight(primIdx); 
        yy = h0*beamWidthMult;
        xx = yy*slendernessRatio;          
      end

      % dt = 1; tFinal = 20;
      dt = 1; tFinal = 10;
      actuationDir = bendActDir;
      controlAltCallback = @cb_bending;
      meshPrimitive = primitiveInfo.pHandle{primIdx};
      meshSizeX = numPrimitivesForDist(meshPrimitive, xx, [1 0]);
      meshSizeY = numPrimitivesForDist(meshPrimitive, yy, [0 1]);
      tmech17prim_testCompAndBend % run trial

    end
  end
end

close all;