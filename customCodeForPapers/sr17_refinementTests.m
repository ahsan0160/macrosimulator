%
% This script tests the T primitive by removing cells selectively and 
% simulating a small 3x3 array of "hexagons"
%
% (Used for Science Robotics 2017 MACRO design paper)
%
function sr17_refinementTests (repTest, testFlag, controlExpt, override_refinementPct, override_refinementDir, override_tFinal)

if ~exist('repTest', 'var') || repTest == 0
  clear global;
  set(0,'defaultlinelinewidth',4); close all; clf; clc;
  % clear all; 
  global_path_setup
end
% --------------------------------------
% Default values to run a trial
% --------------------------------------
% controlAltCallback = @cb_bending;
controlAltCallback = @cb_compressionAxial;
meshSizeX = 20; meshSizeY = 10;
actuationDir = 0; % for positive curl (anti-clockwise)
dt = 1; tFinal = 10;

meshPrimitive = @primitive_tri;      

global cellProps; global globalTime; global outFolder;

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveOptTest'; detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;
recordForMeasure = 1; measureTimes = linspace(1,100);

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------
seed = ceil(rand()*1000); rng(seed); % seed the random number generator

if ~exist('testFlag','var')
  testFlag = 1;
end
if ~exist('controlExpt','var') || isempty(controlExpt)
  controlExpt = 0;
end


if testFlag == 1
  % ---------------------------------------------------------------------
  % mesh refinement for strain x
  % ---------------------------------------------------------------------
  refinementDir = [1,0];
  refinementPct = 1;
  refinementFcn = @heuristic_mesh_refinement_linStroke;
  controlAltCallback = @cb_compressionAxial;
  actuationDir = [1 0];
  dt = 1; tFinal = 20;

elseif testFlag == 2
  % ---------------------------------------------------------------------
  % mesh refinement for strain y
  % ---------------------------------------------------------------------
  refinementDir = [0,1];
  refinementPct = 1;
  refinementFcn = @heuristic_mesh_refinement_linStroke;
  controlAltCallback = @cb_compressionAxial;
  actuationDir = [0 1];
  dt = 1; tFinal = 20;

elseif testFlag == 3
  % % ---------------------------------------------------------------------
  % % mesh refinement for strain -x+y
  % % ---------------------------------------------------------------------
  % refinementDir = [-1,1];
  % refinementPct = 1;
  % refinementFcn = @heuristic_mesh_refinement_linStroke;
  % controlAltCallback = @cb_compressionAxial;
  % actuationDir = [0 1];
  % dt = 1; tFinal = 20;

elseif testFlag == 4
  % % ---------------------------------------------------------------------
  % % mesh refinement for strain x+y
  % % ---------------------------------------------------------------------
  % refinementDir = [1,1];
  % refinementPct = 1;
  % refinementFcn = @heuristic_mesh_refinement_linStroke;
  % controlAltCallback = @cb_compressionAxial;
  % actuationDir = [0 1];
  % dt = 1; tFinal = 20;

elseif testFlag == 5
  % ---------------------------------------------------------------------
  % mesh refinement for lin stiffness x
  % ---------------------------------------------------------------------
  refinementDir = [1,0];
  refinementPct = [0.7 0.9];  
  refinementFcn = @heuristic_mesh_refinement_linK;
  controlAltCallback = @cb_compressionAxial;
  actuationDir = [0 1];
  dt = 1; tFinal = 1;

elseif testFlag == 6
  % ---------------------------------------------------------------------
  % mesh refinement for lin stiffness y
  % ---------------------------------------------------------------------
  refinementDir = [0,1];
  refinementPct = [0.7 0.9];  
  refinementFcn = @heuristic_mesh_refinement_linK;
  controlAltCallback = @cb_compressionAxial;
  actuationDir = [0 1];
  dt = 1; tFinal = 1;

elseif testFlag == 7
  % mesh refinement for bend stiffness pTh
  % ---------------------------------------------------------------------
  refinementDir = [0,1];
  refinementPct = [0.7 0.9];  
  refinementFcn = @heuristic_mesh_refinement_bendK;
  controlAltCallback = @cb_bending;
  meshSizeX = 20; meshSizeY = 4;
  actuationDir = [1];
  dt = 1; tFinal = 1;

elseif testFlag == 8
  % mesh refinement for bend stroke pTh
  % ---------------------------------------------------------------------
  refinementDir = [0,1];
  refinementPct = [0.7 0.9];  
  refinementFcn = @heuristic_mesh_refinement_bendStroke;
  controlAltCallback = @cb_bending;
  meshSizeX = 20; meshSizeY = 4;
  actuationDir = [1];
  dt = 1; tFinal = 10;

end


edgeMetric = @meshPrimitiveEdgeNodes;
[nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ]= meshPrimitive(meshSizeX,meshSizeY);
trPos = 0*stPos;


% [A,Ae] = genAdjMats(eSet,size(stPos,2));
% showNet(stPos,A,'r');
[A,Ae] = genAdjMats(eSet,size(stPos,2));
showNet(stPos,A,'r',1,':');

nm = strcat('hmrExpt_',num2str(testFlag),'_',nm,'_'); 
if controlExpt
  nm = strcat(nm, 'cntrl_');
else
  nm = strcat(nm, 'sample_');
end

if isequal(controlAltCallback, @cb_bending) 
  if actuationDir(1) == 1
    nm = strcat(nm, 'pTh');
  else
    nm = strcat(nm, 'nTh');
  end
elseif isequal(controlAltCallback, @cb_compressionAxial)
  if actuationDir(1) == 1
    nm = strcat(nm, 'actX');
  else
    nm = strcat(nm, 'actY');
  end   
end





if exist('override_tFinal','var') && ~isempty(override_tFinal)
  tFinal = override_tFinal;
end
if ~controlExpt
  if exist('override_refinementPct','var') && ~isempty(override_refinementPct)
    refinementPct = override_refinementPct;
  end
  if exist('override_refinementDir','var') && ~isempty(override_refinementDir)
    refinementDir = override_refinementDir;
  end
  [stPos,eSet,eTheta, expectedProperty] = refinementFcn(...
      stPos,eSet,eTheta,refinementDir, refinementPct);
  trPos = stPos*0;
else
  expectedProperty = struct();
  expectedProperty.name = '';
end


dataLog.testFlag = testFlag;
dataLog.refinementDir = refinementDir;
dataLog.refinementPct = refinementPct;
dataLog.expectedProperty = expectedProperty;

[A,Ae] = genAdjMats(eSet,size(stPos,2));
showNet(stPos,A,'b',4);
title(strcat('Expected: ', expectedProperty.name));
% [A,Ae] = genAdjMats(eSet,size(stPos,2));
% showNet(stPos,A,'b');
% return
% keyboard


[anch, fX, fY, fZ, zEdges] = constraintsForCallbacks(...
  meshPrimitive, controlAltCallback, stPos, eSet, eTheta);


% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
[name,edgeSet,edgeTheta] = deal(nm, eSet, eTheta);
[startPos,targetPos] = deal(stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (anch, fX, fY, fZ);

globalTime = tic;
outFolder = nm; outFolder = strcat(num2str(globalTime),'_', nm);

% ----- MAIN CALL TO SIMULATOR ----- 

helper_runControllerSimulator

globalRunTime = toc(globalTime)

% --------------------------------------------------------------------------
% Data storage
% --------------------------------------------------------------------------
dataLog.randSeed = seed;
dataLog.globalRunTime = globalRunTime;
% record macroComponents into dataLog
for fn = fieldnames(macroComponents)'
  if strcmpi(fn,'netInputs')
    continue;
  end
  dataLog.(fn{1}) = macroComponents.(fn{1});
end

dataLogOutputFile = strcat(outFolder,'_dataLog.mat');

save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pOptTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file without measurements\n');

