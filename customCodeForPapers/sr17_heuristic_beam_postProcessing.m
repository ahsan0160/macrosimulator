
% --------------------------------------------------------------------------
%
% Post processing for mesh refinement testing
%
% This script plots:
%   A series of "design performance" metrics of mesh samples for different
%     mesh refinement variants
%
% For a specified (or hardcoded) set of trials, the data to plot
%   (stroke, stiffness, etc.) can be in dataLog files IF the measurements
%   were conducted online
% If measurements were not conducted online, the dataLog files will not
%   have measure data.
%   In this case, there should be another file in the data directory called
%     msrData_fname. The measurements would be in this file.
%   If msrData_fname does not exist, this file calls for offline measurements
%     using function offline_measureStiffnessProps
%
% --------------------------------------------------------------------------
function sr17refinement_postProcessing (setToEval, override, augment, fileListIdx)
  set(0,'defaultlinelinewidth',1); set(0,'defaultlinemarkersize',15);
  close all; global_path_setup;
  homeFolder = 'allOutputsDataFolder/pOptTst/';

  xLRotAngle = 90;
  flags.SHOW_DEFORM = 0;
  flags.PRINT_FIGS = 0;
  flags.PLOTS_TO_INCLUDE = [1,2,3,5,6];
  % flags.PLOTS_TO_INCLUDE = [2,3,4,5];
  % flags.PLOTS_TO_INCLUDE = [2,3,5];
  % flags.PLOTS_TO_INCLUDE = [1,2,3,4,5,6];
  % flags.PLOTS_TO_INCLUDE = [3];
  % flags.PLOTS_TO_INCLUDE = [1,4];
  % flags.PLOTS_TO_INCLUDE = [2,3];

  outlierThreshold = 1.5;

  if ~exist('setToEval','var') || isempty(setToEval)
    setToEval = 21;
  end
  if ~exist('override','var') || isempty(override)
    override = 0;
  end
  if ~exist('augment','var') || isempty(augment)
    augment = 0;
  end
  if ~exist('fileListIdx')
    fileListIdx = 1:1e3;
  end

  allDataFile = 'customCodeForPapers/sr17_heuristic_beam_datasets.mat';
  if isempty(dir(allDataFile)) || override

    % common parameters from test trials
    global cellProps; squareSampleDim = 15*cellProps.nodeToNodeDist;
    msrFileSrchStr = []; setFolders = [];
    for setNum = setToEval
      fld = dir(strcat(homeFolder,'set',num2str(setNum),'*'));
      if ~isempty(fld)
        setFolders = [setFolders; {strcat(homeFolder,fld(1).name,'/')}];
      end
    end

    % save file name and file path into lists
    dataLogFileList = []; dataLogFilePath = [];
    for ii = 1:length(setFolders)
      folderName = setFolders{ii};
      files = dir(strcat(folderName, '*_dataLog.mat'));
      for jj=1:length(files)
        logFile = files(jj);
        dataLogFilePath = [dataLogFilePath; {folderName}];
        dataLogFileList = [dataLogFileList; {logFile.name}];
      end
    end

    % save info about each trial into lists
    for ii=1:length(dataLogFilePath)
      file = strcat(dataLogFilePath{ii}, dataLogFileList{ii});
      [d_variant{ii,1},d_pSize(ii,:),d_aDir{ii,1},d_sample{ii,1}] = ...
        parseFileName(file);
    end
    data_info.variant = d_variant; data_info.pSize = d_pSize;
    data_info.aDir = d_aDir; data_info.sample = d_sample;

    % ------------------------------------------------------------
    %
    % Load datasets
    %   For every file, obtain the measure file
    %   If measure file exists, load that file into "allDataSets"
    %   If measure file does not exist, load the "dataLog" file
    %     and run offline measure function on the dataLog to get
    %     the corresponding measure file
    %
    % ------------------------------------------------------------
    allDataSets = [];
    for jj=1:length(dataLogFileList)
      if ~any(fileListIdx == str2num(data_info.variant{jj}))
        continue;
      end
      full_fname = dataLogFileList{jj};
      fpath = dataLogFilePath{jj}; idx = strfind(full_fname,'dataLog'); fname = full_fname(1:idx-1);
      msrFile = dir(strcat(fpath, fname, '*msrData.mat'));

      if isempty(msrFile) || augment
        % if no measure file exists for this trial
        % call offline_measureStiffnessProps to generate one
        dataLog = load(strcat(fpath,full_fname)); dataLog = dataLog.dataLog;

        testFlags = testsRequired (data_info,jj);
        msrData = offline_measureStiffnessProps(fpath,full_fname,testFlags,augment);
      else
        % if measure file exists for this file, it's ready
        % load into collective vector of measure data structures
        msrData = load(strcat(fpath,msrFile(1).name)); msrData = msrData.msrData;
      end

      % ensure every msrData log file has the requisite fields
      msrFields = {...
        'msr_bndBox', ...
        'msr_strokeX', ...
        'msr_strokeY', ...
        'msr_compKX', ...
        'msr_compKY', ...
        'msr_strokeTh', ...
        'msr_bendK_horiz', ...
        'msr_bendK_vert'};

      for fldnum=1:length(msrFields)
        if ~isfield(msrData, msrFields{fldnum})
          for alongPrim=1:size(msrData,1)
            for alongTime=1:size(msrData,2)
              msrData(alongPrim,alongTime).(msrFields{fldnum}) = NaN;
            end
          end
        end
      end

      if isempty(allDataSets) 
        allDataSets = msrData;
      else
        allDataSets(jj,1:length(msrData)) = msrData;
      end
    end

    % parse out the appropriate data
    % ------------------------------------
    outlierThreshold = 1.25;

    % BEAM SAMPLE, ANGULAR STROKE 
    sample = 'beam'; aDir = ''; selectVar = {};
    filt_idx = filterDatasets (allDataSets, data_info, selectVar, sample, aDir);
    filt_bm = allDataSets(filt_idx,:); filt_bm_info = filterDataInfo(data_info,filt_idx);

    varNames = cellfun(@(x) strcat('Var: ',x), filt_bm_info.variant, 'UniformOutput',false);
    data_y1_raw =[]; data_y2_raw =[];
    daya_y3_raw = [];
    for ii=1:size(filt_bm,1)
      % keyboard
      aa = filt_bm(ii,1).msr_bendK_horiz; data_y1_raw(ii,:) = aa;
      aa = filt_bm(ii,2).msr_strokeTh; data_y2_raw(ii,:) = aa;
      aa = filt_bm(ii,1).macro; 
      aa2 = filt_bm(ii,2).macro; 
      data_y3_raw(ii).startPos = aa.startPos;
      data_y3_raw(ii).nodePos = aa2.nodePos;
      data_y3_raw(ii).A = aa.A;
    end
    bm_kTh = data_y1_raw; 
    bm_strokeTh = data_y2_raw;
    bm_stPos = data_y3_raw;

    % figure(99);
    % tmp1 = bm_kTh;
    % for ind = 1:length(varNames)
    %   ll = bm_kTh(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh(ind,:) = ll;
    %   ll = bm_kY(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kY(ind,:) = ll;
    % end
    % tmp2 = bm_kTh;
    % plot(tmp1,'r.')
    % hold on
    % plot(tmp2,'b.')
    % keyboard

    normalizer = max(median(bm_kTh,2,'omitnan')); bm_kTh = bm_kTh./normalizer;

    bm_kTh_std = std(bm_kTh,0,2,'omitnan');

    clear allDataSets filt_bm dataLogFilePath dataLogFileList files flags;
    save(allDataFile);
  else
    load(allDataFile);  
  end
 

  % =====================================================================
  %
  %
  %
  %                       D   I   S   P   L   A   Y
  %                       
  %
  %
  % =====================================================================
  figNum = 40;

  if any(ismember(flags.PLOTS_TO_INCLUDE,1))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % angular stroke; segmented by variants
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    % plot data
    % ------------------------------------
    bm_kTh
    figure(figNum); 
    % subplot(1,2,1);  hold all; 
    boxplot(bm_kTh',varNames);
    ylim([-0.25 1.5]);
    ylabel('K'); set(gca,'xticklabels',varNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Relative K_x vs. primitives \n(unpowered mesh)'));

    % bottom left corner pos (x,y), wid, height (in px)

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bendRelStiff_byVariant');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end

  end


  if any(ismember(flags.PLOTS_TO_INCLUDE,2))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % angular stroke; segmented by variants
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    numVars = 2^9;
    variations = zeros(numVars,12);
    variations(:,1:9) = de2bi((0:2^9-1)');
    variations(:,10) = variations(:,7);
    variations(:,11) = variations(:,8);
    variations(:,12) = variations(:,9);

    % have the index for each variant saved in the file name
    % should look up the actual variant in the variations array to find 
    % actual variant information, such as num of cells removed, etc.
    actualVariantIndices = cellfun(@str2num,data_info.variant);
    numCellsRemoved = sum(variations(actualVariantIndices,:),2);

    % plot data
    % ------------------------------------
    reordered_bm_kTh = reshape(bm_kTh, length(bm_kTh(:)), 1);
    reordered_actualVariantIndices = ...
      repmat(actualVariantIndices,size(bm_kTh,2),1);
    reordered_numCellsRemoved = repmat(numCellsRemoved, size(bm_kTh,2), 1);

    figure(figNum); 
    boxplot(reordered_bm_kTh, reordered_numCellsRemoved,'sym','r.');
    set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
    xlabel('Number of cells removed from refinement unit');
    ylabel(sprintf('K_{th}'));
    title(sprintf('Stiffness K_{th} change by mesh degradation'));


    bendkRelationsFile = 'primitiveHelperFiles/meshRefine_bendKrelations.mat';
    if ~exist(bendkRelationsFile,'file')
      save(bendkRelationsFile, ...
        'reordered_bm_kTh',...
        'reordered_numCellsRemoved',...
        'reordered_actualVariantIndices',...
        'variations');
    end

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bendRelStiff_byMeshRefineLevel');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end

  end



  if any(ismember(flags.PLOTS_TO_INCLUDE,3))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % angular stroke; segmented by variants
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    numVars = 2^9;
    variations = zeros(numVars,12);
    variations(:,1:9) = de2bi((0:2^9-1)');
    variations(:,10) = variations(:,7);
    variations(:,11) = variations(:,8);
    variations(:,12) = variations(:,9);

    % have the index for each variant saved in the file name
    % should look up the actual variant in the variations array to find 
    % actual variant information, such as num of cells removed, etc.
    actualVariantIndices = cellfun(@str2num,data_info.variant);
    numCellsRemoved = sum(variations(actualVariantIndices,:),2);

    % plot data
    % ------------------------------------
    reordered_bm_strokeTh = reshape(bm_strokeTh, length(bm_strokeTh(:)), 1);
    reordered_actualVariantIndices = ...
      repmat(actualVariantIndices,size(bm_strokeTh,2),1);
    reordered_numCellsRemoved = repmat(numCellsRemoved, size(bm_strokeTh,2), 1);

    figure(figNum); 
    boxplot(reordered_bm_strokeTh, reordered_numCellsRemoved,'sym','r.');
    set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
    xlabel('Number of cells removed from refinement unit');
    ylabel(sprintf('Stroke_{th}'));
    title(sprintf('Stroke_{th} change by mesh degradation'));

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bendStroke_byMeshRefineLevel');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end


    maxStroke = max(bm_strokeTh);
    maxStrokeIdx = find(bm_strokeTh == maxStroke);
    figNum = figNum+1; figure(figNum); %hold all;
    subplot(1,2,1);
    hold all;
    % showNet()
    showNet(bm_stPos(maxStrokeIdx).startPos, bm_stPos(maxStrokeIdx).A, 'r');
    showNet(bm_stPos(maxStrokeIdx).nodePos, bm_stPos(maxStrokeIdx).A, 'b');

    minStroke = min(bm_strokeTh);
    minStrokeIdx = find(bm_strokeTh == minStroke);
    subplot(1,2,2);
    hold all;
    % showNet()
    showNet(bm_stPos(minStrokeIdx).startPos, bm_stPos(minStrokeIdx).A, 'r');
    showNet(bm_stPos(minStrokeIdx).nodePos, bm_stPos(minStrokeIdx).A, 'b');

    bendStrokeRelationsFile = ...
      'primitiveHelperFiles/meshRefine_bendStrokeRelations.mat';
    if ~exist(bendStrokeRelationsFile,'file')
      save(bendStrokeRelationsFile, ...
        'reordered_bm_strokeTh',...
        'reordered_numCellsRemoved',...
        'reordered_actualVariantIndices',...
        'variations');
    end

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','bendStroke_maxAndMin_shapes');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end


    % keyboard

  end



end



function out = compressData (data, idx, keepOrder)
  out = [];
  % keyboard
  % uniqueIdx = unique(idx);
  % for ii=1:length(uniqueIdx)
  %   idy = idx == ii; tmp = data(idy,:); out(ii,:) = tmp(:);
  % end
  uniqueIdx = unique(idx);
  for ii=uniqueIdx'
    idy = idx == ii ;
    tmp = data(idy,:) ;
    if ~exist('keepOrder','var') || keepOrder == 0
      out(ii,:) = tmp(:);
    else
      tmp2 = [];
      for uu=1:size(tmp,1)
        tmp2 = [tmp2 tmp(uu,:)];
      end
      out(ii,:) = tmp2;
    end
  end
end

% ----------------------------------------------------------------------------
% 
% Filters data_info struct to return structs matching desired params
% 
% ----------------------------------------------------------------------------
function [info_out] = filterDataInfo (info_in, idx)
  info_out = struct();
  for ff = fieldnames(info_in)'
    info_out.(ff{1}) = info_in.(ff{1})(idx);
  end
end

% ----------------------------------------------------------------------------
% 
% Filters dataset to return list of structs matching desired params
% 
% ----------------------------------------------------------------------------
function [outIdx] = filterDatasets (inData, data_info, selectVar, sample, aDir)
  outIdx = ones(size(inData,1),1);

  if ~strcmpi(selectVar,'') 
    idx = ismember(data_info.variant,selectVar); outIdx = outIdx & idx;
  end
  if ~strcmpi(sample,'')
    % keyboard
    idx = ismember(data_info.sample,sample); outIdx = outIdx & idx;
  end
  if ~strcmpi(aDir,'')
    idx = ismember(data_info.aDir,aDir); outIdx = outIdx & idx;
  end
end

% ----------------------------------------------------------------------------
% 
% Parses a filename (with path to file included) for information about 
%   the trial
% 
% ----------------------------------------------------------------------------
function [variant, pSize, actDir, sample] = parseFileName (fname)
  [~, matchSt, matchFin] = primitiveByShortName(fname,'vs');

  tmp1str = fname(1:matchFin+1);
  if ~isempty( strfind(tmp1str,'square') )
    sample = 'square';
  elseif ~isempty( strfind(tmp1str,'beam') )
    sample = 'beam';
  end
  tmp2str = fname(matchFin+1:end); 
  chopOffEnd = strfind(tmp2str,'_dataLog.mat')-1; 
  tmp2str = tmp2str(1:chopOffEnd);

  [st,fin] = regexp(tmp2str,'_([1-9]+)');
  rowNum = str2num(tmp2str(st(1)+1: fin(1))); 
  colNum = str2num(tmp2str(st(2)+1: fin(2)));
  pSize = [rowNum colNum]; actDir = tmp2str(fin(2)+1: end-5);

  [st, fin] = regexp(tmp2str,'var([0-9]+)');
  % variant = strcat('Var: ', (tmp2str(st(1)+3: fin(1)))); 
  variant = tmp2str(st(1)+3: fin(1)); 
end

% ----------------------------------------------------------------------------
% 
% 
% 
% ----------------------------------------------------------------------------
function testFlags = testsRequired(data_info, idx)
  testFlags = struct(); testFlags.compKX = 0; testFlags.compKY = 0; testFlags.bend_horiz = 0; testFlags.bend_vert = 0;
  if strcmpi(data_info.sample(idx),'square')
    testFlags.compKX = 1; testFlags.compKY = 1;
  elseif strcmpi(data_info.sample(idx),'beam')
    testFlags.bend_horiz = 1; 
    % testFlags.bend_vert = 1;
    if strcmpi(data_info.aDir(idx),'actX')||strcmpi(data_info.aDir(idx),'actY')
      testFlags.compKX = 1; testFlags.compKY = 1;
    end
  end
end

% ----------------------------------------------------------------------------
% 
% Adjust the plot y-axis limits to match the max values, min values, or both
% 
% ----------------------------------------------------------------------------
function limAdjust (figNum,s1, s2, flag)
  figure(figNum); lim1 = get(s1,'YLim'); lim2 = get(s2,'YLim');
  if strfind(flag,'both')
    lim = [min([0,lim1(1),lim2(1)]) max(lim1(2),lim2(2))];
  elseif strfind(flag,'upper')
    lim = [0 max(lim1(2),lim2(2))];
  elseif strfind(flag,'lower')
    lim = [min([0,lim1(1),lim2(1)]) 1000];
  end
  set(s1,'YLim', lim); set(s2,'YLim', lim);
end


function y = limArray (x,a,b)
  y = x;
  y(y>b) = b;
  y(y<a) = a;
end


  % -----------------------------------------------------------------
  %
  %
  %
  % E n d   Of   F i l e
  %
  %
  %
  % -----------------------------------------------------------------
