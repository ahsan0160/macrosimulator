
clear all; %close all; clc;
set(0,'defaultlinelinewidth',4);

aa = 15;
xrange = aa:aa;
yrange = aa:aa;

aDir = [1 0; 0 1];

handlesToTest = [9,10,11];

reps = 1:10;


for uu=1:length(handlesToTest)
  for rep=1:length(reps)
    for ii = 1:length(xrange)
      xx = xrange(ii);
      for jj = 1:length(yrange)
        yy = yrange (jj);
        for kk = 1:size(aDir,1)
          close all; clf;
          clc; clear simulator; global_path_setup; repTest = 1;
          actuationDir = aDir(kk,:);

          handleBeingTested = primitiveInfo.pHandle{handlesToTest(uu)};
          txx = numPrimitivesForDist(handleBeingTested, xx, [1 0]);
          tyy = numPrimitivesForDist(handleBeingTested, yy, [0 1]);

          [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = handleBeingTested(txx,tyy);
          % input('')
          iros17prim_testCompXY
        end
      end
    end
  end
end


