
% --------------------------------------------------------------------------
%
% Post processing for primitive testing
%
% This script plots:
%   A series of "design performance" metrics of mesh samples of different
%     mesh primitive constructors
%
% For a specified (or hardcoded) set of trials, the data to plot
%   (stroke, stiffness, etc.) can be in dataLog files IF the measurements
%   were conducted online
% If measurements were not conducted online, the dataLog files will not
%   have measure data.
%   In this case, there should be another file in the data directory called
%     msrData_fname. The measurements would be in this file.
%   If msrData_fname does not exist, this file calls for offline measurements
%     using function offline_measureStiffnessProps
%
% --------------------------------------------------------------------------
function tmech17prim_postProcessing (setToEval, override, augment)
  set(0,'defaultlinelinewidth',1); set(0,'defaultlinemarkersize',15);
  close all; global_path_setup;
  homeFolder = 'allOutputsDataFolder/pTst/';

  xLRotAngle = 90;
  flags.SHOW_DEFORM = 0;
  flags.PRINT_FIGS = 0;
  flags.PLOTS_TO_INCLUDE = [1,2,3,5,6];
  % flags.PLOTS_TO_INCLUDE = [1,2,3,4,5,6];
%   flags.PLOTS_TO_INCLUDE = [6];
  % flags.PLOTS_TO_INCLUDE = [1,4];
  % flags.PLOTS_TO_INCLUDE = [2,3];

  outlierThreshold = 1.5;

  if ~exist('setToEval','var') || isempty(setToEval)
    setToEval = 25;
  end
  if ~exist('override','var') || isempty(override)
    override = 0;
  end
  if ~exist('augment','var') || isempty(augment)
    augment = 1;
  end

  % common parameters from test trials
  global cellProps; squareSampleDim = 15*cellProps.nodeToNodeDist;
  msrFileSrchStr = []; setFolders = [];
  for setNum = setToEval
    fld = dir(strcat(homeFolder,'set',num2str(setNum),'*'));
    if ~isempty(fld)
      setFolders = [setFolders; {strcat(homeFolder,fld(1).name,'/')}];
    end
  end

  % save file name and file path into lists
  dataLogFileList = []; dataLogFilePath = [];
  for ii = 1:length(setFolders)
    folderName = setFolders{ii};
    files = dir(strcat(folderName, '*_dataLog.mat'));
    for jj=1:length(files)
      logFile = files(jj);
      dataLogFilePath = [dataLogFilePath; {folderName}];
      dataLogFileList = [dataLogFileList; {logFile.name}];
    end
  end

  % save info about each trial into lists
  for ii=1:length(dataLogFilePath)
    file = strcat(dataLogFilePath{ii}, dataLogFileList{ii});
    [d_meshPrim{ii,1},d_pSize(ii,:),d_aDir{ii,1},d_sample{ii,1}] = ...
      parseFileName(file);
  end
  data_info.meshPrim = d_meshPrim; data_info.pSize = d_pSize;
  data_info.aDir = d_aDir; data_info.sample = d_sample;

  % ------------------------------------------------------------
  %
  % Load datasets
  %   For every file, obtain the measure file
  %   If measure file exists, load that file into "allDataSets"
  %   If measure file does not exist, load the "dataLog" file
  %     and run offline measure function on the dataLog to get
  %     the corresponding measure file
  %
  % ------------------------------------------------------------
  allDataSets = [];
  for jj=1:length(dataLogFileList)
    full_fname = dataLogFileList{jj};
    fpath = dataLogFilePath{jj}; idx = strfind(full_fname,'dataLog'); fname = full_fname(1:idx-1);
    msrFile = dir(strcat(fpath, fname, '*msrData.mat'));

    if isempty(msrFile) || override
      % if no measure file exists for this trial
      % call offline_measureStiffnessProps to generate one
      dataLog = load(strcat(fpath,full_fname)); dataLog = dataLog.dataLog;

      testFlags = testsRequired (data_info,jj);
      msrData = offline_measureStiffnessProps(fpath,full_fname,testFlags,augment);
    else
      % if measure file exists for this file, it's ready
      % load into collective vector of measure data structures
      msrData = load(strcat(fpath,msrFile(1).name)); msrData = msrData.msrData;
    end

    % ensure every msrData log file has the requisite fields
    msrFields = {...
      'msr_bndBox', ...
      'msr_strokeX', ...
      'msr_strokeY', ...
      'msr_compKX', ...
      'msr_compKY', ...
      'msr_strokeTh', ...
      'msr_bendK_horiz', ...
      'msr_bendK_vert'};

    for fldnum=1:length(msrFields)
      if ~isfield(msrData, msrFields{fldnum})
        for alongPrim=1:size(msrData,1)
          for alongTime=1:size(msrData,2)
            msrData(alongPrim,alongTime).(msrFields{fldnum}) = NaN;
          end
        end
      end
    end

    if isempty(allDataSets) 
      allDataSets = msrData;
    else
      allDataSets(jj,1:length(msrData)) = msrData;
    end
  end

  % =====================================================================
  %
  %
  %
  %                       D   I   S   P   L   A   Y
  %                       
  %
  %
  % =====================================================================
  figNum = 40;

  if any(ismember(flags.PLOTS_TO_INCLUDE,1))
    % ----------------------------------------------------
    % ----------------------------------------------------
    % stroke in x, y; segmented by primitive
    % ----------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    % parse out the appropriate data
    % ------------------------------------
    sample = 'square'; aDir = 'actX'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_data = allDataSets(filt_idx,:); filt_data_info = filterDataInfo(data_info,filt_idx);
    
    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    if flags.SHOW_DEFORM
      figure; 
    end
    for ii=1:size(filt_data,1)
      dim = filt_data(ii,1).msr_bndBox(3);
      strokeX = abs(filt_data(ii,1).msr_bndBox(3)-filt_data(ii,2).msr_bndBox(3)); data_y1_raw(ii) = strokeX/dim;
      if flags.SHOW_DEFORM
        subplot(floor(size(filt_data,1)/3), 4, ii); hold all;
        showShape(filt_data(ii,1).macro.nodePos); showShape(filt_data(ii,2).macro.nodePos);
      end
    end
    sq_actX_strainX = compressData(data_y1_raw', ic); 

    sample = 'square'; aDir = 'actY'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_data = allDataSets(filt_idx,:); filt_data_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    if flags.SHOW_DEFORM
      figure; 
    end    
    for ii=1:size(filt_data,1)
      dim = filt_data(ii,1).msr_bndBox(4);
      strokeY = abs(filt_data(ii,1).msr_bndBox(4)-filt_data(ii,2).msr_bndBox(4)); data_y2_raw(ii) = strokeY/dim;
      if flags.SHOW_DEFORM
        subplot(floor(size(filt_data,1)/3), 4, ii); hold all;
        showShape(filt_data(ii,1).macro.nodePos); showShape(filt_data(ii,2).macro.nodePos);
      end
    end
    sq_actY_strainY = compressData(data_y2_raw', ic); 

    % plot data
    % ------------------------------------
    figure(figNum); 
    bar(100*[sq_actX_strainX sq_actY_strainY]);
    ylabel('Percent Strain'); legend({'Actuated strain in x-','Actuated strain in y-'})
    set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title('Actuation strain along principal axes');

    % bottom left corner pos (x,y), wid, height (in px)
    hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1000 600]; set(hFig, 'Position', ggg);

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','strainXY_byPrim');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    end

  end


  if any(ismember(flags.PLOTS_TO_INCLUDE,2))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % linear stiffness in x and y; segmented by primitive
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;

    % parse out the appropriate data
    % ------------------------------------
    outlierThreshold = 1.25;

    % SQUARE SAMPLE, STIFFNESS IN X and Y
    sample = 'square'; aDir = ''; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_sq = allDataSets(filt_idx,:); filt_sq_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y1_raw =[]; data_y2_raw =[];
    for ii=1:size(filt_sq,1)
      aa = filt_sq(ii,1).msr_compKX; data_y1_raw(ii,:) = aa;
      aa = filt_sq(ii,1).msr_compKY; data_y2_raw(ii,:) = aa;
    end

    sq_kX = compressData(data_y1_raw, ic); sq_kY = compressData(data_y2_raw, ic);
    for ind = 1:length(primNames)
      ll = sq_kX(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX(ind,:) = ll;
      ll = sq_kY(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY(ind,:) = ll;
    end

    normalizer = max(median(sq_kX,2,'omitnan'));
    sq_kX = sq_kX./normalizer; sq_kX_std = std(sq_kX,0,2,'omitnan');

    normalizer = max(median(sq_kY,2,'omitnan'));
    sq_kY = sq_kY./normalizer; sq_kY_std = std(sq_kY,0,2,'omitnan');

    % BEAM SAMPLE, BENDING STIFFNESS
    sample = 'beam'; aDir = ''; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_bm = allDataSets(filt_idx,:); filt_bm_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y_raw =[];
    for ii=1:size(filt_bm,1)
      aa = filt_bm(ii,1).msr_bendK_horiz; data_y_raw(ii,:) = aa;
    end
    bm_kTh = compressData(data_y_raw, ic); 
    for ind = 1:length(primNames)
      ll = bm_kTh(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh(ind,:) = ll;
    end

    normalizer = max(median(bm_kTh,2,'omitnan'));
    bm_kTh = bm_kTh./normalizer; bm_kTh_std = std(bm_kTh,0,2,'omitnan');

    % plot data
    % ------------------------------------
    sq_kX
    figure(figNum); 
    subplot(1,3,1);  hold all; boxplot(sq_kX',primNames);
    ylim([-0.25 1.5]);
    ylabel('K'); set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Relative K_x vs. primitives \n(unpowered mesh)'));

    subplot(1,3,2);  hold all; boxplot(sq_kY',primNames);
    ylim([-0.25 1.5]);
    ylabel('K'); set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Relative K_y vs. primitives \n(unpowered mesh)'));

    subplot(1,3,3);  hold all; boxplot(bm_kTh',primNames);
    ylim([-0.25 2]);
    ylabel('K'); set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Relative K_{th} vs. primitives \n(unpowered mesh)'));

    % bottom left corner pos (x,y), wid, height (in px)

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','linRelStiffXY_byPrim');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    else
      hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 1200 400]; set(hFig, 'Position', ggg);
    end
  end


  if any(ismember(flags.PLOTS_TO_INCLUDE,3))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % symmetric stiffness meshes only; by prim
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;
    outlierThreshold = 1.25;

    % plot the stiffness in x- and y- for only those primitives that 
    % have comparable stiffness in the two dirs (within 10% of each other)
    sample = 'square'; aDir = ''; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_sq = allDataSets(filt_idx,:); filt_sq_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y1_raw =[]; data_y2_raw =[];
    for ii=1:size(filt_sq,1)
      aa = filt_sq(ii,1).msr_compKX; data_y1_raw(ii,:) = aa;
      aa = filt_sq(ii,1).msr_compKY; data_y2_raw(ii,:) = aa;
    end
    sq_kX = compressData(data_y1_raw, ic); 
    sq_kY = compressData(data_y2_raw, ic); 

    for ind = 1:length(primNames)
      ll = sq_kX(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX(ind,:) = ll;
      ll = sq_kY(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY(ind,:) = ll;
    end

    normalizer = max(median(sq_kX,2,'omitnan'));
    sq_kX = sq_kX./normalizer; sq_kX_std = std(sq_kX,0,2,'omitnan');

    normalizer = max(median(sq_kY,2,'omitnan'));
    sq_kY = sq_kY./normalizer; sq_kY_std = std(sq_kY,0,2,'omitnan');

    sq_kX_std = std(sq_kX,0,2,'omitnan');
    sq_kY_std = std(sq_kY,0,2,'omitnan');

    % matched sample ttest identifies (in variable h) the indices of
    % the primitives that have statistically low probability of being 
    % from same distribution
    [h,p,ci,stats] = ttest(sq_kX',sq_kY','alpha', 0.05); h = ~logical(h);
    
    xlabelNames = primNames(h);
    symmetric_kX = mean(sq_kY(h,:),2, 'omitnan');
    symmetric_kY = mean(sq_kX(h,:),2, 'omitnan');
    
    figure(figNum); 
    bar(mean([symmetric_kX symmetric_kY],2)); 
    set(gca,'xticklabels',xlabelNames); ylabel('K (N/m)'); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Symmetric-stiffness meshes'));

    % bottom left corner pos (x,y), wid, height (in px)
    hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 300 400]; set(hFig, 'Position', ggg);

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','symmetricMeshK_byPrim');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    end

  end

  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
  % 
  % PLOT 4 IS NOT VERY USEFUL, IGNORE IT
  % 
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  if any(ismember(flags.PLOTS_TO_INCLUDE,4))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % change in stiffnesses during actuation; by prim
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum); %hold all;


    % SQUARE SAMPLE, ACT-X STIFFNESSES
    sample = 'square'; aDir = 'actX'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_sq = allDataSets(filt_idx,:); filt_sq_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y1_rawSt =[]; data_y1_rawFin =[]; data_y2_rawSt =[]; data_y2_rawFin =[];
    for ii=1:size(filt_sq,1)
      aa = filt_sq(ii,1).msr_compKX; data_y1_rawSt(ii,:) = aa;
      aa = filt_sq(ii,2).msr_compKX; data_y1_rawFin(ii,:) = aa;
      aa = filt_sq(ii,1).msr_compKY; data_y2_rawSt(ii,:) = aa;
      aa = filt_sq(ii,2).msr_compKY; data_y2_rawFin(ii,:) = aa;
    end
    sq_kX_actX_st = compressData(data_y1_rawSt, ic); sq_kX_actX_fin = compressData(data_y1_rawFin, ic);
    sq_kY_actX_st = compressData(data_y2_rawSt, ic); sq_kY_actX_fin = compressData(data_y2_rawFin, ic);

    for ind = 1:length(primNames)
      ll = sq_kX_actX_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX_actX_st(ind,:) = ll;
      ll = sq_kX_actX_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX_actX_fin(ind,:) = ll;
      ll = sq_kY_actX_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY_actX_st(ind,:) = ll;
      ll = sq_kY_actX_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY_actX_fin(ind,:) = ll;
    end

    sq_kX_actX_st_std = std(sq_kX_actX_st,0,2,'omitnan'); sq_kX_actX_fin_std = std(sq_kX_actX_fin,0,2,'omitnan');
    sq_kY_actX_st_std = std(sq_kY_actX_st,0,2,'omitnan'); sq_kY_actX_fin_std = std(sq_kY_actX_fin,0,2,'omitnan');

    % SQUARE SAMPLE, ACT-Y STIFFNESSES
    sample = 'square'; aDir = 'actY'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_sq = allDataSets(filt_idx,:); filt_sq_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y1_rawSt =[]; data_y1_rawFin =[]; data_y2_rawSt =[]; data_y2_rawFin =[];
    for ii=1:size(filt_sq,1)
      aa = filt_sq(ii,1).msr_compKX; data_y1_rawSt(ii,:) = aa;
      aa = filt_sq(ii,2).msr_compKX; data_y1_rawFin(ii,:) = aa;
      aa = filt_sq(ii,1).msr_compKY; data_y2_rawSt(ii,:) = aa;
      aa = filt_sq(ii,2).msr_compKY; data_y2_rawFin(ii,:) = aa;
    end

    sq_kX_actY_st = compressData(data_y1_rawSt, ic); sq_kX_actY_fin = compressData(data_y1_rawFin, ic);
    sq_kY_actY_st = compressData(data_y2_rawSt, ic); sq_kY_actY_fin = compressData(data_y2_rawFin, ic);

    for ind = 1:length(primNames)
      ll = sq_kX_actY_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX_actY_st(ind,:) = ll;
      ll = sq_kX_actY_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kX_actY_fin(ind,:) = ll;
      ll = sq_kY_actY_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY_actY_st(ind,:) = ll;
      ll = sq_kY_actY_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; sq_kY_actY_fin(ind,:) = ll;
    end
    sq_kX_actY_st_std = std(sq_kX_actY_st,0,2,'omitnan'); sq_kX_actY_fin_std = std(sq_kX_actY_fin,0,2,'omitnan');
    sq_kY_actY_st_std = std(sq_kY_actY_st,0,2,'omitnan'); sq_kY_actY_fin_std = std(sq_kY_actY_fin,0,2,'omitnan');



    % BEAM SAMPLE, ACT-X BENDING STIFFNESS
    sample = 'beam'; aDir = 'actX'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_bm = allDataSets(filt_idx,:); filt_bm_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y_rawSt =[]; data_y_rawFin =[];
    for ii=1:size(filt_bm,1)
      aa = filt_bm(ii,1).msr_bendK_horiz; data_y_rawSt(ii,:) = aa;
      aa = filt_bm(ii,2).msr_bendK_horiz; data_y_rawFin(ii,:) = aa;
    end
    
    bm_kTh_actX_st = compressData(data_y_rawSt, ic); bm_kTh_actX_fin = compressData(data_y_rawFin, ic);

    for ind = 1:length(primNames)
      ll = bm_kTh_actX_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh_actX_st(ind,:) = ll;
      ll = bm_kTh_actX_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh_actX_fin(ind,:) = ll;
    end

    bm_kTh_actX_st_std = std(bm_kTh_actX_st,0,2,'omitnan'); bm_kTh_actX_fin_std = std(bm_kTh_actX_fin,0,2,'omitnan');

    % BEAM SAMPLE, ACT-Y BENDING STIFFNESS
    sample = 'beam'; aDir = 'actY'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_bm = allDataSets(filt_idx,:); filt_bm_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y_rawSt =[]; data_y_rawFin =[];
    for ii=1:size(filt_bm,1)
      aa = filt_bm(ii,1).msr_bendK_horiz; data_y_rawSt(ii,:) = aa;
      aa = filt_bm(ii,2).msr_bendK_horiz; data_y_rawFin(ii,:) = aa;
    end
    bm_kTh_actY_st = compressData(data_y_rawSt, ic); bm_kTh_actY_fin = compressData(data_y_rawFin, ic);

    for ind = 1:length(primNames)
      ll = bm_kTh_actY_st(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh_actY_st(ind,:) = ll;
      ll = bm_kTh_actY_fin(ind,:); ll(ll > outlierThreshold*median(ll)) = NaN; bm_kTh_actY_fin(ind,:) = ll;
    end

    bm_kTh_actY_st_std = std(bm_kTh_actY_st,0,2,'omitnan'); bm_kTh_actY_fin_std = std(bm_kTh_actY_fin,0,2,'omitnan');

    start_kX_actX = mean(sq_kX_actX_st, 2, 'omitnan');
    start_kY_actX = mean(sq_kY_actX_st, 2, 'omitnan');
    start_kTh_actX = mean(bm_kTh_actX_st, 2, 'omitnan');

    fin_kX_actX = mean(sq_kX_actX_fin, 2, 'omitnan');
    fin_kY_actX = mean(sq_kY_actX_fin, 2, 'omitnan');
    fin_kTh_actX = mean(bm_kTh_actX_fin, 2, 'omitnan');
    
    start_kX_actY = mean(sq_kX_actY_st, 2, 'omitnan');
    start_kY_actY = mean(sq_kY_actY_st, 2, 'omitnan');
    start_kTh_actY = mean(bm_kTh_actY_st, 2, 'omitnan');

    fin_kX_actY = mean(sq_kX_actY_fin, 2, 'omitnan');
    fin_kY_actY = mean(sq_kY_actY_fin, 2, 'omitnan');
    fin_kTh_actY = mean(bm_kTh_actY_fin, 2, 'omitnan');

    chng_kX_actX = fin_kX_actX - start_kX_actX; 
    chng_kY_actX = fin_kY_actX - start_kY_actX; 
    chng_kTh_actX = fin_kTh_actX - start_kTh_actX; 

    chng_kX_actY = fin_kX_actY - start_kX_actY; 
    chng_kY_actY = fin_kY_actY - start_kY_actY; 
    chng_kTh_actY = fin_kTh_actY - start_kTh_actY; 

    % figure(99); clf;
    % subplot(2,3,1); bar([start_kX_actX fin_kX_actX], 'stacked'); title('kX_{actX}');
    % subplot(2,3,2); bar([start_kY_actX fin_kY_actX], 'stacked'); title('kY_{actX}');
    % subplot(2,3,3); bar([start_kTh_actX fin_kTh_actX], 'stacked'); title('kTh_{actX}');
    % subplot(2,3,4); bar([start_kX_actY fin_kX_actY], 'stacked'); title('kX_{actY}');
    % subplot(2,3,5); bar([start_kY_actY fin_kY_actY], 'stacked'); title('kY_{actY}');
    % subplot(2,3,6); bar([start_kTh_actY fin_kTh_actY], 'stacked'); title('kTh_{actY}');
    % hFig = gcf; ggg = get(hFig, 'Position');
    % ggg = [100 100 1600 600]; set(hFig, 'Position', ggg);
    % return

    figure(figNum); 
    cmap = [1 0.5 0.5; 0.2 0.2 0.2; 0.2 0.2 1];
    subplot(2,2,1);
    bar([chng_kX_actX chng_kY_actX chng_kTh_actX]);
    colormap(cmap); 
    set(gca,'xticklabels',primNames); ylabel('\Delta K'); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Change in K after axial (x-) actuation'));
    legend({'chngkXactX', 'chngkYactX', 'chngkThactX'});

    subplot(2,2,3);
    bar([chng_kX_actY chng_kY_actY chng_kTh_actY]); 
    colormap(cmap); 
    set(gca,'xticklabels',primNames); ylabel('\Delta K'); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Change in K after axial (y-) actuation'));
    legend({'chngkXactY', 'chngkYactY', 'chngkThactY'});

    subplot(2,2,[2 4]);
    chngkX = mean([chng_kX_actX chng_kX_actY],2);
    chngkY = mean([chng_kY_actX chng_kY_actY],2);
    chngkTh = mean([chng_kTh_actX chng_kTh_actY],2);
    bar([chngkX chngkY chngkTh]);
    colormap(cmap); 
    set(gca,'xticklabels',primNames); ylabel('\Delta K'); set(gca,'XTickLabelRotation',xLRotAngle);
    title(sprintf('Change in K after axial actuation'));
    legend({'chngkX', 'chngkY', 'chngkTh'});

    % bottom left corner pos (x,y), wid, height (in px)
    hFig = gcf; ggg = get(hFig, 'Position');
    ggg = [100 100 1400 600]; set(hFig, 'Position', ggg);

    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','act_changeInK_byPrim');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    end
  end


  if any(ismember(flags.PLOTS_TO_INCLUDE,5))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % stroke in theta; segmented by primitive
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum);
    % outlierThreshold = 2;
    % outlierThreshold = inf;

    % BEAM SAMPLE, BENDING STROKE
    sample = 'beam'; aDir = 'pTh'; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_bm = allDataSets(filt_idx,:); filt_bm_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y_rawSt =[]; 
    if flags.SHOW_DEFORM
      figure; 
    end
    for ii=1:size(filt_bm,1)
      aa = filt_bm(ii,2).msr_strokeTh; 
      % aa(aa>outlierThreshold*median(aa)) = NaN; 
      data_y_rawSt(ii,:) = aa;
      if flags.SHOW_DEFORM
        subplot(floor(size(filt_bm,1)/3), 4, ii); hold all;
        showShape(filt_bm(ii,1).macro.nodePos); showShape(filt_bm(ii,2).macro.nodePos);
      end
    end

    bm_strainTh = compressData(data_y_rawSt, ic); 
    % normalizer = max(median(bm_strainTh,2,'omitnan')); bm_strainTh = bm_strainTh./normalizer;
    bm_strainTh_std = std(bm_strainTh,0,2,'omitnan');

    figure(figNum);
    bar(mean(bm_strainTh,2)*180/pi);
    ylabel('Angle (deg)'); set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);
    title('Radial strain in bending sample');

    % bottom left corner pos (x,y), wid, height (in px)
    hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 400 400]; set(hFig, 'Position', ggg);
    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','strainTh_byPrim');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    end    
  end


  if any(ismember(flags.PLOTS_TO_INCLUDE,6))
    % ----------------------------------------------------
    % --------------------------------------------------------------------------------------------------------
    % plot primitives on modulus vs. density scatterplot
    % --------------------------------------------------------------------------------------------------------
    % ----------------------------------------------------
    figNum = figNum+1; figure(figNum);
    % outlierThreshold = 1;

    % SQUARE SAMPLE, ACT-X STIFFNESSES
    sample = 'square'; aDir = ''; prim = {};
    filt_idx = filterDatasets (allDataSets, data_info, prim, sample, aDir);
    filt_sq = allDataSets(filt_idx,:); filt_sq_info = filterDataInfo(data_info,filt_idx);

    [primNames, ia, ic] = unique(data_info.meshPrim(filt_idx));
    data_y1_raw =[]; data_y2_raw =[]; 
    data_y3_raw =[]; data_y4_raw = [];

    global cellProps;
    cellWidth = cellProps.Wcell;
    nodeArmArea = cellProps.nodeArmLen * 3.5;
    rownorm = @(X) sqrt(sum((X).^2,2));
    for ii=1:size(filt_sq,1)
      aa = filt_sq(ii,1).msr_compKX; 
      % aa(aa>outlierThreshold*median(aa)) = NaN; 
      data_y1_raw(ii,:) = aa;
      aa = filt_sq(ii,1).msr_compKY; 
      % aa(aa>outlierThreshold*median(aa)) = NaN; 
      data_y2_raw(ii,:) = aa;
      data_y3_raw(ii,:) = filt_sq(ii,1).msr_bndBox;

      pos = filt_sq(ii,1).macro.nodePos; edg = filt_sq(ii,1).macro.edgeSet;
      shape = shapeToPointCloud(pos,edg,10);
      shapeBnd = boundary(shape(1:2,:)', 0.45);
      encArea1 = polyarea(shape(1,shapeBnd), shape(2,shapeBnd));
      shapeBnd = boundary(shape(1:2,:)', 0.95);
      encArea2 = polyarea(shape(1,shapeBnd), shape(2,shapeBnd));

      nEdg = size(edg,2);
      allCellLen = rownorm((pos(:,edg(1,:))-pos(:,edg(2,:)))');
      allCellLen = allCellLen - 2*cellProps.nodeArmLen;
      allNodeArmArea = nEdg*2*nodeArmArea;
      allCellArea = sum(allCellLen)*cellWidth;

      data_y4_raw(ii,:) = [encArea1 encArea2];
      data_y5_raw(ii,:) = allNodeArmArea + allCellArea;
    end

    sq_kX = compressData(data_y1_raw, ic); 
    sq_kX_std = std(sq_kX,0,2, 'omitnan');

    sq_kY = compressData(data_y2_raw, ic); 
    sq_kY_std = std(sq_kY,0,2, 'omitnan');

    sq_bndBox = compressData(data_y3_raw, ic, 1);
    tmp (:,1) = mean(sq_bndBox(:,1:4:end),2); tmp (:,2) = mean(sq_bndBox(:,2:4:end),2);
    tmp (:,3) = mean(sq_bndBox(:,3:4:end),2); tmp (:,4) = mean(sq_bndBox(:,4:4:end),2);
    sq_bndBox = tmp;

    primEncArea = compressData(data_y4_raw, ic); 
    primCovArea = mean(compressData(data_y5_raw, ic),2,'omitnan'); 

    primKX = mean(sq_kX,2,'omitnan');
    primKY = mean(sq_kY,2,'omitnan');

    primRho = 100*repmat(primCovArea,1,size(primEncArea,2))./primEncArea;
    primRhoMean = mean(primRho,2);
    primRhoStd = std(primRho,0,2);
    primE = mean([primKX primKY],2); % square sample, "A" and "L" of tensile test equal
    primE_raw = [sq_kX sq_kY];

    for prim = 1:size(primE_raw)
      ll = primE_raw(prim,:); ll(ll > 1.5*median(ll)) = NaN;
      primE_raw(prim,:) = ll;
    end

    primE_raw_std = std(primE_raw,0,2,'omitnan');

    figure(figNum); hold all;
    subplot(1,2,1); hold all;
    errorbar(primRhoMean,primE,...
      primE_raw_std,primE_raw_std,...
      primRhoStd,primRhoStd,...
      's');

    ylabel('Modulus (N/mm)'); 
    xlabel('Area Cover Density (%)'); 

    xx = primRhoMean; yy = primE;
    % text(xx-0.005,yy-0.25,cellstr(primNames)); % data labels
    text(xx+0.005,yy+1.25,cellstr(primNames)); % data labels

    % xlim([0 0.75]);

    subplot(1,2,2); cla; hold all;
    keyboard
    bar(primE./(primRhoMean/100));
    ylabel('Modulus/Density (N/mm)'); set(gca,'xticklabels',primNames); set(gca,'XTickLabelRotation',xLRotAngle);

    % bottom left corner pos (x,y), wid, height (in px)
    hFig = gcf; ggg = get(hFig, 'Position'); ggg = [100 100 800 600]; set(hFig, 'Position', ggg);
    if flags.PRINT_FIGS
      fname = strcat('publicationFigures/fig_','modVsDensity');
      print(figNum,fname, '-dpdf', '-r300'); print(figNum,fname, '-dpng', '-r300');
    end    

  end


end

function out = compressData (data, idx, keepOrder)
  out = [];
  % keyboard
  % uniqueIdx = unique(idx);
  % for ii=1:length(uniqueIdx)
  %   idy = idx == ii; tmp = data(idy,:); out(ii,:) = tmp(:);
  % end
  uniqueIdx = unique(idx);
  for ii=uniqueIdx'
    idy = idx == ii ;
    tmp = data(idy,:) ;
    if ~exist('keepOrder','var') || keepOrder == 0
      out(ii,:) = tmp(:);
    else
      tmp2 = [];
      for uu=1:size(tmp,1)
        tmp2 = [tmp2 tmp(uu,:)];
      end
      out(ii,:) = tmp2;
    end
  end
end

% ----------------------------------------------------------------------------
% 
% Filters data_info struct to return structs matching desired params
% 
% ----------------------------------------------------------------------------
function [info_out] = filterDataInfo (info_in, idx)
  info_out = struct();
  for ff = fieldnames(info_in)'
    info_out.(ff{1}) = info_in.(ff{1})(idx);
  end
end

% ----------------------------------------------------------------------------
% 
% Filters dataset to return list of structs matching desired params
% 
% ----------------------------------------------------------------------------
function [outIdx] = filterDatasets (inData, data_info, prim, sample, aDir)
  outIdx = ones(size(inData,1),1);

  if ~strcmpi(prim,'')
    idx = ismember(data_info.meshPrim,prim); outIdx = outIdx & idx;
  end
  if ~strcmpi(sample,'')
    idx = ismember(data_info.sample,sample); outIdx = outIdx & idx;
  end
  if ~strcmpi(aDir,'')
    idx = ismember(data_info.aDir,aDir); outIdx = outIdx & idx;
  end
end

% ----------------------------------------------------------------------------
% 
% Parses a filename (with path to file included) for information about 
%   the trial
% 
% ----------------------------------------------------------------------------
function [meshPrim, pSize, actDir, sample] = parseFileName (fname)
  [meshPrim, matchSt, matchFin] = primitiveByShortName(fname,'vs');

  tmp1str = fname(1:matchFin+1);
  if ~isempty( strfind(tmp1str,'square') )
    sample = 'square';
  elseif ~isempty( strfind(tmp1str,'beam') )
    sample = 'beam';
  end
  tmp2str = fname(matchFin+1:end); chopOffEnd = strfind(tmp2str,'dataLog.mat')-1; tmp2str = tmp2str(1:chopOffEnd);

  [st,fin] = regexp(tmp2str,'_([1-9]+)');
  rowNum = str2num(tmp2str(st(1)+1: fin(1))); colNum = str2num(tmp2str(st(2)+1: fin(2)));
  pSize = [rowNum colNum]; actDir = tmp2str(fin(2)+1: end-1);
end

% ----------------------------------------------------------------------------
% 
% 
% 
% ----------------------------------------------------------------------------
function testFlags = testsRequired(data_info, idx)
  testFlags = struct(); testFlags.compKX = 0; testFlags.compKY = 0; testFlags.bend_horiz = 0; testFlags.bend_vert = 0;
  if strcmpi(data_info.sample(idx),'square')
    testFlags.compKX = 1; testFlags.compKY = 1;
  elseif strcmpi(data_info.sample(idx),'beam')
    testFlags.bend_horiz = 1; 
    % testFlags.bend_vert = 1;
    if strcmpi(data_info.aDir(idx),'actX')||strcmpi(data_info.aDir(idx),'actY')
      testFlags.compKX = 1; testFlags.compKY = 1;
    end
  end
end

% ----------------------------------------------------------------------------
% 
% Adjust the plot y-axis limits to match the max values, min values, or both
% 
% ----------------------------------------------------------------------------
function limAdjust (figNum,s1, s2, flag)
  figure(figNum); lim1 = get(s1,'YLim'); lim2 = get(s2,'YLim');
  if strfind(flag,'both')
    lim = [min([0,lim1(1),lim2(1)]) max(lim1(2),lim2(2))];
  elseif strfind(flag,'upper')
    lim = [0 max(lim1(2),lim2(2))];
  elseif strfind(flag,'lower')
    lim = [min([0,lim1(1),lim2(1)]) 1000];
  end
  set(s1,'YLim', lim); set(s2,'YLim', lim);
end

  % -----------------------------------------------------------------
  %
  %
  %
  % E n d   Of   F i l e
  %
  %
  %
  % -----------------------------------------------------------------
