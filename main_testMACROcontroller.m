%
% This script sets up a MACRO mesh (a default or from external inputs)
% and runs the MACRO controller to simulate the shape matching performance
% The data for the entire trial is saved in a data log file for post-processing
%
% (Used for RA-L 2017 with IROS 2017 option)
%

if ~exist('repTest', 'var') || repTest == 0

  set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;
  global_path_setup


  % -----------------------------------------------------------------------
  % For controller testing
  % Generate default networks from primitive-generators
  % Test configurations are set from geometric deformations of the "startPos"
  % -----------------------------------------------------------------------
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_rect (1,1);
  % [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_truss (2,3);
  [nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ] = primitive_truss (3,3);

end


% output print locations
% --------------------------------------------------------------------------
outFolder = 'controllerTest';
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
global globalTime
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;


% --------------------------------------------------------------------------
% Data logger
% --------------------------------------------------------------------------
dataLog = struct();

% experiment details

% time
dataLog.simTime = [];

% pos sets
dataLog.startPos = [];
dataLog.targetPos = [];
dataLog.pos = [];

% stiffness sets
dataLog.startConfig = [];
dataLog.targetConfig = [];
dataLog.config = [];

% error sets
dataLog.errPos = [];
dataLog.errConfig = [];
dataLog.errLen = [];
dataLog.errHull = [];


% --------------------------------------------------------------------------
% Network specific inputs
% --------------------------------------------------------------------------
controlAltCallback = [];

nm = strcat('cTst_',nm);
trPos = stPos;

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------

% order of tests
% [compression, bending, noise]
testsToDo = [1,1,1];

% seed the random number generator
% seed = ceil(rand()*1000);
seed = 337;
rng(seed);
dataLog.randSeed = seed;

% generate random number from uniform distr between a, b
randab = @(a,b) a+(b-a)*rand();

% creating a random scaling factor for compression tests
randScaling = @() randab(0.75,1);

% random angle for bending test
randBendTheta = @() randab(0*pi/180, 10*pi/180);


% --------------------------------------------------------------------------
% Compression setup
% --------------------------------------------------------------------------
if testsToDo(1) == 1
  scaleX = randScaling(); scaleY = randScaling();
  tt = trPos; trans1 = createScaling(scaleX, scaleY);
  tt = transformPoint(tt', trans1)'; trPos = tt;
  nm = sprintf('%sCxCy', nm);
  nm = strrep(nm,'.','_');

  dataLog.scaleX = scaleX;
  dataLog.scaleY = scaleY;
end

% --------------------------------------------------------------------------
% Bending setup
% --------------------------------------------------------------------------
if testsToDo(2) == 1
  bendTheta = randBendTheta();

  tt = trPos;
  trans2 = createRotation(bendTheta);
  tt = transformPoint(tt',trans2)';
  trPos = tt;

  nm = sprintf('%sTh', nm);
  nm = strrep(nm,'.','_');

  dataLog.bendTheta = bendTheta;
end


% --------------------------------------------------------------------------
% Add noise
% --------------------------------------------------------------------------
if testsToDo(3) == 1
  trPos = awgn(trPos,30); % 30db == 1/1000*signal gaussian noise added
  nm = sprintf('%sNs', nm);
end

% reset node 1 to origin
trPos(1,:) = trPos(1,:)-trPos(1,1);
trPos(2,:) = trPos(2,:)-trPos(2,1);


% trPos = stPos;
% trPos(2,2) = 0.05;
% trPos(1,3) = 0.075; trPos(2,3) = 0.8; trPos(1,4) = 0.95; trPos(2,4) = 0.95;


% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
[name,edgeSet,edgeTheta,startPos,targetPos] = deal(...
  nm, eSet, eTheta, stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (...
  anch, fX, fY, fZ);

anchors=[1];

dataLog.edgeSet = eSet; dataLog.edgeTheta = eTheta; dataLog.anchors = anch;
dataLog.fixedInX = fX; dataLog.fixedInY = fY; dataLog.fixedInZ = fZ;

globalTime = tic;
outFolder = nm;
outFolder = strcat(num2str(globalTime),'_', nm);
% keyboard
helper_runControllerSimulator
globalRunTime = toc(globalTime)

dataLog.globalRunTime = globalRunTime;
dataLog.macroComponents = macroComponents;

% --------------------------------------------------------------------------
% Data storage
% --------------------------------------------------------------------------
dataLogOutputFile = strcat(outFolder,'_dataLog.mat');

save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/cTst/', dataLogOutputFile), ...
  'dataLog');
