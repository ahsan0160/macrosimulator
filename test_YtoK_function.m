
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
%
%
% A simple test script that uses the current startPos and targetPos and
% uses the YtoK function in the old v2 (YtoKfullRes) and the new v3 YtoK
% to generate a set of stiffnesses. For each set of stiffnesses,
% the current KtoY function is used to generate a set of node positions.
% For each set of node positions, a shapeMatchMetric is calculated and the
% startPos, pos from YtoKfullRes and pos from YtoK is plotted with their
% shapeMatchMetric stated.
%
% The script quickly allows checking whether a version of the YtoK function
% is producing an acceptable set of stiffnesses (ones that can be used to
% reconstruct the given set of node positions).
%
%
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------

function test_YtoK_function ()

  tic; aaSt= YtoKfullRes(startPos, Ae, cellProps.kM*ones(size(Ae,1),1)); toc
  tic; aaStPos = KtoY(aaSt, Ae, startPos); toc

  tic; bbSt= YtoKlowRes(startPos, A); toc
  tic; bbStPos = KtoY(bbSt, Ae, startPos); toc

  aaStPerform = shapeMatchMetric (startPos, edgeSet, aaStPos, edgeSet);
  bbStPerform = shapeMatchMetric (startPos, edgeSet, bbStPos, edgeSet);

  fprintf(2, 'StartPos performance: old = %.3f\n', aaStPerform);
  fprintf(2, 'StartPos performance: new = %.3f\n', bbStPerform);

  keyboard

  tic; aaTr= YtoKfullRes(targetPos, Ae, cellProps.kM*ones(size(Ae,1),1)); toc
  tic; aaTrPos = KtoY(aaTr, Ae, targetPos); toc

  tic; bbTr= YtoKlowRes(targetPos, A); toc
  tic; bbTrPos = KtoY(bbTr, Ae, targetPos); toc

  aaTrPerform = shapeMatchMetric (targetPos, edgeSet, aaTrPos, edgeSet);
  bbTrPerform = shapeMatchMetric (targetPos, edgeSet, bbTrPos, edgeSet);

  fprintf(2, 'TargetPos performance: old = %.3f\n', aaTrPerform);
  fprintf(2, 'TargetPos performance: new = %.3f\n', bbTrPerform);



  ff=figure(99);
  subplot(3,2,1); hold all; showShape(startPos, 'k', 1, edgeSet);

  subplot(3,2,3); hold all; showShape(aaStPos, 'r', 1, edgeSet);
  title(sprintf('Int/Union: %.2f', aaStPerform));

  subplot(3,2,5); hold all; showShape(bbStPos, 'b', 1, edgeSet);
  title(sprintf('Int/Union: %.2f', bbStPerform));

  subplot(3,2,2); hold all; showShape(targetPos, 'k', 1, edgeSet);

  subplot(3,2,4); hold all; showShape(aaTrPos, 'r', 1, edgeSet);
  title(sprintf('Int/Union: %.2f', aaTrPerform));

  subplot(3,2,6); hold all; showShape(bbTrPos, 'b', 1, edgeSet);
  title(sprintf('Int/Union: %.2f', bbTrPerform));


  printFig(ff, 'testYtoK', 'testCase');

  debugExit = 1;
  return

  keyboard

end






function metric = shapeMatchMetric (s1, e1, s2, e2)
% -----------------------------------------------------------------------------
% takes two shapes, converts them to point clouds using shapeToPointCloud
% function, then returns a metric of match using the ratio of
% intersection(s1,s2) over union(s1,s2)
%
% conversion to point clouds increases accruacy of the set operations
% note, after point cloud generation, all non-boundary nodes are discarded
% to reduce inaccuracy of set operation computations
% -----------------------------------------------------------------------------
  nVirt = 10;

  s1cloud = shapeToPointCloud (s1, e1, nVirt);
  s2cloud = shapeToPointCloud (s2, e2, nVirt);

  intArea = shapeSetOp(s1cloud, s2cloud, 'intersection');
  unionArea = shapeSetOp(s1cloud, s2cloud, 'union');

  % s1bndNodes = boundary(s1cloud(1:2,:)');
  % s1bnd = s1cloud(:,s1bndNodes);
  % s2bndNodes = boundary(s2cloud(1:2,:)');
  % s2bnd = s2cloud(:,s2bndNodes);

  % intArea = shapeSetOp(s1bnd, s2bnd, 'intersection');
  % unionArea = shapeSetOp(s1bnd, s2bnd, 'union');

  metric = intArea / unionArea;
% keyboard
end
% -----------------------------------------------------------------------------


