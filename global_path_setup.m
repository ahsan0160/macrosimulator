% 
% global_path_setup
% 


addpath(genpath('generalHelperFiles'));
addpath(genpath('primitiveConstructors'));
addpath(genpath('simulatorHelperFiles'));
addpath(genpath('controllerHelperFiles'));
addpath(genpath('postProcessingHelperFiles'));
addpath(genpath('primitiveHelperFiles'));
addpath(genpath('primitiveTestFiles'));
addpath(genpath('publicationFigures'));
addpath(genpath('measureFiles'));
addpath(genpath('customCodeForPapers'));

warning('off','map:polygon:noExternalContours');

global cellProps
cellProps = ActiveCellProperties();

global primitiveInfo
if exist('commonPrimitiveInfo.mat','file')
  primitiveInfo = load('commonPrimitiveInfo.mat');
else
  disp('PRIMTIVE INFO NOT LOADED');
end

