
% Test case 1
% -----------------------------------------------------------------------------
% edgeSet = [1,2; 2,4; 1,3; 3,4]';
% edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';
% startPos = [0 0; 1 0; 0 1; 1 1]';
% targetPos =	[0 0; 1 0; 0 1; 0.8 0.9]';
% anchors = 1; fixedInX = []; fixedInY = 2; fixedInZ = [];
% name = 'test01';

% Test case 2
% -----------------------------------------------------------------------------
% edgeSet = [1,2; 2,4; 1,3; 3,4]';
% edgeTheta = [0 0 0; 0 0 pi/2; 0 0 pi/2; 0 0 0]';
% startPos = [0 0; 1 0; 0 1; 1 1]';
% targetPos =	[0 0; 1 0; 0 1; 0.8 0.9]';
% anchors = 1; fixedInX = []; fixedInY = 2; fixedInZ = [];
% name = 'test02';

% Two cells in a chain
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = two_cell_chain ()

% Three cells in a chain
% -----------------------------------------------------------------------------

% Single square: unachievable
% Target is collapsed square corner-to-corner
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_01 ();

% Single square; unachievable
% target is one of the corners collapsed somewhat towards other corner
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_02 ();

% Single square
% target has one side shrunk towards adjacent node
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_03 ();

% Single square: unachievable
% Target is collapsed square corner-to-corner
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_04 ();

% Single Triangle
% Target is a shorter triangle of the same base; much shorter than achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_01 ();

% Single Triangle
% Target is a shorter triangle of the same base; achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_02 ();

% Single Triangle
% One of the lateral sides of the triangle is shorter: unachievable position
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_03 ();

% Single Triangle
% One of the lateral sides of the triangle is shorter: achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_04 ();

% Two-triangle truss
% Target is flattened vertically: unachievably low
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_01 ();

% Two-triangle truss
% Target is flattened vertically: achievable
%  SOMETHING WRONG WITH THIS ONE ?!?!
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_02 ();


% Two-triangle truss
% Target is compressed sideways: unachievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_03 ();

% Two-triangle truss
% Target is compressed sideways: achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_04 ();

% Two-triangle truss
% Target is compressed sideways assymmetrically: unachievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_05 ();

% Two-triangle truss
% Target is compressed sideways assymmetrically: achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_06 ();


% Four triangle truss
% Target is a curved beam: ?achievable
% -----------------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = triangle_truss_long_01 ();


% large truss structure
% test compression
% -----------------------------------------------------------------------------
  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_truss (1,1);
  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_truss (10,10);

  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
    fixedInX,fixedInY,fixedInZ] = primitive_truss (8,8);
  % [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
  %   fixedInX,fixedInY,fixedInZ] = primitive_rect (10,10);

  [left] = pointSetEdgeNodes(startPos,edgeSet,'left',0.1);
  [right] = pointSetEdgeNodes(startPos,edgeSet,'right',0.1);
  fixedInX = [fixedInX, left'];

    tt = targetPos;

    trans1 = createScaling(0.8, 0.8);
    tt = transformPoint(tt', trans1)';
    targetPos = tt;

    % trans2 = createRotation(0,0,pi/20);
    % targetPos = transformPoint(tt', trans2)';

    % keyboard
  % nodeCount = size(startPos,2);
  % [A,Ae] = genAdjMats (edgeSet, nodeCount);
  % startPos = posFromTheta(...
  %   cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
  % targetPos = targetPos * cellProps.nodeToNodeDist;