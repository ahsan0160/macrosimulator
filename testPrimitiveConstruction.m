
% 
% Testing a new primitive by visualizing construction
% 

function testPrimitiveConstruction (x,y) 
  global_path_setup;
  % [~,eSet,eTheta,stPos] = primitive_hex (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_rect (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_tri (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hyb3t2s (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hyb2t2h (x,y, 1); 
  [~,eSet,eTheta,stPos] = primitive_hybs2o (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hyb4th (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hybtshs (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hybshd (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hybt2d (x,y, 1); 
  % [~,eSet,eTheta,stPos] = primitive_hyb2tsts (x,y, 1); 

  pos2 = posFromTheta(25,size(stPos,2),eSet,eTheta); 
  figure(3); clf;
  showShape(shapeToPointCloud(stPos,eSet,3));
  figure(4); clf;
  showShape(shapeToPointCloud(pos2,eSet,3),'b');

end

