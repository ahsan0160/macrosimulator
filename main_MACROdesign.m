% --------------------------------------------------------------------------
% This is a script to perform design of a MACRO mechanism from specifications
% User input specifications can be in a file in this version
%   Graphical user input interface will be defined in a later version
%
% Inputs:
% Shape information:
%     Kinematic properties: number of links, joints, etc.
%   Size of overall mechanism
% Task information:
%   Desired shape change
%     Desired range of motion of joints
%
% Actions of this script:
%   Structural design:
%   Creates a set of MACRO primitives that addresses the desired task
%   Describes connections between these primitives
%   For each primitive:
%     Sets up local coordinate frames
%     Sets size of the primitive
%
% Functional design:
%   Describes the location of inputs
%   Describes the actuation profile and achievable range of motion
%
% Simulates the mechanism actuating using MACROcontroller and MACROsimulator
%
% Outputs:
%   Mechanism design and performance metrics from the controller and simulator
% Allows iterative design changes from the results of the first round
% --------------------------------------------------------------------------


global_path_setup
set(0,'defaultlinelinewidth',4);
close all;
clear; clf; clc;

% --------------------------------------------------------------------------
% common parameters
% --------------------------------------------------------------------------


% --------------------------------------------------------------------------
% INPUTS
% --------------------------------------------------------------------------
% load inputs for debug, request inputs for release
% --------------------------------------------------------------------------
stroke = 20; % percent of one of the dimensions
[dimX, dimY, bndPts, trPts] = designInput_linearActuator (stroke);

% --------------------------------------------------------------------------
% SEGMENT
% --------------------------------------------------------------------------
% segment mechanism into appropriate hierarchy of MACROs
% --------------------------------------------------------------------------
macroCount = segmentMechanism(bndPts, trPts); % = 1 for the moment

% --------------------------------------------------------------------------
% DESIGN INDIVIDUAL MACRO
% --------------------------------------------------------------------------

MACROs = struct;
% iterate over collection of MACROs, saving each output design
% --------------------------------------------------------------------------
for ii=1:macroCount
  [pos, edges, thetas, inputs] = MACROdesign (bndPts, trPts);
  MACROs(ii).nodePos = pos;
  MACROs(ii).edgeSet = edges;
  MACROs(ii).edgeTheta = thetas;
  MACROs(ii).inputs = inputs;
end

MACROs

% --------------------------------------------------------------------------
% attach MACROs according to segmentation connectivity
% --------------------------------------------------------------------------





