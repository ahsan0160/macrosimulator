
% -------------------------------------- 
% Aligns network to reference
% Matches centroids of nets
% Rotates net to minimize node pos err
% -------------------------------------- 
function [Y, netErr] = alignNets (X, Xref)
	% keyboard
	if size(X,1) == 3
		X(3,:)= [];
	end
	if size(Xref,1) == 3
		Xref(3,:)= [];
	end

	% Centroid and all operations are made for the nodes arranged as n x 2 mat
	X = X';
	Xref = Xref';
% keyboard
	Xcenter = centroid(X);
	XrefCenter = centroid(Xref);

	rownorm = @(X) sqrt(sum((X).^2,2));

	% Match centroids of the two networks
	offsetToAlign = repmat(XrefCenter, size(X,1), 1);

	Y = X - repmat(Xcenter, size(X,1),1);
	XrefAdj = Xref - repmat(XrefCenter, size(Xref,1), 1);

	% Rotate Y to minimize normed node pos errors
	netPosErr = @(xx) sum(rownorm(xx-XrefAdj));
	R = @(theta, xx) ([cos(theta) -sin(theta); sin(theta) cos(theta)]*xx')';

	alignErr = @(theta, xx) netPosErr(R(theta, xx));

	[theta, netErr] = fminsearch(@(theta) alignErr(theta, Y), 0);
	Y = R(theta,Y);

	Y = Y + offsetToAlign;

	Y = Y';
	Y(3,:) = 0;
end

