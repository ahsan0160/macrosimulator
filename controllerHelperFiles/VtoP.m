% input mapping: forward and backward from true input V to virtual input P
function P = VtoP (V, Ae)
	global cellProps
	if ~exist('cellProps', 'var')
		cellR = 1.5;
	else
		cellR = cellProps.cellR;
	end
	P = (Ae*V).^2/cellR;
end





