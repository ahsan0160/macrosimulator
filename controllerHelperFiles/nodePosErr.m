function [err, errSum] = nodePosErr (X, Y)
% 	keyboard
	rownorm = @(a) sqrt(sum((a).^2,2));
    XX= alignNets(X,Y);
    XX(3,:) = [];
    % keyboard
    err = rownorm( Y'-XX' );
	errSum = sum(err);
end