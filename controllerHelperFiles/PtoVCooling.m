function V = PtoVCooling (P,Ae,fanR)
	if ~exist('fanR')
		fanR = 100;
	end
	
	% Modify input power to find the cooling power necessary
	Pneg = P;
	Pneg(Pneg > 0) = 0; Pneg = -Pneg;

	% V = (Pneg*fanR).^(1/2);
	V = fminsearch(@(V) norm(Pneg*fanR-(Ae*V).^2), rand(size(Ae,2),1));
	V = V - min(V);
	% V = V - V(1);

	% keyboard
end
