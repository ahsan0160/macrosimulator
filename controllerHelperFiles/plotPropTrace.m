function plotPropTrace (t,array)
	nn = size(array,1);
	mm = size(array,2);
	for ii=1:nn
		for jj=1:mm
			subplot(nn,mm,(ii-1)*mm+jj);
			hold all;
			plot(t,array(ii,jj),'.b');
		end
	end
end