function P = VtoP (V,Ae,fanR)
	if ~exist('fanR')
		fanR = 100;
	end
	P = (Ae*V).^2/fanR;
end