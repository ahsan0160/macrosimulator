
function K = YtoKfullRes (Y, Ae, Kguess)
% Y to K takes a given set of node positions, and outputs a set of Niti
% stiffnesses that would generate these node positions
% For an Active Cell, the equilibrium position of the cell is the elastic
% equilibrium of the NiTi springs and the passive spring. If the NiTi 
% stiffness changes, the equilibrium position changes. This is a one-to-one
% map, and can be directly looked up in the Active Cell properties object.

% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% 
% 
% V2: Stable as of 161121, Works well for small structures. Takes forever
%  for larger structures.
% 
% 
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% function K = YtoK (Y, Ae, Kguess)
	global cellProps
	if ~exist('cellProps', 'var')
		nitiL0 = 0.8; kSpring = 0.2; springL0 = 1.75;
		kM = 0.0302; kA = 0.7189;
	else
		nitiL0 = cellProps.nitiL0;
		kSpring = cellProps.kSpring; springL0 = cellProps.springL0;
		kM = cellProps.kM; kA = cellProps.kA;
	end
	if ~exist('Kguess') || isempty(Kguess)
		Kguess = 0.5*(kA-kM)*rand(size(Ae,1),1);
	end
	if size(Y,1) == 3
		Y(3,:)=[];
	end

	posErr = @(kk) err(kk,Y, nitiL0, kSpring, springL0, Ae);

	options = optimoptions(@fmincon,'Display','off'...
		,'Algorithm','interior-point' ,'DiffMinChange', 0.01 ...
        , 'TolFun', 1e-1, 'MaxFunEvals', 100000);

	[K, fval] = fmincon(posErr, Kguess, [], [], [],[], ...
		kM*ones(size(Kguess)), kA*ones(size(Kguess)), [], options);	
end


function pp = err (kk,Y, nitiL0, kSpring, springL0, Ae) 
	rownorm = @(X) sqrt(sum((X).^2,2));
	eqPos = @(r) KtoY(r, Ae, Y);

    bb = eqPos(kk); bb = alignNets(bb,Y); XX= alignNets(bb,Y); XX(3,:)=[];
    err = rownorm( Y'-XX' ); errSum = sum(err);

	[nodeErr, pp] = nodePosErr(bb,Y);
	% A = [0 1 1 0; 1 0 0 1; 1 0 0 1; 0 1 1 0];
	% figure(1); showNet(bb, A, [0 0 1], 1, '--'); pause(0.03);
end



% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% 
% 
% V1: Abandoned for inaccuracy and inefficiency
% 
% 
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------

% function K = YtoK (Y, Ae, Kguess)
% 	global cellProps
% 	if ~exist('cellProps', 'var')
% 		nitiL0 = 0.8;
% 		kSpring = 0.2;
% 		springL0 = 1.75;
% 		kM = 0.0302;
% 		kA = 0.7189; 
% 		nodeArmLen = eps;
% 	else
% 		nitiL0 = cellProps.nitiL0;
% 		kSpring = cellProps.kSpring;
% 		springL0 = cellProps.springL0;
% 		kM = cellProps.kM;
% 		kA = cellProps.kA; 
% 		nodeArmLen = cellProps.nodeArmLen;
% 	end

% 	if ~exist('Kguess') || isempty(Kguess)
% 		% Kguess = 0.5*(kA-kM)*rand(size(Ae,1),1);
% 		Kguess = 0.99*kA*ones(size(Ae,1),1);
% 	end

% 	rownorm = @(X) sqrt(sum((X).^2,2));
% 	cellLen = rownorm(Ae*Y');
% 	K = Kguess;
	
% 	cellPotentialArray = @(K) ...
% 		1/2*K.*(cellLen-nitiL0-nodeArmLen*2).^2 + ...
% 		1/2*kSpring*(cellLen-springL0-nodeArmLen*2).^2;
% 	totalNetPotential = @(k) sum(cellPotentialArray(k));        



% 	K = fmincon(totalNetPotential, K, [], [], [],[], ...
% 		kM*ones(size(Kguess)), kA*ones(size(Kguess)));




% end
