
% output mapping: from true output Y (node positions) to k (cell stiffness)
function Y = KtoY (K, Ae, Yguess)
% keyboard
	global cellProps
	if ~exist('cellProps', 'var')
		nitiL0 = 0.8; kSpring = 0.2; springL0 = 1.75;
		kM = 0.0302; kA = 0.7189; nodeArmLen = eps;
	else
		nitiL0 = cellProps.nitiL0; kSpring = cellProps.kSpring;
		springL0 = cellProps.springL0;
		kM = cellProps.kM; kA = cellProps.kA;
		nodeArmLen = cellProps.nodeArmLen;
	end

	if ~exist('Yguess') || isempty(Yguess)
		Yguess = rand(3, size(Ae,2));
	end

	if size(Yguess,1) == 3
		Yguess (3,:) = [];
	end

	rownorm = @(X) sqrt(sum((X).^2,2));
	cellLen = @(X) rownorm(Ae*X');
	Y = Yguess;
	
   	cellPotentialArray = @(X) ...
		1/2*K.*(cellLen(X)-nitiL0-nodeArmLen*2).^2 + ...
		1/2*kSpring*(cellLen(X)-springL0-nodeArmLen*2).^2;
	totalNetPotential = @(X) sum(cellPotentialArray(X));        
	options = optimset('Display', 'off', 'MaxFunEvals', 300);
	% options = optimset('MaxFunEvals', 1e2);
	Y = fminsearch(totalNetPotential, Y, options);

	Y = alignNets(Y,Yguess);
	Y(3,:) = [];
	
end