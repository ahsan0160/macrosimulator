function saveFrameToGif (filename, imind, cm, firstFrame, delay)
	if ~exist('firstFrame', 'var') 
		firstFrame = 0;
	end
	if ~exist('delay', 'var')
		delay = 0.1;
	end
	if firstFrame == 1
		% disp('>>>First frame written');
		% keyboard
		imwrite(imind,cm,filename,'gif', 'Loopcount',inf, 'DelayTime',delay);
	else
		% disp('>>> frame written');
		imwrite(imind,cm,filename,'gif','WriteMode','append', 'DelayTime',delay);
	end
end



