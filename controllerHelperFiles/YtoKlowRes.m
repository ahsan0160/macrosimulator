
function K = YtoKlowRes (Y, Ae)
% Y to K takes a given set of node positions, and outputs a set of Niti
% stiffnesses that would generate these node positions
% For an Active Cell, the equilibrium position of the cell is the elastic
% equilibrium of the NiTi springs and the passive spring. If the NiTi 
% stiffness changes, the equilibrium position changes. This is a one-to-one
% map, and can be directly looked up in the Active Cell properties object.

% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% 
% 
% V3: Stable as of 161201, Works well for simple deformations.
% Fast for larger structures, accuracy not well established yet.
% 
% 
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------


	global cellProps
	cellProps = ActiveCellProperties(0);
	if ~exist('cellProps', 'var')
		nitiL0 = 0.8; kSpring = 0.2; springL0 = 1.75;
		kM = 0.0302; kA = 0.7189;
	else
		nitiL0 = cellProps.nitiL0;
		kSpring = cellProps.kSpring; springL0 = cellProps.springL0;
		kM = cellProps.kM; kA = cellProps.kA;
	end

	if size(Y,1) == 3
		Y(3,:)=[];
	end

	rownorm = @(X) sqrt(sum((X).^2,2));
	nodeToNode = rownorm([ (Ae*Y(1,:)')'; (Ae*Y(2,:)')' ]');
	cellLengths = nodeToNode - 2*cellProps.nodeArmLen;

	K = arrayfun(@(x) cellProps.cellXeqToK(x), cellLengths);
end
