
function [struct_outputs] = CommonInterfaceForModules (...
  debugMode, optional_fileNameToSaveDataTo, ...
  struct_macroComponents, struct_additionalInformation)
% A common interface for the modules in this system
% This interface is not officially enforced, but is used as a styleguide for
% all modules that handle MACRO manipulation, currently including:
%   MACROsimulator
%   MACROcontroller
%
% INPUTS:
% ---------------------------------------------------------------------------
% optional_fileNameToSaveDataTo
%   intuitively obvious
% struct_macroComponents
%   includes A, Ae, nodePos, edgeSet
% struct_additionalInformation
%   for simulator, includes runtime, timestep, bool_animate, controlInputs
%   for controller, includes startPos, targetPos, startK, targetK
%
% OUTPUTS:
% ---------------------------------------------------------------------------
% struct_outputs
%   wraps all outputs into a struct

  disp('>>THIS IS A TEMPLATE FUNCTION: NO OUTPUTS EXPECTED.<<');
end
