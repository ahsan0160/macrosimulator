%
%
% HELPER SCRIPT TO RUN A SIMULATOR AND CONTROLLER PREDEFINED BEFORE
% IF SIMULATOR AND CONTROLLER SETUP PARAMETERS ARE NOT AVAILABLE,
%  THIS SCRIPT WILL FAIL
%
%

if ~exist('dataLog','var')
  dataLog = struct();
end

nodeCount = size(startPos,2);
[A,Ae] = genAdjMats (edgeSet, nodeCount);
startPos = posFromTheta(...
  cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
% startPos = startPos * cellProps.nodeToNodeDist;
targetPos = targetPos * cellProps.nodeToNodeDist;

if ~isa(controlAltCallback,'function_handle')
  usingController = 1;
else
  usingController = 0;
  if ~exist('actuationDir','var')
    actuationDir = [1 0];
  end
  if ~exist('bndThreshold','var')
    bndThreshold = 16.75;
  end
  dataLog.actuationDir = actuationDir;
end

if ~exist('recordForMeasure','var')
  recordForMeasure = 0;
end
if ~exist('measureTimes','var') || isempty(measureTimes)
  % if measure is active
  %   set to record initial data
  %   final time data is saved by default
  measureTimes = [1];
end
if ~exist('edgeMetric','var')
  edgeMetric = @pointSetEdgeNodes;
end
if ~exist('meshPrimitive', 'var')
  meshPrimitive = [];
end

% --------------------------------------------------------------------------
% Controller inputs
% --------------------------------------------------------------------------
startK = []; targetK = [];
macroComponents.A = A;      macroComponents.Ae = Ae;
if size(startPos,1) == 2
  startPos(3,:) = 0;
end
if size(targetPos,1) == 2
  targetPos(3,:) = 0;
end
controllerInfo.startPos = startPos;     controllerInfo.targetPos = targetPos;
controllerInfo.startK = startK;         controllerInfo.targetK = targetK;
controllerOutFile = strcat(name, '_Cntrl');

% --------------------------------------------------------------------------
% Simulator setup
% --------------------------------------------------------------------------
netInputs.nodeCount = nodeCount;
netInputs.edgeSet = edgeSet;
netInputs.edgeTheta = edgeTheta;

netInputs.anchors = anchors;  netInputs.fixedInX = fixedInX;
netInputs.fixedInY = fixedInY;  netInputs.fixedInZ = fixedInZ;
if exist('zEdges', 'var')
  netInputs.zEdges = zEdges;
end

% --------------------------------------------------------------------------
% Simulator inputs
% --------------------------------------------------------------------------
simulatorInfo.animate = 1;
simulatorInfo.targetPos = targetPos;
simulatorInfo.showTargetPos = 1;
simulatorInfo.showStartPos = 1;
simulatorOutFile = strcat(name, '_Sim');
macroComponents.netInputs = netInputs;

% record netInputs into dataLog
for fn = fieldnames(netInputs)'
  dataLog.(fn{1}) = netInputs.(fn{1});
end

st = tic;
simulator = MACROsimulator(debugMode, outFolder, simulatorOutFile, ...
  macroComponents,simulatorInfo)
elapsed = toc(st);
fprintf(2, 'Simulator init took: %.3f seconds \n', elapsed);

st = tic;
controller = MACROcontroller(debugMode, outFolder, controllerOutFile, ...
  macroComponents, controllerInfo)
elapsed = toc(st);
fprintf(2, 'Controller init took: %.3f seconds \n', elapsed);

% changes made to YtoK function for reducing computational time
% and using a simpler model of cell-node interactions
% (basically ignoring the node contributions)
% --------------------------------------------------------------------------
% test_YtoK_function
% if exist('debugExit', 'var')
%   return
% end

desiredK0 = controller.startK;    desiredKf = controller.targetK;

rownorm = @(X) sqrt(sum((X).^2,2));
dispSize = [30, 1, 6, 10];

% Setup parameters for simulation
tStart=0;
if ~exist('dt','var')
  dt = 1;
end
if ~exist('tFinal','var')
  tFinal = 20;
end
stationaryWindow = 1e5;
startingArea = MACROerror_hull(startPos, [], A);
errTolerance = 0.01;
set(controller, 'dt', dt);
set(controller, 'intermediateSteps', max(ceil(1/dt), 10));

% --------------------------------------------------------------------------
% Run simulator, updating control inputs for each segment
% --------------------------------------------------------------------------

% Setup figures to plot progress
% ------------------------------------------------------------------------
if detailedFigs
  pl_posErrTrace=figure;hold on;
    xlabel('Time');ylabel('NodePos Err');
  pl_lenErrTrace=figure;hold on;
    xlabel('Time');ylabel('CellLength Err');
  pl_hullErrTrace=figure;hold on;
    xlabel('Time');ylabel('Convex-HullArea Err');
  pl_configErrTrace=figure;hold on;
    xlabel('Time');ylabel('||(k_{f} - k)||_2');
  pl_liveConfigTrace = figure; hold on;
end

% Initialize Tracking variables
% ------------------------------------------------------------------------
netErr = [];
itrCount = 0;

itrT = tStart;      latestNodePos = simulator.macro.nodePos;

dataLog.startPos = startPos;
dataLog.targetPos = targetPos;
dataLog.startConfig = controller.startK;
dataLog.targetConfig = controller.targetK;

nodeInputV = [];
coolingInputV = [];

% Simulation loop
% ------------------------------------------------------------------------
while itrT < tFinal
  itrCount = itrCount + 1;

  % Run controller; update inputs
  % ------------------------------------------------
  if usingController
    controllerInfo.startPos = latestNodePos;
    controllerInfo.startPos(3,:) = [];
    % keyboard
    st = tic;
    [nodeInputV, coolingInputV] = ...
      controller.updateController(latestNodePos, targetPos);
    elapsed = toc(st);
    fprintf(2,'Itr: %.3f, Controller took %.3f seconds \n', itrT, elapsed);
  else
    disp('Using callback')
    if itrCount > 1 || isempty(nodeInputV)
      [nodeInputV, coolingInputV] = ...
        controlAltCallback(latestNodePos, edgeSet, ...
          actuationDir, bndThreshold, edgeMetric, meshPrimitive);
    end
  end

  % measure system for stiffness, stroke, etc.
  % store an entire clone of the MACRO in the dataLog for post processing
  % NOT conduct measure tests in situ, since it can slow down simulation
  if recordForMeasure
    if any(itrCount == measureTimes)
      record = struct();
      record.recItr = itrCount;
      record.recTime = itrT;
      macroTmp = copy(simulator.macro);
      record.macro = macroTmp;
      dataLog.records(itrCount) = record;
      clear record;
      clear macroTmp;
    end
  end

  % Simulate the MACRO with given inputs
  % simulate for dt seconds, with intermediate steps of dt/2 seconds
  % ------------------------------------------------
  st = tic;
  figure(1); simulator.run(dt, dt, nodeInputV, coolingInputV);
  elapsed = toc(st);
  fprintf(2, 'Itr: %.3f, Simulator took %.3f seconds \n', itrT, elapsed);

  st = tic;

  % Obtain latest positions
  % ------------------------------------------------
  latestNodePos = simulator.macro.nodePos;

  % Compute updated error metrics and other information (config)
  % ------------------------------------------------
  errPos = MACROerror_nodePos(latestNodePos, targetPos, A);
  errLen = MACROerror_cellLength(latestNodePos, targetPos, A);
  errHull = MACROerror_hull(latestNodePos, targetPos, A)/startingArea;
  errConfig = get(controller, 'configErr');
  currentConfig = get(controller,'startK');
  errArea = MACROerror_areaIOU(latestNodePos, targetPos, edgeSet);

  % Record all metrics into dataLog
  % ------------------------------------------------
  dataLog.simTime(:,itrCount) = itrT;
  dataLog.simInputs(:,:,itrCount) = [nodeInputV coolingInputV];
  dataLog.pos(:,:,itrCount) = latestNodePos;
  dataLog.config(:,itrCount) = currentConfig;
  dataLog.errPos(:,itrCount) = errPos;
  dataLog.errConfig(:,itrCount) = errConfig;
  dataLog.errLen(:,itrCount) = errLen;
  dataLog.errHull(:,itrCount) = errHull;
  dataLog.errArea(:,itrCount) = errArea;

  % Point-wise plots of the metrics
  % ------------------------------------------------
  if detailedFigs
    figure(pl_posErrTrace); plot(itrT, errPos, 'ko');
    figure(pl_lenErrTrace); plot(itrT, errLen, 'ko');
    figure(pl_hullErrTrace); plot(itrT, errHull, 'ko');
    figure(pl_configErrTrace); plot(itrT, abs(errConfig), 'ko');
    figure(pl_liveConfigTrace); clf; hold on;
    radarPlot([desiredK0 desiredKf dataLog.config], ...
      's:', 'LineWidth', 3);
    radarPlot([desiredK0 desiredKf], ...
      'ko-', 'LineWidth', 2);
  end

  % Plot the configuration
  % ------------------------------------------------
  % figure(4); hold on;   plot(itrT, currentConfig,'ko');

  errCurrent = errHull;
  if isnan(errCurrent)
    errCurrent = errLen;
  end
  netErr = [netErr errCurrent];

  % Update simulation time
  % ------------------------------------------------
  itrT = itrT + dt;


  if usingController
    % Check whether chosen error metric is stationary
    % ------------------------------------------------
    if length(netErr) < stationaryWindow
      windowMeanError = mean(netErr);
    else
      windowMeanError = mean(netErr(end+1-stationaryWindow:end));
      windowErrorVar = std(netErr(end+1-stationaryWindow:end));
    end
    % Check absolute error
    % ------------------------------------------------
    if abs(errCurrent) < errTolerance
      disp('Absolute error at current iteration: ')
      finalError = errCurrent
    end

    % Termination criteria; low error variance over window
    % ------------------------------------------------
    if abs(sum(errConfig)) < 0.01
      % keyboard % break
      disp(strcat('Error config fell below threshold to: ', ...
        num2str(abs(sum(errConfig)))));
    end

    if length(netErr) > stationaryWindow && windowErrorVar < errTolerance/10
      disp('Error stationary in the recent window, at: ')
      finalError = errCurrent
      windowMeanError
      windowErrorVar
      break
    end
  end

  pause(dt);

  elapsed = toc(st);
  fprintf(2, 'Itr: %.3f Misc work: %.3f seconds \n', itrT, elapsed);

end

% if measure is requested, always save the last time step
if recordForMeasure
  record = struct();
  record.recItr = itrCount;
  record.recTime = itrT;
  macroTmp = copy(simulator.macro);
  record.macro = macroTmp;
  dataLog.records(itrCount) = record;
  clear record;
end

if detailedFigs
  figure(pl_posErrTrace);
    plot(dataLog.simTime, dataLog.errPos, 'ko-');
  figure(pl_lenErrTrace);
    plot(dataLog.simTime, dataLog.errLen, 'ko-');
  figure(pl_hullErrTrace);
    plot(dataLog.simTime, dataLog.errHull, 'ko-');
  figure(pl_configErrTrace);
    plot(dataLog.simTime,abs(dataLog.errConfig),'o-');
end


if usingController
  % keyboard
  pl_configTrace = plotConfigTrace(dataLog.simTime, dataLog.config, dataLog.errConfig, cellProps, controller);
else
  % pl_configTrace = figure;
end


if detailedFigs
  pl_configPath = plotDesiredPath(desiredK0, desiredKf, dataLog.config);
  axis auto;
end

if ~debugMode
  if detailedFigs
    printFig(pl_posErrTrace, outFolder, name, 'posErrTrace');
    printFig(pl_lenErrTrace, outFolder, name, 'lenErrTrace');
    printFig(pl_hullErrTrace, outFolder, name, 'hullErrTrace');
    printFig(pl_configErrTrace, outFolder, name, 'configErrTrace');

  % printFig(pl_mappingTtoK, outFolder, name, 'mappingTtoK')
    printFig(pl_liveConfigTrace, outFolder, name, 'liveConfigTrace');
    printFig(pl_configTrace, outFolder, name, 'configTrace');
    printFig(pl_configPath, outFolder, name, 'configPath');
  end
end

