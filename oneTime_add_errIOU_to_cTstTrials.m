
%
% This is a one-time code for adding the MACROerror_areaIOU **INTO**
% the data log files for allOutputsDataFolder/cTst/*
%

clear all; close all; clc;
global_path_setup

directory = 'allOutputsDataFolder/cTst/';

files = dir(strcat(directory,'*_dataLog.mat'));

PLOT_EXISTING_ERRORS = 0;
PLOT_NEW_ERRORS = 0;
OVERWRITE_EXISTING_ERRORS = 1;
OVERWRITE_DATALOGS = 1;


% when using parfor, cannot save from workspace directly
% save indirectly by saving the filename and the error array
datasetsToSave = struct()

parfor ii=1:length(files)

  name = files(ii).name;

  tmp = load(strcat(directory,name));
  dataLog = tmp.dataLog;

  if isfield(dataLog, 'errArea')
    fprintf(2, '>> %s contains errArea already. Plotting... \n', name);
    if ~PLOT_EXISTING_ERRORS
      figure (777); plot(dataLog.simTime, dataLog.errArea, 'o--');
      title(name); pause(1);
    end
    if ~OVERWRITE_EXISTING_ERRORS
      continue;
    end
  else
    fprintf(2, '>> %s does not contain errArea. Generating... \n', name);
  end

  targetPos = dataLog.targetPos;
  edgeSet = dataLog.edgeSet;
  dataLog.errArea = [];
  for jj=1:length(dataLog.simTime)
    currPos = dataLog.pos(:,:,jj);
    dataLog.errArea (:,jj) = MACROerror_areaIOU(currPos, targetPos, edgeSet);
  end

  if PLOT_NEW_ERRORS 
    figure (778); plot(dataLog.simTime, dataLog.errArea, 'o--');
    title(name);
    pause(1);
  end

  datasetsToSave(ii).name = name;
  datasetsToSave(ii).dataLog = dataLog;
end


for ii=1:length(files)
  if OVERWRITE_DATALOGS 
    name = datasetsToSave(ii).name;
    dataLog = datasetsToSave(ii).dataLog;
    save(strcat(directory,name),'dataLog');
  end
end
