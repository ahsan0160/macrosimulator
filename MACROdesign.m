function [pos, edges, thetas, inputs] = MACROdesign (...
  bndPts, trPts, preferredPrimitive)
% This module designs a set of node positions and cell edge information
% (connectivity and angles) such that the shape matches the provided shape
% and the required deformation of the shape
%
% INPUTS:
% ---------------------------------------------------------------------------
% bndPts: boundary points of the shape desired
% trPts: boundary points of the shape once deformed
% preferredPrimitive: optional instruction from caller about primtive type
%
% OUTPUTS:
% ---------------------------------------------------------------------------
% pos: node positions
% edges: edge set
% thetas: edge thetas
% inputs: requisite control inputs that causes the necessary shape change
  disp('>>MACRO design<<');
  [pos, edges, thetas, inputs] = deal ([]);

  % available primitives
  primitiveList = {'primitive_rect', 'primitive_truss'};

  global cellProps
  cellProps = ActiveCellProperties();

  % TILING
  % --------------------------
  % generate a tiling of simple primitives (if preferred primitive not given)
  % OR generate tiling of given primitive
  % rough match to bndPts
  for primitiveItr = 1:length(primitiveList)
    if ~exist ('preferredPrimitive', 'var')
      candPrimitive = primitiveList{primitiveItr};
    else
      candPrimitive = preferredPrimitive;
    end
    [pos1, edges1, thetas1] = generateCandidateTiling (...
      bndPts, candPrimitive);
 

    % REFINEMENT OF SHAPE
    % --------------------------
    % add/remove cells or nodes to better match the given shape
    [pos2, edges2, thetas2] = refineTiling (pos1, edges1, thetas1, bndPts)



    % MAP DEFORMATION
    % --------------------------
    % find a geometric method of transforming the candidate MACRO to match
    % the target shape after deformation
    [pos3, edges3, thetas3] = deformShapeToTarget (...
      bndPts, trPts, pos2, edges2, thetas2);


    % CONTROL TEST
    % --------------------------
    % test controller on this start-target configuration
    % two step process: control to deform MACRO to match bndPts
    % followed by control to deform MACRO to match trPts

    startPos = pos3;
    targetPos = pos3;
    edgeSet = edges3;
    edgeTheta = thetas3;

    [inputs1] = controllerTest (startPos, targetPos, edgeSet, edgeTheta);
    [inputs2] = controllerTest (startPos, targetPos, edgeSet, edgeTheta);


    % VALIDATE COMPLETENESS
    % --------------------------
    % if controller can sufficiently control the deformation to given specs
    % return macro and controller information
    % else: repeat process with different primitive or different refinement

    [pos, edges, thetas] = deal(pos2, edges2, thetas2);
    break

  end

end


function [pos, edges, thetas] = generateCandidateTiling (...
  bndPts, candPrimitive)
% determines the number of primitive instances required to roughly
% fill the shape
% calls the appropriate primitive generation method
%
% INPUTS:
% ---------------------------------------------------------------------------
% bndPts: boundary points of the shape desired
% candPrimitive: optional instruction from caller about primtive type
%
% OUTPUTS:
% ---------------------------------------------------------------------------
% pos: node positions
% edges: edge set
% thetas: edge thetas
  disp('<< function: generateCandidateTiling >>');

  global cellProps

  dimX = max(bndPts(1,:)) - min(bndPts(1,:));
  dimY = max(bndPts(2,:)) - min(bndPts(2,:));

  numX = ceil(dimX / cellProps.nodeToNodeDist);
  numY = ceil(dimY / cellProps.nodeToNodeDist);

  [name, edgeSet,edgeTheta,startPos,targetPos,anchors,...
      fixedInX,fixedInY,fixedInZ] = primitive_rect (numX,numY);

  nodeCount = size(startPos,2);
  [A,Ae] = genAdjMats (edgeSet, nodeCount);
  startPos = posFromTheta(...
    cellProps.cellL0, nodeCount, edgeSet, edgeTheta);

    pos = startPos;
    edges = edgeSet;
    thetas = edgeTheta;

    figure(1); hold all;
    showNet(pos, A);
    showShape(bndPts);

    disp('<< Rough tiling completed >>');
end






function [pos2, edges2, thetas2] = refineTiling (...
  pos1, edges1, thetas1, bndPts)
%
% INPUTS:
% ---------------------------------------------------------------------------
% OUTPUTS:
% ---------------------------------------------------------------------------
  disp('<< function: refineTiling >>');
  [pos2, edges2, thetas2] = deal(pos1, edges1, thetas1);


  disp('<< Structural Refinement Completed >>');

end






function [pos3, edges3, thetas3] = deformShapeToTarget (...
  bndPts, trPts, pos2, edges2, thetas2)
%
% INPUTS:
% ---------------------------------------------------------------------------
% OUTPUTS:
% ---------------------------------------------------------------------------
  disp('<< function: deformShapeToTarget >>');

  [pos3, edges3, thetas3] = deal (pos2, edges2, thetas2);




  disp('<< Geometric transformation of MACRO to target completed >>');


end





function [inputs] = controllerTest (startPos, targetPos, edgeSet, edgeTheta)
%
% INPUTS:
% ---------------------------------------------------------------------------
% OUTPUTS:
% ---------------------------------------------------------------------------
  disp('<< function: deformShapeToTarget >>');

  global cellProps

  inputs = [];

  % nodeCount = size(startPos,2);
  % [A,Ae] = genAdjMats (edgeSet, nodeCount);
  % startPos = posFromTheta(...
  %   cellProps.cellL0, nodeCount, edgeSet, edgeTheta);

  % % ------------------------------------------------------------------
  % % Controller inputs
  % % ------------------------------------------------------------------
  % macroComponents.A = A;    macroComponents.Ae = Ae;
  % startK = []; targetK = [];
  % if size(startPos,1) == 2
  %   startPos(3,:) = 0;
  % end
  % if size(targetPos,1) == 2
  %   targetPos(3,:) = 0;
  % end
  % controllerInfo.startPos = startPos;   controllerInfo.targetPos = targetPos;
  % controllerInfo.startK = startK;     controllerInfo.targetK = targetK;
  % name = 'design';
  % controllerOutFile = strcat(name, '_Cntrl');

  % debugMode = 0;
  % outFolder = 'allOutputs/zzOutputs';
  % controller = MACROcontroller(debugMode, outFolder, controllerOutFile, ...
  %   macroComponents, controllerInfo)


  disp('<< Geometric transformation of MACRO to target completed >>');


end
