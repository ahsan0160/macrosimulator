%
% Inputs:
%   Description of the robot: MACRO network specification
% Target shape
%
% Actions:
%   Computes the equilibrium config of the MACRO for the given specification
%   Computes the configuration corresponding to the target shape
%   Segments the path in config space from eq. config. to the target config.
%   For each of the k-segments, x_j from x_0 to x_f:
%     Uses MACROcontroller to determine appropriate control inputs V_j
%     Uses MACROsimulator to simulate effect of V_j on the segment_{j-1}
%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%
set(0,'defaultlinelinewidth',4); close all; clear; clf; clc;
global_path_setup


globalTime = tic;

% output print locations
% --------------------------------------------------------------------------
outFolder = 'zzOutputs';
detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties(1);
cellProps = ActiveCellProperties(0);
name = 'primitiveStructuralTests';
debugMode = 0;

% --------------------------------------------------------------------------
% Network specific inputs
% --------------------------------------------------------------------------

% for a bunch of test cases, etc.
% --------------------------------
helper_genSimulatorInputs

controlAltCallback = [];

% testing scheme for the nodes
% controlAltCallback is a way to bypass the controller and specifying exact
% inputs (could be constant)
% --------------------------------------------------------------------------

% testing compression
controlAltCallback = @cb_compressionX;

helper_runControllerSimulator

toc(globalTime)


% --------------------------------------------------------------------------
% Data storage block
% Save data for post-processing
% --------------------------------------------------------------------------
if debugMode == 0

end

