%
% Inputs:
%   Description of the robot: MACRO network specification
% Target shape
%
% Actions:
%   Computes the equilibrium config of the MACRO for the given specification
%   Computes the configuration corresponding to the target shape
%   Segments the path in config space from eq. config. to the target config.
%   For each of the k-segments, x_j from x_0 to x_f:
%     Uses MACROcontroller to determine appropriate control inputs V_j
%     Uses MACROsimulator to simulate effect of V_j on the segment_{j-1}
%
% Outputs:
%   Saves trial data into file:
%     Starting config, Starting pos, Target config, Target pos
%     For all segments: Intermediate config, Intermediate pos, Control inputs
%

global_path_setup
set(0,'defaultlinelinewidth',4);
clear; clf; clc;

% common input parameters
% ---------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties();
name = 'test';
debugMode = 1;

repCount = 50;

% ---------------------------------------------------------------------
% Network specific inputs
% ---------------------------------------------------------------------
% Single square: unachievable
% Target is collapsed square corner-to-corner
% -------------------------------------------------------------------
[name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_01 ();

% Single Triangle
% Target is a shorter triangle of the same base; much shorter than achievable
% -------------------------------------------------------------------
% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_04 ();



nodeCount = size(startPos,2);
[A,Ae] = genAdjMats (edgeSet, nodeCount);
startPos = posFromTheta(...
  cellProps.cellL0, nodeCount, edgeSet, edgeTheta);
% startPos = startPos * cellProps.nodeToNodeDist;




% resolution = 'step_0-5';
resolution = 'step_1-0';
fname = strcat(name,'_workspace_',resolution);
load (strcat(fname,'.mat'));


repTestCase = [];
repTestConfigErr = [];
repTestHullErr = [];

rng(100);
wspacePts = size(tempArray,1);
selectedPts = randi([1,wspacePts],repCount,1);
repTestCase = posArray(:,:,selectedPts);

for repItr = 1:repCount
  targetPos = squeeze(repTestCase(:,:,repItr));

  clear controller
  clear simulator
  clear netInputs

  netInputs.nodeCount = nodeCount;
  netInputs.edgeSet = edgeSet;
  netInputs.edgeTheta = edgeTheta;

  netInputs.anchors = anchors;
  netInputs.fixedInX = fixedInX;
  netInputs.fixedInY = fixedInY;
  netInputs.fixedInZ = fixedInZ;

  % ---------------------------------------------------------------------
  % Controller inputs
  % ---------------------------------------------------------------------
  startK = [];
  targetK = [];
  macroComponents.A = A;
  macroComponents.Ae = Ae;
  if size(startPos,1) == 2
    startPos(3,:) = 0;
  end
  if size(targetPos,1) == 2
    targetPos(3,:) = 0;
  end
  controllerInfo.startPos = startPos;
  controllerInfo.targetPos = targetPos;
  controllerInfo.startK = startK;
  controllerInfo.targetK = targetK;
  controllerOutFile = strcat(name, '_Cntrl');

  % ---------------------------------------------------------------------
  % Simulator inputs
  % ---------------------------------------------------------------------
  simulatorInfo.animate = 1;
  simulatorInfo.targetPos = targetPos;
  simulatorOutFile = strcat(name, '_Sim');
  macroComponents.netInputs = netInputs;

  simulator = MACROsimulator(debugMode, simulatorOutFile, ...
    macroComponents,simulatorInfo)

  controller = MACROcontroller(debugMode, controllerOutFile, ...
    macroComponents, controllerInfo)


  desiredK0 = controller.startK;
  desiredKf = controller.targetK;
  % keyboard

  rownorm = @(X) sqrt(sum((X).^2,2));
  dispSize = [30, 1, 6, 10];

  % ---------------------------------------------------------------------
  % Run simulator, updating control inputs for each segment
  % ---------------------------------------------------------------------

  % Setup figures to plot progress
  % -------------------------------------------------------------------
  % pl_posErrTrace = figure; hold on;
  %   xlabel('Time'); ylabel('Node Pos Error');
  % pl_lenErrTrace = figure; hold on;
  %   xlabel('Time'); ylabel('Cell Length Error');
  % pl_hullErrTrace = figure; hold on;
  %   xlabel('Time'); ylabel('Convex-Hull Area Error');
  % pl_configErrTrace = figure; hold on;
  %   xlabel('Time'); ylabel('|| (k_{f} - k) ||_2');

  % figure(3); clf;
  % subplot(4,1,1); hold on;  xlabel('Time'); ylabel('Node Pos Error');
  % subplot(4,1,2); hold on;  xlabel('Time'); ylabel('Cell Length Error');
  % subplot(4,1,3); hold on;  xlabel('Time'); ylabel('Convex-Hull Area Error');
  % subplot(4,1,4); hold on;  xlabel('Time'); ylabel('Config Error');
  % figure(4); clf;

  % Setup parameters for simulation
  % -------------------------------------------------------------------
  tStart=0;
  dt = 0.2;
  tFinal=20;
  stationaryWindow = 10;
  startingArea = MACROerror_hull(startPos, [], A);
  errTolerance = 0.01;
  set(controller, 'dt', 5*dt);

  % Initialize Tracking variables
  % -------------------------------------------------------------------
  netErr = [];
  itrCount = 0;
  series_simTime = [];
  series_errPos = [];
  series_errLen = [];
  series_errHull = [];
  series_errConfig = [];
  series_config = [];

  itrT = tStart;
  latestNodePos = simulator.macro.nodePos;

  % Simulation loop
  while itrT < tFinal

    % Run controller; update inputs
    % -------------------------------------------------------------------
    controllerInfo.startPos = latestNodePos;
    controllerInfo.startPos(3,:) = [];
    [nodeInputV, coolingInputV] = ...
      controller.updateController(latestNodePos, targetPos)

    % Simulate the MACRO with given inputs
    % -------------------------------------------------------------------
    figure(1); simulator.run(dt, nodeInputV, coolingInputV);

    % Obtain latest positions
    % -------------------------------------------------------------------
    latestNodePos = simulator.macro.nodePos;

    % Compute updated error metrics and other information (config)
    % -------------------------------------------------------------------
    errPos = MACROerror_nodePos(latestNodePos, targetPos, A);
    errLen = MACROerror_cellLength(latestNodePos, targetPos, A);
    errHull = MACROerror_hull(latestNodePos, targetPos, A)/startingArea;
    errConfig = get(controller, 'configErr');
    currentConfig = get(controller,'startK');

    % Record all metrics into series_ variables
    % -------------------------------------------------------------------
    itrCount = itrCount + 1;
    series_simTime(:,itrCount) = itrT;
    series_errPos(:,itrCount) = errPos;
    series_errLen(:,itrCount) = errLen;
    series_errHull(:,itrCount) = errHull;
    series_errConfig(:,itrCount) = errConfig;
    series_config(:,itrCount) = currentConfig;

    % if itrCount == 15
    %   keyboard
    % end

    % Point-wise plots of the metrics
    % -------------------------------------------------------------------
    % figure(3);
    % subplot(4,1,1); hold on;  plot(itrT, errPos, 'ko');
    % subplot(4,1,2); hold on;  plot(itrT, errLen, 'ko');
    % subplot(4,1,3); hold on;  plot(itrT, errHull, 'ko');
    % subplot(4,1,4); hold on;  plot(itrT, abs(errConfig), 's');

    % figure(pl_posErrTrace); plot(itrT, errPos, 'ko');
    % figure(pl_lenErrTrace); plot(itrT, errLen, 'ko');
    % figure(pl_hullErrTrace); plot(itrT, errHull, 'ko');
    % figure(pl_configErrTrace); plot(itrT, abs(errConfig), 'ko');

    % Plot the configuration
    % -------------------------------------------------------------------
    % figure(4); hold on;   plot(itrT, currentConfig,'ko');

    errCurrent = errHull;
    if isnan(errCurrent)
      errCurrent = errLen;
    end
    netErr = [netErr errCurrent];

    % Update simulation time
    % -------------------------------------------------------------------
    itrT = itrT + dt;

    % Check whether chosen error metric is stationary
    % -------------------------------------------------------------------
    if length(netErr) < stationaryWindow
      windowMeanError = mean(netErr);
    else
      windowMeanError = mean(netErr(end+1-stationaryWindow:end));
      windowErrorVar = std(netErr(end+1-stationaryWindow:end));
    end
    % Check absolute error
    % -------------------------------------------------------------------
    if abs(errCurrent) < errTolerance
      disp('Absolute error at current iteration: ')
      finalError = errCurrent
    end

    % Termination criteria; low error variance over window
    % -------------------------------------------------------------------
    % if abs(sum(errConfig)) < 0.01
    %   % keyboard
    %   break
    % end

    if length(netErr) > stationaryWindow && windowErrorVar < errTolerance/10
      disp('Error stationary in the recent window, at: ')
      finalError = errCurrent
      windowMeanError
      windowErrorVar
      break
    end
    pause(dt);
  end

  ff = figure(1);
  axis off;
  printFig(ff,'zzRepTestOutputs',name, strcat('repTest_', num2str(repItr)));
  repTestConfigErr(repItr) = abs(sum(errConfig));
  repTestHullErr(repItr) = errHull;

end



time=clock;
tstamp = strcat(...
  num2str(time(1)),'_',num2str(time(2)),'_',...
  num2str(time(3)),'_',num2str(time(4)),'_',...
  num2str(time(5)),'_',num2str(time(6)));
outFname = sprintf('zzRepTestResults/%s_repTest_%d_%s.mat', ...
    name, repCount, tstamp);
save(outFname, ...
  'repTestCase', ...
  'repTestConfigErr', ...
  'repTestHullErr');
