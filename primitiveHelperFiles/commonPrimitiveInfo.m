
clear;

pHandle = {...
  @primitive_hex, ...
  @primitive_rect, ...
  @primitive_tri, ...
  @primitive_hyb3t2s, ...
  @primitive_hyb2t2h, ...
  @primitive_hybs2o, ...
  @primitive_hyb4th, ...
  @primitive_hybtshs, ...
  @primitive_hybshd, ...
  @primitive_hybt2d, ...
  @primitive_hyb2tsts, ...
  };

pNames = {...
  'primitive_hex', ...
  'primitive_rect', ...
  'primitive_tri', ...
  'primitive_hyb3t2s', ...
  'primitive_hyb2t2h', ...
  'primitive_hybs2o', ...
  'primitive_hyb4th', ...
  'primitive_hybtshs', ...
  'primitive_hybshd', ...
  'primitive_hybt2d', ...
  'primitive_hyb2tsts', ...
  };

pShortNames = {...
  'pHex', ...          % 1                  
  'pRect', ...         % 2                  
  'pTri', ...          % 3                  
  'pHyb3t2s', ...      % 4                      
  'pHyb2t2h', ...      % 5                      
  'pHybs2o', ...       % 6                     
  'pHyb4th', ...       % 7                     
  'pHybtshs', ...      % 8                      
  'pHybshd', ...       % 9                     
  'pHybt2d', ...       % 10                     
  'pHyb2tsts', ...     % 11                      
  };

pOutNames = {...
  'H', ...          % 1                  
  'R', ...         % 2                  
  'T', ...          % 3                  
  'T_3.S_2', ...      % 4                      
  'T.H.T.H', ...      % 5                      
  'S.O_2', ...       % 6                     
  'T_4.H', ...       % 7                     
  'T.S.H.S', ...      % 8                      
  'S.H.D', ...       % 9                     
  'T_2.D', ...       % 10                     
  'T_2.S.T.S', ...     % 11                      
  };


pHeight = [
  2*sqrt(3)/2     % @primitive_hex
  1               % @primitive_rect
  sqrt(3)/2       % @primitive_tri
  1+sqrt(3)       % @primitive_hyb3t2s
  sqrt(3)         % @primitive_hyb2t2h
  3+sqrt(2)       % @primitive_hybs2o
  2*sqrt(3)       % @primitive_hyb4th
  2+sqrt(3)       % @primitive_hybtshs
  5+sqrt(3)       % @primitive_hybshd
  2+2*sqrt(3)     % @primitive_hybt2d
  sqrt(3)         % @primitive_hyb2tsts
  ];


nRectX = @(x) ceil(x);
nRectY = @(x) ceil(x);

nTrussX = @(x) ceil( x-0.5 );
nTrussY = @(x) ceil( 2*x / sqrt(3) );

nHexX = @(x) ceil( (x-0.5) / 1.5 );
nHexY = @(x) ceil( x / sqrt(3) );

nTriX = @(x) ceil( (x-1) / 0.5 );
nTriY = @(x) ceil( 2*x / sqrt(3) );

nHyb3t2sX = @(x) ceil(x);
nHyb3t2sY = @(x) ceil( ( x-sqrt(3)/2 ) / (1 + sqrt(3)/2) );

nHyb2t2hX = @(x) ceil( (x-1) / 2 );
nHyb2t2hY = @(x) ceil( x / sqrt(3) );

nHybs2oX = @(x) ceil( (x-1) / (2+sqrt(2)) );
nHybs2oY = @(x) ceil( (x-2-1/sqrt(2)) / (1+1/sqrt(2)) );

nHyb4thX = @(x) 1 + ...
  (x-4 > 0)*( ...
    floor((x-4)/7)*3 + ...
    3*(mod(x-4,7)>6) + ...
    (mod(x-4,7)<=6)*(2*(mod(x-4,7)>3) + 1*(mod(x-4,7)<=3)) );
nHyb4thY = @(x) ceil ( x / (2*sqrt(3)) );

nHybtshsX = @(x) 1 + ceil( (x-2-sqrt(3)) / (1.5+sqrt(3)/2) );
nHybtshsY = @(x) 1 + ceil( (x-2.5-3*sqrt(3)/2) / (1+sqrt(3)) );

nHybshdX = @(x) 1 + ceil( (x - (2+3*sqrt(3))) / (1.5+3*sqrt(3)/2) );
nHybshdY = @(x) 1 + ceil( (x - (6.5+3*sqrt(3)/2)) / (3.5+sqrt(3)) );

nHybt2dX = @(x) 1 + ceil( (x - (4+3*sqrt(3)/2)) / (2+sqrt(3)) );
nHybt2dY = @(x) 1 + ceil( (x - (2+2*sqrt(3))) / (1.5+sqrt(3)) );

nHyb2tstsX = @(x) ceil ( (x) / (0.5*(1+sqrt(3))) );
nHyb2tstsY = @(x) ceil ( (x) / (0.5*(1+sqrt(3))) );

save('commonPrimitiveInfo.mat');
