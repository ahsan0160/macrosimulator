% returns handle of graphics
function hh = showShape (shapeP, ...
	optionalColor, addVirtualNodes, edges, shrinkFactor)
% -----------------------------------------------------------------------------
% -----------------------------------------------------------------------------
	%
	% uses MATLAB's 'boundary' function
	% 	only works on 2D shapes for now
	%
	shapeP = shapeP(1:2, :);

	if ~exist('addVirtualNodes', 'var') || isempty(addVirtualNodes)
		addVirtualNodes = 0;
	end

	if addVirtualNodes
		shapeP = shapeToPointCloud(shapeP, edges, 100);
	end

	if ~exist('shrinkFactor','var') || isempty(shrinkFactor)
		bnd = boundary(shapeP');
	else
		bnd = boundary(shapeP', shrinkFactor);
	end

	hh = [];
	if ~exist('optionalColor', 'var') || strcmp(optionalColor,'')
		hold on;
		hhh = plot(shapeP(1,:), shapeP(2,:), 'o', ...
			'MarkerSize', 3);
		hh = [hh, hhh];
		hhh = plot(shapeP(1,bnd), shapeP(2,bnd), 'o-', ...
			'MarkerSize', 1, 'LineWidth', 1);
		hh = [hh, hhh];
	else
		hold on;
		hhh = plot(shapeP(1,:), shapeP(2,:), strcat(optionalColor,'o'), ...
			'MarkerSize', 3);
		hh = [hh, hhh];
		hhh = plot(shapeP(1,bnd), shapeP(2,bnd), strcat(optionalColor,'o-'), ...
			'MarkerSize', 1, 'LineWidth', 1);
		hh = [hh, hhh];
	end
	axis equal;
end
% -----------------------------------------------------------------------------


