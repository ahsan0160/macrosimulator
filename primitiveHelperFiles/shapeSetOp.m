
% -----------------------------------------------------------------------------
function areaOut = shapeSetOp (shape1, shape2, operation, shrinkFactor, debugMode)
% -----------------------------------------------------------------------------
% performs a set operation specified by 'operation' between two point sets
% does NOT work as expected in many cases when points lie on straight lines
% -----------------------------------------------------------------------------
	if ~exist('operation', 'var')
		operation = 'union';
	end
	if ~exist('shrinkFactor', 'var')
		shrinkFactor = 0; % uses the convex hull
	end

	shape1bnd = boundary(shape1(1:2,:)', shrinkFactor);
	shape1Area = polyarea(shape1(1,shape1bnd), shape1(2,shape1bnd));

	shape2bnd = boundary(shape2(1:2,:)', shrinkFactor);
	shape2Area = polyarea(shape2(1,shape2bnd), shape2(2,shape2bnd));

	% calling polybool on the point sets independently causes problems
	% using the boundary to only consider polybool between contours is more accurate
	% operationP = callPolybool (operation, shape1, shape2);
	operationP = callPolybool (operation, shape1(:,shape1bnd), shape2(:,shape2bnd));

	if exist('debugMode','var') && debugMode == 1
		figure; hold all;
		showShape(shape1(1:2,:),'r', 0, [], shrinkFactor);
		showShape(shape2(1:2,:),'b', 0, [], shrinkFactor);
		showShape(operationP, 'k', 0, [], shrinkFactor);
		% keyboard
	end

	if (isempty(operationP))
		areaOut = 0; return;
	end

	% operationBnd = boundary(operationP(1:2,:)', shrinkFactor);
	% areaOut = polyarea(...
	% 	operationP(1,operationBnd),operationP(2,operationBnd));
	areaOut = polyarea(operationP(1,:),operationP(2,:));
	
	% keyboard
	if exist('debugMode','var') && debugMode == 1
		title(sprintf('shape = %.1f, new = %.1f, %s = %.1f', ...
			shape1Area, shape2Area, operation, areaOut));
	end

	% keyboard
end
% -----------------------------------------------------------------------------



function [operationP] = callPolybool (op, shape1, shape2)
	[xa, ya] = polybool(op, ...
		shape1(1,:), shape1(2,:), ...
		shape2(1,:), shape2(2,:));
	operationP = [xa(~isnan(xa)); ya(~isnan(ya))];
end
