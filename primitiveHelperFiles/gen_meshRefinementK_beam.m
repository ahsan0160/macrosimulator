% -----------------------------------------------------------------------------
% mesh refinement for stiffness using search on the mesh
%   if dataset is saved for optimal refinement, load it, apply to mesh, return
%   else
%     removes edges from selected unit of the primitive (spokes, or full hex)
%     and measure stiffness, then save to dataset
%     return the optimal refined mesh after search
% -----------------------------------------------------------------------------
%
% This script tests the T primitive by removing cells selectively and 
% simulating a small 3x3 array of "hexagons"
%
% (Used for Science Robotics 2017 MACRO design paper)
%

if ~exist('repTest', 'var') || repTest == 0
  clear global;
  set(0,'defaultlinelinewidth',4); close all; clear all; clf; clc;
  global_path_setup
  varNum = randi(2^9-1);
  varNum = 64;
end

% --------------------------------------
% Default values to run a trial
% --------------------------------------
controlAltCallback = @cb_bending;
% controlAltCallback = @cb_compressionAxial;
meshSizeX = 16; meshSizeY = 4;
actuationDir = 1; % for positive curl (anti-clockwise)
dt = 1; tFinal = 10;
dt = 1; tFinal = 5;

meshPrimitive = @primitive_tri;      

global cellProps; global globalTime; global outFolder;

% output print locations
% --------------------------------------------------------------------------
outFolder = 'primitiveOptTest'; detailedFigs = 0;

% common input parameters
% --------------------------------------------------------------------------
cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
debugMode = 0;
recordForMeasure = 1; measureTimes = linspace(1,100);

% --------------------------------------------------------------------------
% setup of test parameters
% --------------------------------------------------------------------------
seed = ceil(rand()*1000); rng(seed); % seed the random number generator

edgeMetric = @meshPrimitiveEdgeNodes;
[nm,eSet,eTheta,stPos,trPos,anch,fX,fY,fZ]= meshPrimitive(meshSizeX,meshSizeY);

nm = strcat('pOptTst_',nm); 
if isequal(controlAltCallback, @cb_bending) 
  if actuationDir(1) == 1
    nm = strcat(nm, 'pTh');
  else
    nm = strcat(nm, 'nTh');
  end
elseif isequal(controlAltCallback, @cb_compressionAxial)
  if actuationDir(1) == 1
    nm = strcat(nm, 'actX');
  else
    nm = strcat(nm, 'actY');
  end   
end

% --------------------------------------------------------------------------
% mesh refinement for stiffness
% --------------------------------------------------------------------------
nm = strcat(nm, '_var',num2str(varNum));
cla;
[A,Ae] = genAdjMats(eSet,size(stPos,2));
showNet(stPos,A,'r',1,'--');
[stPos,eSet,eTheta] = gen_mesh_refinement_variant(stPos,eSet,eTheta, varNum);
[A,Ae] = genAdjMats(eSet,size(stPos,2));
showNet(stPos,A,'b',2);

% keyboard
trPos = 0*stPos;
[anch, fX, fY, fZ, zEdges] = constraintsForCallbacks(...
  meshPrimitive, controlAltCallback, stPos, eSet, eTheta);

% --------------------------------------------------------------------------
% Run test
% --------------------------------------------------------------------------
[name,edgeSet,edgeTheta] = deal(nm, eSet, eTheta);
[startPos,targetPos] = deal(stPos, trPos);
[anchors,fixedInX,fixedInY,fixedInZ] = deal (anch, fX, fY, fZ);

globalTime = tic;
outFolder = nm; outFolder = strcat(num2str(globalTime),'_', nm);

% ----- MAIN CALL TO SIMULATOR ----- 
helper_runControllerSimulator

globalRunTime = toc(globalTime)

% --------------------------------------------------------------------------
% Data storage
% --------------------------------------------------------------------------
dataLog.randSeed = seed;
dataLog.globalRunTime = globalRunTime;
% record macroComponents into dataLog
for fn = fieldnames(macroComponents)'
  if strcmpi(fn,'netInputs')
    continue;
  end
  dataLog.(fn{1}) = macroComponents.(fn{1});
end

dataLogOutputFile = strcat(outFolder,'_dataLog.mat');

save(strcat('allOutputs/', outFolder, '/',dataLogOutputFile), 'dataLog');
save(strcat('allOutputsDataFolder/pOptTst/', dataLogOutputFile), 'dataLog');
fprintf(1, '>>dataLog saved to file without measurements\n');
