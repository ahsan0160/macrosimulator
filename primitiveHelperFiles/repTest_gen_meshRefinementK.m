% 
% This file collects the data for TMECH 17 primitives paper 
% extended from IROS 17 submitted on primitives)
% 
% This script uses the tmech17prim_testCompAndBend file to run the simulation
%   tmech17prim_testCompAndBend requires inputs about:
%     mesh size (X,Y)
%     mesh primitive
%     callback to use instead of controller
%     actuation direction for callback
%     run time
%     time step
%     recording time
%   Note NO measurements on the meshes are conducted by the tmech17prim file
% 
% (Used for TMECH 2017 primitives paper)
% 

clear all; %close all; clc;
clear global;
set(0,'defaultlinelinewidth',4);

repeatEntireDataCollection = 1;

for rep=repeatEntireDataCollection
  for variant = 472:2^9
    % Clear some of the variables
    close all; clf;
    clc; clear simulator; clear dataLog;
    global_path_setup; repTest = 1;

    varNum = variant;
    gen_meshRefinementK % run trial
  end
end

close all;