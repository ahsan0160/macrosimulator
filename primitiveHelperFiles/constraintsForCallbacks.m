% given a primitive mesh and callback, generates the appropriate constraints
% for MACRO construction
function [anch, fX, fY, fZ, zEdges] = constraintsForCallbacks (...
  primitive, callback, pos, edges, eTheta) 
  global primitiveInfo;

  [anch, fX, fY, fZ, zEdges] = deal([]);
  % ------------------------------------------------------------------------
  % CONDITIONALS SORTED BY CALLBACKS
  % ------------------------------------------------------------------------

  % AXIAL COMPRESSION: 
  %   node 1 is anchored
  %   left nodes are fixed in x, can slide in y
  %   bottom nodes are fixed in y, can slide in x
  if isequal(callback, @cb_compressionAxial) 
    anch = [1];
    fX = meshPrimitiveEdgeNodes (primitive, pos, edges, 'left');
    fY = meshPrimitiveEdgeNodes (primitive, pos, edges, 'bottom');


  % BENDING: 
  %   node 1 is anchored
  %   left nodes are fixed in x, can slide in y
  %   all edges BETWEEN the top layer and the "non-top layer" 
  %     are zEdges (disconnected)
  elseif isequal(callback, @cb_bending)
    anch = [1];
    fX = meshPrimitiveEdgeNodes (primitive, pos, edges, 'left');

    allNodes = 1:size(pos,2);
    topNodes = meshPrimitiveEdgeNodes(primitive, pos,edges,'top');    
    nonTopNodes = setdiff(1:size(pos,2),topNodes);

    minTopNodeY = min(pos(2,topNodes));
    nonTopNodesRejects = nonTopNodes(pos(2,nonTopNodes) > minTopNodeY);

    nonTopNodes = setdiff(nonTopNodes,nonTopNodesRejects);

    % showShape(pos,'g');
    % showShape(pos(:,topNodes),'r');
    % showShape(pos(:,nonTopNodes),'b');
    % keyboard
    
    idx1 = ismember(edges(1,:), topNodes); % edge start in topNodes
    idx2 = ismember(edges(2,:), nonTopNodes); % edge end in nonTopNodes
    idx3 = ismember(edges(1,:), nonTopNodes); % edge start in nonTopNodes
    idx4 = ismember(edges(2,:), topNodes); % edge end in topNodes
    zEdges = [];
    % connections between top and non-top nodes are z-edges
    zEdges = [zEdges edges(:,idx1 & idx2)];
    zEdges = [zEdges edges(:,idx3 & idx4)];

    % connections within non-top nodes are z-edges
    zEdges = [zEdges edges(:,idx3 & idx2)];
  end

end


































































