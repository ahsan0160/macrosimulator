function [outP, outE, outET] = reflectShape (shapeP, shapeE, shapeET, vect)
% -----------------------------------------------------------------------------
% given a shape and a vector, returns the shape reflected along the vector
% -----------------------------------------------------------------------------
	outP = shapeP;
	outE = shapeE;
	outET = shapeET;

	% reflect position set along specified vector
	v1 = vect(:,1); v2 = vect(:,2);
	for ii=1:size(shapeP,2)
		p = shapeP(:,ii);
		projP = projPointOnLine(p', [v1' v2'])';
		qq = p + 2*(projP - p);
		outP(:,ii) = qq;
	end

	% change angle of edges
	theta0 = atan2((v2(2)-v1(2)),(v2(1)-v1(1)));
	outET(3,:) = 2*theta0 - outET(3,:);

	% update node labels to make sure node 1 is at the bottom of the shape
	% and as far left as possible
	% outP
	% outE
	% round(outET)
	% figure(97);hold all; axis equal; 
	% showShape(outP);
	% [A,Ae] = genAdjMats(outE, 4); showNet(outP, A);
 % 	keyboard
	
	[outP, outE, outET] = updateNodeLabels (outP, outE, outET);
	
	% outP
	% outE
	% round(outET)
	% figure(98);hold all; axis equal; 
	% showShape(outP);
	% [A,Ae] = genAdjMats(outE, 4); showNet(outP, A);
 % 	keyboard

	[outP, outE, outET] = updateEdgeLabels (outP, outE, outET);

	% outP
	% outE
	% round(outET)
	% figure(99);hold all; axis equal; 
	% showShape(outP);
	% [A,Ae] = genAdjMats(outE, 4); showNet(outP, A);
 % 	keyboard
end
