function [outP, outE, outET, expectedTh] = heuristic_mesh_refinement_bendK ...
  (shapeP, shapeE, shapeET, direction, optPctRange)
% -----------------------------------------------------------------------------
% heuristic mesh refinement for strain
%   removes edges that would optimize for strain in a given direction
% -----------------------------------------------------------------------------
  if ~exist('direction','var')
    direction = [1 0];
  end
  if ~exist('optPctRange','var')
    optPctRange = [0.9 1];
  end
  DEBUG = 0;
  
  relationsFile = 'primitiveHelperFiles/meshRefine_bendStrokeRelations.mat';
  if ~exist(relationsFile,'file')
    keyboard
    return
  else
    load(relationsFile);
  end

  figure(777); 
  % subplot(1,2,1);  hold all; 
  boxplot(reordered_bm_strokeTh, reordered_numCellsRemoved,'sym','r.');
  set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
  xlabel('Number of cells removed from refinement unit');
  ylabel(sprintf('K_{th}'));
  title(sprintf('Stiffness K_{th} change by mesh degradation'));

  relevantTh = reordered_bm_strokeTh;

  lims = [min(relevantTh) max(relevantTh)];
  dif = lims(2)-lims(1);

  effLims(1) = optPctRange(1)*dif + lims(1);
  effLims(2) = optPctRange(2)*dif + lims(1);
  % effLims

  idx = find(relevantTh >= effLims(1) & relevantTh <= effLims(2));
  [m,f] = mode(reordered_actualVariantIndices(idx));

  thForM = relevantTh(idx(find(m == reordered_actualVariantIndices(idx))));
  meanthForM = mean(thForM,'omitnan');
  stdthForM = std(thForM,'omitnan');
  
  expectedTh.mean = meanthForM;
  expectedTh.std = stdthForM;
  expectedTh.name = sprintf('Theta: %.2f +/- %.2f', meanthForM, stdthForM);

  if DEBUG
    figure(456);
    % hist(reordered_actualVariantIndices(idx));
    % [n,e] = histcounts(reordered_actualVariantIndices(idx),...
    %   'Normalization','countdensity');

  end

  [outP, outE, outET] = gen_mesh_refinement_variant (shapeP,shapeE,shapeET,m);

end
% -----------------------------------------------------------------------------




