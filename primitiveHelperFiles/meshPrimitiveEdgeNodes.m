
% --------------------------------------------------------------------------
% Uses positions of nodes and edge connectivity information to find the
% left hand boundary of a node set
% SIMILAR TO pointSetEdgeNodes
%   BUT specifice to mesh primitives
% --------------------------------------------------------------------------
function [nodeLayer] = meshPrimitiveEdgeNodes (...
  primitive, pos, edges, side)
  global primtiveInfo;
  nodeLayer = [];

  p1 = pos(:,edges(1,:));
  p2 = pos(:,edges(2,:));
  maxInterNodeDist = max(sqrt(sum((p2-p1).^2,1)));
  errDist = maxInterNodeDist * 0.25;

  numNodes = size(pos,2);
  numEdges = size(edges,2);

  left = strcmpi(side,'left'); right = strcmpi(side,'right');
  top = strcmpi(side,'top'); bottom = strcmpi(side,'bottom');


  psTm = 0.5;
  psTm = 1.5;

  % ----------------------------------------------------
  %
  % PRIMITIVE rect
  %
  % ----------------------------------------------------
  if isequal(     primitive,  @primitive_rect)
    nodeLayer = pointSetEdgeNodes (pos, edges, side);
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hex
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hex)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 0.5);
    else
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 1);
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE tri
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_tri)
    nodeLayer = pointSetEdgeNodes (pos, edges, side, 0.5);
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hyb3t2s
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hyb3t2s)
    nodeLayer = pointSetEdgeNodes (pos, edges, side, 0.75);
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hyb2t2h
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hyb2t2h)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 1);
    elseif bottom || top
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 0.25);
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hybs2o
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hybs2o)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 2);
      rightLayer = filterNodes(pos, nodeLayer, 'right', 0, errDist);
      leftLayer = filterNodes(pos, nodeLayer, 'left', 0, errDist);
      layerBetween = setdiff(nodeLayer,rightLayer);
      layerBetween = setdiff(layerBetween,leftLayer);
      nodeLayer = setdiff (nodeLayer, layerBetween);
    elseif bottom || top
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 2);
      topLayer = filterNodes(pos, nodeLayer, 'top', 0, errDist);
      bottomLayer = filterNodes(pos, nodeLayer, 'bottom', 0, errDist);
      layerBetween = setdiff(nodeLayer,topLayer);
      layerBetween = setdiff(layerBetween,bottomLayer);
      nodeLayer = setdiff (nodeLayer, layerBetween);
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hyb4th
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hyb4th)
    nodeLayer = 1:numNodes;

    % slice up the node layer into unique-ish slices
    if left || right
      slices = uniquetol(pos(2,nodeLayer)',0.01);
    elseif bottom || top
      slices = uniquetol(pos(1,nodeLayer)',0.01);
    end

    % for each slice, find required extremum and add to selection
    selection = [];
    for ii=1:length(slices)
      sliceVal = slices(ii);
      if left || right
        sliceNds = nodeLayer(...
          ( (pos(2,nodeLayer) <= sliceVal+errDist) & ...
            (pos(2,nodeLayer) >= sliceVal-errDist) ) );
        if left
          relevantNode = sliceNds(pos(1,sliceNds) <= min(pos(1,sliceNds)));
        elseif right
          relevantNode = sliceNds(pos(1,sliceNds) >= max(pos(1,sliceNds)));
        end

      elseif bottom || top
        sliceNds = nodeLayer(...
          ( (pos(1,nodeLayer) <= sliceVal+errDist) & ...
            (pos(1,nodeLayer) >= sliceVal-errDist) ) );
        if bottom
          % slices left of (node 1X - one maxInterNodeDist) is ignored
          if sliceVal < pos(1,1) - maxInterNodeDist - errDist
            continue
          end
          relevantNode = sliceNds(pos(2,sliceNds) <= min(pos(2,sliceNds)));
        elseif top
          % slices right of (topRightNodeX + one maxInterNodeDist) is ignored
          topLayer = filterNodes(pos, [], 'top', 0, errDist);
          topRightNode = topLayer(pos(1,topLayer) >= max(pos(1,topLayer)));
          if sliceVal > pos(1,topRightNode) + maxInterNodeDist + errDist
            continue
          end
          relevantNode = sliceNds(pos(2,sliceNds) >= max(pos(2,sliceNds)));
        end
      end
      selection = [selection relevantNode];
    end
    nodeLayer = selection;
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hybtshs
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hybtshs)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 1.5);
      slices = uniquetol(pos(1,nodeLayer)',0.01);

      % remove nodes in a specific slice
      if left
        slicesToRemove = 3;
      elseif right
        slicesToRemove = 2;
      end
      for jj=slicesToRemove
        sliceNds = nodeLayer(...
          ( (pos(1,nodeLayer) <= slices(jj)+errDist) & ...
            (pos(1,nodeLayer) >= slices(jj)-errDist) ) );
        nodeLayer = setdiff(nodeLayer, sliceNds);
      end

      if left
        rightLayer = filterNodes(pos, nodeLayer, 'right', 0, errDist);
        topMostNode = filterNodes(pos, rightLayer, 'top', 1, errDist);
        bottomMostNode = filterNodes(pos, rightLayer, 'bottom', 1, errDist);
        rightLayerMinusMiddle = setdiff (rightLayer,topMostNode);
        rightLayerMinusMiddle = setdiff (rightLayerMinusMiddle,bottomMostNode);
        nodeLayer = setdiff (nodeLayer, rightLayerMinusMiddle);
      elseif right
        leftLayer = filterNodes(pos, nodeLayer, 'left', 0, errDist);
        topMostNode = filterNodes(pos, leftLayer, 'top', 1, errDist);
        bottomMostNode = filterNodes(pos, leftLayer, 'bottom', 1, errDist);
        leftLayerMinusMiddle = setdiff (leftLayer,topMostNode);
        leftLayerMinusMiddle = setdiff (leftLayerMinusMiddle,bottomMostNode);
        nodeLayer = setdiff (nodeLayer, leftLayerMinusMiddle);
      end

    elseif top || bottom
      % remove nodes in a specific slice
      if top
        nodeLayer = pointSetEdgeNodes (pos, edges, side, 1.5);
        slices = uniquetol(pos(2,nodeLayer)',0.01);
        slicesToRemove = 2;
      elseif bottom
        nodeLayer = pointSetEdgeNodes (pos, edges, side, 1.5);
        slices = uniquetol(pos(2,nodeLayer)',0.01);
        slicesToRemove = 3;
      end
      for jj=slicesToRemove
        sliceNds = nodeLayer(...
          ( (pos(2,nodeLayer) <= slices(jj)+errDist) & ...
            (pos(2,nodeLayer) >= slices(jj)-errDist) ) );
        nodeLayer = setdiff(nodeLayer, sliceNds);
      end
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hybshd
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hybshd)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 1.5);

    elseif top || bottom
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 3.5);

      % pick the top of each vertical slice IN THESE NODES
      slices = uniquetol(pos(1,nodeLayer)',0.001);
      selection = [];
      for ii=1:length(slices)
        sliceVal = slices(ii);
        sliceNds = nodeLayer(...
          ( (pos(1,nodeLayer) <= sliceVal+errDist) & ...
            (pos(1,nodeLayer) >= sliceVal-errDist) ) );
        if top
          relevantNode = sliceNds(pos(2,sliceNds) >= max(pos(2,sliceNds)));
        elseif bottom
          relevantNode = sliceNds(pos(2,sliceNds) <= min(pos(2,sliceNds)));
        end
        selection = [selection relevantNode];
      end
      nodeLayer = selection;
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hybt2d
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hybt2d)
    if left || right
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 2.5);
    elseif bottom || top
      nodeLayer = pointSetEdgeNodes (pos, edges, side, 1.5);
    end
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  % ----------------------------------------------------
  %
  % PRIMITIVE hyb2tsts
  %
  % ----------------------------------------------------
  elseif isequal( primitive,  @primitive_hyb2tsts)
    nodeLayer = pointSetEdgeNodes (pos, edges, side, 0.8);
    % hh = showShape(pos(:,nodeLayer),'b'); pause(psTm);

  end

  if exist('hh','var')
    delete(hh);
  end
end



function nd = filterNodes (pos, selection, side, numNodes, errDist)
  if numNodes == 0
    numNodes = 1e10; % get all the filtered nodes
  end
  if isempty(selection)
    selection = 1:size(pos,2); % use all nodes in pos
  end
  if strcmpi(side,'left')
    nodeIdx = find(pos(1,selection) <= min(pos(1,selection)+errDist), ...
      numNodes);
  elseif strcmpi(side,'bottom')
    nodeIdx = find(pos(2,selection) <= min(pos(2,selection)+errDist), ...
      numNodes);
  elseif strcmpi(side,'right')
    nodeIdx = find(pos(1,selection) >= max(pos(1,selection)-errDist), ...
      numNodes);
  elseif strcmpi(side,'top')
    nodeIdx = find(pos(2,selection) >= max(pos(2,selection)-errDist), ...
      numNodes);
  end
  % keyboard
  nd = selection(nodeIdx);
end
