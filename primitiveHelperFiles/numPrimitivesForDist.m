% a set of mesh sizing functions for each primitive
% given a dimension in x, function returns number of primitive units needed
% to tile that distance

% direction [1 0] = x-
% direction [0 1] = y- 
function num = numPrimitivesForDist (primitive, len, direction) 
  % keyboard
  global primitiveInfo;
  % keyboard

  if isequal(primitive, @primitive_rect)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nRectX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nRectY(len);
    end
  elseif isequal(primitive,@primitive_truss)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nTrussX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nTrussY(len);
    end
  elseif isequal(primitive,@primitive_hex)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHexX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHexY(len);
    end
  elseif isequal(primitive,@primitive_tri)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nTriX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nTriY(len);
    end
  elseif isequal(primitive,@primitive_hyb3t2s)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHyb3t2sX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHyb3t2sY(len);
    end
  elseif isequal(primitive,@primitive_hyb2t2h)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHyb2t2hX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHyb2t2hY(len);
    end
  elseif isequal(primitive,@primitive_hybs2o)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHybs2oX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHybs2oY(len);
    end
  elseif isequal(primitive,@primitive_hyb4th)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHyb4thX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHyb4thY(len);
    end
  elseif isequal(primitive,@primitive_hybtshs)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHybtshsX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHybtshsY(len);
    end
  elseif isequal(primitive,@primitive_hybshd)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHybshdX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHybshdY(len);
    end
  elseif isequal(primitive,@primitive_hybt2d)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHybt2dX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHybt2dY(len);
    end
  elseif isequal(primitive,@primitive_hyb2tsts)
    if direction(1) == 1 && direction(2) == 0
      num = primitiveInfo.nHyb2tstsX(len); 
    elseif direction(1) == 0 && direction(2) == 1
      num = primitiveInfo.nHyb2tstsY(len);
    end
  else
    fprintf(2, '\t*** BAD PRIMITIVE HANDLE ***\n');
    keyboard;
  end

end