function [outP, outE, outET] = shapeAdditionCleanup ...
  (shapeP, shapeE, shapeET, newP, newE, newET)
% -----------------------------------------------------------------------------
% add new shape to old shape
% logistics function
%   handles the merging of the node sets
%   removes duplicate edges
%   removes duplicate nodes
%   reindexes nodes to ensure nodeset is labeled n_1...n_N for N nodes
% acts as a computational magnet between the new shape and old shape
% -----------------------------------------------------------------------------
  nn = size(shapeP,2); newE = newE + nn;
  tmpP = [shapeP newP]; 
  tmpE = [shapeE newE];
  tmpET = [shapeET newET];

  rownorm = @(X) sqrt(sum((X).^2,2));
  magnetTolerance = 0.2;

  % parse by new edges
  for ii=1:size(newE,2)

    % for each new edge, consider both end points
    for kk=1:2

      % for each end point n1, n2 find if any other point 
      % in the combined shape are within 10% edgelength distance
      ni = newE(kk,ii); tt = repmat(tmpP(:,ni), 1, size(tmpP,2));
      dd = tmpP-tt; dd(:,ni) = 1; % distance to self at least 1
      nj = (rownorm(dd') < magnetTolerance);
 
      % if found node n_j, merge this node with node from original shape 
      if any(nj) 
        nj = find(nj); % get the index of the node
        % fprintf(2,'replacing node %d with %d\n', ni,nj);

        % remove this end point n_i from point set
        % update edgeSet to replace n_i with n_j 
        tmpE(1,tmpE(1,:)==ni) = nj; tmpE(2,tmpE(2,:)==ni) = nj;
        tmpET(1,tmpET(1,:)==ni) = nj; tmpET(2,tmpET(2,:)==ni) = nj;
        newE(1,newE(1,:)==ni) = nj; newE(2,newE(2,:)==ni) = nj;
        newET(1,newET(1,:)==ni) = nj; newET(2,newET(2,:)==ni) = nj;

        tmpP(:,ni) = [inf inf];
      end
    end
  end

  % remove duplicate edges
  for ii=1:size(tmpE,2) 
    ee = tmpE(:,ii); eArray = repmat(ee,1,size(tmpE,2));
    dif = eArray - tmpE; ind = rownorm(dif') < eps;
    ind(1:ii) = 0; ind=find(ind); 
    tmpE(1,ind) = inf;
    tmpET(1,ind) = inf;

    ee = flipud(ee); eArray = repmat(ee,1,size(tmpE,2));
    dif = eArray - tmpE; ind = rownorm(dif') < eps;
    ind(1:ii) = 0; ind=find(ind); 
    tmpE(1,ind) = inf;
    tmpET(1,ind) = inf;
  end
  tmpE(:,tmpE(1,:)==inf) = [];
  tmpET(:,tmpET(1,:)==inf) = [];

  % remove marked nodes
  tmpP(:,tmpP(1,:)==inf) = [];
  [outP, outE, outET] = updateEdgeLabels(tmpP, tmpE, tmpET);

end
% -----------------------------------------------------------------------------




