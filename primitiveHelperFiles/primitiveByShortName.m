
% --------------------------------------------------------------------------
% Returns primitive handle for a given primitive short-name
% --------------------------------------------------------------------------
function [meshPrimitive, matchSt, matchFin] = primitiveByShortName (...
  nameStr, outputType)

  global primitiveInfo;

  if ~exist('outputType','var')
    outputType = 'l';
  end

  meshPrimitive = [];
  for idx = 1:length(primitiveInfo.pShortNames)
    aa = strfind(nameStr, primitiveInfo.pShortNames{idx});
    if ~isempty( aa )
      if strcmpi(outputType,'l')
        meshPrimitive = primitiveInfo.pHandle{idx};
      elseif strcmpi(outputType,'s')
        meshPrimitive = primitiveInfo.pShortNames{idx};
      elseif strcmpi(outputType,'vs')
        meshPrimitive = primitiveInfo.pOutNames{idx};
      end
      matchSt = aa;
      matchFin = aa+length(primitiveInfo.pShortNames{idx})-1;
      return
    end
  end
end

