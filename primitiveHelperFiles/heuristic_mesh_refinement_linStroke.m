function [outP, outE, outET, expectedStroke] = heuristic_mesh_refinement_linStroke ...
  (shapeP, shapeE, shapeET, direction, optPctRange)
% -----------------------------------------------------------------------------
% heuristic mesh refinement for strain
%   removes edges that would optimize for strain in a given direction
% -----------------------------------------------------------------------------
  if ~exist('direction','var')
    direction = [1 0];
  end
  if ~exist('optPctRange','var')
    optPctRange = 1;
  end

  DEBUG = 0;
  nn = size(shapeP,2); 
  [tmpP, tmpE, tmpET] = deal(shapeP, shapeE, shapeET); 

  rownorm = @(X) sqrt(sum((X).^2,2));
  alignmentThreshold = 0.1; % top 10% or bottom 10%
  % alignmentThreshold = 0; % top 10% or bottom 10%

  allNodes = 1:size(tmpP,2);
  bndNodes = boundary(tmpP', 1);
  intNodes = setdiff (allNodes, bndNodes);

  if DEBUG 
    showShape(tmpP, 'r');
    showShape(tmpP(:,intNodes),'b');
  end

  toVisit = intNodes;
  edgesToRemove = [];
  for ii=1:length(toVisit)
    visiting = toVisit(ii);
    [ix,iy] = find(tmpE==visiting);
    neighbors = [tmpE(1,iy(ix==2)) tmpE(2,iy(ix==1))];
    if DEBUG
      showShape(tmpP(:,visiting),'k');
      showShape(tmpP(:,neighbors), 'g');
    end

    st = tmpP(:,visiting);
    projDist = [];
    for jj = 1:length(neighbors)
      fin = tmpP(:,neighbors(jj));
      vect = fin-st;
      projDist(jj) = abs(dot(vect, direction));
    end

    if optPctRange == 1
      optProj = max(projDist);
      idx = find(projDist < (1-alignmentThreshold)*optProj);
    elseif optPctRange == 0
      optProj = min(projDist);
      idx = find(projDist > (1+alignmentThreshold)*optProj);
    end
    selectedEdges = [repmat(visiting,1,length(idx)); neighbors(idx)];
    edgesToRemove = [edgesToRemove selectedEdges];        
  end

  % remove selected
  for ii=1:size(edgesToRemove,2) 
    ee = edgesToRemove(:,ii); eArray = repmat(ee,1,size(tmpE,2));
    dif = eArray - tmpE; ind = rownorm(dif') < eps;
    % ind(1:ii) = 0; 
    ind=find(ind); 
    tmpE(1,ind) = inf;
    tmpET(1,ind) = inf;

    ee = flipud(ee); eArray = repmat(ee,1,size(tmpE,2));
    dif = eArray - tmpE; ind = rownorm(dif') < eps;
    ind(1:ii) = 0; ind=find(ind); 
    tmpE(1,ind) = inf;
    tmpET(1,ind) = inf;
  end
  tmpE(:,tmpE(1,:)==inf) = [];
  tmpET(:,tmpET(1,:)==inf) = [];


  meanStroke = optPctRange;
  stdStroke = std(optPctRange);

  expectedStroke.meanStroke = meanStroke;
  expectedStroke.stdStroke = stdStroke;
  expectedStroke.name = sprintf('Stroke: %.2f +/- %.2f',meanStroke,stdStroke);

  if DEBUG
    figure;
    showShape(tmpP,'k',1,tmpE,1);
    keyboard
  end

  [outP, outE, outET] = updateEdgeLabels(tmpP, tmpE, tmpET);

end
% -----------------------------------------------------------------------------




