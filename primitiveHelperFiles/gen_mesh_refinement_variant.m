function [outP, outE, outET] = gen_mesh_refinement_variant ...
  (shapeP, shapeE, shapeET, varNum)
% -----------------------------------------------------------------------------
% mesh refinement variant generation for stiffness measurement
% -----------------------------------------------------------------------------
  if ~exist('varNum','var')
    varNum = randi(2^9-1);
  end

  DEBUG = 0;
  nn = size(shapeP,2); 
  [tmpP, tmpE, tmpET] = deal(shapeP, shapeE, shapeET); 

  rownorm = @(X) sqrt(sum((X).^2,2));
  alignmentThreshold = 0.1; % top 10% or bottom 10%

  allNodes = 1:size(tmpP,2);
  bndNodes = boundary(tmpP', 0.75);
  intNodes = setdiff (allNodes, bndNodes);

  if DEBUG 
    showShape(tmpP, 'r');
    showShape(tmpP(:,intNodes),'b');
    % pause(1);
  end
  newP = tmpP(:,intNodes);
  bnd2Nodes = boundary(newP', 0.75);
  bnd2Nodes = intNodes(bnd2Nodes);
  bndNodes = unique([bndNodes; bnd2Nodes']);
  int2Nodes = setdiff (intNodes, bnd2Nodes);

  intNodes = int2Nodes;

  if DEBUG 
    showShape(tmpP(:,intNodes),'g');
    % keyboard
  end


  % identify 'hexagon' shapes in the mesh 
  % store these 'hexagon's in easily accessible arrays
  % -------------------------------------------------------------------
  hexagonCenters = [];
  hexagonMemberNodes = []; % 7 nodes, [1 center, 6 boundary]
  toVisit = [];
  visited = [];

  toVisit(1) = intNodes(1); % initialize nodes to visit

  while (~isempty(toVisit)) 
    visiting = toVisit(1);
    toVisit(1) = [];
    visited = [visited visiting];

    neighbors = connectedTo(tmpE,visiting);
    skipAddingHex = 0;

    % boundary nodes are not considered
    if length(neighbors) < 6 
      continue
    end

    % check to make sure neighbors do not overlap with a previously selected
    % hexagon in more than 2 points 
    uu = [];
    for kk=1:length(neighbors)
      uu(:,:,kk) = (hexagonMemberNodes - neighbors(kk)) == 0;
    end
    uu = sum(uu,3); uu = sum(uu,2);
    if any(uu > 2)
      skipAddingHex = 1;
    end

    if ~skipAddingHex
      tmp = [visiting neighbors];
      hexagonMemberNodes = [hexagonMemberNodes; tmp];
    end

    % add relevant (internal) neighbors to search path
    internalNeighbors = setdiff(neighbors, bndNodes);
    internalNeighbors = setdiff(internalNeighbors,hexagonMemberNodes(:,1));
    toVisit = [toVisit internalNeighbors];
    toVisit = unique(toVisit);
    toVisit = setdiff(toVisit,visited); % dont visit again

    if DEBUG
      hexagonMemberNodes
      h1 = showShape(tmpP(:,visiting),'k');
      if skipAddingHex
        h2 = showShape(tmpP(:,neighbors),'c');
      else
        h2 = showShape(tmpP(:,neighbors),'g');
      end
      % keyboard
      delete(h1);
      delete(h2);
    end
    
  end

  if DEBUG
    figure;
    showShape(tmpP,'r')
    for yyy=1:size(hexagonMemberNodes,1)
      showShape(tmpP(:,hexagonMemberNodes(yyy,2:end)),'k');
    end
  end

  % there are two sets of edges in each hexagonal unit
  % the boundary of the hexagons can have edges removed in antipodal pairs
  % the internal edges of the hexagon (spokes) can be removed in isolation
  % generate a list of possible variations
  % -------------------------------------------------------------------

  % for each hexagon, label the edges as 1-6 (spokes) 7-12 (boundary)
  %       (5)_____7_____(4) 
  %         /\         /\
  %        /  \       /  \
  %       12   2     3    8
  %   (6)/___1__\(7)/___4__\(3)
  %      \      /   \      /
  %       11   6     5    9
  %        \  /       \  /
  %      (1)\/____10___\/(2)
  % 
  numVars = 2^9;
  variations = zeros(numVars,12);
  variations(:,1:9) = de2bi((0:2^9-1)');
  variations(:,10) = variations(:,7);
  variations(:,11) = variations(:,8);
  variations(:,12) = variations(:,9);

  if DEBUG
    % variations
  end


  % parse all hexagon units and modify the hexagons (uniformly) according
  % to a selected variation
  % -------------------------------------------------------------------
  strainOptDataset = struct();
  for ii=varNum
    edgesToRemove = variations(ii,:);

    % apply edgesToRemove on each hexagonMember in the mesh
    [refineP,refineE,refineET] = deal(tmpP,tmpE,tmpET);

    for jj=1:size(hexagonMemberNodes,1)
      hexN = hexagonMemberNodes(jj,:);
      hexP = refineP(:,hexN);

      topN = hexN(hexP(2,:)>= max(hexP(2,:)));
      bottomN = hexN(hexP(2,:)<= min(hexP(2,:)));
      topP = hexP(:,hexP(2,:)>= max(hexP(2,:)));
      bottomP = hexP(:,hexP(2,:)<= min(hexP(2,:)));

      % for each hexagon, identify the (nodeNum) in the actual hexagon
      new1N = bottomN(bottomP(1,:)<=min(bottomP(1,:)));
      new2N = bottomN(bottomP(1,:)>=max(bottomP(1,:))); 
      new3N = hexN(hexP(1,:)>=max(hexP(1,:))); % right most
      new4N = topN(topP(1,:)>=max(topP(1,:))); 
      new5N = topN(topP(1,:)<=min(topP(1,:)));
      new6N = hexN(hexP(1,:)<=min(hexP(1,:))); % left most
      new7N = hexN(1);

      % new1P = bottomP(:,bottomP(1,:)<=min(bottomP(1,:)));
      % new2P = bottomP(:,bottomP(1,:)>=max(bottomP(1,:))); 
      % new3P = hexP(:,hexP(1,:)>=max(hexP(1,:))); % right most
      % new4P = topP(:,topP(1,:)>=max(topP(1,:))); 
      % new5P = topP(:,topP(1,:)<=min(topP(1,:)));
      % new6P = hexP(:,hexP(1,:)<=min(hexP(1,:))); % left most
      % new7P = refineP(:,hexagon(1));

      % all 12 edges 
      %       (5)_____7_____(4) 
      %         /\         /\
      %        /  \       /  \
      %       12   2     3    8
      %   (6)/___1__\(7)/___4__\(3)
      %      \      /   \      /
      %       11   6     5    9
      %        \  /       \  /
      %      (1)\/____10___\/(2)
      % 
      allHexEdges = [
          [new6N; new7N], [new5N; new7N], [new4N; new7N],...
          [new3N; new7N], [new2N; new7N], [new1N; new7N],...
          [new4N; new5N], [new3N; new4N], [new2N; new3N],...
          [new1N; new2N], [new1N; new6N], [new5N; new6N],...
        ];

      % apply edgesToRemove logic on each (nodeNum)-(nodeNum) edge
      remE = allHexEdges(:,find(edgesToRemove));
    [refineP,refineE,refineET] = removeSelectedEdges(refineP,...
      refineE,refineET,remE);
      if DEBUG
        figure(88); clf;
        [A,Ae] = genAdjMats(tmpE,size(tmpP,2)); 
        showNet(tmpP,A,'r', 1,':');
        [A,Ae] = genAdjMats(refineE,size(refineP,2)); 
        showNet(refineP,A,'b',3);
        % keyboard
      end    
    end
  end

  if DEBUG
    figure(88); clf;
    [A,Ae] = genAdjMats(tmpE,size(tmpP,2)); 
    showNet(tmpP,A,'r', 1,':');
    [A,Ae] = genAdjMats(refineE,size(refineP,2)); 
    showNet(refineP,A,'b',3);
    keyboard
  end    

  % remove dangling edges
  %   edges with end points that connect to no other edges
  [A,Ae] = genAdjMats(refineE,size(refineP,2));
  danglingEdgeEnds = find(sum(abs(Ae),1) == 1);
  while ~isempty(danglingEdgeEnds)
    danglingEdges = [];
    for eEnd=danglingEdgeEnds
      [x, eIdx] = find(refineE == eEnd);
      danglingEdges = [danglingEdges refineE(:,eIdx)];
    end
    danglingEdges
    [refineP,refineE,refineET] = removeSelectedEdges(refineP,...
        refineE,refineET,danglingEdges);
  
    [A,Ae] = genAdjMats(refineE,size(refineP,2));
    danglingEdgeEnds = find(sum(abs(Ae),1) == 1);

    if DEBUG
      figure(88); clf;
      [A,Ae] = genAdjMats(tmpE,size(tmpP,2)); 
      showNet(tmpP,A,'r', 1,':');
      [A,Ae] = genAdjMats(refineE,size(refineP,2)); 
      showNet(refineP,A,'b',3);
      keyboard
    end
  end

  % remove dangling nodes
  %   nodes without any edges connecting to them
  danglingNodes = setdiff(1:size(refineP,2),unique(refineE(:)));
  refineP(:,danglingNodes) = [];

  [refineP,refineE,refineET] = updateEdgeLabels(refineP, refineE, refineET);

  if DEBUG
    figure(88); clf;
    [A,Ae] = genAdjMats(tmpE,size(tmpP,2)); 
    showNet(tmpP,A,'r', 1,':');
    [A,Ae] = genAdjMats(refineE,size(refineP,2)); 
    showNet(refineP,A,'b',3);
    keyboard
  end    

  [outP, outE, outET] = updateEdgeLabels(refineP, refineE, refineET);

  if DEBUG
    figure(88); clf;
    [A,Ae] = genAdjMats(tmpE,size(tmpP,2)); 
    showNet(tmpP,A,'r', 1,':');
    [A,Ae] = genAdjMats(outE,size(outP,2)); 
    showNet(outP,A,'b',3);
    keyboard
  end    
end
% -----------------------------------------------------------------------------



function [refineP,refineE,refineET] = removeSelectedEdges(tmpP,tmpE,tmpET,remE)
  [refineP,refineE,refineET] = deal(tmpP,tmpE,tmpET);

  rownorm = @(X) sqrt(sum((X).^2,2));

  % remove selected
  for ii=1:size(remE,2) 
    ee = remE(:,ii); eArray = repmat(ee,1,size(refineE,2));
    dif = eArray - refineE; ind = rownorm(dif') < eps;
    ind(1:ii) = 0; ind=find(ind); 
    refineE(1,ind) = inf;
    refineET(1,ind) = inf;

    ee = flipud(ee); eArray = repmat(ee,1,size(refineE,2));
    dif = eArray - refineE; ind = rownorm(dif') < eps;
    ind(1:ii) = 0; ind=find(ind); 
    refineE(1,ind) = inf;
    refineET(1,ind) = inf;
  end
  refineE(:,refineE(1,:)==inf) = [];
  refineET(:,refineET(1,:)==inf) = [];


end