function [outP, outE, outET, expectedK] = heuristic_mesh_refinement_linK ...
  (shapeP, shapeE, shapeET, direction, optPctRange)
% -----------------------------------------------------------------------------
% heuristic mesh refinement for strain
%   removes edges that would optimize for strain in a given direction
% -----------------------------------------------------------------------------
  if ~exist('direction','var')
    direction = [1 0];
  end
  if ~exist('optPctRange','var')
    optPctRange = [0.9 1];
  end
  DEBUG = 0;
  
  relationsFile = 'primitiveHelperFiles/meshRefine_linKrelations.mat';
  if ~exist(relationsFile,'file')
    keyboard
    return
  else
    load(relationsFile);
  end

  figure(777); 
  subplot(1,2,1);  hold all; 
  boxplot(reordered_sq_kX, reordered_numCellsRemoved,'sym','r.');
  set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
  xlabel('Number of cells removed from refinement unit');
  ylabel(sprintf('K_x'));
  title(sprintf('Stiffness K_x change by mesh degradation'));

  subplot(1,2,2);  hold all; 
  boxplot(reordered_sq_kY, reordered_numCellsRemoved,'sym','r.');
  set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
  xlabel('Number of cells removed from refinement unit');
  ylabel(sprintf('K_y'));
  title(sprintf('Stiffness K_y change by mesh degradation'));

  if direction(1) == 1
    relevantK = reordered_sq_kX;
  elseif direction(2) == 1
    relevantK = reordered_sq_kY;
  end

  lims = [min(relevantK) max(relevantK)];
  dif = lims(2)-lims(1);

  effLims(1) = optPctRange(1)*dif + lims(1);
  effLims(2) = optPctRange(2)*dif + lims(1);
  % effLims

  idx = find(relevantK >= effLims(1) & relevantK <= effLims(2));
  [m,f] = mode(reordered_actualVariantIndices(idx));

  kForM = relevantK(idx(find(m == reordered_actualVariantIndices(idx))));
  meankForM = mean(kForM,'omitnan');
  stdkForM = std(kForM,'omitnan');
  
  expectedK.mean = meankForM;
  expectedK.std = stdkForM;
  expectedK.name = sprintf('K: %.2f +/- %.2f', meankForM, stdkForM);

  if DEBUG
    figure(456);
    % hist(reordered_actualVariantIndices(idx));
    % [n,e] = histcounts(reordered_actualVariantIndices(idx),...
    %   'Normalization','countdensity');

  end

  [outP, outE, outET] = gen_mesh_refinement_variant (shapeP,shapeE,shapeET,m);

end
% -----------------------------------------------------------------------------




