%
% This function takes a mesh as a bulk object and returns
% the tightes bounding box of the object after appropriate alignment
% NOTE: if the experiment is run with bottom nodes fixed in Y
%     and left nodes fixed in X, no alignment needs to be done
% NOTE 2: no-alignment-required assumption is made for the moment
%
% Updated: Feb 23, 2017
%
function [boundingBox] = measure_meshBoundDims (pos, edges, nVirtualNodes)

  shrinkFactor = 0.001; % convex hull
  if ~exist('nVirtualNodes','var')
    nVirtualNodes = 10;
  end
  densePos = shapeToPointCloud(pos, edges, nVirtualNodes);
  bndNodes = boundary(densePos(1:2,:)',shrinkFactor);

  % figure;
  % showShape(pos);
  % showShape(densePos(:,bndNodes));
  % keyboard

  alignedDensePos = alignToShrinkBBox(densePos);
  boundingBox = boundingBoxFromPointCloud(alignedDensePos);
end


% NOT NECESSARY UNTIL EXPERIMENTS ARE CHANGED TO NEED
% PRE-ALIGNMENT
function [alignedPos] = alignToShrinkBBox (pos)
  alignedPos = pos;
end

% calculates the bounding box for a given point cloud
% outputs bbox of the format: [leftMostX, leftMostY, width, height]
function [bbox] = boundingBoxFromPointCloud (pos)
  leftMostX = min(pos(1,:));
  leftMostY = min(pos(2,:));
  boxWidth = max(pos(1,:)) - min(pos(1,:));
  boxHeight = max(pos(2,:)) - min(pos(2,:));

  bbox = [leftMostX, leftMostY, boxWidth, boxHeight];
end

