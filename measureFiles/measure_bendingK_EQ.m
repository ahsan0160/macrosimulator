% 
% This function uses the MACRO eq optimization to compute a bulk 
% bending stiffness for a given mesh
% 
% dirVect = +/- 1: +1 = pos theta dir, -1 = neg theta dir
function [overallK, overallE] = measure_bendingK_EQ (...
    macro, dirVect, bndThreshold, timeString) 
  set(0,'defaultlinelinewidth',1);

  debugCall = 1;

  if ~exist('bndThreshold','var')
    bndThreshold = 16.75;
  end
  edges = macro.edgeSet; currentEqPos = macro.nodePos;

  % force is applied to the tip of sample to the right
  % forceVector = 0.25*[0 dirVect];   % apply a 0.25N force 
  forceVector = 1*[0 dirVect];   % apply a 0.25N force 
  forceNodes = pointSetEdgeNodes (currentEqPos, edges, 'right', bndThreshold);

  [A, ~] = genAdjMats(edges, macro.nodeCount);
  steps = 3;

  reps = 10;
  overallK = [];
  overallE = [];

  for kk=1:reps
    macroClone = copy(macro);
    macroClone.delUnderExtForceLim = 5000;

    % add a bit of noise to the macroClone node pos: 20 dB = 1% pos error
    macroClone.nodePos = awgn(currentEqPos, 20); 

    for intermSteps=1:steps
      ff = forceVector; %/steps;
      newEqPos = macroClone.minEnergyPos(debugCall, ff, forceNodes);
      macroClone.nodePos = newEqPos;
      maxBeamHeight = max( newEqPos(2,:) );

      if debugCall
        outF=figure(56); clf; hold all; 
        showNet(currentEqPos,A,'b'); showNet(newEqPos,A,'r');
        global outFolder;
        if ~isempty('outFolder')
          printFig(outF, ...
            strcat(outFolder, '/msr_', timeString, '/'), ...
            strcat('msr_',num2str(intermSteps)));
        end
        pause(0.1);
      end
    end
    [K,E,del,P] = computeBendK(currentEqPos, newEqPos, edges, forceVector(2));

    overallK = [overallK; K]
    overallE = [overallE; E]

    if debugCall
      figure(56); clf; hold all;
      showNet(currentEqPos,A,'b'); showNet(newEqPos,A,'r');
    end

    clear macroClone;
  end

  overallK
  overallE
end



% Given a pair of point sets represending a beam sample,
%  with stPos = straight beam
%  and finPos = curved beam
% Computes the bending stiffness using euler-bernoulli 
%  1st order approximation
function [K, E, del, P] = computeBendK (stPos, finPos, edges, force) 

  % load a bunch of stuff
  global cellProps;
  if ~exist('cellProps','var')
    cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
  end

  % obtain boundary node listings
  % bndThreshold = 16.75; 
  bndThreshold = 25; 
  topNodes = pointSetEdgeNodes (stPos, edges, 'top', bndThreshold);
  bottomNodes = pointSetEdgeNodes (stPos, edges, 'bottom', bndThreshold);
  leftNodes = pointSetEdgeNodes (stPos, edges, 'left', bndThreshold);
  rightNodes = pointSetEdgeNodes (stPos, edges, 'right', bndThreshold);

  % compute some geometric parameters of the beam from stPos
  L = max(stPos(1,rightNodes)) - min(stPos(1,leftNodes));
  b = cellProps.Wcell;
  h = max(stPos(2,topNodes)) - min(stPos(2,bottomNodes));

  topRightNode = rightNodes(stPos(2,rightNodes) == max(stPos(2,rightNodes)));
  del = finPos(2,topRightNode) - stPos(2,topRightNode);

  P = force;
  K = P/del;

  E = K * L^3 * 4 / (b*h^3);

end 