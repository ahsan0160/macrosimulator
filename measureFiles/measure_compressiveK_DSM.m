% 
% This function uses DSM method to compute a generalized stiffness matrix
% for a given mesh (specified by pos and edges)
% It also returns a stiffness in the direction specified by dirVect
% 
% 
function [directionalK, deformation] = measure_compressiveK_DSM (...
    macro, dirVect) 
  set(0,'defaultlinelinewidth',1);
  generalizedK = ones(size(macro.edgeSet,2));
  directionalK = 1;
  deformation = [0; 0];

  % ------------------------------------------------------------------------
  % Clone the MACRO mesh itself (pos and edges)
  % ------------------------------------------------------------------------
  nodePos = macro.nodePos(1:2,:);
  nodePos = awgn(nodePos, 30);
  edges = macro.edgeSet;

  % ------------------------------------------------------------------------
  % Setup for directional force exertion 
  % Fix boundaries, Add forces to the other boundary
  % ------------------------------------------------------------------------
  if dirVect(1) == 1 % x-axis testing
    [leftBnd] = pointSetEdgeNodes(nodePos,edges,'left', 0.01);
    [rightBnd] = pointSetEdgeNodes(nodePos,edges,'right', 0.01);

    fixedNodes = leftBnd;
    measureSide = 'right';
    forces = ... % 1N to -x
      [rightBnd' -1*ones(size(rightBnd')) 0*ones(size(rightBnd'))]; 

  elseif dirVect(2) == 1 % y-axis testing
    [bottomBnd] = pointSetEdgeNodes(nodePos,edges,'bottom', 0.01);
    [topBnd] = pointSetEdgeNodes(nodePos,edges,'top', 0.01);

    measureSide = 'top';
    fixedNodes = bottomBnd;
    forces = ... % 1N to -y
     [topBnd' 0*ones(size(topBnd')) -1*ones(size(topBnd'))]; 

  else % something is wrong
    disp('BAD INPUTS TO measure_compressiveStiffness'); return;
  end

  % ------------------------------------------------------------------------
  % Calculate stiffnesses 
  % ------------------------------------------------------------------------
  generalizedK = computeGlobalStiffness(macro, nodePos, edges);
  directionalK = computeBulkStiffness(...
    generalizedK, nodePos, edges, fixedNodes, forces, measureSide);

end



function dirK = computeBulkStiffness (...
  generalizedK, nodePos, edges, fixed, forces, measureSide)
  
  dirK = 1;

  % add effects of boundary conditions
  reduced_kk = generalizedK;

  % first remove the known displacements (fixed)
  for i=1:length(fixed)       % reduce stiffness mat by applying constraints
    fixedNodeX = 2*fixed(i)-1;  % index of fixed node in master
    fixedNodeY = 2*fixed(i);    %   stiffness eqn
    
    % removing rows of x and y coordinates, adding 1 to node index diagonal
    reduced_kk (fixedNodeX, :) = zeros(1,size(reduced_kk,2));
    reduced_kk (fixedNodeY, :) = zeros(1,size(reduced_kk,2));
    reduced_kk (fixedNodeX, fixedNodeX) = 1;
    reduced_kk (fixedNodeY, fixedNodeY) = 1;
  end
  reduced_kk;

  % create force vector
  forceVect = zeros(size(reduced_kk,1), 1);
  for i=1:size(forces, 1)
      nodeNum = forces(i,1); forceX = forces(i,2); forceY = forces(i,3);
      forceVect(2*nodeNum-1) = forceX; forceVect(2*nodeNum) = forceY;
  end

  % ------------------------------------------------------------------------
  % Calculate deformation from stiffness matrix
  % ------------------------------------------------------------------------
  try
    u = pinv(reduced_kk)*forceVect;
  catch
    disp('ERROR in deformation calculation');
    keyboard
  end
  deformation = [u(1:2:end) u(2:2:end)]'; % mm

  %% final solution display
  deformedPos = nodePos + deformation;

  numEdge = size(edges,2); numNode = size(nodePos,2);
  [A, Ae] = genAdjMats (edges, numNode);
  % showNet(nodePos, A, 'b');
  % showNet(deformedPos, A, 'r');
  % for i=1:numNode  
  %   text( nodePos(1,i)+3, nodePos(2, i)+3, num2str(i) );
  % end

  msrBnd = pointSetEdgeNodes(nodePos,edges,measureSide, 0.01);

  bulkForce = mean(abs(forceVect(forceVect~=0)));
  if strcmpi(measureSide,'right')
    bulkDeformation = mean(nodePos(1,msrBnd) - deformedPos(1,msrBnd));
  else
    bulkDeformation = mean(nodePos(2,msrBnd) - deformedPos(2,msrBnd));
  end

  dirK = bulkForce/bulkDeformation;

end




function generalizedK = computeGlobalStiffness (macro, nodePos, edges)
  numEdge = size(edges,2); numNode = size(nodePos,2);

  % ------------------------------------------------------------------------
  % Compute the Global Stiffness Matrix from the pos-edge matrices and some 
  %   properties of the MACRO such as cell stiffnesses
  % ------------------------------------------------------------------------
  %% calculate truss lengths and angles
  phi = [];       % angle between each element and the global x-axis
  L = [];
  for i=1:numEdge
    st = nodePos(:, edges(1,i));
    fin = nodePos(:, edges(2,i));
    phi(i) = atan( (fin(2)-st(2))/(fin(1)-st(1)) );
    L(i) = norm( [fin(2)-st(2),fin(1)-st(1)] );
  end
  phi = phi';     % phi array allows transformation to local coordinates
  L = L';

  %% member stiffness matrices
  % stores all the k_bar for each in preparation for the assembly
  k_bar_collection = zeros(numEdge, 2*numNode, 2*numNode);  

  for i=1:numEdge
    x1e = 2*edges(1,i)-1;    % indices for the master stiffness matrix
    y1e = 2*edges(1,i);
    x2e = 2*edges(2,i)-1;
    y2e = 2*edges(2,i);
    % keyboard
    t = phi(i);
    ks = full(macro.netCellKComp(edges(1,i),edges(2,i))); % N/mm

    c = @(t) cos(t);
    s = @(t) sin(t);
    % globalization step
    k_e = ...
      [ +c(t)^2,     +s(t)*c(t),    -c(t)^2,      -s(t)*c(t)
        +s(t)*c(t),  +s(t)^2,       -s(t)*c(t),   -s(t)^2
        -c(t)^2,     -s(t)*c(t),    +c(t)^2,      +s(t)*c(t)
        -s(t)*c(t),  -s(t)^2,       +s(t)*c(t),   +s(t)^2];
    k_e = ks * k_e;     % member stiffness matrix
    
    k_bar_collection(i, x1e:y1e, x1e:y1e) = k_e(1:2,1:2);
    k_bar_collection(i, x1e:y1e, x2e:y2e) = k_e(1:2,3:4);
    k_bar_collection(i, x2e:y2e, x1e:y1e) = k_e(3:4,1:2);
    k_bar_collection(i, x2e:y2e, x2e:y2e) = k_e(3:4,3:4);
  end

  %% merge member matrices to form global stiffness matrix
  kk = squeeze(sum(k_bar_collection, 1));

  generalizedK = kk;
end
