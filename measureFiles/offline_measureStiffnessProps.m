%
% This function uses the MACRO eq optimization to compute a bulk
% bending stiffness for a given mesh
%
% INPUTS:
% The function takes as input a dataLog file
%
% OUTPUTS:
% Computes a set of stiffness properties of the MACRO stored in the dataLog
%   Identifies mesh primitive from filename
%   Uses meshPrimitiveEdgeNodes function to find edges
%   Applies forces on selected edges or nodes to obtain compressive and bending
%     for the primitive accordingly
%   All measures are stored in a struct "msrData", which is returned
%   The msrData is also saved in a file in the same directory as the dataLog 
%     that can be used for plotting and post processing
%
function msrData = offline_measureStiffnessProps (...
  folder, dataLogFname, testFlags, augment)
  set(0,'defaultlinelinewidth',1);

  DEBUG = 0;

  if ~exist('folder', 'var') || isempty(folder)
    folder = 'allOutputsDataFolder/pTst/set25 offlineMsrTest/';
  end

  if ~exist('dataLogFname', 'var') || isempty(dataLogFname)
    dataLogFname = dir(strcat(folder,'*dataLog.mat'));
    dataLogFname = dataLogFname(1).name;
  end

  if ~exist('testFlags','var')
    testFlags = struct();
    testFlags.compKX = 1;
    testFlags.compKY = 1;
    testFlags.bend_horiz = 1;
    testFlags.bend_vert = 1;
    testFlags.numRecords = [];
  end

  if ~exist('augment','var')
    augment = 1;
  else
    testFlags
    augment    
  end

  tmp = load(strcat(folder,dataLogFname));
  dataLog = tmp.dataLog;

  msrData = struct();

  % if the dataLog does not have records, return immediately
  if ~isfield(dataLog, 'records') || isempty(dataLog.records)
    return
  end

  if ~isfield(testFlags,'numRecords') || isempty(testFlags.numRecords)
    numRecords = length(dataLog.records);
  else
    numRecords = testFlags.numRecords;
  end
  msrData = [];

  % --------------------------------------------------------
  % Common data across records
  % --------------------------------------------------------
  % common properties of the mesh that are useful for measurement computations
  [meshPrim, actuationType, actuationDir] = parseFileName(dataLogFname);

  stPos = dataLog.startPos;
  edges = dataLog.edgeSet;
  topNodes = meshPrimitiveEdgeNodes (meshPrim, stPos, edges, 'top');
  bottomNodes = meshPrimitiveEdgeNodes (meshPrim, stPos, edges, 'bottom');
  leftNodes = meshPrimitiveEdgeNodes (meshPrim, stPos, edges, 'left');
  rightNodes = meshPrimitiveEdgeNodes (meshPrim, stPos, edges, 'right');
  bndNodes.topNodes = topNodes; bndNodes.bottomNodes = bottomNodes;
  bndNodes.leftNodes = leftNodes; bndNodes.rightNodes = rightNodes;

  % --------------------------------------------------------
  % Per record measurement computations
  % --------------------------------------------------------
  % parse the recorded MACRO snapshots, computing a set of stiffnesses
  % for every MACRO instance recorded
  for jj = 1:numRecords%length(dataLog.records)
    if jj~=1 && jj~=length(dataLog.records)
      continue
    end
    record = dataLog.records(jj);
    record.meshPrimitive = meshPrim;
    record.bndNodes = bndNodes;

    mm = copy(record.macro);
    eSet = mm.edgeSet;
    record.msr_bndBox = calc_bndBox(mm.nodePos, eSet, bndNodes);

    % dx, dy
    [dx,dy] = calc_stroke_XY(mm.nodePos, mm.startPos, eSet);
    [record.msr_strokeX, record.msr_strokeY] = deal(dx,dy);


    % kx(0), kx(t) 
    if testFlags.compKX 
      record.msr_compKX = calc_compK_XY(mm, 'x');
    % else
    %   record.msr_compKX = NaN;      
    end

    % ky(0), ky(t)
    if testFlags.compKY
      record.msr_compKY = calc_compK_XY(mm, 'y');
    % else
    %   record.msr_compKY = NaN;      
    end

    % kth_1(0), kth_0(t)
    if testFlags.bend_horiz
      record.msr_bendK_horiz = calc_bendK_beam(mm,'horizontal');

      % dth
      record.msr_strokeTh = calc_stroke_Th(mm.nodePos, mm.startPos, eSet);
    % else
    %   record.msr_bendK_horiz = NaN;      
    end

    % kth_2(0), kth_2(t)
    if testFlags.bend_vert
      record.msr_bendK_vert = calc_bendK_beam(mm,'vertical');
    % else
    %   record.msr_bendK_vert = NaN;      
    end

    record
    msrData = [msrData record];
    % keyboard
  end

  trialFname = dataLogFname;
  idx = strfind(trialFname,'dataLog');
  trialFname = trialFname(1:idx-1);

  msrFname = strcat(trialFname,'__msrData.mat');
  if exist(strcat(folder,msrFname),'file') && augment 
    tmp = load(strcat(folder, msrFname));
    oldData = tmp.msrData;
    for ff=fieldnames(msrData)'
      for ii=1:size(msrData,1)
        for jj=1:size(msrData,2)
          if ~isfield( oldData(ii,jj), (ff{1}) )
            % this field in new msr file does not exist in old file
            oldData(ii,jj).(ff{1}) = msrData(ii,jj).(ff{1});
          else
            % if this field exists in old file, augment to it
            %   IF the field is a measure
            % keyboard
            if ~isempty(strfind(ff{1},'msr_compK')) ...
              || ~isempty(strfind(ff{1},'msr_bendK')) ...
              || ~isempty(strfind(ff{1},'msr_stroke'))
              ttOld = oldData(ii,jj).(ff{1});
              ttNew = msrData(ii,jj).(ff{1});
              oldData(ii,jj).(ff{1}) = [ttOld ttNew];
            % else
            %   oldData(ii,jj).(ff{1}) = msrData(ii,jj).(ff{1});              
            end
          end
        end
      end
    end
    msrData = oldData
    disp('augmented overwrite');
  end

  save(strcat(folder, msrFname),'msrData');
end






function [meshPrim, actuationType, actuationDir] = parseFileName(fname)
  meshPrim = primitiveByShortName(fname);
  if ~isempty(strfind(fname, 'actX'))
    actuationType = @cb_compressionAxial;
    actuationDir = [1 0];
  elseif ~isempty(strfind(fname, 'actY'))
    actuationType = @cb_compressionAxial;
    actuationDir = [0 1];
  elseif ~isempty(strfind(fname, 'pTh'))
    actuationType = @cb_bending;
    actuationDir = 1;
  elseif ~isempty(strfind(fname, 'nTh'))
    actuationType = @cb_bending;
    actuationDir = 1;
  end
end





% given a macro, computes the stroke in x- direction using the boundary nodes
% only the right boundary is necessary
function out = calc_bndBox (pos, edges, bndNodes)
  DEBUG = 0;
  if ~exist('bndNodes','var')
    bndNodes.leftNodes = pointSetEdgeNodes (pos, edges, 'left', 0.25);
    bndNodes.bottomNodes = pointSetEdgeNodes (pos, edges, 'bottom', 0.25);
    bndNodes.rightNodes = pointSetEdgeNodes (pos, edges, 'right', 0.25);
    bndNodes.topNodes = pointSetEdgeNodes (pos, edges, 'top', 0.25);
  end
  rightBndVal = mean(pos(1,bndNodes.rightNodes));
  leftBndVal = mean(pos(1,bndNodes.leftNodes));
  topBndVal = mean(pos(2,bndNodes.topNodes));
  bottomBndVal = mean(pos(2,bndNodes.bottomNodes));

  out = [ leftBndVal
          bottomBndVal
          (rightBndVal-leftBndVal)
          (topBndVal-bottomBndVal)]';
end


% ------------------------------------------------------------------
% 
% S T R O K E   X- AND Y-
% 
% ------------------------------------------------------------------
% given a macro, computes the stroke in x- direction using the boundary nodes
function [outX,outY] = calc_stroke_XY (pos, stPos, edges, bndNodes)
  DEBUG = 0;
  if ~exist('bndNodes','var')
    bndNodes.leftNodes = pointSetEdgeNodes (stPos, edges, 'left', 0.25);
    bndNodes.bottomNodes = pointSetEdgeNodes (stPos, edges, 'bottom', 0.25);
    bndNodes.rightNodes = pointSetEdgeNodes (stPos, edges, 'right', 0.25);
    bndNodes.topNodes = pointSetEdgeNodes (stPos, edges, 'top', 0.25);
  end

  reps = 5;
  outX = [];
  outY = [];
  tmpPos = pos;
  for kk=1:reps
    pos = awgn(tmpPos,20);
    rightNodes = bndNodes.rightNodes;
    rightNodePos = pos(:,rightNodes);
    rightNodeStPos = stPos(:,rightNodes);

    topNodes = bndNodes.topNodes;
    topNodePos = pos(:,topNodes);
    topNodeStPos = stPos(:,topNodes);

    outX(kk) = mean( abs(rightNodePos(1,:) - rightNodeStPos(1,:)) );
    outY(kk) = mean( abs(topNodePos(1,:) - topNodeStPos(1,:)) );
  end
end



% ------------------------------------------------------------------
% 
% S T R O K E   THETA
% 
% ------------------------------------------------------------------
% given a macro, computes the stroke in x- direction using the boundary nodes
function [outTh] = calc_stroke_Th (pos, stPos, edges, bndNodes)
  DEBUG = 0;
  if ~exist('bndNodes','var')
    bndNodes.leftNodes = pointSetEdgeNodes (stPos, edges, 'left', 0.25);
    bndNodes.bottomNodes = pointSetEdgeNodes (stPos, edges, 'bottom', 0.25);
    bndNodes.rightNodes = pointSetEdgeNodes (stPos, edges, 'right', 0.25);
    bndNodes.topNodes = pointSetEdgeNodes (stPos, edges, 'top', 0.25);
  end

  reps = 5;
  outTh = [];
  tmpPos = pos;
  for kk=1:reps
    pos = awgn(tmpPos,20); 

    rightNodes = bndNodes.rightNodes;
    rightNodePos = pos(:,rightNodes);
    rightNodeStPos = stPos(:,rightNodes);

    leftNodes = bndNodes.leftNodes;
    leftNodePos = pos(:,leftNodes);
    leftNodeStPos = stPos(:,leftNodes);

    sampleLen = mean(rightNodeStPos(1,:)) - mean(leftNodeStPos(1,:));

    topNodes = bndNodes.topNodes;
    topNodePos = pos(:,topNodes);
    topNodeStPos = stPos(:,topNodes);

    topLeftYst = topNodeStPos(2,topNodeStPos(1,:) <= min(topNodeStPos(1,:)));
    topRightYst = topNodeStPos(2,topNodeStPos(1,:) >= max(topNodeStPos(1,:)));

    topLeftYfin = topNodePos(2,topNodePos(1,:) <= min(topNodePos(1,:)));
    topRightYfin = topNodePos(2,topNodePos(1,:) >= max(topNodePos(1,:)));

    delSt = topRightYst - topLeftYst;
    delFin = topRightYfin - topLeftYfin;

    def = delFin - delSt;
    outTh(kk) = atan(def/sampleLen);
  end

  % keyboard
end



% ------------------------------------------------------------------
% 
% S T I F F N E S S   I N   X-  A N D  Y-
% 
% ------------------------------------------------------------------
function out = calc_compK_XY (macro, direction)
  DEBUG = 0;
  if ~exist('direction','var')
    direction = 'x';
  end
 
  edges = macro.edgeSet; cPos = macro.nodePos;
  bndNodes.leftNodes = pointSetEdgeNodes (cPos, edges, 'left', 0.25);
  bndNodes.bottomNodes = pointSetEdgeNodes (cPos, edges, 'bottom', 0.25);
  bndNodes.rightNodes = pointSetEdgeNodes (cPos, edges, 'right', 0.25);
  bndNodes.topNodes = pointSetEdgeNodes (cPos, edges, 'top', 0.25);

  if strcmpi(direction,'x')
    % 1N force is applied to the right side of the sample along -x direction
    forceNodes = bndNodes.rightNodes;
    forceVector = 0.5*[-1 0];
  elseif strcmpi(direction,'y')
    % 1N force is applied to the top side of the sample along -y direction
    forceNodes = bndNodes.topNodes;
    forceVector = 0.5*[0 -1]; 
  end

  trialK = [];
  reps = 4;
  reps = 5;
  % reps = 6;
  % reps = 10;
  for kk=1:reps
    clone = copy(macro);

    % update constraints for testing
    clone.nodeConstraints = 0*clone.nodeConstraints;  % reset
    clone.nodeConstraints(:,1) = 1;   % anchor the first node
    if strcmpi(direction,'x')
      clone.nodeConstraints(1,bndNodes.leftNodes) = 1;
    elseif strcmpi(direction,'y')
      clone.nodeConstraints(2,bndNodes.bottomNodes) = 1;
    end

    % add a bit of noise to the clone node pos: 20 dB = 1% pos error
    clone.nodePos = awgn(macro.nodePos, 20);
    newPos = clone.minEnergyPos(0, forceVector/length(forceNodes), forceNodes);

    if exist('DEBUG','var') && DEBUG
      [A, ~] = genAdjMats(edges, macro.nodeCount);
      figure(56); clf; hold all; showNet(cPos,A,'b'); showNet(newPos,A,'r');
    end

    currentBBox = calc_bndBox (cPos, edges, bndNodes);
    newBBox = calc_bndBox (newPos, edges);

    deformation = [currentBBox(3) - newBBox(3); currentBBox(4) - newBBox(4)];
    tmp = sum(abs(forceVector')./abs(deformation));
    trialK = [trialK tmp];
    clear clone;
    % keyboard
  end
  out = trialK;

end


% ------------------------------------------------------------------
% 
% S T I F F N E S S   I N   THETA-  
% 
% ------------------------------------------------------------------
function out = calc_bendK_beam (macro, alignment)
  DEBUG = 0;
  if ~exist('alignment','var')
    alignment = 'horizontal';
  end
 
  edges = macro.edgeSet; cPos = macro.nodePos;
  leftNodes = pointSetEdgeNodes (cPos, edges, 'left', 0.25);
  bottomNodes = pointSetEdgeNodes (cPos, edges, 'bottom', 0.25);
  rightNodes = pointSetEdgeNodes (cPos, edges, 'right', 0.25);
  topNodes = pointSetEdgeNodes (cPos, edges, 'top', 0.25);

  absForce = 0.5;
  if strcmpi(alignment,'horizontal')
    % force is applied upward to the right tip of beam
    forceVector = absForce*[0 1];   % apply a 0.25N force 
    forceNodes = pointSetEdgeNodes (cPos, edges, 'right', 0.25);
  elseif strcmpi(alignment,'vertical')
    % force is applied upward to the right tip of beam
    forceVector = absForce*[-1 0];   % apply a 0.25N force 
    forceNodes = pointSetEdgeNodes (cPos, edges, 'top', 0.25);
  end

  [A, ~] = genAdjMats(edges, macro.nodeCount);
  steps = 1;

  % reps = 3;
  reps = 5;
  % reps = 10;
  overallK = [];

  for kk=1:reps
    clone = copy(macro); clone.delUnderExtForceLim = 5000;
    
    % update constraints for testing
    clone.nodeConstraints = 0*clone.nodeConstraints;  % reset
    if strcmpi(alignment,'horizontal')
      clone.nodeConstraints(:,leftNodes) = 1;
      clone.nodeConstraints(1,rightNodes) = 1;
    elseif strcmpi(alignment,'vertical')
      clone.nodeConstraints(:,bottomNodes) = 1;
      clone.nodeConstraints(2,topNodes) = 1;
    end

    % add a bit of noise to the clone node pos: 20 dB = 1% pos error
    clone.nodePos = awgn(cPos, 20);

    for intermSteps=1:steps
      newPos = clone.minEnergyPos(0,forceVector/length(forceNodes),forceNodes);
      clone.nodePos = newPos; 

      if exist('DEBUG','var') && DEBUG 
        outF=figure(56); clf; hold all;
        showNet(cPos,A,'b'); showNet(newPos,A,'r'); pause(0.1);
      end
    end

    % compute some geometric parameters of the beam from stPos
    global cellProps;
    if ~exist('cellProps','var')
      cellProps = ActiveCellProperties(1); cellProps = ActiveCellProperties(0);
    end
    b = cellProps.Wcell;
    if strcmpi(alignment,'horizontal')
      L = max(cPos(1,rightNodes)) - min(cPos(1,leftNodes));
      h = max(cPos(2,topNodes)) - min(cPos(2,bottomNodes));
      topRightNode = rightNodes(cPos(2,rightNodes) == max(cPos(2,rightNodes)));
      del = mean(abs(newPos(2,topRightNode) - cPos(2,topRightNode)));

    elseif strcmpi(alignment,'vertical')
      L = max(cPos(1,topNodes)) - min(cPos(1,bottomNodes));
      h = max(cPos(2,rightNodes)) - min(cPos(2,leftNodes));
      topLeftNode = leftNodes(cPos(1,leftNodes) == min(cPos(1,leftNodes)));
      del = mean(abs(newPos(1,topLeftNode) - cPos(1,topLeftNode)));
    end

    % keyboard
    P = norm(forceVector); K = P/del;
    E = K * L^3 * 4 / (b*h^3);

    overallK = [overallK K];
    clear clone;
  end
  out = overallK;
end

