% 
% This function uses the MACRO eq optimization to compute a bulk directional
% stiffness for a given mesh 
% 
% 
function [directionalK, deformation] = measure_compressiveK_EQ (...
    macro, dirVect) 
  set(0,'defaultlinelinewidth',1);

  edges = macro.edgeSet; currentEqPos = macro.nodePos;

  forceVector = -0.5*dirVect;   % apply a 0.1N force 

  if dirVect(1) == 1
    forceNodes = pointSetEdgeNodes (currentEqPos, edges, 'right');
  else
    forceNodes = pointSetEdgeNodes (currentEqPos, edges, 'top');
  end 

  debugCall = 1;

  reps = 3;
  directionalK = 0;

  trialK = 0;
  for kk=1:reps
    macroClone = copy(macro);

    % add a bit of noise to the macroClone node pos: 20 dB = 1% pos error
    macro.nodePos = currentEqPos;
    macroClone.nodePos = awgn(macro.nodePos, 20); 
    newEqPos = macroClone.minEnergyPos(debugCall, forceVector, forceNodes);

    [A, ~] = genAdjMats(edges, macro.nodeCount);
    % figure(56); clf; hold all;
    % showNet(currentEqPos,A,'b'); showNet(newEqPos,A,'r');

    currentBBox = measure_meshBoundDims(currentEqPos, edges, 10);
    newBBox = measure_meshBoundDims(newEqPos, edges, 10);

    deformation = [currentBBox(3) - newBBox(3); currentBBox(4) - newBBox(4)]
    % trialK = trialK + norm(forceVector)/norm(deformation);
    tmp = sum(abs(forceVector')./abs(deformation));
    trialK = trialK + tmp;

    clear macroClone;
  end

  directionalK = trialK / reps;

%   keyboard
