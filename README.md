
# Modular Active-Cell Robot (MACRO) Simulator #
## Author: Ahsan Nawroj ##
### GRABLab, Yale University ###

This is a Simulator package to model Modular Active-Cell Robot (MACRO) modules.

MACROs are networked structures created from Active Cells and passive nodes. 

The primary purpose of this package is to provide a realistic visualization of the deformation of MACRO modules for given input voltages at the boundary of the MACRO nodes.

A MACROcontroller module allows the computation of necessary inputs to cause a desired deformation in the MACRO. This controller works on a discrete-simulation of the MACRO. 

Writing a detailed README is ongoing work: for explanations about this project as well as academic publications related to it, please refer to http://www.eng.yale.edu/grablab/research_discrete_material_robots.html.
