function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hybt2d (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

startPos = zeros(2,18);
edgeSet = zeros(2,24);
edgeTheta = zeros(3,24);

srt = sqrt(3);
startPos (:,  1) = [0,0]';
startPos (:,  2) = [-0.5,srt/2]';
startPos (:,  3) = [0.5,srt/2]';
startPos (:,  4) = [-1.5-srt/2,0.5+srt/2]';
startPos (:,  5) = [-0.5-srt/2,0.5+srt/2]';
startPos (:,  6) = [0.5+srt/2,0.5+srt/2]';
startPos (:,  7) = [1.5+srt/2,0.5+srt/2]';
startPos (:,  8) = [-1-srt/2,0.5+srt]';
startPos (:,  9) = [1+srt/2,0.5+srt]';
startPos (:, 10) = [-1-srt/2,1.5+srt]';
startPos (:, 11) = [1+srt/2,1.5+srt]';
startPos (:, 12) = [-1.5-srt/2,1.5+3*srt/2]';
startPos (:, 13) = [-0.5-srt/2,1.5+3*srt/2]';
startPos (:, 14) = [0.5+srt/2,1.5+3*srt/2]';
startPos (:, 15) = [1.5+srt/2,1.5+3*srt/2]';
startPos (:, 16) = [-0.5,2+3*srt/2]';
startPos (:, 17) = [0.5,2+3*srt/2]';
startPos (:, 18) = [0,2+2*srt]';


edgeTheta = ...
          [0 0 4*pi/6;        %  1,  2;   
           0 0 2*pi/6;        %  1,  3;   
           0 0 0*pi/6;        %  2,  3;   
           0 0 5*pi/6;        %  2,  5;   
           0 0 1*pi/6;        %  3,  6;   
           0 0 0*pi/6;        %  4,  5;   
           0 0 2*pi/6;        %  4,  8;   
           0 0 4*pi/6;        %  5,  8;   
           0 0 0*pi/6;        %  6,  7;   
           0 0 2*pi/6;        %  6,  9;   
           0 0 4*pi/6;        %  7,  9;   
           0 0 3*pi/6;        %  8, 10;   
           0 0 3*pi/6;        %  9, 11;   
           0 0 4*pi/6;        %  10,12;   
           0 0 2*pi/6;        %  10,13;   
           0 0 4*pi/6;        %  11,14;   
           0 0 2*pi/6;        %  11,15;   
           0 0 0*pi/6;        %  12,13;   
           0 0 1*pi/6;        %  13,16;   
           0 0 0*pi/6;        %  14,15;   
           0 0 5*pi/6;        %  14,17;   
           0 0 0*pi/6;        %  16,17;   
           0 0 2*pi/6;        %  16,18;   
           0 0 4*pi/6 ]';     %  17,18    
           
edgeSet = ...
    [1,  2; 1,  3; 2,  3; 2,  5; 3,  6;
     4,  5; 4,  8; 5,  8; 6,  7; 6,  9;
     7,  9; 8, 10; 9, 11; 10,12; 10,13;
     11,14; 11,15; 12,13; 13,16; 14,15;
     14,17; 16,17; 16,18; 17,18 ]';



% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHybt2d_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end


% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0; layerOffsetY = 0;
  for yy=1:ny
    xx = 1;

    % optOffsetY for the layer is found by:
    %   find left most nodes of shape
    %   find highest height of these nodes, then adding sqrt(3)/2 to it
    if yy == 1
      optOffsetY = 0;
      layerOffsetY = 0;
    else
      topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
      topRightNodeY = max(shapeP(2,topLayerNodes));
      layerOffsetY = topRightNodeY - (0.5+sqrt(3));
      
      % leftMostNodesInShape = find( shapeP(1,:) <= min(shapeP(1,:)) );
      % leftTopMostNodeInShapeY = max(shapeP(2,leftMostNodesInShape));
      % layerOffsetY = leftTopMostNodeInShapeY + sqrt(3)/2;
    end

    if debugMode
      disp(sprintf('found layer opt yy = %.2f', layerOffsetY));
    end


    for xx=1:nx
      % no work needed for very first template unit at all, skip everything
      if xx == 1 && yy == 1
        if debugMode
          disp('very first copy, skip all additions');
        end
        continue;
      end

      % [xx, yy]
      if xx==1
        if mod(yy,2) == 0 % even layers have no optOffsetX
          optOffsetX = 1+sqrt(3)/2;
        else              % odd layers have optOffsetX for first unit
          optOffsetX = 0;
        end
      else
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        topLayerRightNode = topLayerNodes(...
          shapeP(1,topLayerNodes) >= max(shapeP(1,topLayerNodes)));
        topLayerRightNodeX = shapeP(1,topLayerRightNode);
        optOffsetX = topLayerRightNodeX + 2+sqrt(3);
      end
      optOffsetY = layerOffsetY;

      if debugMode
        disp(sprintf('stacking at x=%.2f, y=%.2f', optOffsetX, optOffsetY));
      end

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      % add new appropiately placed template to shape
      % -------------------------
      [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
        shapeP, shapeE, shapeET, ...
        templateP + offset, templateE, templateET);

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.05);
      end
    end
  end

  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

end
