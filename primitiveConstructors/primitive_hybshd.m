function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hybshd (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

sq3 = sqrt(3);

startPos = zeros(2,36);
edgeSet = zeros(2,48);
edgeTheta = zeros(3,48);

startPos (:,  1) = [0, 0]';
startPos (:,  2) = [1+sq3, 0]';
startPos (:,  3) = [-sq3/2, 0.5]';
startPos (:,  4) = [sq3/2, 0.5]';
startPos (:,  5) = [1+sq3/2, 0.5]';
startPos (:,  6) = [1+3*sq3/2, 0.5]';
startPos (:,  7) = [-sq3/2, 1.5]';
startPos (:,  8) = [sq3/2, 1.5]';
startPos (:,  9) = [1+sq3/2, 1.5]';
startPos (:, 10) = [1+3*sq3/2, 1.5]';
startPos (:, 11) = [0, 2]';
startPos (:, 12) = [1+sq3, 2]';
startPos (:, 13) = [-0.5-sq3/2, 1.5+sq3/2]';
startPos (:, 14) = [1.5+3*sq3/2, 1.5+sq3/2]';
startPos (:, 15) = [-0.5-sq3, 2+sq3/2]';
startPos (:, 16) = [-0.5, 2+sq3/2]';
startPos (:, 17) = [1.5+sq3, 2+sq3/2]';
startPos (:, 18) = [1.5+2*sq3, 2+sq3/2]';
startPos (:, 19) = [-0.5-sq3, 3+sq3/2]';
startPos (:, 20) = [-0.5, 3+sq3/2]';
startPos (:, 21) = [1.5+sq3, 3+sq3/2]';
startPos (:, 22) = [1.5+2*sq3, 3+sq3/2]';
startPos (:, 23) = [-0.5-sq3/2, 3.5+sq3/2]';
startPos (:, 24) = [0, 3+sq3]';
startPos (:, 25) = [1+sq3, 3+sq3]';
startPos (:, 26) = [1.5+3*sq3/2, 3.5+sq3/2]';
startPos (:, 27) = [-sq3/2, 3.5+sq3]';
startPos (:, 28) = [sq3/2, 3.5+sq3]';
startPos (:, 29) = [1+sq3/2, 3.5+sq3]';
startPos (:, 30) = [1+3*sq3/2, 3.5+sq3]';
startPos (:, 31) = [-sq3/2, 4.5+sq3]';
startPos (:, 32) = [sq3/2, 4.5+sq3]';
startPos (:, 33) = [1+sq3/2, 4.5+sq3]';
startPos (:, 34) = [1+3*sq3/2, 4.5+sq3]';
startPos (:, 35) = [0, 5+sq3]';
startPos (:, 36) = [1+sq3, 5+sq3]';

edgeTheta = ...
          [0 0 5*pi/6;        %  1,  3;   
           0 0 1*pi/6;        %  1,  4;   
           0 0 0*pi/6;        %  4,  5;   
           0 0 5*pi/6;        %  2,  5;   
           0 0 1*pi/6;        %  2,  6;   
           0 0 3*pi/6;        %  3,  7;   
           0 0 3*pi/6;        %  4,  8;   
           0 0 3*pi/6;        %  5,  9;   
           0 0 3*pi/6;        %  6, 10;   
           0 0 4*pi/6;        %  7, 13;   
           0 0 1*pi/6;        %  7, 11;   
           0 0 5*pi/6;        %  8, 11;   
           0 0 0*pi/6;        %  8,  9;   
           0 0 1*pi/6;        %  9, 12;   
           0 0 5*pi/6;        %  10,12;   
           0 0 2*pi/6;        %  10,14;   
           0 0 4*pi/6;        %  11,16;   
           0 0 2*pi/6;        %  12,17;   
           0 0 5*pi/6;        %  13,15;   
           0 0 1*pi/6;        %  13,16;   
           0 0 5*pi/6;        %  14,17;   
           0 0 1*pi/6;        %  14,18;   
           0 0 3*pi/6;        %  15,19;   
           0 0 3*pi/6;        %  16,20;   
           0 0 3*pi/6;        %  17,21;   
           0 0 3*pi/6;        %  18,22;   
           0 0 1*pi/6;        %  19,23;   
           0 0 4*pi/6;        %  20,23;   
           0 0 2*pi/6;        %  20,24;   
           0 0 4*pi/6;        %  21,25;   
           0 0 1*pi/6;        %  21,26;   
           0 0 5*pi/6;        %  22,26;   
           0 0 2*pi/6;        %  23,27;   
           0 0 0*pi/6;        %  24,27;   
           0 0 1*pi/6;        %  24,28;   
           0 0 5*pi/6;        %  25,29;   
           0 0 1*pi/6;        %  25,30;   
           0 0 4*pi/6;        %  26,30;   
           0 0 3*pi/6;        %  27,31;   
           0 0 0*pi/6;        %  28,29;   
           0 0 3*pi/6;        %  28,32;   
           0 0 3*pi/6;        %  29,33;   
           0 0 3*pi/6;        %  30,34;   
           0 0 1*pi/6;        %  31,35;   
           0 0 0*pi/6;        %  32,35;   
           0 0 0*pi/6;        %  32,33;   
           0 0 1*pi/6;        %  33,36;   
           0 0 5*pi/6;        %  34,36    
           ]';

edgeSet = ...
    [1,  3; 1,  4; 4,  5; 2,  5; 2,  6; 3,  7;
     4,  8; 5,  9; 6, 10; 7, 13; 7, 11; 8, 11;
     8,  9; 9, 12; 10,12; 10,14; 11,16; 12,17;
     13,15; 13,16; 14,17; 14,18; 15,19; 16,20;
     17,21; 18,22; 19,23; 20,23; 20,24; 21,25;
     21,26; 22,26; 23,27; 24,27; 24,28; 25,29;
     25,30; 26,30; 27,31; 28,29; 28,32; 29,33;
     30,34; 31,35; 32,35; 32,33; 33,36; 34,36 ]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHybshd_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end

% keyboard
% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0;
  for yy=1:ny

    xx = 1;
    % [xx, yy]
    if yy == 1
      optOffsetY = 0;
    else
        % offset y is highest y pos in shape minus 1
        if nx == 1
          optOffsetY = max(shapeP(2,:))-2;
        else
          optOffsetY = max(shapeP(2,:))-(3.5+ sq3/2);  
        end
    end
    if debugMode
      disp(sprintf('found opt yy = %.2f', optOffsetY));
    end

    for xx=1:nx
      if xx == 1 && yy == 1
        continue;
      end

      % [xx, yy]
      if debugMode
        disp(sprintf('stacking in x at y=%.2f', optOffsetY));
      end
      if xx==1
        optOffsetX = 0;
      else

        % for attaching in x after the first col
        % optOffsetX is found by finding the highest X value
        % in the current layer of the shape and adding (1+2/sqrt(2))
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        rightmostNodeX = max(shapeP(1,topLayerNodes));
        rightmostNodeY = max(shapeP(2,topLayerNodes));

        if mod(xx,2) == 0 % even numbered unit
          if xx == 2 % first even unit is special
            optOffsetX = rightmostNodeX + (0.5 + sq3/2);
          else
            optOffsetX = rightmostNodeX + (4.5 + sq3/2);
          end
          optOffsetY = optOffsetY + (1.5+ sq3/2);
        else % odd numbered unit  
          optOffsetX = rightmostNodeX + (0.5+sq3/2);
          optOffsetY = optOffsetY - (1.5+ sq3/2);
        end
        % keyboard
      end
      if debugMode
        disp(sprintf('found opt x = %.2f', optOffsetX));
      end

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      % add new appropiately placed template to shape
      % -------------------------
      [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
        shapeP, shapeE, shapeET, ...
        templateP + offset, templateE, templateET);

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.05);
      end
      % keyboard
    end
  end
  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
else
  if debugMode
    figure(3); clf; showShape(shapeToPointCloud(startPos,edgeSet,10), 'r');
    frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
    saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
  end
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

% keyboard
end
% -----------------------------------------------------------------------------





% function plotProgress (shapeP, newP)
%   figure(1); clf; hold all; axis equal;
%   tmp2 = [shapeP newP]; tmp2Bnd = boundary(tmp2',1);
%   shapeBnd = boundary(shapeP',1); tmpBnd = boundary(newP',1);
%   plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
%   plot(newP(1,tmpBnd), newP(2,tmpBnd), 'go--');
%   plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
% end

