function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hyb2tsts (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

startPos = zeros(2,4);
edgeSet = zeros(2,5);
edgeTheta = zeros(3,5);

startPos (:,  1) = [0, 0]';
startPos (:,  2) = [-0.5, sqrt(3)/2]';
startPos (:,  3) = [0.5, sqrt(3)/2]';
startPos (:,  4) = [0, sqrt(3)]';

edgeTheta = ...
          [0 0 2*pi/3;        %  1,  2;    
           0 0 1*pi/3;        %  1,  3;    
           0 0 0*pi/3;        %  2,  3;    
           0 0 1*pi/3;        %  2,  4;    
           0 0 2*pi/3 ]';     %  3,  4;    

edgeSet = ...
    [1,  2; 1,  3; 2,  3; 2,  4; 3,  4; ]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHyb2tsts_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end

% keyboard
% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;
  rotTemplateP = rotatePointSet(startPos,[],pi/2); rotTemplateP(3,:)=[];
  rotTemplateE = edgeSet;
  rotTemplateET = edgeTheta; rotTemplateET(3,:) = rotTemplateET(3,:) +pi/2;

  xx = 1; yy = 1;
  layerOffsetY = 0; optOffsetX = 0; optOffsetY = 0;
  for yy=1:ny

    flip = 0;
    if mod(yy,2) == 0
      flip = 1;
    end

    xx = 1;
    % [xx, yy]
    if yy == 1
      layerOffsetY = 0;
      optOffsetY = 0;
    else
      layerOffsetY = max(shapeP(2,:));
      optOffsetY = layerOffsetY;
    end
    if debugMode
      disp(sprintf('found opt yy = %.2f', optOffsetY));
    end

    for xx=1:nx
      if xx == 1 && yy == 1
        flip = ~flip;
        continue;
      end

      % [xx, yy]
      if debugMode
        disp(sprintf('stacking in x at y=%.2f', optOffsetY));
      end
      if xx==1
        if nx == 1
          if flip
            optOffsetX = sqrt(3)/2;
            optOffsetY = layerOffsetY + 0.5;
          else
            optOffsetX = 0;
            optOffsetY = layerOffsetY;
          end
        else % nx is not 1
          if flip
            optOffsetX = sqrt(3)/2;
            optOffsetY = layerOffsetY + 0.5;
          else
            optOffsetX = 0;
            optOffsetY = layerOffsetY - (sqrt(3)/2-0.5);
          end
        end
      else
        % flip = ~flip;

        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        rightmostNodeX = max(shapeP(1,topLayerNodes));
        rightmostNodeY = max(shapeP(2,topLayerNodes));

        if flip
          optOffsetX = rightmostNodeX + 0.5+sqrt(3); 
          optOffsetY = rightmostNodeY - sqrt(3)/2;
        else 
          if xx == 2
            optOffsetX = rightmostNodeX + 0.5+sqrt(3)/2;
            optOffsetY = rightmostNodeY - (1+(sqrt(3)/2-0.5));
          else
            optOffsetX = rightmostNodeX + 1+sqrt(3);
            optOffsetY = rightmostNodeY - sqrt(3);
          end
        end
      end
      if debugMode
        disp(sprintf('found opt x = %.2f', optOffsetX));
        % flip
      end

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));
% keyboard
      % add new appropiately placed template to shape
      % -------------------------
      if ~flip
        [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
          shapeP, shapeE, shapeET, ...
          templateP + offset, templateE, templateET);
      else
        [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
          shapeP, shapeE, shapeET, ...
          rotTemplateP + offset, rotTemplateE, rotTemplateET);        
      end

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      flip = ~flip;

      if debugMode
        pause(0.05);
      end
      % keyboard
    end
  end
  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
else
  if debugMode
    figure(3); clf; showShape(shapeToPointCloud(startPos,edgeSet,10), 'r');
    frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
    saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
  end
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

% keyboard
end
% -----------------------------------------------------------------------------





% function plotProgress (shapeP, newP)
%   figure(1); clf; hold all; axis equal;
%   tmp2 = [shapeP newP]; tmp2Bnd = boundary(tmp2',1);
%   shapeBnd = boundary(shapeP',1); tmpBnd = boundary(newP',1);
%   plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
%   plot(newP(1,tmpBnd), newP(2,tmpBnd), 'go--');
%   plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
% end

