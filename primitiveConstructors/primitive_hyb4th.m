function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hyb4th (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

sq32 = sqrt(3)/2;
startPos = ...
  [0, 0;          1, 0;           2, 0;
   -0.5, sq32;    0.5, sq32;      1.5, sq32;          2.5, sq32;
   -1, 2*sq32;    0, 2*sq32; 2,   2*sq32;             3, 2*sq32;
   -0.5, 3*sq32;  0.5, 3*sq32;    1.5, 3*sq32;        2.5, 3*sq32;
   0, 4*sq32;     1, 4*sq32;      2, 4*sq32]';

edgeSet = ...
  [1,2; 1,5; 1,4; 2,5; 2,3; 2,6; 3,6; 3,7; 4,5; 4,8; 4,9;
   5,6; 5,9; 6,7; 6,10; 7,10; 7,11; 8,9; 8,12; 9,12; 9,13;
   10,11; 10,14; 10,15; 11,15; 12,13; 12,16; 13,16; 13,14; 
   13,17; 14,15; 14,17; 14,18; 15,18; 16,17; 17,18]';

p3 = pi/3;
edgeTheta = ...
  [0 0 0*p3; 0 0 1*p3; 0 0 2*p3; 0 0 2*p3; 0 0 0*p3;
   0 0 1*p3; 0 0 2*p3; 0 0 1*p3; 0 0 0*p3; 0 0 2*p3;
   0 0 1*p3; 0 0 0*p3; 0 0 2*p3; 0 0 0*p3; 0 0 1*p3;
   0 0 2*p3; 0 0 1*p3; 0 0 0*p3; 0 0 1*p3; 0 0 2*p3;
   0 0 1*p3; 0 0 0*p3; 0 0 2*p3; 0 0 1*p3; 0 0 2*p3;
   0 0 0*p3; 0 0 1*p3; 0 0 2*p3; 0 0 0*p3; 0 0 1*p3;
   0 0 0*p3; 0 0 2*p3; 0 0 1*p3; 0 0 2*p3; 0 0 0*p3;
   0 0 0*p3]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHyb4th_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end

% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0; layerOffsetY = 0;
  jaggedCutoffLayer = 2;
  increasedUnits = 0;
  jaggedCount = 0;
  for yy=1:ny

    xx = 1;
    % [xx, yy]
    if yy == 1      
      optOffsetY = 0;
      layerOffsetY = 0;
    else
      topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
      leftOfTopNodesY = min(shapeP(2,topLayerNodes));

      % if there is only one row of units
      % need to make sure layer offset is properly set 
      if nx == 1
        layerOffsetY = leftOfTopNodesY - sqrt(3)/2;
      else
        layerOffsetY = leftOfTopNodesY - 3*sqrt(3)/2;
      end

    end
    if debugMode
      disp(sprintf('found layer opt yy = %.2f', layerOffsetY));
    end

    if mod(yy,jaggedCutoffLayer) == 0
      jaggedCount = jaggedCount + 1;
      jaggedLayer = 1;
    else
      jaggedLayer = 0;
    end

    
    % Attempts to fix the bias in the structure: FAILURE So far
    % ----------------------------------------------------------
    % if increasedUnits
    %   nx = nx - jaggedCount;
    %   increasedUnits = 0;
    % end
    % if mod(yy+1,jaggedCutoffLayer) == 0
    %   nx = nx + jaggedCount;
    %   increasedUnits = increasedUnits + 1;
    % end
    
    % if mod(jaggedCount,2)==0
    %   jaggedLayer = 0;
    % end

    for xx=1:nx
      if xx == 1 && yy == 1
        if debugMode
          disp('very first copy, skip all additions');
        end
        continue;
      end


      % [xx, yy]

      % first unit placed in x ...
      tmpxx = xx;
      if xx==1
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        leftOfTopNodesX = min(shapeP(1,topLayerNodes));

        % special case for nx = 1
        if nx == 1 
          if jaggedLayer
            optOffsetY = layerOffsetY - sqrt(3)/2;
            optOffsetX = leftOfTopNodesX + 2;
          else
            optOffsetX = leftOfTopNodesX - 0.5;
            optOffsetY = layerOffsetY;
          end
        
        % nx ~= 1
        else
          if jaggedLayer
            tmpxx = xx + 1;
            optOffsetY = layerOffsetY + 2*sqrt(3)/2;
            optOffsetX = leftOfTopNodesX - 0.5;
          else
            optOffsetY = layerOffsetY;
            optOffsetX = leftOfTopNodesX - 2.5;
          end
        end

      % subsequent units placed in x ...
      else
        if jaggedLayer
          tmpxx = xx + 1;
        end
          
        % 3 cases: 1st, 2nd, 3rd unit, and then repeat
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        leftmostNodeX = min(shapeP(1,topLayerNodes));
        rightmostNodeX = max(shapeP(1,topLayerNodes));

        % 3rd unit
        if mod(tmpxx-1,3) == 0
          topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
          rightmostNodeX = max(shapeP(1,topLayerNodes));
          optOffsetX = rightmostNodeX + 3;
          optOffsetY = layerOffsetY;
        % 2nd unit
        elseif mod(tmpxx-1,3) == 2
          topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
          rightmostNodeX = max(shapeP(1,topLayerNodes));
          optOffsetX = rightmostNodeX + 0.5;
          optOffsetY = layerOffsetY + sqrt(3)/2;
        % 1st unit
        else
          if tmpxx == 2 % first one in the layer (previous max y in unit 1)
            % possible to have the first unit missing for jagged edges
            topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
            rightmostNodeX = max(shapeP(1,topLayerNodes));
            optOffsetX = rightmostNodeX;
            optOffsetY = layerOffsetY + 2*sqrt(3)/2;
          else % can be the second one in the layer (previous max y in unit 2)
            topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
            rightmostNodeX = max(shapeP(1,topLayerNodes));
            optOffsetX = rightmostNodeX + 5;
            optOffsetY = layerOffsetY + sqrt(3);            
          end
        end

      end
      if debugMode
        disp(sprintf('stacking at x=%.2f, y=%.2f', optOffsetX, optOffsetY));
      end

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      % add new appropiately placed template to shape
      % -------------------------
      [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
        shapeP, shapeE, shapeET, ...
        templateP + offset, templateE, templateET);

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.0005);
      end
      % keyboard
      % input('');
    end
  end
  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

end
% -----------------------------------------------------------------------------





% function plotProgress (shapeP, newP)
%   figure(1); clf; hold all; axis equal;
%   tmp2 = [shapeP newP]; tmp2Bnd = boundary(tmp2',1);
%   shapeBnd = boundary(shapeP',1); tmpBnd = boundary(newP',1);
%   plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
%   plot(newP(1,tmpBnd), newP(2,tmpBnd), 'go--');
%   plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
% end

