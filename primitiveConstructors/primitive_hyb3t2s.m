function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hyb3t2s (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

edgeSet = ...
    [1,2; 1,3; 2,3; 2,4; 3,5; 4 5; 4 6; 5 6]';

edgeTheta = ...
    [0 0 2*pi/3;
     0 0 pi/3;
     0 0 0;
     0 0 pi/2;
     0 0 pi/2;
     0 0 0;
     0 0 pi/3;
     0 0 2*pi/3]';

startPos = [0     0;
            -0.5  sqrt(3)/2;
            0.5   sqrt(3)/2;
            -0.5  (1+sqrt(3)/2);
            0.5   (1+sqrt(3)/2);
            0   1+sqrt(3)]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHyb3t2s_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end

% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0;
  for yy=1:ny

    xx = 1;
    % [xx, yy]
    if yy == 1
      optOffsetY = 0;
    else
      % optOffsetY is found by finding the highest Y value in the
      % existing shape this is reasonable since construction in row-major
      % this offset is reduced by sqrt(3)/2 to get node 1 to touch the square
      optOffsetY = max(shapeP(2,:))-sqrt(3)/2;
    end
    if debugMode
      disp(sprintf('found opt yy = %.2f', optOffsetY));
    end

    for xx=1:nx
      if xx == 1 && yy == 1
        continue;
      end

      % [xx, yy]
      if debugMode
        disp(sprintf('stacking in x at y=%.2f', optOffsetY));
      end
      if xx==1
        if mod(yy,2) ~= 0
          % for odd layers the offset for node 1 is 0
          optOffsetX = 0;
        else
          % for even layers, offset node 1 by +0.5
          optOffsetX = 0.5;
        end
      else

        % for attaching in x after the first col
        % optOffsetX is found by finding the highest X value
        % in the current layer of the shape and adding +1
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        rightmostNodeX = max(shapeP(1,topLayerNodes));
        optOffsetX = rightmostNodeX + 1;
        % keyboard
      end
      if debugMode
        disp(sprintf('found opt x = %.2f', optOffsetX));
      end

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      % add new appropiately placed template to shape
      % -------------------------
      [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
        shapeP, shapeE, shapeET, ...
        templateP + offset, templateE, templateET);

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.05);
      end
      % keyboard
    end
  end
  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

end
% -----------------------------------------------------------------------------





% function plotProgress (shapeP, newP)
%   figure(1); clf; hold all; axis equal;
%   tmp2 = [shapeP newP]; tmp2Bnd = boundary(tmp2',1);
%   shapeBnd = boundary(shapeP',1); tmpBnd = boundary(newP',1);
%   plot(shapeP(1,shapeBnd), shapeP(2,shapeBnd), 'ro-');
%   plot(newP(1,tmpBnd), newP(2,tmpBnd), 'go--');
%   plot(tmp2(1,tmp2Bnd), tmp2(2,tmp2Bnd), 'bo-');
% end

