function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_tri (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

edgeSet = ...
    [1,2;
     2,3;
     1,3]';
edgeTheta = ...
    [0 0 0;
     0 0 2*pi/3;
     0 0 pi/3]';

startPos = [
    0 0;
    1 0;
    0.5 sqrt(3)/2]';

% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pTri_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end

% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;
  [flipTemplateP, flipTemplateE, flipTemplateET] = ...
      reflectShape(templateP, templateE, templateET, [0 0; 1 0]');

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0;
  for yy=1:ny
    xx = 1; 

    % template needs to be flipped to allow vertical layer growth
    flip = 0;
    if mod(yy,2) == 0
        flip = 1;
    end
    if debugMode
      disp(sprintf('layer: %d, primitive flip = %s', yy, flip));
    end

    if yy == 1
        optOffsetY = 0;
    else
        % optOffsetY is found by finding the highest Y value in the
        % existing shape this is reasonable since construction in row-major
        optOffsetY = max(shapeP(2,:));
    end
    if debugMode
      disp(sprintf('found opt yy = %.2f', optOffsetY));
    end


    for xx=1:nx
      if xx == 1 && yy == 1
        flip = ~flip;
        continue;
      end

      if debugMode
        disp(sprintf('stacking in x at y=%.2f', optOffsetY));
      end

      if xx==1
        if flip==0
          % for even layers, offset for first element in layer is 0
          optOffsetX = 0;
        else
          % flipped layer will always have a layer below it
          % x-offset is the amount to get to the leftmost node in
          %  the topmost layer of existing nodes
          topLayerNodes = find(shapeP(2,:) > optOffsetY-0.1);
          leftmostNode = find(...
              shapeP(1,:) == min(shapeP(1,topLayerNodes)),1,'first');
          optOffsetX = min(shapeP(1,leftmostNode));
        end
      else

        if flip == 0
          % if no flip, offset in x is found by attaching node 1 at -0.5 from
          % the node to the far rightmost of nodes in the top layer
          topLayerNodes = find(shapeP(2,:) > optOffsetY);
          rightmostNode = find(...
              shapeP(1,:) == max(shapeP(1,topLayerNodes)),1,'last');
          optOffsetX = shapeP(1,rightmostNode)-0.5;
        else
          % if flipping, find top right most node x, add 0.5 to this x
          topLayerNodes = find(shapeP(2,:) > optOffsetY);
          rightmostNode = find(...
              shapeP(1,:) == max(shapeP(1,topLayerNodes)),1,'last');
          optOffsetX = shapeP(1,rightmostNode)+0.5;
        end
      end
      if debugMode
        disp(sprintf('found opt x = %.2f', optOffsetX));
      end

      % add new appropiately placed template to shape
      % -------------------------
      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      if flip == 0
        % for odd layers, add template to shape
        [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
            shapeP, shapeE, shapeET, ...
            templateP + offset, templateE, templateET);
      else
        % for even layers, add flipped template to shape
        [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
            shapeP, shapeE, shapeET, ...
            flipTemplateP + offset, flipTemplateE, flipTemplateET);
      end

      if debugMode
        % figure(3); clf; showShape(shapeP, 'r');
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,10), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.1);
      end
 
      flip = ~flip;
    end
  end
  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
    0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

end