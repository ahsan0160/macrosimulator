function [netName, edgeSet, edgeTheta, startPos, targetPos, anchors, fixedInX, fixedInY, fixedInZ] = primitive_hex (nx,ny, debugMode)
% >> Created 2017 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% >> (c) 2017 Yale University; All rights reserved.

if ~exist('debugMode','var')
  debugMode = 0; % activates printing during setup
end

edgeSet = ...
    [1,2; 1,3; 3,4; 4,5; 5,6; 2,6]';
edgeTheta = ...
    [0 0 0;
     0 0 2*pi/3;
     0 0 pi/3;
     0 0 0;
     0 0 -pi/3;
     0 0 pi/3]';

startPos = ...
    [0 0;
     1 0;
     -0.5 sqrt(3)/2;
     0 sqrt(3);
     1 sqrt(3);
     1.5 sqrt(3)/2]';


% for debugging
nodeCount = size(startPos,2);
[A,Ae] = genAdjMats(edgeSet, nodeCount);

netName = strcat('pHex_',num2str(nx),'_',num2str(ny));

if debugMode
  figure(3); clf;
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 1);
end


% -----------------------------------------------------------------
% template replication if necessary
% make copies in x and y, then stitch the copies together
% along nodes/edges
% -----------------------------------------------------------------
if nx ~= 1 || ny ~= 1
  % keep track of overall shape created thus far
  shapeP = startPos; shapeE = edgeSet; shapeET = edgeTheta;

  % template of duplicating shape
  templateP = startPos; templateE = edgeSet; templateET = edgeTheta;

  xx = 1; yy = 1;
  optOffsetX = 0; optOffsetY = 0; layerOffsetY = 0;
  for yy=1:ny
    xx = 1;

    % optOffsetY for the layer is found by:
    %   find left most nodes of shape
    %   find highest height of these nodes, then adding sqrt(3)/2 to it
    if yy == 1
      optOffsetY = 0;
      layerOffsetY = 0;
    else
      leftMostNodesInShape = find( shapeP(1,:) <= min(shapeP(1,:)) );
      leftTopMostNodeInShapeY = max(shapeP(2,leftMostNodesInShape));
      layerOffsetY = leftTopMostNodeInShapeY + sqrt(3)/2;
    end

    if debugMode
      disp(sprintf('found layer opt yy = %.2f', layerOffsetY));
    end


    for xx=1:nx
      % no work needed for very first template unit at all, skip everything
      if xx == 1 && yy == 1
        if debugMode
          disp('very first copy, skip all additions');
        end
        continue;
      end

      % [xx, yy]
      if xx==1
        optOffsetX = 0;
        optOffsetY = layerOffsetY;

      elseif mod(xx,2) == 0   % even number attachment, need zig-zag up

        if xx == 2 % special case for the FIRST SUCH ZIG-ZAG UP in a layer
          % for zig-zag up the first time in layer, optOffsetX is found by:
          %   find top layer of shape (including last attached template)
          %   find rightMost node of this top layer
          %   offsetX is the x-coordinate of this node + .5
          topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
          topRightNodeX = max(shapeP(1,topLayerNodes));
          optOffsetX = topRightNodeX + 0.5;

        else % every OTHER ZIG-ZAG UP in a layer
          % for zig-zag up generally in layer, optOffsetX is found by:
          %   find top layer of shape (including last attached template)
          %   find rightMost node of this top layer
          %   offsetX is the x-coordinate of this node + 2
          topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
          topRightNodeX = max(shapeP(1,topLayerNodes));
          optOffsetX = topRightNodeX + 2;

        end

        % add to optOffsetY as well by sqrt(3)/2
        optOffsetY = layerOffsetY + sqrt(3)/2;

        % [optOffsetX optOffsetY]

      else

        % for no-zig-zag, optOffsetX is found by:
        %   find top layer of shape (including last attached template)
        %   find rightMost node of this top layer
        %   offsetX is 0.5 + this rightMost node x
        topLayerNodes = find(shapeP(2,:) >= max(shapeP(2,:)));
        topLayerRightNode = topLayerNodes(...
          shapeP(1,topLayerNodes) >= max(shapeP(1,topLayerNodes)));
        topLayerRightNodeX = shapeP(1,topLayerRightNode);
        optOffsetX = topLayerRightNodeX + 0.5;

        % optOffsetY is the layerOffsetY
        optOffsetY = layerOffsetY;

        % [optOffsetX optOffsetY]
      end

      if debugMode
        disp(sprintf('stacking at x=%.2f, y=%.2f', optOffsetX, optOffsetY));
      end

      % figure(99);
      % showShape(shapeToPointCloud (shapeP, shapeE, 5),'r');
      % % keyboard

      offset = repmat([optOffsetX; optOffsetY], 1, size(templateP,2));

      % add new appropiately placed template to shape
      % -------------------------
      [shapeP, shapeE, shapeET] = shapeAdditionCleanup(...
        shapeP, shapeE, shapeET, ...
        templateP + offset, templateE, templateET);

      if debugMode
        figure(3); clf; showShape(shapeToPointCloud(shapeP,shapeE,3), 'r');
        frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
        saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0);
      end

      if debugMode
        pause(0.05);
      end
    end
  end

  nodeCount = size(shapeP,2);
  [A,Ae] = genAdjMats(shapeE, nodeCount); showNet(shapeP, A);
  % keyboard
  startPos = shapeP; edgeSet = shapeE; edgeTheta = shapeET;
end

targetPos = startPos;
targetPos (:,2:end) = targetPos (:,2:end) + ...
  0.2*(rand(size(targetPos (:,2:end)))-0.5);

anchors = 1; fixedInX = []; fixedInY = []; fixedInZ = [];


if debugMode
  frame = getframe; im = frame2im(frame); [imind,cm] = rgb2ind(im,256);
  saveFrameToGif(strcat('allOutputs/',netName,'.gif'), imind, cm, 0, 3);
end

end
