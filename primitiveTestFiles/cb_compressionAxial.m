
function [nodeInputV, coolingInputV] = cb_compressionAxial (...
  pos, edges, actuationDir, threshold, edgeMetric, primitive)
% --------------------------------------------------------------------------
% ** Callback function **
% Replaces the stiffness-controller with a prescribed input
% Details:
%   Applies a constant current across the left and right side of the mesh
% --------------------------------------------------------------------------
  if ~exist('actuationDir','var') || isempty(actuationDir)
    actuationDir = [1 0];
  end

  nodeInputV = zeros(size(pos,2),1);
  nodeInputI = zeros(size(pos,2),1);
  coolingInputV = zeros(size(pos,2),1);;

  if ~exist('edgeMetric','var')
    edgeMetric = @pointSetEdgeNodes;
  end

  currentDiff = 2;
  % if ~exist('threshold','var')
    threshold = 25;
  % end

  if actuationDir(1) == 1
    if isequal (edgeMetric, @pointSetEdgeNodes)
      layer1 = pointSetEdgeNodes(pos,edges,'left',threshold);
      layer2 = pointSetEdgeNodes(pos,edges,'right',threshold);
    elseif isequal(edgeMetric, @meshPrimitiveEdgeNodes)
      layer1 = meshPrimitiveEdgeNodes(primitive, pos,edges,'left');
      layer2 = meshPrimitiveEdgeNodes(primitive, pos,edges,'right');
    end
  else
    if isequal (edgeMetric, @pointSetEdgeNodes)
      layer1 = pointSetEdgeNodes(pos,edges,'bottom',threshold);
      layer2 = pointSetEdgeNodes(pos,edges,'top',threshold);
    elseif isequal(edgeMetric, @meshPrimitiveEdgeNodes)
      layer1 = meshPrimitiveEdgeNodes(primitive, pos,edges,'bottom');
      layer2 = meshPrimitiveEdgeNodes(primitive, pos,edges,'top');
    end
  end

% keyboard
  nodeInputI(layer1) = -currentDiff/2;
  nodeInputI(layer2) = currentDiff/2;

  global cellProps;
  if isempty(cellProps)
    cellProps = ActiveCellProperties();
  end

  nNodes = size(pos,2);
  nEdges = size(edges,2);
  A = sparse(edges(1,:), edges(2,:), 1, nNodes, nNodes, nEdges);
  A = full(A+A');

  netL = genLaplacian(A, cellProps.cellR);
  nodeInputV = pinv(netL)*nodeInputI;
  nodeInputV = nodeInputV-min(nodeInputV);
end




function L = genLaplacian(connMat, cellR)
  cc = connMat;
  condNet = cc * (1/cellR);
  nodeCount = size(cc,1);
  L = zeros(nodeCount);
  for i = 1:nodeCount
    L(i,i) = sum(condNet(i,:));
    xx = find(condNet(i,:) ~= 0);
    L(i, xx) = -condNet(i, xx);
  end
end
