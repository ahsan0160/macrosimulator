
function [nodeInputV, coolingInputV] = cb_bending (...
  pos, edges, actuationDir, threshold, edgeMetric, primitive)
% --------------------------------------------------------------------------
% ** Callback function **
% Replaces the stiffness-controller with a prescribed input
% Details:
%   Applies a constant current along the top surface of the mesh (actuationDir)
%   All other nodes are at 0V
% --------------------------------------------------------------------------
    
  nodeInputV = zeros(size(pos,2),1);
  nodeInputI = zeros(size(pos,2),1);
  coolingInputV = zeros(size(pos,2),1);

  if ~exist('edgeMetric','var')
    edgeMetric = @pointSetEdgeNodes;
  end

  currentDiff = 20;
  if ~exist('threshold','var')
    threshold = 25;
  end
  % threshold = 100;

  if actuationDir(1) == 1
    if isequal (edgeMetric, @pointSetEdgeNodes)
      actuationLayer = edgeMetric(pos,edges,'top',threshold);
    elseif isequal(edgeMetric, @meshPrimitiveEdgeNodes)
      actuationLayer = edgeMetric(primitive, pos,edges,'top');
    end
  else
    if isequal (edgeMetric, @pointSetEdgeNodes)
      actuationLayer = edgeMetric(pos,edges,'bottom',threshold);
    elseif isequal(edgeMetric, @meshPrimitiveEdgeNodes)
      actuationLayer = edgeMetric(primitive, pos,edges,'bottom');
    end
  end

  % keyboard
  leftmostNode = actuationLayer(...
    find(pos(1,actuationLayer) <= min(pos(1,actuationLayer))));
  rightmostNode = actuationLayer(...
    find(pos(1,actuationLayer) >= max(pos(1,actuationLayer))));

  nodeInputI(leftmostNode) = -currentDiff/2; 
  nodeInputI(rightmostNode) = currentDiff/2;

  global cellProps;
  if isempty(cellProps)
    cellProps = ActiveCellProperties();
  end

  nNodes = size(pos,2);
  nEdges = size(edges,2);
  A = sparse(edges(1,:), edges(2,:), 1, nNodes, nNodes, nEdges);
  A = full(A+A');

  netL = genLaplacian(A, cellProps.cellR);
  nodeInputV = pinv(netL)*nodeInputI;
  nodeInputV = nodeInputV-min(nodeInputV);
end 




function L = genLaplacian(connMat, cellR)
  cc = connMat;
  condNet = cc * (1/cellR);
  nodeCount = size(cc,1);
  L = zeros(nodeCount);
  for i = 1:nodeCount
    L(i,i) = sum(condNet(i,:));
    xx = find(condNet(i,:) ~= 0);
    L(i, xx) = -condNet(i, xx);
  end
end
