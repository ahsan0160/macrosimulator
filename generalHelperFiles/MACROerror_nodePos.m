
function err = MACROerror_nodePos (net1pos, net2pos, A)
	if sum(net1pos(:)) == 0 || sum(net2pos(:)) == 0
		err = 0; return;
	end

	net2adjusted = alignNets(net2pos, net1pos);
	err = norm(net2adjusted-net1pos);
end

