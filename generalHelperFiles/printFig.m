function printFig(ff, optionalFolder, fileName, optionalString)
% ===========================================================
% PRINT SPECIFIED FIGURE TO FILE
% ===========================================================
% keyboard
  if isempty(ff)
    ff = figure(1);
  end
  if ~exist('optionalString', 'var')
    optionalString = '';
  end
  if ~exist('optionalFolder', 'var')
    optionalFolder = 'zzOutputs';
  end
  if ~exist('fileName', 'var')
    disp('COULD NOT PRINT FIGURE: NO FILE NAME');
    return
  end

  % sanitize inputs
  optionalString = strrep(optionalString,'.','_');
  optionalString = strrep(optionalString,',','');

  outf = figure(ff);
  time=clock;
  time=ceil(time);

  pngFolderName=sprintf('allOutputs/%s', optionalFolder);
  pdfFolderName=sprintf('allOutputs/%s/pdfs', optionalFolder);
  if ~exist(pngFolderName,'dir')
    mkdir(pngFolderName);
  end
  if ~exist(pdfFolderName,'dir')
    mkdir(pdfFolderName);
  end

  % tstamp = strcat(...
  %   num2str(time(1)),'_',num2str(time(2)),'_',...
  %   num2str(time(3)), '_', num2str(time(4)),'_',...
  %   num2str(time(5)),'_',num2str(time(6)));
  tstamp = strcat(...
    num2str(time(2)),'_',...
    num2str(time(3)), '_', num2str(time(4)),'_',...
    num2str(time(5)));

  outPngName = sprintf('%s/%s_%s_%s', ...
      pngFolderName, fileName, optionalString, tstamp);
  outPdfName = sprintf('%s/%s_%s_%s', ...
      pdfFolderName, fileName, optionalString, tstamp);
  % keyboard
  print (outf, strcat(outPngName,'.png'), '-dpng', '-r144')
  print (outf, strcat(outPdfName, '.pdf'), '-dpdf', '-r300' ,'-fillpage')
end






