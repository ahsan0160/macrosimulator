function [connectedNodes] = connectedTo (shapeE, node)
% -----------------------------------------------------------------------------
% given a node, returns list of all nodes connected to this node in the edgeset
% -----------------------------------------------------------------------------
% keyboard
	connectedNodes = ...
		[shapeE(2,shapeE(1,:)==node) shapeE(1,shapeE(2,:)==node)];