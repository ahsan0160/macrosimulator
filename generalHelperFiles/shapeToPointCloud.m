function outP = shapeToPointCloud (shapeP, shapeE, nVirtualPoints)
% -----------------------------------------------------------------------------
% takes a point set and edges connecting them, and returns 
% a new point set with nVirtualPoints intermediate virtual points along
% each edge
% 
% this function should be run before calling matlab's 'boundary'
% function or area calculations, since triangulations are more
% accurate with a denser point cloud than the sparse MACRO shapes
% -----------------------------------------------------------------------------
	
	outP = shapeP;

	% along each edge if shape, add nVirtualPoints to the outP point set
	for ii=1:size(shapeE, 2)
		currentEdge = shapeE(:,ii);
		stNode = shapeP(:,currentEdge(1));
		finNode = shapeP(:,currentEdge(2));

		intermediateNodes = linspaceNDim(stNode, finNode, nVirtualPoints+2);
		intermediateNodes = intermediateNodes(:,2:end-1);
		outP = [outP intermediateNodes];
	end

	% figure(999); clf;
	% subplot(2,1,1); hold all; showShape(shapeP, 'r'); 
	% subplot(2,1,2); hold all; showShape(outP, 'b');

	% initArea = shapeSetOp(shapeP, shapeP, 'union')
	% detailedArea = shapeSetOp(outP, outP, 'union')

	% keyboard	
end
% -----------------------------------------------------------------------------


