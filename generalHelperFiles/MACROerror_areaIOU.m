% --------------------------------------------------------------------------
% Error function to evaluate matching between two shapes
%
% Uses Point set positions and edges
%
% Method:
%
%
% --------------------------------------------------------------------------

function err = MACROerror_areaIOU (net1pos, net2pos, edgeSet, debugMode)
  if sum(net1pos(:)) == 0 || sum(net2pos(:)) == 0
    err = 0; return;
  end
  
  % add a tiny tiny amount of noise to the position signals
  % (problems arise with purely straight lines, sometimes)
  %   60 dB noise, 1/1000000 signal amplitude
  net1pos = awgn(net1pos, 60); net2pos = awgn(net2pos, 60); 

  % temporary variables to store the pos sets and add extra nodes
  net1X = net1pos; net2X = net2pos; netE = edgeSet;


  % pretty tight boundary calculation, but not the tightest
  %   avoids super tight corners
  shrinkFactor = 0.9;   

  % create a denser point cloud with virt nodes along all edges
  % tried with 1000 extra nodes, no significant difference from 100
  nVirtNodes = 100;
  net1X = shapeToPointCloud(net1pos,edgeSet,nVirtNodes);
  net2X = shapeToPointCloud(net2pos,edgeSet,nVirtNodes);
  netE = edgeSet;

  % keyboard
  if ~exist('debugMode', 'var')
    debugMode = 0;
  end

  % align the nets to remove euclidean transformation differences
  net2X = alignNets(net2X, net1X);

  % keyboard
  areaIntersection = shapeSetOp (net1X, net2X, 'intersection', shrinkFactor, debugMode);
  areaUnion = shapeSetOp(net1X, net2X, 'union', shrinkFactor, debugMode);

  err = areaIntersection/areaUnion;

end




