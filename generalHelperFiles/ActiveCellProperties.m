function y = ActiveCellProperties (hiRes)
% List of properties for the Active Cell
% These properties are used as parameters in several modules
% ------------------------------------------------------------------------

% Cell property resolution
if ~exist('hiRes', 'var')
	hiRes = 1;
end

% Nitinol Properties
y.m = 0.01; 
y.c = 4; 
y.nitiL0 = 0.8; 
y.kM = 0.0302; % N/mm from cell experimental data
y.kA = 0.7189; % N/mm from cell experimental data

% Spring Properties: cell v3 opt 1
% y.kSpring = 0.2; 
% y.springL0 = 1.75;

% Spring Properties: cell v4
y.springL0 = 35;	% mm
y.kSpring = 0.142;	% N/mm

% Cell properties
y.cellR = 1.5;
% y.cellInitL0 = 26.08;
% y.cellL0 = 26.08;
y.cellL0 = 25.0677;

% y.node_kt = 500; % random initial guess
% y.node_kt = 13326; % N/mm, bullshit: obtained from FEA bending stress 

y.node_kt = 0.0173; % Nmm/rad :obtained from FEA torsional stress analysis
% y.node_kt = 1e15; % Nmm/rad :obtained from FEA torsional stress analysis
% y.node_kt = 100*0.0173; % Nmm/rad 
% y.node_kt = 1e10; % Nmm/rad : no deflection at all
% y.node_kt = 0.001; % Nmm/rad : collapses mesh

y.nodeArmLen = 12.5; 
% y.nodeArmLen = 5; 
y.nodeToNodeDist = y.cellL0+y.nodeArmLen*2;

y.LendCaps = 1.65;
y.L0coil = 9.14; 
y.LcoilInCell = 15;
y.numCoils = 2;

y.Wcell = 10.5; % mm, measured across end-cap squares

% obtain f1 as two fits (forward T and backward T)
% -----------------------------------------------------------------------
fNiti_MtoA_AtoM = mappingTtoK (y.kM, y.kA);
y.fMtoA = fNiti_MtoA_AtoM{1}; 
y.fAtoM = fNiti_MtoA_AtoM{2};


rownorm = @(X) sqrt(sum((X).^2,2));


% a low resolution version of the cell that assumes niti stiffness
% and cell equilibrium length form a linear map
% -----------------------------------------------------------------------
	if hiRes == 0 
		cModel = Model_ActiveCell_v5();

		[~,~,nitiEqns, figH] = cModel.coilShowTtoL0K(...
			linspace(cModel.niti.austStartT, cModel.niti.austFinishT,100), ...
			'mart');
		springModel = cModel.springModel;
		close(figH);

		kRange = zeros(size(nitiEqns,1),1);
		xEqRange = zeros(size(nitiEqns,1),1);
		for kk=1:size(nitiEqns,1)
			kRange (kk) = nitiEqns(kk,1);
			xEqRange(kk) = fzero(@(x) polyval(springModel-nitiEqns(kk,:),x),0);
		end

		% converts a given K to the equilibrium cell L0
		% constrains the values within the cellL0 limits for aust and mart eq
		KtoXeq = polyfit(kRange,xEqRange,1);
 		minCellL0 = min(xEqRange); maxCellL0 = max(xEqRange);
		y.cellKtoXeq = @(x) max(min(polyval(KtoXeq,x),maxCellL0),minCellL0);

		% converts a given cell length to the niti stiffness that 
		% generates it
		XeqToK = polyfit(xEqRange, kRange, 1);
		y.cellXeqToK = @(x) max(min(polyval(XeqToK,x),y.kA),y.kM);
	end

end

