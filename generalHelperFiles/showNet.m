
% -------------------------------------- 
% Uses node positions and adjacency mat
% to plot network
% -------------------------------------- 
function showNet (pos, A, edgeColor, lineWidth, lineStyle)
	if nargin < 5
		lineStyle = '-';
	end
	if nargin < 4
		lineWidth = 1;
	end
	if nargin < 3
		edgeColor = [1 0 0];
	end
		
	pos = pos';
	hold all;
	plot(pos(:,1), pos(:,2), 'o', 'MarkerSize', 3);
	A = triu(A);
	for ii=1:length(A)
		ee = find(A(ii,:));
		for jj=1:length(ee)
			t = [pos(ii,:); pos(ee(jj),:)];
			% keyboard
			line(t(:,1), t(:,2), 'Color', edgeColor, ...
				'LineWidth', lineWidth, ...
				'LineStyle', lineStyle);
		end
	end
	axis equal
	% axis off
	% xlim([-0.75 1.75]); 
	% ylim([-0.75 1.75]);
end 

