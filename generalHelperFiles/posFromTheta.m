function nodePos = posFromTheta(cLen, nCount, eSet, eTheta)
% =================================================================
% 
% posFromTheta
% 
% Given the length of each cell, number of nodes and the angles of the
% edges, obtains the positons of the nodes
% 
% =================================================================
global cellProps
cellProps = ActiveCellProperties();
% if ~exist('cellProps','var')
% cellProps = ActiveCellProperties();
% end
initVect = [cLen+2*cellProps.nodeArmLen 0 0];
nodePos = inf*ones(3, nCount);

[roll, pitch, yaw] = deal(eTheta(1,:),eTheta(2,:),eTheta(3,:));

for ii=1:size(eSet,2)

  % get the direct cosine matrix from the given euler angles
  cG = cos(roll(ii));  sG = sin(roll(ii));
  cB = cos(pitch(ii)); sB = sin(pitch(ii));
  cA = cos(yaw(ii));   sA = sin(yaw(ii));

  Rz = [cA -sA 0
  sA cA  0
  0   0  1];
  Ry = [cB  0  sB
  0   1  0
  -sB 0  cB];
  Rx = [1  0   0
  0  cG -sG
  0  sG  cG];

  dcm = Rz*Ry*Rx;

  st = eSet(1,ii); % position of start node
  fin = eSet(2,ii);      

  if nodePos(1,st) == inf && nodePos(1,fin) == inf % both node pos unknown
    nodePos(:,st) = [0 0 0]';
    nodePos(:,fin) = dcm*initVect' + nodePos(:,st);

  elseif nodePos(1,st) == inf % start node pos not known
    nodePos(:,st) = - dcm*initVect' + nodePos(:,fin);

  elseif nodePos(1,fin) == inf % fin node pos not known
    nodePos(:,fin) = dcm*initVect' + nodePos(:,st);
  end
end

end

