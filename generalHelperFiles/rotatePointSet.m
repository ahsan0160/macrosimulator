function posOut = rotatePointSet (posIn, rotateAbout, rotateBy)
	posOut = posIn(1:2,:);

	R = [cos(rotateBy) -sin(rotateBy); 
		 sin(rotateBy) cos(rotateBy)];

	% dd = posIn-rotateAbout;
	% posOut = posIn-rotateAbout;

	% keyboard
	posOut = R*posOut;
	posOut(3,:) = 0;
end