

function fitresult = mappingTtoK (kMref, kAref)
% keyboard
if exist('simulatorHelperFiles/TtoKmapping.mat', 'file')
	load('simulatorHelperFiles/TtoKmapping.mat');
	if kA == kAref && kM == kMref
		return
	end 
end
T = linspace(0,120,200);
kA = kAref; 		kM = kMref;
T1m2a = 40;		T2m2a = 50;		
T1a2m = 45;		T2a2m = 25;

% piecewise linear profiles
% -----------------------------------------------------------------------
% define piecewise linear function for mart to aust curve
k = kM*ones(size(T));
k(T>T2m2a) = kA;
k(T<T2m2a & T > T1m2a)=...
	kM + (T(T<T2m2a & T>T1m2a)-T1m2a)*(kA-kM)/(T2m2a-T1m2a);

% define piecewise linear function for aust to mart curve
k2 = kM*ones(size(T));
k2(T>T1a2m) = kA;
k2(T>T2a2m & T<T1a2m) = ...
	kM + (T(T>T2a2m & T<T1a2m)-T2a2m)*(kA-kM)/(T1a2m-T2a2m);
figure(1); clf; hold all; plot(T,k, 'o--'); plot(T,k2,'o--');



% % Fitting fourier series to data
% % Code is generated automatically using cftool
% % -----------------------------------------------------------------------
[fitresult, gof] = createFits_TtoK(T, k, k2);
save('simulatorHelperFiles/TtoKmapping.mat');

end