function [outP, outE, outET] = updateNodeLabels (shapeP, shapeE, shapeET)
	outP = shapeP; outE = shapeE; outET = shapeET;

	tmpP = shapeP + 1e3; % move shape far to right and top
	tmpP(2,:) = tmpP(2,:) - min(tmpP(2,:)); % bring shape down to hit x-axis

	lowestNodeLayer = find(tmpP(2,:) < 0.1);
	leftMostNodeInBottomLayer = find(...
		tmpP(1,lowestNodeLayer) < min(tmpP(1,lowestNodeLayer))+0.1);
	leftMostNodeInBottomLayer = lowestNodeLayer(leftMostNodeInBottomLayer);

	% leftMostNodeInBottomLayer should be node 1
	% nNodes = max(shapeE(:));

	outE(outE==leftMostNodeInBottomLayer) = inf;
	outE(outE==1) = leftMostNodeInBottomLayer;
	outE(outE==inf) = 1;

	tt = outP(:,1);
	outP(:,1) = outP(:,leftMostNodeInBottomLayer);
	outP(:,leftMostNodeInBottomLayer) = tt;

	outP(1,:) = outP(1,:) - outP(1,1);
	outP(2,:) = outP(2,:) - outP(2,1);
	% keyboard
end

