
function [nodeLayer] = pointSetEdgeNodes (pos, edges, side, thresh)
% --------------------------------------------------------------------------
% Uses positions of nodes and edge connectivity information to find the 
% left hand boundary of a node set
% --------------------------------------------------------------------------

  if ~exist('side', 'var')
      side = 'left';
  end

  errTol = 5;
  virtualNodes = 100;
  tmpPos = shapeToPointCloud (pos,edges,virtualNodes);
  bndNodeNum = boundary(tmpPos(1:2,:)',.9);

  % ---------------------------------------------------------
  % finds the tight boundary nodes for the overall shape
  % ---------------------------------------------------------

  % % * * * * OBSOLETE * * * *  
  % % ----------------------------------------
  % % straight wall method: OBSOLETE
  % % ----------------------------------------
  % ymax = max(pos(2,:)); ymin = min(pos(2,:));
  % xmax = max(pos(1,:)); xmin = min(pos(1,:));
  % overallHeight = ymax - ymin;
  % overallWidth = xmax - xmin;

  % yThTop = ymax - threshold*overallHeight;
  % yThBot = ymin + threshold*overallHeight;
  % xThLeft = xmin + threshold*overallWidth;
  % xThRight = xmax - threshold*overallWidth;

  % idxL = find((tmpPos(2,bndNodeNum) < yThTop) & ...
  %     (tmpPos(2,bndNodeNum) > yThBot) & ...
  %     (tmpPos(1,bndNodeNum) < xThRight));

  % idxR = find((tmpPos(2,bndNodeNum) < yThTop) & ...
  %     (tmpPos(2,bndNodeNum) > yThBot) & ...
  %     (tmpPos(1,bndNodeNum) > xThLeft));

  % idxT = find((tmpPos(1,bndNodeNum) > xThLeft) & ...
  %     (tmpPos(1,bndNodeNum) < xThRight) & ...
  %     (tmpPos(2,bndNodeNum) > yThBot));

  % idxB = find((tmpPos(1,bndNodeNum) > xThLeft) & ...
  %     (tmpPos(1,bndNodeNum) < xThRight) & ...
  %     (tmpPos(2,bndNodeNum) < yThTop));
  
  % prelimRlvntBndL = bndNodeNum(idxL);
  % prelimRlvntBndR = bndNodeNum(idxR);
  % prelimRlvntBndT = bndNodeNum(idxT);
  % prelimRlvntBndB = bndNodeNum(idxB);

  % ----------------------------------------
  % straight wall with allowance method
  % ----------------------------------------
  leftNodeIdx = find(tmpPos(1,:) <= min(tmpPos(1,:)), 1);
  leftMostNode = tmpPos(:,leftNodeIdx);

  rightNodeIdx = find(tmpPos(1,:) >= max(tmpPos(1,:)), 1);
  rightMostNode = tmpPos(:,rightNodeIdx);
  
  bottomNodeIdx = find(tmpPos(2,:) <= min(tmpPos(2,:)), 1);
  bottomMostNode = tmpPos(:,bottomNodeIdx);

  topNodeIdx = find(tmpPos(2,:) >= max(tmpPos(2,:)), 1);
  topMostNode = tmpPos(:,topNodeIdx);

  p1 = pos(:,edges(1,:));
  p2 = pos(:,edges(2,:));
  maxInterNodeDist = max(sqrt(sum((p2-p1).^2,1)));

  % cellProps = ActiveCellProperties();
  % threshold = 0.7*cellProps.nodeToNodeDist;
  if ~exist('thresh', 'var')
    threshold = 0.65*maxInterNodeDist;
  else
    % thresh
    threshold = thresh * maxInterNodeDist;    
  end
  % threshold
  prelimRlvntBndL = find(tmpPos(1,:) <= leftMostNode(1) + threshold);
  prelimRlvntBndR = find(tmpPos(1,:) >= rightMostNode(1) - threshold);
  prelimRlvntBndT = find(tmpPos(2,:) >= topMostNode(2) - threshold);
  prelimRlvntBndB = find(tmpPos(2,:) <= bottomMostNode(2) + threshold);


  % ---------------------------------------------------------
  % filter original node pos for correct node set
  % ---------------------------------------------------------
  if strcmpi(side, 'left')
    nodeLayer = filterBoundary(pos,tmpPos,edges,prelimRlvntBndL);
  elseif strcmpi(side, 'right')
    nodeLayer = filterBoundary(pos,tmpPos,edges,prelimRlvntBndR);
  elseif strcmpi(side, 'top')
    nodeLayer = filterBoundary(pos,tmpPos,edges,prelimRlvntBndT);
  elseif strcmpi(side, 'bottom')
    nodeLayer = filterBoundary(pos,tmpPos,edges,prelimRlvntBndB);
  else 
    disp('ERROR');
    keyboard
  end

  % figure; 
  % showShape(pos);
  % showShape(pos(:,filterBoundary(pos,tmpPos,edges,prelimRlvntBndB)));
  % keyboard

  while isempty(nodeLayer)
    disp('==> HAD TO GO DOWN RABBIT HOLE...');

    figure; showShape(pos);
    line([min(pos(1,:)) max(pos(1,:))], [yThTop yThTop]);
    line([min(pos(1,:)) max(pos(1,:))], [yThBot yThBot]);
    line([xThLeft xThLeft], [min(pos(2,:)) max(pos(2,:))]);
    line([xThRight xThRight], [min(pos(2,:)) max(pos(2,:))]);
    showShape(pos);
    showShape(tmpPos(:,bndNodeNum));
    showShape(pos(:,nodeLayer));
    keyboard

    [nodeLayer] = pointSetEdgeNodes(pos,edges,side,threshold-0.05);
  end

end


function rlvntBnd = filterBoundary (pos, densePos, edges, prelimRlvntBnd)
  % parse through actual node pos, and look for all nodes that come close
  % to the prelimRlvntBnd
  rlvntBnd = [];
  rownorm = @(X) sqrt(sum((X).^2,2));

  % cellProps = ActiveCellProperties();
  p1 = pos(:,edges(1,:));
  p2 = pos(:,edges(2,:));
  maxInterNodeDist = max(sqrt(sum((p2-p1).^2,1)));

  for kk=1:size(pos, 2)
    actualNodeToBndVects = ...
      repmat(pos(:,kk),1,length(prelimRlvntBnd)) - ...
      densePos(:,prelimRlvntBnd);
    actualNodeToBndDists = rownorm(actualNodeToBndVects');
    % if any(find(actualNodeToBndDists < 0.25*cellProps.nodeToNodeDist))
    if any(find(actualNodeToBndDists < 0.25*maxInterNodeDist))
      rlvntBnd = [rlvntBnd kk];
    end
  end
end
