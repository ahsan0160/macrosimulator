
function err = MACROerror_cellLength (net1pos, net2pos, A)   	
	if sum(net1pos(:)) == 0 || sum(net2pos(:)) == 0
		err = 0; return;
	end
	
	net2adjusted = alignNets(net2pos, net1pos);
	[x,y] = find(triu(A));
	
	rownorm = @(X) sqrt(sum((X).^2,2));
	cellLengths1 = rownorm((net1pos(:,x) - net1pos(:,y))');
	cellLengths2 = rownorm((net2adjusted(:,x) - net2adjusted(:,y))');
    
	err = mean(abs(cellLengths1 - cellLengths2));
end

