


global_path_setup
set(0,'defaultlinelinewidth',1);
clear tempArray; 
clear posArray;
clc;

pl_workspace = figure;

% common input parameters
% --------------------------------------------------------------------------
global cellProps
cellProps = ActiveCellProperties();
name = 'test';

% [name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_square_01 ();
[name, edgeSet,edgeTheta,startPos,targetPos,anchors,fixedInX,fixedInY,fixedInZ] = unit_triangle_04 ();

resolution = 'step_0-5';
% resolution = 'step_1-0';
fname = strcat('zzWorkspaces/',name,'_workspace_',resolution);
load (strcat(fname,'.mat'));

% template_macro = MACRO(edgeSet, edgeTheta);

pointCloud = reshape(posArray, 3, ...
	size(posArray,2)*size(posArray,3));

clf; hold on;
% fprintf(2,'Plotting point cloud (start)\n');
% plot3(pointCloud(1,:), pointCloud(2,:), pointCloud(3,:), ...
% 	'o', 'Color', 'b','MarkerSize', 10);
% axis equal;
% axis off
% fprintf(2,'Plotting point cloud (end)\n');

hold on;
fprintf(2,'Plotting nodes and edges (start)\n');
for kk=1:5:size(tempArray,1)
	r = squeeze(posArray(:,:,kk));
	for ee = 1:size(edgeSet,2)
		st = edgeSet(1,ee);
		fin = edgeSet(2,ee);
		line( [r(1,st) r(1,fin)], [r(2,st) r(2,fin)], ...
              [r(3,st) r(3,fin)],'LineStyle', '-', ...
                        'LineWidth', 1);
	end
end
axis equal;
axis off
fprintf(2,'Plotting nodes and edges (end)\n');

% figure(2);  hold on;
% for kk=1:size(tempArray,1)
% 	template_macro.show(2, posArray(:,:,kk));	
% end


printFig(pl_workspace, '', ...
	name, strcat('_workspace_', resolution));


