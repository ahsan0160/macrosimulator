% -----------------------------------------------------------------------------
function [outP, outE, outET] = updateEdgeLabels (shapeP, shapeE, shapeET)
% -----------------------------------------------------------------------------
% 1. ensure no missing node labels exist in edgeset
% 2. ensure that the first edge is node 1 to some other node
% 3. sort edge order so connectivity is maintained in reading the edgeset
% 	 start to end:
% 	 if any edge contains only unparsed nodes (left to right), the edge is
%	  moved to end of the list
% -----------------------------------------------------------------------------
	[outP, outE, outET] = deal (shapeP, shapeE, shapeET);
	% keyboard
	% remove missing node labels
	% -------------------------------------------------------------
	outE = removeMissingNodeLabels (outE, size(outP,2));

	% move edge with start node 1 to head of list
	% -------------------------------------------------------------
	indx = outE(1,:)==1;
	if ~any(indx)
		indy = outE(2,:)==1;
		if ~any(indy)
			disp('node 1 is missing. error!');
			keyboard
		else
			[outE, outET] = flipEdge(outE, outET, find(indy,1));
		end
	else
		[outE, outET] = swapEdge(outE, outET, 1, find(indx,1));
	end


	% edge ordering so connectivity is maintained
	% -------------------------------------------------------------
	ii = 2; % for each edge in the edgeset after the first
	repeatCnt = zeros(1,size(outE,2));
	while ii <= size(outE,2)
		% if either node in this edge has not appeared before in the parsed set
		% keyboard
		if (any(outE(1,ii)==outE(1,1:ii-1) | outE(1,ii)==outE(2,1:ii-1)) == 0) ...
			&& ...
			(any(outE(2,ii)==outE(1,1:ii-1) | outE(2,ii)==outE(2,1:ii-1)) == 0)

			% move (not copy) this edge to the end of the edgeset
			tt1 = outE(:,ii); tt2 = outET(:,ii);

			outE(:,ii:end-1) = outE(:,ii+1:end); outE(:,end) = tt1;
			outET(:,ii:end-1) = outET(:,ii+1:end); outET(:,end) = tt2;

			repeatCnt(ii) = repeatCnt(ii)+1;
			if any(repeatCnt > size(outE,2)+1)
				keyboard
				break;
			end
			ii = ii - 1;
		end
		ii = ii + 1;
	end
	if any(repeatCnt)
		% repeatCnt
	end

end
% -----------------------------------------------------------------------------




function [outE, outET] = flipEdge(shapeE, shapeET, index)
	[outE, outET] = deal(shapeE, shapeET);

	% swap nodes in edgeset at index
	nn = outE(1,index);
	outE(1,index) = outE(2,index);
	outE(2,index) = nn;

	% change the edge angle
	outET(3,index) = outET(3,index) + pi;

	% constrain edge angles to -2pi to 2pi
	if outET(3,index) > 2*pi
		outET(3,index) = outET(3,index) - 2*pi;
	elseif outET(3,index) < -2*pi
		outET(3,index) = outET(3,index) + 2*pi;
	end


end



function [outE, outET] = swapEdge(shapeE, shapeET, index1, index2)
	[outE, outET] = deal(shapeE,shapeET);

	tt1 = outE(:,index1);
	tt2 = outET(:,index1);

	outE(:,index1) = outE(:,index2);
	outE(:,index2) = tt1;

	outET(:,index1) = outET(:,index2);
	outET(:,index2) = tt2;
end


% -----------------------------------------------------------------------------
function [outE] = removeMissingNodeLabels (shapeE, nNodes)
% -----------------------------------------------------------------------------
% relabels nodes in the edgeset so no missing node numbers exist
% -----------------------------------------------------------------------------
	outE = shapeE;
	ii = 1;
	for ii=1:max(outE(:)) % for each node in the label set
		indx = (outE(1,:)==ii) + (outE(2,:)==ii); % edges this node appears
		while ~any(indx) && ii <= nNodes % if node label appears nowhere
			% keyboard
			% renumber the nodes in the edgeset to remove this particular label
			outE(1,outE(1,:)>ii) = outE(1,outE(1,:)>ii)-1;
			outE(2,outE(2,:)>ii) = outE(2,outE(2,:)>ii)-1;

			% check for presence of node label again:
			% in case multiple consecutive labels are missing
			indx = (outE(1,:)==ii) + (outE(2,:)==ii);
		end
		outE;
	end
end
% -----------------------------------------------------------------------------


