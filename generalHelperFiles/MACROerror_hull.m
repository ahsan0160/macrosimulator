
function err = MACROerror_hull (net1pos, net2pos, edgeSet)
	if sum(net1pos(:)) == 0 || sum(net2pos(:)) == 0
		err = 0; return;
	end
	
	try
		if ~any(net1pos(3,:))	% 2D set of points
			[kk1, area1] = convhull(net1pos(1,:)', net1pos(2,:)');
			if ~isempty(net2pos)
				[kk2, area2] = convhull(net2pos(1,:)', net2pos(2,:)');
			else
				area2 = 0;
			end
			err = area1-area2;

		else 	% 3D 
			[kk1, vol1] = convhull(net1pos(1,:)',net1pos(2,:)',net1pos(3,:)');
			if ~isempty(net2pos)
				[kk2,vol2]=convhull(net2pos(1,:)',net2pos(2,:)',net2pos(3,:)');
			else
				vol2 = 0;
			end
			err = vol1-vol2; 
			% afsdf
		end
	catch ME
		if (strcmp(ME.identifier,'MATLAB:convhull:EmptyConvhull2DErrId'))
			err = NaN;
		else
			err = 0;
		end
	end 

end

