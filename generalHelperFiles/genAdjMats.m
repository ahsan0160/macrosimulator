function [A, Ae] = genAdjMats (edgeSet, nNodes)
	nEdges = size(edgeSet,2);
	A = sparse(edgeSet(1,:), edgeSet(2,:), 1, nNodes, nNodes, nEdges);
	A = full(A+A');

	Ae = full(sparse(1:nEdges, edgeSet(1,:), 1, nEdges, nNodes, nEdges));
	Ae = Ae + full(sparse(1:nEdges, edgeSet(2,:), -1, nEdges, nNodes, nEdges));
