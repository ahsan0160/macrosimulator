% Model_ActiveCell_v5.m
%
% ActiveCell is a combination of two Nitniol coils with a passive biasing 
% spring. 
% 
% The cell has a rest length equal to the equlibrium length of the two coils 
% with the spring, and a stiffness equal to the coil stiffness at the 
% equlibrium point (give a particular temperature).
% 
% As a cell is activated with power dissipation, the temperature rise in the
% coils causes a change in the stiffness of the coils, while the coil rest 
% length remains unchanged. The changing stiffness of the coils is visualized 
% in the n coilShowTtoL0K. 
% 
% With a temeprature change in the Nitinol coils, the stiffness profiles of 
% the coils change, and this changes the equlibrium point of the coils with 
% the passive spring. Thus the 'rest length' of the cell is changed. The cell 
% has a net stiffness equal to the added stiffness of the spring and the coils 
% at the current temeprature. Given a specific temperature, and the state of 
% the cell (fully martensite, transitioning to austenite, fully austenite or 
% transitioning to martensite), the stiffness of the cell and the 
% 'rest length' of the cell is provided by the function cellMapTtoL0andK. 
% Note: The cell may have different stiffness in compressive and tensile 
% directions, and both are output from this function.
% 
% Active Cell Network Simulations
% Created 2014 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% (c) 2014 Yale University
% All rights reserved.
classdef Model_ActiveCell_v5 < hgsetget
  properties (Constant = true)
    F_MART = 2; F_AUST = 3; M_TO_A = 4; A_TO_M = 5;
  end

  properties
    % MEASURED VALUES
    % ---------------
    numCoils; LendCaps; L0coil; LcoilInCell; L0cell; springL0; kSpring;
    martModel; martRefModel; austModel; springModel;

    xAustEq; yAustEq; % spring intersection with aust curve
    mAustEq; % k of niti where spring intersects with aust curve
    
    xMartEq; yMartEq; % spring intersection with mart curve
    mMartEq; % k of niti where spring intersects with mart curve
    mMartLin; % k of niti in compression in martensite state

    % addition of xEq with the end cap thickness
    L0martEq; L0austEq;
    
    niti = struct('austStartT', 40, 'austFinishT', 50, ...
            'martStartT', 45, 'martFinishT', 25);
  end
  
  methods
  % ========================================================================
  % 
  % MODEL CONSTRUCTOR
  % Uses data-driven parametrizations performed externally to generate 
  % a model of the Nitinol coils. This model is used to obtain eqlibrium
  % points of the coils in the cell with passive biasing spring, as well
  % as modifying this equlibria given temperature changes
  % 
  % ========================================================================
    function obj = Model_ActiveCell_v5()
      obj.calculateNiTiHeatProperties();
      
      global cellProps
      if ~exist('cellProps','var')
        cellProps = ActiveCellProperties();
      end
      obj.LendCaps = cellProps.LendCaps;
      obj.L0coil = cellProps.L0coil;
      obj.LcoilInCell = cellProps.LcoilInCell;
      obj.springL0 = cellProps.springL0;
      obj.kSpring = cellProps.kSpring;
      obj.numCoils = cellProps.numCoils;

      load('nitinolModelParams.mat');

      % Generate models
      % ---------------
      obj.austModel = @(x,l0,n)n*meanK*austRefCoilL0/l0*(x-l0);

      obj.martRefModel = @(x,a,b,c,d,e,f,g,xSt,xEnd) ...
        (1-H(x,xSt)).*(a.*x+b)+ ...
        (H(x,xSt)-H(x,xEnd)).*(c.*exp(-d.*x)+e) + ...
        H(x,xEnd) .* (f.*x+g) + ...
        endpoints_penalty*abs((a*xSt+b) - (c*exp(-d*xSt+e))) + ...
        endpoints_penalty*abs((c*exp(-d*xEnd+e) - (f*xEnd+g)));
      obj.martModel = @(x,L0,n) ...
        n*obj.martRefModel((x-L0)/L0*martRefCoilL0,...
        a,b,c,d,e,f,g,x1,x2);
      obj.springModel = [-obj.kSpring obj.kSpring*obj.springL0];

      % Compute default equlibrium points
      % ---------------------------------
      [obj.xAustEq] = fzero(@(x) ...
        polyval(obj.springModel,x)-...
          obj.austModel(x,obj.L0coil,obj.numCoils),0);
      obj.yAustEq = polyval(obj.springModel, obj.xAustEq);
      
      [obj.xMartEq] = fzero(@(x) ...
        polyval(obj.springModel,x)-...
          obj.martModel(x,obj.L0coil,obj.numCoils),0);
      obj.yMartEq = polyval(obj.springModel, obj.xMartEq);

      % Compute stiffnesses at the equlibrium points
      % --------------------------------------------
      % NUMERICAL STIFFNESS COMPUTATION
      obj.mAustEq = meanK*austRefCoilL0/obj.L0coil*obj.numCoils;
      x1Scaled = obj.L0coil + x1/martRefCoilL0*obj.L0coil;
      x2Scaled = obj.L0coil + x2/martRefCoilL0*obj.L0coil;
  
      xx = obj.xMartEq*[0.99 1.01];
      yy = obj.martModel(xx,obj.L0coil,obj.numCoils);
      obj.mMartEq = (yy(2)-yy(1))/(xx(2)-xx(1));
  
      xx = obj.L0coil*[1 1.01];
      yy = obj.martModel(xx,obj.L0coil,obj.numCoils);
      obj.mMartLin = (yy(2)-yy(1))/(xx(2)-xx(1));

      obj.L0martEq = obj.xMartEq + 2*obj.LendCaps;
      obj.L0austEq = obj.xAustEq + 2*obj.LendCaps;

      obj.L0cell = obj.L0martEq;  % initially assume mart state
    end


  % ========================================================================
  % 
  % SHOW COIL BEHAVIOR CHANGE WITH TEMPERATURE
  % 
  % ========================================================================
    function [kArray, L0, nitiEqns, kk] = coilShowTtoL0K (obj, Tarray, state)
      kk = figure; clf; hold on;
      set(gca, 'ColorOrder', colormap('gray'))
      % austStrain = linspace(obj.LcoilInCell/obj.L0coil,2);
      % martStrain = linspace(obj.LcoilInCell/obj.L0coil,5);
      austStrain = linspace(1,2);
      martStrain = linspace(1,5);
      plot([obj.LcoilInCell obj.LcoilInCell], [0 5],'k-.');

      % compute forces for both profiles
      targetAustF = obj.austModel(...
        austStrain*obj.L0coil, obj.L0coil,obj.numCoils);
      targetMartF = obj.martModel(...
        martStrain*obj.L0coil, obj.L0coil, obj.numCoils);

      % plot force-extension profiles
      targetAustPlot = plot(austStrain*obj.L0coil, targetAustF, '-', ...
        'LineWidth', 4, 'color', 0.75*[1 0 0]);
      targetMartPlot = plot(martStrain*obj.L0coil, targetMartF, '-', ...
        'LineWidth', 4, 'color', 0.75*[0 1 0]);
      legend([targetAustPlot targetMartPlot], ...
        {'Target: Austenite','Target: Martensite'})

      % plot spring equation
      xx=0:max(martStrain*obj.L0coil);
      springX = xx(xx <= obj.springL0);
      plot(springX, polyval(obj.springModel,springX),'b-');

      axis auto;
      xlim([min(xx) max(xx)]);

      % show equlibrium points
      plot(obj.xAustEq, ...
        obj.austModel(obj.xAustEq,obj.L0coil,obj.numCoils), ...
        'ko', 'MarkerSize', 15);
      plot(obj.xMartEq, ...
        obj.martModel(obj.xMartEq,obj.L0coil,obj.numCoils), ...
        'ko', 'MarkerSize', 15);

      nitiEqns = [];

      if strcmpi(state,'mart')
        % Show the changing slope from martensite equlibrium 
        % to austenite equlibrium, for given temperature range
        % ------------------------------------------------------------
        kArray = -1*ones(size(Tarray));
        kArray(Tarray < obj.niti.austStartT) = obj.mMartEq;
        kArray(Tarray > obj.niti.austFinishT) = obj.mAustEq;
        kArray(kArray < 0) = interp1(...
          [obj.niti.austStartT obj.niti.austFinishT],...
          [obj.mMartEq obj.mAustEq],...
          Tarray(kArray < 0));

        L0 = -1*ones(size(Tarray));
        L0(Tarray < obj.niti.austStartT) = obj.xMartEq;
        L0(Tarray > obj.niti.austFinishT) = obj.xAustEq;
        L0(L0 < 0) = interp1(...
          [obj.niti.austStartT obj.niti.austFinishT],...
          [obj.xMartEq obj.xAustEq],...
          Tarray(L0 < 0));

        F = polyval(obj.springModel, L0);
        for ii=1:length(Tarray) 
          tmpEqn = [kArray(ii) F(ii)-kArray(ii)*L0(ii)];
          nitiEqns = [nitiEqns; tmpEqn];
          tmpX = linspace(L0(ii)-1, L0(ii)+1);
          % tmpX = linspace(obj.LcoilInCell, L0(ii)+1);
          plot(tmpX, polyval(tmpEqn, tmpX), '-');
        end
        xlim([0 obj.springL0]);


      elseif strcmpi(state, 'aust')
        % Show the changing slope from austenite equlibrium 
        % to martensite equlibrium, for given temperature range
        % ------------------------------------------------------------
        kArray = -1*ones(size(Tarray));
        kArray(Tarray > obj.niti.martStartT) = obj.mAustEq;
        kArray(Tarray < obj.niti.martFinishT) = obj.mMartEq;
        kArray(kArray < 0) = interp1(...
          [obj.niti.martStartT obj.niti.martFinishT],...
          [obj.mAustEq obj.mMartEq],...
          Tarray(kArray < 0));

        L0 = -1*ones(size(Tarray));
        L0(Tarray > obj.niti.martStartT) = obj.xAustEq;
        L0(Tarray < obj.niti.martFinishT) = obj.xMartEq;
        L0(L0 < 0) = interp1(...
          [obj.niti.martStartT obj.niti.martFinishT],...
          [obj.xAustEq obj.xMartEq],...
          Tarray(L0 < 0));

        F = polyval(obj.springModel, L0);
        for ii=1:length(Tarray) 
          tmpEqn = [kArray(ii) F(ii)-kArray(ii)*L0(ii)];
          nitiEqns = [nitiEqns; tmpEqn];
          tmpX = linspace(L0(ii)-1, L0(ii)+1);
          % tmpX = linspace(obj.LcoilInCell, L0(ii)+1);
          plot(tmpX, polyval(tmpEqn, tmpX), '-');
        end
        xlim([0 obj.springL0]);
      end

    end


  % ========================================================================
  % 
  % CELL PROPERTY MAPPING GIVEN TEMPERATURE
  % At a given temperature, the stiffness of coils is linearly mapped from
  % martensitic stiffness to austenitic stiffness accounting for the 
  % temperature hysteresis of nitinol.
  % The compressive and tensile stiffness depends on the state of the coils 
  % at the present time.
  % 
  % The cell rest length is computed by obtaining the updated equilibrium
  % points of the coils (with updated stiffness) and the spring.
  % 
  % ========================================================================
    function [L0cell, kExt, kComp, state] = cellMapTtoL0andK ...
      (obj, T, transType)
      if nargin == 2
        transType = obj.M_TO_A;
      end

      % set up some range matrices to easily compute linear mappings
      T_ToAust = [obj.niti.austStartT   obj.niti.austFinishT];
      T_ToMart = [obj.niti.martStartT   obj.niti.martFinishT];
      x_ToAust = [obj.xMartEq     obj.xAustEq];
      x_ToMart = [obj.xAustEq     obj.xMartEq];
      kComp_ToAust =  [obj.mMartLin   obj.mAustEq];
      kComp_ToMart =  [obj.mAustEq    obj.mMartLin];
      kExt_ToAust =   [obj.mMartEq    obj.mAustEq];
      kExt_ToMart =   [obj.mAustEq    obj.mMartEq];
      
      ll=[]; 
      transType (transType == obj.F_MART) = obj.M_TO_A;
      transType (transType == obj.F_AUST) = obj.A_TO_M;

      m2a = transType == obj.M_TO_A;
      a2m = transType == obj.A_TO_M;

      [newL0 kExt kComp state] = deal(ones(size(transType)));
      
      % CONDITION 1:
      % temperature not sufficient to begin m2a transformation
      % will remain in martensite state:
      %   in extension, stiffness = mart-spring-eq stiffness
      %   in compression, stiffness = mart-linear stiffness
      %   rest length of cell is unchanged
      idx1 = m2a & T < obj.niti.austStartT;
      if any(idx1)
        kExt(idx1) = obj.mMartEq;
        kComp(idx1) = obj.mMartLin;
        state(idx1) = obj.F_MART; 
        newL0(idx1) = obj.L0martEq;
      end

      % CONDITION 2:
      % martensite to austenite transformation
      %   in extension, stiffness = mart to aust mapping
      %   in compression, stiffness = martLin to aust mapping
      %   rest length = intersection of kExt eqn with spring
      idx2 = m2a & T >= obj.niti.austStartT & T <= obj.niti.austFinishT;
      if any(idx2)
        kExt(idx2) = interp1(T_ToAust,kExt_ToAust,T(idx2),'pchip');
        kComp(idx2) = interp1(T_ToAust,kComp_ToAust, T(idx2),'pchip');
        state(idx2) = obj.M_TO_A; 
        newL0(idx2) = interp1(T_ToAust,x_ToAust,T(idx2), 'pchip')+...
          2*obj.LendCaps;
      end    

      % CONDITION 3:
      % martensite to austenite transformation complete
      %   in extension, stiffness = aust
      %   in compression, stiffness = aust
      %   rest length = eq length with aust
      idx3 = m2a & T > obj.niti.austFinishT;
      if any(idx3)
        kExt(idx3) = obj.mAustEq;
        kComp(idx3) = obj.mAustEq;
        state(idx3) = obj.F_AUST;
        newL0(idx3) = obj.L0austEq;
      end

      % CONDITION 4:
      % temperature too high for reversal to martensite
      % remains in austenite state
      % IDENTICAL TO CONDITION 3
      idx4 = a2m & T > obj.niti.martStartT;
      if any(idx4)
        kExt(idx4) = obj.mAustEq;
        kComp(idx4) = obj.mAustEq;
        state(idx4) = obj.F_AUST;
        newL0(idx4) = obj.L0austEq;
      end

      % CONDIION 5:
      % transition to martensite initiated
      % INVERSE OF CONDITION 2
      idx5 = a2m & T <= obj.niti.martStartT & T >= obj.niti.martFinishT;
      if any(idx5)
        kExt(idx5) = interp1(T_ToMart,kExt_ToMart,T(idx5),'pchip');
        kComp(idx5) = interp1(T_ToMart,kComp_ToMart, T(idx5),'pchip');
        state(idx5) = obj.A_TO_M; 
        newL0(idx5) = interp1(T_ToMart,x_ToMart,T(idx5), 'pchip')+...
          2*obj.LendCaps;
      end

      % CONDITION 6:
      % transformation to martensite completed
      % IDENTICAL TO CONDITION 1
      idx6 = a2m & T < obj.niti.martFinishT;
      if any(idx6)
        kExt(idx6) = obj.mMartEq;
        kComp(idx6) = obj.mMartLin;
        state(idx6) = obj.F_MART; 
        newL0(idx6) = obj.L0martEq;
      end
                    
      % cell stiffness = coil stiffness + spring stiffness 
      % in both directions
      kComp = kComp + obj.kSpring;
      kExt = kExt + obj.kSpring;
      
      % cell length is the new equilibrium point of coils and spring 
      % added to the end cap thickness
      L0cell = newL0;
      % keyboard
    end

    


  % ========================================================================
  % 
  % SHOW CELL BEHAVIOR CHANGE WITH TEMPERATURE
  % 
  % ========================================================================
    function cellShowTtoL0K (obj, Tarray, startingState) 
      figure(1); clf; hold on;
      set(gca, 'ColorOrder', colormap('gray'))
      austStrain = linspace(obj.LcoilInCell/obj.L0coil,2);
      martStrain = linspace(obj.LcoilInCell/obj.L0coil,5);

      % compute forces for both profiles
      targetAustF = obj.austModel(...
        austStrain*obj.L0coil, obj.L0coil,obj.numCoils);
      targetMartF = obj.martModel(...
        martStrain*obj.L0coil, obj.L0coil, obj.numCoils);

      % plot force-extension profiles
      targetAustPlot = plot(austStrain*obj.L0coil, targetAustF, '-', ...
        'LineWidth', 4, 'color', 0.75*[1 0 0]);
      targetMartPlot = plot(martStrain*obj.L0coil, targetMartF, '-', ...
        'LineWidth', 4, 'color', 0.75*[0 1 0]);
      legend([targetAustPlot targetMartPlot], ...
        {'Target: Austenite','Target: Martensite'})

      % plot spring equation
      xx=0:max(martStrain*obj.L0coil);
      springX = xx(xx <= obj.springL0);
      plot(springX, polyval(obj.springModel,springX),'b-');

      axis auto;
      xlim([min(xx) max(xx)]);
      
      % show equlibrium points
      plot(obj.xAustEq, ...
        obj.austModel(obj.xAustEq,obj.L0coil,obj.numCoils), ...
        'ko', 'MarkerSize', 15);
      plot(obj.xMartEq, ...
        obj.martModel(obj.xMartEq,obj.L0coil,obj.numCoils), ...
        'ko', 'MarkerSize', 15);

      state = startingState;

      for ii=1:length(Tarray)
        if exist('h1')  & exist('h2')
          delete(h1); delete(h2);
        end
        [L0cell, cellKExt, cellKComp, state] = ...
          obj.cellMapTtoL0andK (Tarray(ii), state(1));
        L0 = L0cell-obj.L0coil-obj.LendCaps*2;
        yy = polyval(obj.springModel,L0);
        kExt = cellKExt - obj.kSpring;
        kComp = cellKComp - obj.kSpring;

        % kExt = cellKExt;
        % kComp = cellKComp;

        tmpEqnExt = [kExt yy-kExt.*L0]
        tmpXExt = linspace(L0, L0+5);
        tmpEqnComp = [kComp yy-kComp*L0];
        tmpXComp = linspace(L0-5, L0);

        h1= plot(tmpXExt, polyval(tmpEqnExt, tmpXExt), 'k-');
        h2= plot(tmpXComp, polyval(tmpEqnComp, tmpXComp), 'k-');
        xlim([0 obj.springL0]);
        % input('');
        axis auto
        pause(0.01);
        hold on;
        % keyboard
      end
    end
    
    
    
    % ------------------------------------------------------------
    % Calculates the heat calculation properties
    % including mass of niti coils and specific heat
    % ------------------------------------------------------------
    function calculateNiTiHeatProperties (obj)
      wireDia = 0.305; % mm
      coilInnerDia = 1; % mm

      % center to middle of torus ring
      torus_R = coilInnerDia + wireDia/2; 
      % radius of tube
      torus_r = wireDia/2; 
      
      loopVol = 2*pi^2* torus_R * torus_r^2;
      numLoops = (40/(0.48*25.4)) * 7.06;
      nitiVol = loopVol*numLoops * 2; % 2 coils
      
      rho_Ni = 8.908 / 1000; % g-mm^-3
      rho_Ti = 4.506 / 1000; % g-mm^-3
      q_Ni_in_coil = 0.5;
      nitiRho = (q_Ni_in_coil*rho_Ni + (1-q_Ni_in_coil)*rho_Ti);
      nitiMass = nitiRho * nitiVol;
      
      cNiti = 0.2; % specific heat (dynalloy) in cal/g*C
      cNiti = cNiti * 4.18; % J/g*C
      
      obj.niti.mass = nitiMass;
      obj.niti.c = cNiti;
            % keyboard
    end
    

    function dT = computeT(obj, dEnergy)
      mc = obj.niti.mass*obj.niti.c;
      dT = dEnergy/mc;
    end


    function hh=plotLine (obj,x,y,k,xLimits)
      if nargin < 5
        xLimits = [x-5, x+5];
      end
      xRange = linspace(xLimits(1),xLimits(2),10);
      eqn = [k  y-k*x];
      hh=plot(xRange,polyval(eqn,xRange,'k-'));
    end

  end
end


