

classdef Model_Fan < hgsetget

	properties
		w = 125.664; 	% rad/s = 1200RPM
		V = 12;			% V
		R = 100; 		% ohms
		q = 11.6699; 	% Pa, static pressure
		dp = 0.02986;	% m^3/s, airflow

		t = 0; 			% time 
		startupTime = 2; 
		numFans = 4;
		fanPower = 0;

		status = 0;		% initially turned off
		poweringDown = 0;
		poweringUp = 0;
	end
	
	methods

		function obj=Model_Fan()
			operatingV = 6;
			efficiency = (obj.dp*obj.q)/(obj.V^2/obj.R);
			obj.fanPower = obj.numFans * efficiency * (operatingV^2/obj.R);
			obj.t = 0;
		end

		function pwr = coolingPower(obj,dt,status)

			% status is being updated
			if nargin == 2
				status = obj.status;
			end
			pwr = 0;

			if status == 1 & obj.status == 1 % ignore change
				obj.t = obj.t + dt;
				if obj.poweringUp
					pwr = obj.fanPower/obj.startupTime*obj.t;
				else
					pwr = obj.fanPower;
				end
				
				if obj.t > obj.startupTime 
					obj.poweringUp = 0;
				else
					obj.poweringUp = 1;
				end

			elseif status == 1 & obj.status == 0
				obj.t = dt;
				pwr = obj.fanPower/obj.startupTime*obj.t;			
				obj.poweringUp = 1;
				obj.status = status;

			elseif status == 0 & obj.status == 1
				obj.t = obj.startupTime - dt;
				pwr = obj.fanPower/obj.startupTime*obj.t;			
				obj.poweringDown = 1;
				obj.status = status;


			elseif status == 0 & obj.status == 0	% ignore change
				obj.t = obj.t - dt;
				if obj.t < 0
					obj.t = 0;
				end

				if obj.poweringDown
					pwr = obj.fanPower/obj.startupTime*obj.t;
				else
					pwr = 0;
				end
				
				if obj.t > 0 
					obj.poweringDown = 1;
				else
					obj.poweringDown = 0;
				end
			end


		end

	end

end


