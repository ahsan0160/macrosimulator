% MACRO.m
%
% A MACRO is a network of nodes and edges corresponding to fabricated
% 'Nodes' and 'Active Cells'. The action of a MACRO corresponds to the
% deformation undergone by the Active Cells (network edges) as current inputs
% to the nodes of the network are specified.
%
% The deformation of the MACROs are functions of the connectivity of the
% network, the power inputs to the network nodes, forces applied on the
% network nodes and the activation profile of the network edges themselves.
%
% Unless specified, all network edges are assumed to be Actuator Cells. Other
% variants of Active Cells can have different activation profiles and must be
% specified apriori.
%
% Active Cell Network Simulations
% Created 2015 by Ahsan Nawroj <ahsan.nawroj@yale.edu>
% (c) 2015 Yale University
% All rights reserved.

classdef MACRO < hgsetget

properties
  % network representation
  cellCount = 0;  nodeCount = 0;
  startPos;   nodePos;    edgeSet;
  dims = 3;
  connMat;    laplacian;  nodeV;  nodeI;  disconnectedCellIdx;
  A; Ae;

  % inputs
  coolingRate=0;    zEdges;   cellModel;  cellProps
  nodeConstraints;    wallConstraints;
  visualizeWallConstraints;

  % operation parameters
  netSimTime = 0;   netEnergy = 0;

  % network state matrices
  %   network level matrices store info for all cells/nodes
  %   cell-type matrices are stored as edge-set representations
  %     j x cellCount x i matrices, where i,j are integers (1,2 or 3)
  netCellT;   netCellPower;   netCellHeat;  netCellState;
  %   node-type matrices are stored as adjacency-matrix representations
  %     nodeCount x nodeCount x i matrices, where i is an integer
  %   Special matrices:
  %   nodeArmMat: NxNx2 matrix, storing the arm numbers from one node
  %     connecting to the arm numbers of the node connected to it. For
  %     each edge, the start node i, end node j, nodeArmMat(i,j,1) =
  %     arm number of node i, nodeArmMat(i,j,2) = arm number of node j.
  %   netNodeTheta: (2D) NxN matrix that stores the initial angles of the
  %     node arms conneceted by cells. All angles are relative to the
  %     network's x-axis for 2D networks.
  %     (3D) NxNx3 matrix storing the euler angle representation of
  %     cells
  netCellL0;  netCellKComp; netCellKExt;
  nodeArmMat;   netNodeTheta; netNodeTheta_0;   netNodeK;

  boundaryNodes; interiorNodes;

  delUnderExtForceLim = 10;

  nodeArmWidthMax = 6;
  nodeArmWidthMin = 1;
  safeOperatingTemp = 500;
end


methods
% =================================================================
%
%
% MACRO constructor
%
%
% =================================================================
function obj = MACRO (edgeSet, edgeTheta, constraints, ...
  zEdges, nodeV, cellModel)
  if nargin < 1 % Empty MACRO object
    return
  end
  obj.nodeCount = max(edgeSet(:)) - min(edgeSet(:)) + 1;
  if ~exist('constraints', 'var') || isempty(constraints)
    constraints.wallConstraints = {};
    nodeConstraints = zeros(3,obj.nodeCount);
    nodeConstraints(:,1) = 1;
    nodeConstraints(2,2) = 1;
    constraints.nodeConstraints = nodeConstraints;
  end
  if ~exist('zEdges', 'var') || isempty(zEdges)
    zEdges = [];
  end
  if ~exist('nodeV', 'var') || isempty(nodeV)
    nodeV = zeros(obj.nodeCount,1);
  end
  if ~exist('cellModel', 'var') || isempty(cellModel)
    cellModel = Model_ActiveCell_v4();
  end


  % keyboard
  obj.edgeSet = edgeSet;
  [obj.A, obj.Ae] = genAdjMats(obj.edgeSet, obj.nodeCount);
  obj.nodeConstraints = constraints.nodeConstraints;
  obj.wallConstraints = constraints.wallConstraints;
  obj.cellCount = size(obj.edgeSet,2);

  obj.zEdges = zEdges;
  aa = zeros(1,obj.cellCount);
  for ii=1:size(obj.zEdges,2)
    ze = obj.zEdges(:,ii);
    try
    bb = sum(obj.edgeSet-repmat(ze,1,obj.cellCount)==0,1)==2;
  catch
    keyboard
  end
    aa = aa+bb;
  end

  % obj.edgeSet
  % obj.zEdges
  obj.disconnectedCellIdx =  find(aa);

  % generate connectivity matrix
  obj.connMat = sparse(edgeSet(1,:), edgeSet(2,:), 1, ...
    obj.nodeCount, obj.nodeCount, obj.cellCount);
  obj.connMat = double(obj.connMat | obj.connMat');

  obj.nodeV = nodeV;
  obj.cellModel = cellModel;
  global cellProps;
  if ~exist('cellProps','var')
    cellProps = ActiveCellProperties();
  end
  obj.cellProps = cellProps;

  [L0, kExtend, kCompress, state] = ...
            obj.cellModel.cellMapTtoL0andK (25);

  obj.nodePos = posFromTheta(L0, obj.nodeCount, edgeSet, edgeTheta);
  obj.startPos = obj.nodePos;
  % keyboard
  if ~any(obj.nodePos(3,:))
    obj.dims = 2;

    obj.boundaryNodes = ...
      boundary(obj.nodePos(1,:)',obj.nodePos(2,:)');
    if ~isempty(obj.boundaryNodes)
        obj.boundaryNodes(end)=[];
    end
    obj.interiorNodes = ...
      setdiff(1:obj.nodeCount, obj.boundaryNodes);
  else

    % NOT IMPLEMENTED YET FOR 3D: CONVEX HULL?
    % obj.boundaryNodes = ...
    %   boundary(obj.nodePos(1,:)',obj.nodePos(2,:)');
    % obj.boundaryNodes(end)=[];
    % obj.interiorNodes = ...
    %   setdiff(1:obj.nodeCount, obj.boundaryNodes);
  end


  % % create preset plots of wall constraints to show during animation
  % xx = linspace(min(obj.nodePos(1,:))-10, max(obj.nodePos(1,:))+10);
  % yy = linspace(min(obj.nodePos(2,:))-10, max(obj.nodePos(2,:))+10);
  % zz = linspace(min(obj.nodePos(3,:))-10, max(obj.nodePos(3,:))+10);
  % [XX,YY] = meshgrid(xx,yy);
  % for ii=1:length(obj.wallConstraints)
  %   kk = obj.wallConstraints{ii}(XX,YY, zeros(size(XX)));
  %   kk(kk<0) = NaN;
  %   obj.visualizeWallConstraints(:,:,ii) = kk;
  % end

  % generate laplacian from graph information
  obj.laplacian = obj.genLaplacian();
  obj.updateNetworkPower(obj.nodeV);

  % initialize network matrices storing cell and node information
  obj.netNodeTheta_0 = obj.computeNodeAngles(obj.nodePos);
  obj.netNodeTheta = obj.netNodeTheta_0;
  obj.netNodeK = obj.connMat * obj.cellProps.node_kt;

  obj.netCellL0 = triu(obj.connMat)*L0;
  % keyboard
  obj.netCellKComp = triu(obj.connMat)*kCompress;
  obj.netCellKExt = triu(obj.connMat)*kExtend;

  obj.netCellT = ones(obj.cellCount,1)*25;
  obj.netCellPower = ones(obj.cellCount,1)*0;
  obj.netCellHeat = ones(obj.cellCount,1)*0;
  obj.netCellState = ones(obj.cellCount,1)*state;

  nd = [];
  maxArmCnt = zeros(length(obj.connMat),1);
  for ii=1:length(obj.connMat)
    nd(ii,:,:) = zeros(length(obj.connMat),1,2);
    kk=1;
    eSet = find(obj.connMat(ii,:)~=0);
    for jj=1:length(eSet)
      if eSet(jj) > ii % end node label after start node label
        maxArmCnt(eSet(jj)) = maxArmCnt(eSet(jj))+1;
        nd(ii,eSet(jj),:) = [kk maxArmCnt(eSet(jj))];
      else % end node label before start node label
        nd(ii,eSet(jj),:) = flipud(squeeze(nd(eSet(jj),ii,:)));
      end
      kk=kk+1;
    end
  end
  obj.nodeArmMat = nd;
  obj.coolingRate = 0;
end








% =================================================================
%
%
% Network Display
%
%
% =================================================================
function show (obj, pos, sizingParams, colorParams)
  if ~exist('pos', 'var') || isempty(pos)
    pos = obj.nodePos;
  end
  if ~exist('sizingParams', 'var') || isempty(sizingParams)
    sizingParams = [10, 2, 5, 1];
  end
  if ~exist('colorParams', 'var') || isempty(colorParams)
    colorParams = [ 0.0,0.5,0.5;
      1.0,1.0,1.0;
      0.0,0.25,0.25 ];
  end

  hFig = gcf;
  ggg = get(hFig, 'Position');
  ggg(1) = 100; ggg(2) = 100; % bottom left corner pos, px
  ggg(3) = 1200; ggg(4) = 800; % wid, height, px
  set(hFig, 'Position', ggg)

  % load parameters
  nodeMarkerSize = sizingParams(1);
  nodeArmWidth = sizingParams(2);
  nodeLabelFontSize = sizingParams(3);
  cellThickness = sizingParams(4);
  nodeColor = colorParams(1,:);
  nodeTextColor = colorParams(2,:);
  cellColor = colorParams(3,:);
  colorFlags = colorParams(4,:);

  % some default parameters consistent among displays
  edgeLineType = '-';
  edgeLabels = 0;

  r = pos;
  if obj.dims == 2
    r(3,:) = 0*r(3,:);
  end

  % SHOW NODES
  ll = length(r(1,:));
  nodeMarkerSize = nodeMarkerSize* 0.9^log(ll);
  plot3(r(1,:), r(2,:), r(3,:), '.', ...
    'Color', nodeColor, 'MarkerSize', nodeMarkerSize);

  cellActive = 'r';
  cellCooling = 'b';
  for ii=1:obj.cellCount
    st = obj.edgeSet(1,ii); fin = obj.edgeSet(2,ii);
    % if obj.dims==2

    % SHOW CELLS
    setCellColor = cellColor;
    if colorFlags(1) ~= 0
      if obj.netCellPower(ii) > 0
        setCellColor = cellActive;
      elseif obj.netCellPower(ii) < 0
        setCellColor = cellCooling;
      end
    end

    % if any(obj.disconnectedCellIdx == ii)
    %   edgeLineType = ':';
    % else
    %   edgeLineType = '-';
    % end
    % keyboard
    line( [r(1,st) r(1,fin)], [r(2,st) r(2,fin)], ...
    [r(3,st) r(3,fin)],'LineStyle', edgeLineType, ...
      'LineWidth', cellThickness, ...
      'Color', setCellColor);

    % SHOW NODE ARMS
    t1=norm(r(:,st)-r(:,fin));  t2=obj.cellProps.nodeArmLen;
    dx = t2/t1*(r(1,fin)-r(1,st));
    dy = t2/t1*(r(2,fin)-r(2,st));
    dz = t2/t1*(r(3,fin)-r(3,st));

    line( [r(1,st) r(1,st)+dx],[r(2,st) r(2,st)+dy], ...
        [r(3,st) r(3,st)+dz], 'LineStyle', '-', ...
        'LineWidth', nodeArmWidth, 'Color', nodeColor);
    line( [r(1,fin)-dx r(1,fin)], [r(2,fin)-dy r(2,fin)], ...
        [r(3,fin)-dz r(3,fin)], 'LineStyle', '-', ...
        'LineWidth', nodeArmWidth, 'Color', nodeColor);


    if edgeLabels
      labelProps = struct('FontWeight','bold', ...
          'HorizontalAlignment','center', ...
          'BackgroundColor', cellColor,'Color', 'white',...
          'FontSize', ceil(nodeMarkerSize/3));

      text(0.5*(r(1,st)+r(1,fin))-3, ...
          0.5*(r(2,st)+r(2,fin))+4,...
          sprintf('(%d) %.2f^oC', ii, obj.netCellT(ii)), ...
          labelProps);
    end
  end

  % NODE LABELS
  % if ll > 100
  if ll > 10
    nodeLabelFontSize = 0;
  end
  if nodeLabelFontSize > 0
    for i=1:size(r,2)
      text( r(1,i), r(2,i), r(3,i), ...
        sprintf('%d', i), 'FontWeight','bold', ...
        'HorizontalAlignment','center', ...
        'FontSize', nodeLabelFontSize,...
        'Color', nodeTextColor );
    end
  end


  % create preset plots of wall constraints to show during animation
  npts = 100;
  vwalldist = 20;
  xx = linspace(min(obj.nodePos(1,:))-vwalldist, ...
      max(obj.nodePos(1,:))+vwalldist, npts);
  yy = linspace(min(obj.nodePos(2,:))-vwalldist, ...
      max(obj.nodePos(2,:))+vwalldist, npts);
  zz = linspace(min(obj.nodePos(3,:))-vwalldist, ...
      max(obj.nodePos(3,:))+vwalldist, npts);
  [XX,YY] = meshgrid(xx,yy);
  for ii=1:length(obj.wallConstraints)
    kk = 10*obj.wallConstraints{ii}(XX,YY, zeros(size(XX)));
    kk(kk<0.9*max(kk(:))) = NaN;
    plot3(XX,YY,kk,'k.', 'MarkerSize', 1)
  end

  % xlim([min(obj.nodePos(1,:))-10 max(obj.nodePos(1,:))+10]);
  % ylim([min(obj.nodePos(2,:))-10 max(obj.nodePos(2,:))+10]);
  % if obj.dims == 3
  %   zlim([min(obj.nodePos(3,:)) max(obj.nodePos(3,:))]);
  % end
  axis equal;
  view(obj.dims);

  % keyboard
end



% =================================================================
%
%
% Power Dissipation
%
% the cell network is analyzed using the network Laplacian.
% electrical analysis of the laplacian provides the nodal voltages
% given input and ouput currents at the provided nodes. a simple
% evaluation of the power (v^2/R) for each cell gives the power
% dissipation at the cells.
%
%
% =================================================================
function updateNetworkPower (obj, vArray)
  % update voltages on the nodes if currents have changed
  if any(vArray ~= obj.nodeV)
    obj.nodeV = vArray;
  end
  V = obj.nodeV;
  E = obj.edgeSet;

  % power calculation is simply deltaV^2/R
  obj.netCellPower = (V(E(1,:))-V(E(2,:))).^2 /obj.cellProps.cellR;
  obj.nodeI = (V(E(1,:))-V(E(2,:)))/obj.cellProps.cellR;
  obj.netCellPower = obj.netCellPower - obj.coolingRate;
  obj.netCellPower(obj.disconnectedCellIdx) = 0;
end

% =================================================================
%
%
% Run the network
%
% at each time step, update energy buildup in the cells,
% translating the energy dissipation to a new network config
%
%
% =================================================================
function flags = run (obj, dt, voltages)
  flags = 0;
  obj.updateNetworkPower(voltages);

  % update each cell's stiffness and rest lengths
  obj.updateNetworkStatus (dt);
  % obj.netCellT'
  % obj.Ae'*obj.nodeI

    if any(obj.netCellT > obj.safeOperatingTemp)
        fprintf(2, '***Exceeding safe operating temperature***');
        fprintf(2, '***(continuing anyway)***\n');
          flags = 1;
          % keyboard
        % return
    end

  [obj.nodePos, minEnergy] = obj.minEnergyPos();

  % add a tiny amount of noise: SNR = 30 dB
  obj.nodePos = awgn(obj.nodePos, 30);
  obj.netSimTime = [obj.netSimTime, obj.netSimTime(end)+dt];
  fprintf(1,'Sim time: %.1f, Net energy: %.3f\n', ...
    obj.netSimTime(end), minEnergy);
  % obj.nodePos
  obj.netEnergy = [obj.netEnergy minEnergy];
end

% =================================================================
%
%
% updateNetworkStatus
%
% Update the matrices holding the state of cells and nodes
%
%
% =================================================================
function updateNetworkStatus(obj, dt)
  E = obj.edgeSet;

  % cell temperature from Q = mc(deltaT)
  delQ = obj.netCellPower * dt;
  obj.netCellHeat = max(0, obj.netCellHeat + delQ);
  obj.netCellT = 25 + obj.cellModel.computeT(obj.netCellHeat);

  % cell Model transfers (given cell past state) temperature of each
  % cell to the corresponding rest length, stiffnesses and new state
  [L0, kExt, kComp, obj.netCellState] = ...
    obj.cellModel.cellMapTtoL0andK(obj.netCellT, obj.netCellState);

  idx = sub2ind(size(obj.netCellL0), E(1,:), E(2,:));
  obj.netCellL0 (idx) = L0;
  obj.netCellKExt (idx) = kExt;
  obj.netCellKComp (idx) = kComp;

  obj.netNodeTheta = obj.computeNodeAngles(obj.nodePos);

end


% =================================================================
%
%
% networkPotential
%
% Energy stored within the mesh
% This function is called A LOT. Minimize clutter and safety checks.
%
% =================================================================
function W = networkPotential (obj, pos, triuCMat, ...
  debugCall, optionalForce, optionalForceNodes)

  % SETUP SOME VARIABLES
  % ---------------------------------------
  pos = reshape(pos,[3,length(pos)/3]);
  [P,Q] = find(triuCMat~=0);
  P1 = pos(:,P); % edge beginning points
  P2 = pos(:,Q); % edge end points

  % linear index from 2 sets of indices
  idxs = sub2ind(size(triuCMat),P,Q);

  % variables of the current network
  currentCellLen = obj.netCellL0(idxs);
  currentdth = abs(obj.netNodeTheta(idxs) - ...
    obj.netNodeTheta_0(idxs));
  kComp = obj.netCellKComp(idxs);
  kExt = obj.netCellKExt(idxs);
  kTorsional = obj.netNodeK(idxs);

  % variables of the shifted pos network
  newNodeToNode = sqrt(sum((P2-P1).^2,1))';
  newNodeAngles = obj.computeNodeAngles(pos);

  newCellLen = (newNodeToNode-2*obj.cellProps.nodeArmLen);
  lDiff = (newCellLen-currentCellLen);
  eIdxs = lDiff > 0;
  cIdxs = lDiff < 0;

  % NETWORK ENERGY FROM CELL STRAINS
  % ---------------------------------------
  WtransComp = 0; WtransExt = 0;
  if any(eIdxs)
    WtransExt=full(sum(0.5*kExt(eIdxs).*lDiff(eIdxs).^2));
  end
  if any(cIdxs)
    WtransComp=full(sum(0.5*kComp(cIdxs).*lDiff(cIdxs).^2));
  end

  % NETWORK ENERGY FROM NODE STRAINS
  % ---------------------------------------
  Wrot = full(sum(0.5*kTorsional.*(currentdth).^2));


  % NETWORK ENERGY FROM WALL CONSTRAINTS: DEACTIVATED FOR NOW
  % ---------------------------------------
  % WvirtualWall = zeros(length(obj.wallConstraints),size(pos,2));
  % for ii=1:length(obj.boundaryNodes)
  %   for jj=1:length(obj.wallConstraints)
  %     WvirtualWall(jj,obj.boundaryNodes(ii)) = ...
  %       1e7*obj.wallConstraints{jj}...
  %         ( pos(1,obj.boundaryNodes(ii)),  ...
  %           pos(2,obj.boundaryNodes(ii))   );
  %   end
  % end
  % WtotalVirtWalls = sum(WvirtualWall(:));
  WtotalVirtWalls = 0;

  % NETWORK ENERGY FROM DEALING WITH EXTERNAL FORCE
  % ---------------------------------------
  WextForce = 0;
  if ~isempty(optionalForce)
    % The external force is exterted over a distance du
    % Any motion opposing the force costs energy, proportional to the
    %   position changes needed to

    relevantPtPosCurrent = obj.nodePos(:,optionalForceNodes);
    relevantPtPosNew = pos(:,optionalForceNodes);
    du = relevantPtPosNew - relevantPtPosCurrent;

    if length(optionalForce) ~= 3
      optionalForce(3) = 0;
    end
    % Work potential calculation
    for kk=1:length(optionalForceNodes)
      WextForce = WextForce - 0.5*dot(optionalForce,du(:,kk));
    end

  end

  % COST OF GOING FAR FROM PREVIOUS EQ
  % ---------------------------------------
  rownorm = @(X) sqrt(sum((X).^2,2));
  deviationFromCurrentEq = mean(rownorm((obj.nodePos - pos)'));
  % any deviation over delUnderExtForceLim costs a lot of energy
  costOfDeviation = exp(1e3*...
    (deviationFromCurrentEq - obj.delUnderExtForceLim));
  Wsafety = sum(costOfDeviation);

  W = WtransComp + WtransExt + Wrot + WtotalVirtWalls + WextForce + Wsafety;

  if debugCall
    % figure(68); hold all; showShape(obj.nodePos); showShape(pos);
    % pause(0.0001);
    % keyboard
  end
end

% =================================================================
%
%
% computeNodeAngles
%
%
% =================================================================
function theta = computeNodeAngles (obj, pos)
  % compute node angles for each edge connecting each node
  [jj,ii] = find(obj.connMat);
  v = pos(:,jj) - pos(:,ii);

  % cosine method: for 3D angles
  % --------------------------------
  if obj.dims == 3
    theta = zeros(obj.nodeCount, obj.nodeCount,3);
    vx = [1 0 0]; % reference: x axis
    vy = [0 1 0]; % reference: x axis
    vz = [0 0 1]; % reference: x axis
    vLengths = diag(sqrt(v'*v));
    vxLengths = ones(size(vLengths));
    vyLengths = ones(size(vLengths));
    vzLengths = ones(size(vLengths));
    lenXProd = vLengths .* vxLengths;
    dotXProd = vx * v;
    lenYProd = vLengths .* vyLengths;
    dotYProd = vy * v;
    lenZProd = vLengths .* vzLengths;
    dotZProd = vz * v;
    cosTheta = theta;

    idx = sub2ind(size(theta), ii, jj, ones(size(ii)));
    idy = sub2ind(size(theta), ii, jj, 2*ones(size(ii)));
    idz = sub2ind(size(theta), ii, jj, 3*ones(size(ii)));
    cosTheta(idx) = dotXProd(:) ./ lenXProd(:);
    cosTheta(idy) = dotYProd(:) ./ lenYProd(:);
    cosTheta(idz) = dotZProd(:) ./ lenZProd(:);
    theta([idx; idy; idz]) = acos(cosTheta([idx; idy; idz]));

  % tangent method: for 2D angles
  % --------------------------------
  else
    theta = zeros(obj.nodeCount, obj.nodeCount);
    idx = sub2ind(size(obj.connMat), ii, jj);
    theta(idx) = atan2(v(2,:),v(1,:));
  end
end



% ---------------------------------------------------------------------
% minEnergyPos
% ---------------------------------------------------------------------
% Calculates the minimum energy configuration of the network.
function [finPos, fval] = minEnergyPos (obj, debugCall, ...
  optionalForce, optionalForceNodes)

  if ~exist('debugCall','var')
    debugCall = 0;
  end

  posMat = reshape(obj.nodePos, [1 length(obj.nodePos(:))]);
  triuCMat = triu(obj.connMat);

  % ----- POTENTIAL FUNCTION -----
  if ~exist('optionalForce', 'var')
    optionalForce = [];
    optionalForceNodes = [];
  end
  L = @(pos) obj.networkPotential (pos, triuCMat, debugCall, ...
    optionalForce, optionalForceNodes);

  % ----- LINEAR INEQUALITY CONSTRAINTS ------
  A = [];
  b = [];

  % ----- EQUALITY CONSTRAINTS -----
  % Node positions can be:
  % anchored to world, fixed in x, fixed in y, or fixed in z
  Aeqvec = obj.nodeConstraints;
  beq = posMat;
  Aeqvec = reshape(Aeqvec, [1 length(Aeqvec(:))]);
  Aeq = diag(Aeqvec);

  % ----- NON-LINEAR CONSTRAINTS -----
  nlcon = [];

  % ----- FMINCON -----
  if ~exist('optionalForce', 'var')
    options = optimoptions(@fmincon,'Algorithm','interior-point',...
      'MaxFunEvals',275000, 'Display', 'off');
  else
    options = optimoptions(@fmincon,'Algorithm','interior-point',...
      'MaxFunEvals',5000, 'Display', 'off');
  end
  [x, fval] = fmincon(L,posMat,A,b,Aeq,beq,[],[],nlcon,options);

  finPos = reshape(x, [3 length(x)/3]);
end



% ---------------------------------------------------------------------
% deep copy: copies all properties to a new instance of same class
% ---------------------------------------------------------------------
function new = copy(this)
  % Instantiate new object of the same class.
  new = feval(class(this));

  % Copy all non-hidden properties.
  p = properties(this);
  for i = 1:length(p)
    new.(p{i}) = this.(p{i});
  end
end




end % of public methods

methods(Access='private')
% ---------------------------------------------------------------------
% genLaplacian
% ---------------------------------------------------------------------
% generate laplacian for the cell network
function L = genLaplacian(obj)
  cc = obj.connMat;
  for ii=1:size(obj.zEdges,2)
    e = obj.zEdges(:,ii);
    cc(e(1),e(2)) = 0;
    cc(e(2),e(1)) = 0;
  end
  condNet = cc * (1/obj.cellProps.cellR);
  L = zeros(obj.nodeCount);
  % keyboard
  for i = 1:obj.nodeCount
    L(i,i) = sum(condNet(i,:));
    xx = find(condNet(i,:) ~= 0);
    L(i, xx) = -condNet(i, xx);
  end
  % keyboard
end

end % of private methods
end % of class definition










% % =================================================================
% %
% %
% % Network Display
% %
% %
% % =================================================================
% function showPointCloud (obj, netNum, providedPos, varargin)
%   if netNum == 0
%     r = obj.startPos;
%   elseif netNum == 1
%     r = obj.nodePos;
%   else
%     r = providedPos;
%     if isempty(r)
%       disp('Nothing to show: returning.');
%       return
%     end
%   end
%   if size(r,1) ~= 3
%     r(3,:) = 0;
%   end

%   netColor = [0.7, 0.5, 0.6];
%   % sizingParam = [150,15,30,0];
%   sizingParam = [20,1,2,0];
%   % keyboard
%   if nargin == 4
%     sizingParam = varargin{1};
%     % keyboard
%   elseif nargin > 4
%     sizingParam = varargin{1};
%     netColor = varargin{2};
%     % keyboard
%   end
%   nodeMarkerSize = sizingParam(1);
%   edgeLineType = '-';
%   edgeLabels = 0;
%   cellThickness =  sizingParam(2);
%   nodeArmWidth =  sizingParam(3);
%   nodeLabels = sizingParam(4);

%   gg = 0.5;
%   % gg = 0.8;
%   cellColor = netColor.*[gg gg gg];
%   % plot nodes
%   plot3(r(1,:), r(2,:), r(3,:), '.', ...
%       'Color', netColor,...
%       'MarkerSize', nodeMarkerSize);

%   axis equal;
%   view(obj.dims);
% end

