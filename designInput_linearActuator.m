function [dimX, dimY, bndPts, trPts] = designInput_linearActuator ...
  (stroke, alignedIn)
% INPUTS:
% ---------------------------------------------------------------------------
% percentage stroke desired. default of 20%
% optional alignment of stroke. default is random.
%
% OUTPUTS:
% ---------------------------------------------------------------------------
% size of the shape in x and y
% boundary of the shape in necessary vertices
% target shape boundary


  if ~exist('stroke', 'var')
    stroke = 20;
  else
    stroke = mod(stroke,100);
  end

  if ~exist('alignedIn', 'var')
    rng('shuffle'); % random
    alignedIn = randi(2,1,1)-1;
  end

  % for debug, set fixed pseudorandom size generator
  rng(160); % whatever

  dimX = 100 + 100*rand;
  dimY = 100 + 100*rand;

  v1 = [0 0]';
  v2 = [dimX 0]';
  v3 = [0 dimY]';
  v4 = [dimX dimY]';

  bndPts = [v1 v2 v3 v4];

  trPts = bndPts;

  if alignedIn == 0 % stroke in x
    trPts(1,2) = v2(1)*(100-stroke)/100;
    trPts(1,4) = v4(1)*(100-stroke)/100;
  else
    trPts(2,3) = v3(2)*(100-stroke)/100;
    trPts(2,4) = v4(2)*(100-stroke)/100;
  end

end
